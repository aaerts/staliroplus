#ifndef __c3_Boost_converter_V04_h__
#define __c3_Boost_converter_V04_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc3_Boost_converter_V04InstanceStruct
#define typedef_SFc3_Boost_converter_V04InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c3_sfEvent;
  uint8_T c3_tp_Mode2;
  uint8_T c3_tp_Mode1;
  uint8_T c3_tp_Mode3;
  uint8_T c3_tp_Mode4;
  boolean_T c3_isStable;
  boolean_T c3_stateChanged;
  real_T c3_lastMajorTime;
  uint8_T c3_is_active_c3_Boost_converter_V04;
  uint8_T c3_is_c3_Boost_converter_V04;
  boolean_T c3_dataWrittenToVector[8];
  uint8_T c3_doSetSimStateSideEffects;
  const mxArray *c3_setSimStateSideEffectsInfo;
  real_T *c3_S;
  real_T *c3_Ein;
  real_T *c3_L;
  real_T *c3_q;
  real_T *c3_p;
  real_T *c3_C;
  real_T *c3_Eout;
  real_T *c3_Iout;
  real_T *c3_qout;
  real_T *c3_pout;
  real_T *c3_Iin;
} SFc3_Boost_converter_V04InstanceStruct;

#endif                                 /*typedef_SFc3_Boost_converter_V04InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c3_Boost_converter_V04_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c3_Boost_converter_V04_get_check_sum(mxArray *plhs[]);
extern void c3_Boost_converter_V04_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
