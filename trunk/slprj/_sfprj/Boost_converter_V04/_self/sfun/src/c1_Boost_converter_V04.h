#ifndef __c1_Boost_converter_V04_h__
#define __c1_Boost_converter_V04_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_Boost_converter_V04InstanceStruct
#define typedef_SFc1_Boost_converter_V04InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  uint8_T c1_tp_Mode2;
  uint8_T c1_tp_Mode1;
  uint8_T c1_tp_Mode3;
  boolean_T c1_isStable;
  boolean_T c1_stateChanged;
  real_T c1_lastMajorTime;
  uint8_T c1_is_active_c1_Boost_converter_V04;
  uint8_T c1_is_c1_Boost_converter_V04;
  boolean_T c1_dataWrittenToVector[6];
  uint8_T c1_doSetSimStateSideEffects;
  const mxArray *c1_setSimStateSideEffectsInfo;
  real_T *c1_S;
  real_T *c1_Ein;
  real_T *c1_L;
  real_T *c1_q;
  real_T *c1_p;
  real_T *c1_C;
  real_T *c1_Eout;
  real_T *c1_Iout;
  real_T *c1_pout;
} SFc1_Boost_converter_V04InstanceStruct;

#endif                                 /*typedef_SFc1_Boost_converter_V04InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_Boost_converter_V04_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c1_Boost_converter_V04_get_check_sum(mxArray *plhs[]);
extern void c1_Boost_converter_V04_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
