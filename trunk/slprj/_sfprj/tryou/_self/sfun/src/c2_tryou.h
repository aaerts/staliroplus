#ifndef __c2_tryou_h__
#define __c2_tryou_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_tryouInstanceStruct
#define typedef_SFc2_tryouInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  uint8_T c2_tp_Mode2;
  uint8_T c2_tp_Mode1;
  uint8_T c2_tp_Mode3;
  boolean_T c2_isStable;
  boolean_T c2_stateChanged;
  real_T c2_lastMajorTime;
  uint8_T c2_is_active_c2_tryou;
  uint8_T c2_is_c2_tryou;
  boolean_T c2_dataWrittenToVector[7];
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
  real_T *c2_S;
  real_T *c2_Ein;
  real_T *c2_L;
  real_T *c2_q;
  real_T *c2_p;
  real_T *c2_C;
  real_T *c2_Eout;
  real_T *c2_Iout;
  real_T *c2_pout;
  real_T *c2_mode;
} SFc2_tryouInstanceStruct;

#endif                                 /*typedef_SFc2_tryouInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_tryou_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_tryou_get_check_sum(mxArray *plhs[]);
extern void c2_tryou_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
