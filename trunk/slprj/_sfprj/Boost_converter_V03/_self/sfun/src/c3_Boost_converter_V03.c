/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Boost_converter_V03_sfun.h"
#include "c3_Boost_converter_V03.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Boost_converter_V03_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c3_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c3_IN_Mode1                    ((uint8_T)1U)
#define c3_IN_Mode2                    ((uint8_T)2U)
#define c3_IN_Mode3                    ((uint8_T)3U)
#define c3_IN_Mode4                    ((uint8_T)4U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c3_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_j_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_k_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_o_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_q_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_r_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_t_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_u_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_v_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_w_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_x_debug_family_names[2] = { "nargin", "nargout" };

static const char * c3_y_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_ab_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_bb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_cb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_db_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_eb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_fb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_gb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_hb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_ib_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_jb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c3_kb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void initialize_params_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void enable_c3_Boost_converter_V03(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance);
static void disable_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void c3_update_debugger_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void set_sim_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray *c3_st);
static void c3_set_sim_state_side_effects_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void finalize_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void sf_gateway_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void mdl_start_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void zeroCrossings_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void derivatives_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void outputs_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void initSimStructsc3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void c3_eml_ini_fcn_to_be_inlined_22
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static void c3_eml_term_fcn_to_be_inlined_22
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static real_T c3_mrdivide(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_A, real_T c3_B);
static real_T c3_rdivide(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_x, real_T c3_y);
static void c3_isBuiltInNumeric(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance);
static void c3_eml_scalexp_compatible(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance);
static real_T c3_eml_div(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_x, real_T c3_y);
static real_T c3_div(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
                     real_T c3_x, real_T c3_y);
static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber);
static const mxArray *c3_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const real_T c3_u);
static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, void *c3_inData);
static real_T c3_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_nargout, const char_T *c3_identifier);
static real_T c3_b_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_b_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const boolean_T c3_u);
static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static boolean_T c3_c_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_sf_internal_predicateOutput, const char_T
  *c3_identifier);
static boolean_T c3_d_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_c_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const int32_T c3_u);
static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static int32_T c3_e_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_sfEvent, const char_T *c3_identifier);
static int32_T c3_f_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_d_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const uint8_T c3_u);
static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData);
static uint8_T c3_g_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_tp_Mode2, const char_T *c3_identifier);
static uint8_T c3_h_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId);
static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData);
static const mxArray *c3_e_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance);
static const mxArray *c3_f_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const boolean_T c3_u[8]);
static void c3_i_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u);
static void c3_j_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_dataWrittenToVector, const char_T
  *c3_identifier, boolean_T c3_y[8]);
static void c3_k_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T c3_y[8]);
static const mxArray *c3_l_emlrt_marshallIn
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray
   *c3_b_setSimStateSideEffectsInfo, const char_T *c3_identifier);
static const mxArray *c3_m_emlrt_marshallIn
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray *c3_u,
   const emlrtMsgIdentifier *c3_parentId);
static void init_dsm_address_info(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c3_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c3_doSetSimStateSideEffects = 0U;
  chartInstance->c3_setSimStateSideEffectsInfo = NULL;
  chartInstance->c3_tp_Mode1 = 0U;
  chartInstance->c3_tp_Mode2 = 0U;
  chartInstance->c3_tp_Mode3 = 0U;
  chartInstance->c3_tp_Mode4 = 0U;
  chartInstance->c3_is_active_c3_Boost_converter_V03 = 0U;
  chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c3_Boost_converter_V03(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c3_update_debugger_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  uint32_T c3_prevAniVal;
  c3_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c3_is_active_c3_Boost_converter_V03 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
  }

  if (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode4) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
  }

  _SFD_SET_ANIMATION(c3_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  const mxArray *c3_st = NULL;
  c3_st = NULL;
  sf_mex_assign(&c3_st, c3_e_emlrt_marshallOut(chartInstance), false);
  return c3_st;
}

static void set_sim_state_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray *c3_st)
{
  c3_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_st));
  chartInstance->c3_doSetSimStateSideEffects = 1U;
  c3_update_debugger_state_c3_Boost_converter_V03(chartInstance);
  sf_mex_destroy(&c3_st);
}

static void c3_set_sim_state_side_effects_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  if (chartInstance->c3_doSetSimStateSideEffects != 0) {
    chartInstance->c3_tp_Mode1 = (uint8_T)
      (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode1);
    chartInstance->c3_tp_Mode2 = (uint8_T)
      (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode2);
    chartInstance->c3_tp_Mode3 = (uint8_T)
      (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode3);
    chartInstance->c3_tp_Mode4 = (uint8_T)
      (chartInstance->c3_is_c3_Boost_converter_V03 == c3_IN_Mode4);
    chartInstance->c3_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c3_setSimStateSideEffectsInfo);
}

static void sf_gateway_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[2];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 0.0;
  uint32_T c3_b_debug_family_var_map[3];
  real_T c3_b_nargin = 0.0;
  real_T c3_b_nargout = 1.0;
  boolean_T c3_out;
  real_T c3_c_nargin = 0.0;
  real_T c3_c_nargout = 1.0;
  boolean_T c3_b_out;
  real_T c3_d_nargin = 0.0;
  real_T c3_d_nargout = 0.0;
  real_T c3_e_nargin = 0.0;
  real_T c3_e_nargout = 1.0;
  boolean_T c3_c_out;
  real_T c3_f_nargin = 0.0;
  real_T c3_f_nargout = 0.0;
  real_T c3_g_nargin = 0.0;
  real_T c3_g_nargout = 1.0;
  boolean_T c3_d_out;
  real_T c3_h_nargin = 0.0;
  real_T c3_h_nargout = 0.0;
  real_T c3_i_nargin = 0.0;
  real_T c3_i_nargout = 1.0;
  boolean_T c3_e_out;
  real_T c3_j_nargin = 0.0;
  real_T c3_j_nargout = 0.0;
  real_T c3_k_nargin = 0.0;
  real_T c3_k_nargout = 1.0;
  boolean_T c3_f_out;
  real_T c3_l_nargin = 0.0;
  real_T c3_l_nargout = 1.0;
  boolean_T c3_g_out;
  real_T c3_m_nargin = 0.0;
  real_T c3_m_nargout = 0.0;
  real_T c3_n_nargin = 0.0;
  real_T c3_n_nargout = 1.0;
  boolean_T c3_h_out;
  real_T c3_o_nargin = 0.0;
  real_T c3_o_nargout = 1.0;
  boolean_T c3_i_out;
  real_T c3_p_nargin = 0.0;
  real_T c3_p_nargout = 1.0;
  boolean_T c3_j_out;
  real_T c3_q_nargin = 0.0;
  real_T c3_q_nargout = 0.0;
  real_T c3_r_nargin = 0.0;
  real_T c3_r_nargout = 1.0;
  boolean_T c3_k_out;
  real_T c3_s_nargin = 0.0;
  real_T c3_s_nargout = 0.0;
  real_T c3_t_nargin = 0.0;
  real_T c3_t_nargout = 1.0;
  boolean_T c3_l_out;
  real_T c3_u_nargin = 0.0;
  real_T c3_u_nargout = 0.0;
  real_T c3_v_nargin = 0.0;
  real_T c3_v_nargout = 0.0;
  real_T c3_w_nargin = 0.0;
  real_T c3_w_nargout = 0.0;
  real_T c3_x_nargin = 0.0;
  real_T c3_x_nargout = 0.0;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  boolean_T guard9 = false;
  c3_set_sim_state_side_effects_c3_Boost_converter_V03(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c3_lastMajorTime = _sfTime_;
    chartInstance->c3_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iout, 6U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_C, 0U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_p, 2U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_L, 1U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Ein, 5U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_S, 4U, 1U, 0U,
                          chartInstance->c3_sfEvent, false);
    chartInstance->c3_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    if (chartInstance->c3_is_active_c3_Boost_converter_V03 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
      chartInstance->c3_stateChanged = true;
      chartInstance->c3_is_active_c3_Boost_converter_V03 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c3_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_e_debug_family_names,
        c3_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
        c3_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
        c3_sf_marshallIn);
      *chartInstance->c3_p = 0.0;
      chartInstance->c3_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c3_p, 2U, 5U, 12U,
                            chartInstance->c3_sfEvent, false);
      *chartInstance->c3_q = 0.0024;
      chartInstance->c3_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 5U, 12U,
                            chartInstance->c3_sfEvent, false);
      *chartInstance->c3_C = 1.0E-5;
      chartInstance->c3_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c3_C, 0U, 5U, 12U,
                            chartInstance->c3_sfEvent, false);
      *chartInstance->c3_L = 1.0E-6;
      chartInstance->c3_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c3_L, 1U, 5U, 12U,
                            chartInstance->c3_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c3_stateChanged = true;
      chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
      chartInstance->c3_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c3_is_c3_Boost_converter_V03) {
       case c3_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                     chartInstance->c3_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_f_debug_family_names,
          c3_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard9 = false;
        if (CV_EML_COND(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
              *chartInstance->c3_S, 1.0, -1, 0U, *chartInstance->c3_S == 1.0)))
        {
          if (CV_EML_COND(3, 0, 1, CV_RELATIONAL_EVAL(5U, 3U, 1,
                *chartInstance->c3_q, 0.0, -1, 5U, *chartInstance->c3_q >= 0.0)))
          {
            CV_EML_MCDC(3, 0, 0, true);
            CV_EML_IF(3, 0, 0, true);
            c3_out = true;
          } else {
            guard9 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 3U,
              chartInstance->c3_sfEvent, false);
          }

          guard9 = true;
        }

        if (guard9 == true) {
          CV_EML_MCDC(3, 0, 0, false);
          CV_EML_IF(3, 0, 0, false);
          c3_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
          chartInstance->c3_stateChanged = true;
          chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_Mode2 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U,
                       chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_j_debug_family_names,
            c3_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 0U,
              chartInstance->c3_sfEvent, false);
          }

          guard8 = false;
          if (CV_EML_COND(0, 0, 0, *chartInstance->c3_p <= 0.0)) {
            if (CV_EML_COND(0, 0, 1, *chartInstance->c3_q > *chartInstance->c3_C
                            * *chartInstance->c3_Ein)) {
              CV_EML_MCDC(0, 0, 0, true);
              CV_EML_IF(0, 0, 0, true);
              c3_b_out = true;
            } else {
              guard8 = true;
            }
          } else {
            if (!chartInstance->c3_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 0U,
                chartInstance->c3_sfEvent, false);
            }

            if (!chartInstance->c3_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 0U,
                chartInstance->c3_sfEvent, false);
            }

            guard8 = true;
          }

          if (guard8 == true) {
            CV_EML_MCDC(0, 0, 0, false);
            CV_EML_IF(0, 0, 0, false);
            c3_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c3_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_k_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            *chartInstance->c3_p = 0.0;
            chartInstance->c3_dataWrittenToVector[2U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c3_p, 2U, 5U, 0U,
                                  chartInstance->c3_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c3_tp_Mode1 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
            chartInstance->c3_stateChanged = true;
            chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode3;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_Mode3 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                         chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_h_debug_family_names,
              c3_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard7 = false;
            if (CV_EML_COND(7, 0, 0, *chartInstance->c3_S == 1.0)) {
              if (CV_EML_COND(7, 0, 1, *chartInstance->c3_q <= 0.0)) {
                CV_EML_MCDC(7, 0, 0, true);
                CV_EML_IF(7, 0, 0, true);
                c3_c_out = true;
              } else {
                guard7 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 7U,
                  chartInstance->c3_sfEvent, false);
              }

              guard7 = true;
            }

            if (guard7 == true) {
              CV_EML_MCDC(7, 0, 0, false);
              CV_EML_IF(7, 0, 0, false);
              c3_c_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_c_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c3_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_i_debug_family_names,
                c3_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_nargin, 0U,
                c3_sf_marshallOut, c3_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_nargout, 1U,
                c3_sf_marshallOut, c3_sf_marshallIn);
              *chartInstance->c3_q = 0.0;
              chartInstance->c3_dataWrittenToVector[1U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 5U, 7U,
                                    chartInstance->c3_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c3_tp_Mode1 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
              chartInstance->c3_stateChanged = true;
              chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode4;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode4 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U,
                           chartInstance->c3_sfEvent);
            }
          }
        }
        break;

       case c3_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 10U,
                     chartInstance->c3_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_n_debug_family_names,
          c3_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard6 = false;
        if (CV_EML_COND(10, 0, 0, *chartInstance->c3_S == 0.0)) {
          if (CV_EML_COND(10, 0, 1, *chartInstance->c3_p <= 0.0)) {
            CV_EML_MCDC(10, 0, 0, true);
            CV_EML_IF(10, 0, 0, true);
            c3_d_out = true;
          } else {
            guard6 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 10U,
              chartInstance->c3_sfEvent, false);
          }

          guard6 = true;
        }

        if (guard6 == true) {
          CV_EML_MCDC(10, 0, 0, false);
          CV_EML_IF(10, 0, 0, false);
          c3_d_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_o_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          *chartInstance->c3_p = 0.0;
          chartInstance->c3_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c3_p, 2U, 5U, 10U,
                                chartInstance->c3_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c3_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
          chartInstance->c3_stateChanged = true;
          chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_Mode3 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                       chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_q_debug_family_names,
            c3_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 2U,
              chartInstance->c3_sfEvent, false);
          }

          c3_e_out = CV_EML_IF(2, 0, 0, *chartInstance->c3_q <= 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_e_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_r_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            *chartInstance->c3_q = 0.0;
            chartInstance->c3_dataWrittenToVector[1U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 5U, 2U,
                                  chartInstance->c3_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c3_tp_Mode2 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
            chartInstance->c3_stateChanged = true;
            chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode4;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_Mode4 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                         chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_g_debug_family_names,
              c3_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard5 = false;
            if (CV_EML_COND(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
                  *chartInstance->c3_S, 0.0, -1, 0U, *chartInstance->c3_S == 0.0)))
            {
              if (CV_EML_COND(5, 0, 1, CV_RELATIONAL_EVAL(5U, 5U, 1,
                    *chartInstance->c3_p, 0.0, -1, 5U, *chartInstance->c3_p >=
                    0.0))) {
                CV_EML_MCDC(5, 0, 0, true);
                CV_EML_IF(5, 0, 0, true);
                c3_f_out = true;
              } else {
                guard5 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[2U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 5U,
                  chartInstance->c3_sfEvent, false);
              }

              guard5 = true;
            }

            if (guard5 == true) {
              CV_EML_MCDC(5, 0, 0, false);
              CV_EML_IF(5, 0, 0, false);
              c3_f_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_f_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode2 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
              chartInstance->c3_stateChanged = true;
              chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode1;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode1 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U,
                           chartInstance->c3_sfEvent);
            }
          }
        }
        break;

       case c3_IN_Mode3:
        CV_CHART_EVAL(0, 0, 3);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c3_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_w_debug_family_names,
          c3_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard4 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c3_S == 1.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c3_q <= 0.0)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c3_g_out = true;
          } else {
            guard4 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 1U,
              chartInstance->c3_sfEvent, false);
          }

          guard4 = true;
        }

        if (guard4 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c3_g_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_g_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_x_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_m_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_m_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          *chartInstance->c3_q = 0.0;
          chartInstance->c3_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 5U, 1U,
                                chartInstance->c3_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c3_tp_Mode3 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
          chartInstance->c3_stateChanged = true;
          chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode4;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_Mode4 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_l_debug_family_names,
            c3_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_n_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_n_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 6U,
              chartInstance->c3_sfEvent, false);
          }

          if (!chartInstance->c3_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 6U,
              chartInstance->c3_sfEvent, false);
          }

          c3_h_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            *chartInstance->c3_q, *chartInstance->c3_C * *chartInstance->c3_Ein,
            -1, 3U, *chartInstance->c3_q <= *chartInstance->c3_C *
            *chartInstance->c3_Ein));
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_h_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_Mode3 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
            chartInstance->c3_stateChanged = true;
            chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode1;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_Mode1 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                         chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_p_debug_family_names,
              c3_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_o_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_o_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard3 = false;
            if (CV_EML_COND(9, 0, 0, CV_RELATIONAL_EVAL(5U, 9U, 0,
                  *chartInstance->c3_S, 1.0, -1, 0U, *chartInstance->c3_S == 1.0)))
            {
              if (CV_EML_COND(9, 0, 1, CV_RELATIONAL_EVAL(5U, 9U, 1,
                    *chartInstance->c3_q, 0.0, -1, 5U, *chartInstance->c3_q >=
                    0.0))) {
                CV_EML_MCDC(9, 0, 0, true);
                CV_EML_IF(9, 0, 0, true);
                c3_i_out = true;
              } else {
                guard3 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 9U,
                  chartInstance->c3_sfEvent, false);
              }

              guard3 = true;
            }

            if (guard3 == true) {
              CV_EML_MCDC(9, 0, 0, false);
              CV_EML_IF(9, 0, 0, false);
              c3_i_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_i_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode3 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
              chartInstance->c3_stateChanged = true;
              chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode2;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode2 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U,
                           chartInstance->c3_sfEvent);
            }
          }
        }
        break;

       case c3_IN_Mode4:
        CV_CHART_EVAL(0, 0, 4);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c3_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                     chartInstance->c3_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_u_debug_family_names,
          c3_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_p_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_p_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard2 = false;
        if (CV_EML_COND(4, 0, 0, *chartInstance->c3_S == 0.0)) {
          if (CV_EML_COND(4, 0, 1, *chartInstance->c3_p <= 0.0)) {
            CV_EML_MCDC(4, 0, 0, true);
            CV_EML_IF(4, 0, 0, true);
            c3_j_out = true;
          } else {
            guard2 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 4U,
              chartInstance->c3_sfEvent, false);
          }

          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(4, 0, 0, false);
          CV_EML_IF(4, 0, 0, false);
          c3_j_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_j_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_v_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_q_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_q_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          *chartInstance->c3_p = 0.0;
          chartInstance->c3_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c3_p, 2U, 5U, 4U,
                                chartInstance->c3_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c3_tp_Mode4 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
          chartInstance->c3_stateChanged = true;
          chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c3_sfEvent);
          chartInstance->c3_tp_Mode3 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                       chartInstance->c3_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_s_debug_family_names,
            c3_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_r_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_r_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 11U,
              chartInstance->c3_sfEvent, false);
          }

          c3_k_out = CV_EML_IF(11, 0, 0, *chartInstance->c3_q < 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_k_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_t_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_s_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_s_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            *chartInstance->c3_q = 0.0;
            chartInstance->c3_dataWrittenToVector[1U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c3_q, 3U, 5U, 11U,
                                  chartInstance->c3_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c3_tp_Mode4 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
            chartInstance->c3_stateChanged = true;
            chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode4;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
            chartInstance->c3_tp_Mode4 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                         chartInstance->c3_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_m_debug_family_names,
              c3_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_t_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_t_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard1 = false;
            if (CV_EML_COND(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
                  *chartInstance->c3_S, 0.0, -1, 0U, *chartInstance->c3_S == 0.0)))
            {
              if (CV_EML_COND(8, 0, 1, CV_RELATIONAL_EVAL(5U, 8U, 1,
                    *chartInstance->c3_p, 0.0, -1, 5U, *chartInstance->c3_p >=
                    0.0))) {
                CV_EML_MCDC(8, 0, 0, true);
                CV_EML_IF(8, 0, 0, true);
                c3_l_out = true;
              } else {
                guard1 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[2U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 8U,
                  chartInstance->c3_sfEvent, false);
              }

              guard1 = true;
            }

            if (guard1 == true) {
              CV_EML_MCDC(8, 0, 0, false);
              CV_EML_IF(8, 0, 0, false);
              c3_l_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_l_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode4 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c3_sfEvent);
              chartInstance->c3_stateChanged = true;
              chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_Mode1;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
              chartInstance->c3_tp_Mode1 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U,
                           chartInstance->c3_sfEvent);
            }
          }
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    if (chartInstance->c3_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c3_is_c3_Boost_converter_V03) {
   case c3_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_b_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_u_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_u_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_v_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_v_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_c_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_w_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_w_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_d_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_x_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_x_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Boost_converter_V03MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[3];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 1.0;
  boolean_T c3_out;
  real_T c3_b_nargin = 0.0;
  real_T c3_b_nargout = 1.0;
  boolean_T c3_b_out;
  real_T c3_c_nargin = 0.0;
  real_T c3_c_nargout = 1.0;
  boolean_T c3_c_out;
  real_T c3_d_nargin = 0.0;
  real_T c3_d_nargout = 1.0;
  boolean_T c3_d_out;
  real_T c3_e_nargin = 0.0;
  real_T c3_e_nargout = 1.0;
  boolean_T c3_e_out;
  real_T c3_f_nargin = 0.0;
  real_T c3_f_nargout = 1.0;
  boolean_T c3_f_out;
  real_T c3_g_nargin = 0.0;
  real_T c3_g_nargout = 1.0;
  boolean_T c3_g_out;
  real_T c3_h_nargin = 0.0;
  real_T c3_h_nargout = 1.0;
  boolean_T c3_h_out;
  real_T c3_i_nargin = 0.0;
  real_T c3_i_nargout = 1.0;
  boolean_T c3_i_out;
  real_T c3_j_nargin = 0.0;
  real_T c3_j_nargout = 1.0;
  boolean_T c3_j_out;
  real_T c3_k_nargin = 0.0;
  real_T c3_k_nargout = 1.0;
  boolean_T c3_k_out;
  real_T c3_l_nargin = 0.0;
  real_T c3_l_nargout = 1.0;
  boolean_T c3_l_out;
  real_T *c3_zcVar;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  boolean_T guard9 = false;
  c3_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c3_lastMajorTime == _sfTime_) {
    *c3_zcVar = -1.0;
  } else {
    chartInstance->c3_stateChanged = (boolean_T)0;
    if (chartInstance->c3_is_active_c3_Boost_converter_V03 == 0U) {
      chartInstance->c3_stateChanged = true;
    } else {
      switch (chartInstance->c3_is_c3_Boost_converter_V03) {
       case c3_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_f_debug_family_names,
          c3_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard9 = false;
        if (CV_EML_COND(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
              *chartInstance->c3_S, 1.0, -1, 0U, *chartInstance->c3_S == 1.0)))
        {
          if (CV_EML_COND(3, 0, 1, CV_RELATIONAL_EVAL(5U, 3U, 1,
                *chartInstance->c3_q, 0.0, -1, 5U, *chartInstance->c3_q >= 0.0)))
          {
            CV_EML_MCDC(3, 0, 0, true);
            CV_EML_IF(3, 0, 0, true);
            c3_out = true;
          } else {
            guard9 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 3U,
              chartInstance->c3_sfEvent, false);
          }

          guard9 = true;
        }

        if (guard9 == true) {
          CV_EML_MCDC(3, 0, 0, false);
          CV_EML_IF(3, 0, 0, false);
          c3_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_out) {
          chartInstance->c3_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_j_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 0U,
              chartInstance->c3_sfEvent, false);
          }

          guard8 = false;
          if (CV_EML_COND(0, 0, 0, *chartInstance->c3_p <= 0.0)) {
            if (CV_EML_COND(0, 0, 1, *chartInstance->c3_q > *chartInstance->c3_C
                            * *chartInstance->c3_Ein)) {
              CV_EML_MCDC(0, 0, 0, true);
              CV_EML_IF(0, 0, 0, true);
              c3_b_out = true;
            } else {
              guard8 = true;
            }
          } else {
            if (!chartInstance->c3_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 0U,
                chartInstance->c3_sfEvent, false);
            }

            if (!chartInstance->c3_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 0U,
                chartInstance->c3_sfEvent, false);
            }

            guard8 = true;
          }

          if (guard8 == true) {
            CV_EML_MCDC(0, 0, 0, false);
            CV_EML_IF(0, 0, 0, false);
            c3_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c3_b_out) {
            chartInstance->c3_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_h_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard7 = false;
            if (CV_EML_COND(7, 0, 0, *chartInstance->c3_S == 1.0)) {
              if (CV_EML_COND(7, 0, 1, *chartInstance->c3_q <= 0.0)) {
                CV_EML_MCDC(7, 0, 0, true);
                CV_EML_IF(7, 0, 0, true);
                c3_c_out = true;
              } else {
                guard7 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 7U,
                  chartInstance->c3_sfEvent, false);
              }

              guard7 = true;
            }

            if (guard7 == true) {
              CV_EML_MCDC(7, 0, 0, false);
              CV_EML_IF(7, 0, 0, false);
              c3_c_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_c_out) {
              chartInstance->c3_stateChanged = true;
            }
          }
        }
        break;

       case c3_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_n_debug_family_names,
          c3_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard6 = false;
        if (CV_EML_COND(10, 0, 0, *chartInstance->c3_S == 0.0)) {
          if (CV_EML_COND(10, 0, 1, *chartInstance->c3_p <= 0.0)) {
            CV_EML_MCDC(10, 0, 0, true);
            CV_EML_IF(10, 0, 0, true);
            c3_d_out = true;
          } else {
            guard6 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 10U,
              chartInstance->c3_sfEvent, false);
          }

          guard6 = true;
        }

        if (guard6 == true) {
          CV_EML_MCDC(10, 0, 0, false);
          CV_EML_IF(10, 0, 0, false);
          c3_d_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_d_out) {
          chartInstance->c3_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_q_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_e_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 2U,
              chartInstance->c3_sfEvent, false);
          }

          c3_e_out = CV_EML_IF(2, 0, 0, *chartInstance->c3_q <= 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_e_out) {
            chartInstance->c3_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_g_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_f_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard5 = false;
            if (CV_EML_COND(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
                  *chartInstance->c3_S, 0.0, -1, 0U, *chartInstance->c3_S == 0.0)))
            {
              if (CV_EML_COND(5, 0, 1, CV_RELATIONAL_EVAL(5U, 5U, 1,
                    *chartInstance->c3_p, 0.0, -1, 5U, *chartInstance->c3_p >=
                    0.0))) {
                CV_EML_MCDC(5, 0, 0, true);
                CV_EML_IF(5, 0, 0, true);
                c3_f_out = true;
              } else {
                guard5 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[2U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 5U,
                  chartInstance->c3_sfEvent, false);
              }

              guard5 = true;
            }

            if (guard5 == true) {
              CV_EML_MCDC(5, 0, 0, false);
              CV_EML_IF(5, 0, 0, false);
              c3_f_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_f_out) {
              chartInstance->c3_stateChanged = true;
            }
          }
        }
        break;

       case c3_IN_Mode3:
        CV_CHART_EVAL(0, 0, 3);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_w_debug_family_names,
          c3_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_g_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard4 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c3_S == 1.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c3_q <= 0.0)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c3_g_out = true;
          } else {
            guard4 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 1U,
              chartInstance->c3_sfEvent, false);
          }

          guard4 = true;
        }

        if (guard4 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c3_g_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_g_out) {
          chartInstance->c3_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_l_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_h_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 6U,
              chartInstance->c3_sfEvent, false);
          }

          if (!chartInstance->c3_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 6U,
              chartInstance->c3_sfEvent, false);
          }

          c3_h_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            *chartInstance->c3_q, *chartInstance->c3_C * *chartInstance->c3_Ein,
            -1, 3U, *chartInstance->c3_q <= *chartInstance->c3_C *
            *chartInstance->c3_Ein));
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_h_out) {
            chartInstance->c3_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_p_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_i_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard3 = false;
            if (CV_EML_COND(9, 0, 0, CV_RELATIONAL_EVAL(5U, 9U, 0,
                  *chartInstance->c3_S, 1.0, -1, 0U, *chartInstance->c3_S == 1.0)))
            {
              if (CV_EML_COND(9, 0, 1, CV_RELATIONAL_EVAL(5U, 9U, 1,
                    *chartInstance->c3_q, 0.0, -1, 5U, *chartInstance->c3_q >=
                    0.0))) {
                CV_EML_MCDC(9, 0, 0, true);
                CV_EML_IF(9, 0, 0, true);
                c3_i_out = true;
              } else {
                guard3 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 9U,
                  chartInstance->c3_sfEvent, false);
              }

              guard3 = true;
            }

            if (guard3 == true) {
              CV_EML_MCDC(9, 0, 0, false);
              CV_EML_IF(9, 0, 0, false);
              c3_i_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_i_out) {
              chartInstance->c3_stateChanged = true;
            }
          }
        }
        break;

       case c3_IN_Mode4:
        CV_CHART_EVAL(0, 0, 4);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_u_debug_family_names,
          c3_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_nargin, 0U, c3_sf_marshallOut,
          c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_nargout, 1U,
          c3_sf_marshallOut, c3_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_j_out, 2U, c3_b_sf_marshallOut,
          c3_b_sf_marshallIn);
        guard2 = false;
        if (CV_EML_COND(4, 0, 0, *chartInstance->c3_S == 0.0)) {
          if (CV_EML_COND(4, 0, 1, *chartInstance->c3_p <= 0.0)) {
            CV_EML_MCDC(4, 0, 0, true);
            CV_EML_IF(4, 0, 0, true);
            c3_j_out = true;
          } else {
            guard2 = true;
          }
        } else {
          if (!chartInstance->c3_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 4U,
              chartInstance->c3_sfEvent, false);
          }

          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(4, 0, 0, false);
          CV_EML_IF(4, 0, 0, false);
          c3_j_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c3_j_out) {
          chartInstance->c3_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_s_debug_family_names,
            c3_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_nargin, 0U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_nargout, 1U,
            c3_sf_marshallOut, c3_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_k_out, 2U,
            c3_b_sf_marshallOut, c3_b_sf_marshallIn);
          if (!chartInstance->c3_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 11U,
              chartInstance->c3_sfEvent, false);
          }

          c3_k_out = CV_EML_IF(11, 0, 0, *chartInstance->c3_q < 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c3_k_out) {
            chartInstance->c3_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c3_m_debug_family_names,
              c3_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_nargin, 0U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_nargout, 1U,
              c3_sf_marshallOut, c3_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_l_out, 2U,
              c3_b_sf_marshallOut, c3_b_sf_marshallIn);
            guard1 = false;
            if (CV_EML_COND(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
                  *chartInstance->c3_S, 0.0, -1, 0U, *chartInstance->c3_S == 0.0)))
            {
              if (CV_EML_COND(8, 0, 1, CV_RELATIONAL_EVAL(5U, 8U, 1,
                    *chartInstance->c3_p, 0.0, -1, 5U, *chartInstance->c3_p >=
                    0.0))) {
                CV_EML_MCDC(8, 0, 0, true);
                CV_EML_IF(8, 0, 0, true);
                c3_l_out = true;
              } else {
                guard1 = true;
              }
            } else {
              if (!chartInstance->c3_dataWrittenToVector[2U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 8U,
                  chartInstance->c3_sfEvent, false);
              }

              guard1 = true;
            }

            if (guard1 == true) {
              CV_EML_MCDC(8, 0, 0, false);
              CV_EML_IF(8, 0, 0, false);
              c3_l_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c3_l_out) {
              chartInstance->c3_stateChanged = true;
            }
          }
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c3_stateChanged) {
      *c3_zcVar = 1.0;
    } else {
      *c3_zcVar = -1.0;
    }
  }
}

static void derivatives_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[2];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 0.0;
  real_T c3_b_nargin = 0.0;
  real_T c3_b_nargout = 0.0;
  real_T c3_c_nargin = 0.0;
  real_T c3_c_nargout = 0.0;
  real_T c3_d_nargin = 0.0;
  real_T c3_d_nargout = 0.0;
  real_T *c3_L_dot;
  real_T *c3_q_dot;
  real_T *c3_p_dot;
  real_T *c3_C_dot;
  c3_C_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 3);
  c3_p_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 2);
  c3_q_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 1);
  c3_L_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 0);
  *c3_L_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c3_L_dot, 1U, 1U, 0U, chartInstance->c3_sfEvent, false);
  *c3_q_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c3_q_dot, 3U, 1U, 0U, chartInstance->c3_sfEvent, false);
  *c3_p_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c3_p_dot, 2U, 1U, 0U, chartInstance->c3_sfEvent, false);
  *c3_C_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c3_C_dot, 0U, 1U, 0U, chartInstance->c3_sfEvent, false);
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c3_is_c3_Boost_converter_V03) {
   case c3_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_b_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *c3_q_dot = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L) - *chartInstance->c3_Iout;
    _SFD_DATA_RANGE_CHECK(*c3_q_dot, 3U, 4U, 0U, chartInstance->c3_sfEvent,
                          false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *c3_p_dot = -c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C) + *chartInstance->c3_Ein;
    _SFD_DATA_RANGE_CHECK(*c3_p_dot, 2U, 4U, 0U, chartInstance->c3_sfEvent,
                          false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    *c3_q_dot = -*chartInstance->c3_Iout;
    _SFD_DATA_RANGE_CHECK(*c3_q_dot, 3U, 4U, 1U, chartInstance->c3_sfEvent,
                          false);
    *c3_p_dot = *chartInstance->c3_Ein;
    _SFD_DATA_RANGE_CHECK(*c3_p_dot, 2U, 4U, 1U, chartInstance->c3_sfEvent,
                          false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_c_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    *c3_q_dot = -*chartInstance->c3_Iout;
    _SFD_DATA_RANGE_CHECK(*c3_q_dot, 3U, 4U, 2U, chartInstance->c3_sfEvent,
                          false);
    *c3_p_dot = 0.0;
    _SFD_DATA_RANGE_CHECK(*c3_p_dot, 2U, 4U, 2U, chartInstance->c3_sfEvent,
                          false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_d_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    *c3_q_dot = 0.0;
    _SFD_DATA_RANGE_CHECK(*c3_q_dot, 3U, 4U, 3U, chartInstance->c3_sfEvent,
                          false);
    *c3_p_dot = *chartInstance->c3_Ein;
    _SFD_DATA_RANGE_CHECK(*c3_p_dot, 2U, 4U, 3U, chartInstance->c3_sfEvent,
                          false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
    break;
  }
}

static void outputs_c3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  uint32_T c3_debug_family_var_map[2];
  real_T c3_nargin = 0.0;
  real_T c3_nargout = 0.0;
  real_T c3_b_nargin = 0.0;
  real_T c3_b_nargout = 0.0;
  real_T c3_c_nargin = 0.0;
  real_T c3_c_nargout = 0.0;
  real_T c3_d_nargin = 0.0;
  real_T c3_d_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c3_is_c3_Boost_converter_V03) {
   case c3_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_b_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 0U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_b_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 1U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_c_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_c_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 2U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c3_sfEvent);
    break;

   case c3_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c3_d_debug_family_names,
      c3_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargin, 0U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c3_d_nargout, 1U, c3_sf_marshallOut,
      c3_sf_marshallIn);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Eout = c3_mrdivide(chartInstance, *chartInstance->c3_q,
      *chartInstance->c3_C);
    chartInstance->c3_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Eout, 7U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    if (!chartInstance->c3_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_Iin = c3_mrdivide(chartInstance, *chartInstance->c3_p,
      *chartInstance->c3_L);
    chartInstance->c3_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_Iin, 10U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_pout = *chartInstance->c3_p;
    chartInstance->c3_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_pout, 9U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    if (!chartInstance->c3_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U, chartInstance->c3_sfEvent,
        false);
    }

    *chartInstance->c3_qout = *chartInstance->c3_q;
    chartInstance->c3_dataWrittenToVector[5U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c3_qout, 8U, 4U, 3U,
                          chartInstance->c3_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c3_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c3_is_c3_Boost_converter_V03 = c3_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c3_sfEvent);
    break;
  }
}

static void initSimStructsc3_Boost_converter_V03
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_eml_ini_fcn_to_be_inlined_22
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c3_eml_term_fcn_to_be_inlined_22
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c3_mrdivide(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_A, real_T c3_B)
{
  return c3_rdivide(chartInstance, c3_A, c3_B);
}

static real_T c3_rdivide(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_x, real_T c3_y)
{
  return c3_eml_div(chartInstance, c3_x, c3_y);
}

static void c3_isBuiltInNumeric(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c3_eml_scalexp_compatible(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static real_T c3_eml_div(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
  real_T c3_x, real_T c3_y)
{
  return c3_div(chartInstance, c3_x, c3_y);
}

static real_T c3_div(SFc3_Boost_converter_V03InstanceStruct *chartInstance,
                     real_T c3_x, real_T c3_y)
{
  (void)chartInstance;
  return c3_x / c3_y;
}

static void init_script_number_translation(uint32_T c3_machineNumber, uint32_T
  c3_chartNumber, uint32_T c3_instanceNumber)
{
  (void)c3_machineNumber;
  (void)c3_chartNumber;
  (void)c3_instanceNumber;
}

static const mxArray *c3_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const real_T c3_u)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 0, 0U, 0U, 0U, 0), false);
  return c3_y;
}

static const mxArray *c3_sf_marshallOut(void *chartInstanceVoid, void *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  sf_mex_assign(&c3_mxArrayOutData, c3_emlrt_marshallOut(chartInstance, *(real_T
    *)c3_inData), false);
  return c3_mxArrayOutData;
}

static real_T c3_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_nargout, const char_T *c3_identifier)
{
  real_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_nargout), &c3_thisId);
  sf_mex_destroy(&c3_nargout);
  return c3_y;
}

static real_T c3_b_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  real_T c3_y;
  real_T c3_d0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_d0, 1, 0, 0U, 0, 0U, 0);
  c3_y = c3_d0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  *(real_T *)c3_outData = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_mxArrayInData), c3_varName);
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_b_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const boolean_T c3_u)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 11, 0U, 0U, 0U, 0), false);
  return c3_y;
}

static const mxArray *c3_b_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  sf_mex_assign(&c3_mxArrayOutData, c3_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c3_inData), false);
  return c3_mxArrayOutData;
}

static boolean_T c3_c_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_sf_internal_predicateOutput, const char_T
  *c3_identifier)
{
  boolean_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_sf_internal_predicateOutput), &c3_thisId);
  sf_mex_destroy(&c3_sf_internal_predicateOutput);
  return c3_y;
}

static boolean_T c3_d_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  boolean_T c3_y;
  boolean_T c3_b0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_b0, 1, 11, 0U, 0, 0U, 0);
  c3_y = c3_b0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c3_outData = c3_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_mxArrayInData), c3_varName);
  sf_mex_destroy(&c3_mxArrayInData);
}

const mxArray *sf_c3_Boost_converter_V03_get_eml_resolved_functions_info(void)
{
  const mxArray *c3_nameCaptureInfo = NULL;
  c3_nameCaptureInfo = NULL;
  sf_mex_assign(&c3_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c3_nameCaptureInfo;
}

static const mxArray *c3_c_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const int32_T c3_u)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 6, 0U, 0U, 0U, 0), false);
  return c3_y;
}

static const mxArray *c3_c_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  sf_mex_assign(&c3_mxArrayOutData, c3_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c3_inData), false);
  return c3_mxArrayOutData;
}

static int32_T c3_e_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_sfEvent, const char_T *c3_identifier)
{
  int32_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_sfEvent),
    &c3_thisId);
  sf_mex_destroy(&c3_b_sfEvent);
  return c3_y;
}

static int32_T c3_f_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  int32_T c3_y;
  int32_T c3_i0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_i0, 1, 6, 0U, 0, 0U, 0);
  c3_y = c3_i0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c3_outData = c3_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_mxArrayInData), c3_varName);
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_d_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const uint8_T c3_u)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", &c3_u, 3, 0U, 0U, 0U, 0), false);
  return c3_y;
}

static const mxArray *c3_d_sf_marshallOut(void *chartInstanceVoid, void
  *c3_inData)
{
  const mxArray *c3_mxArrayOutData = NULL;
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  c3_mxArrayOutData = NULL;
  sf_mex_assign(&c3_mxArrayOutData, c3_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c3_inData), false);
  return c3_mxArrayOutData;
}

static uint8_T c3_g_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_tp_Mode2, const char_T *c3_identifier)
{
  uint8_T c3_y;
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_y = c3_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_tp_Mode2),
    &c3_thisId);
  sf_mex_destroy(&c3_b_tp_Mode2);
  return c3_y;
}

static uint8_T c3_h_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId)
{
  uint8_T c3_y;
  uint8_T c3_u0;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), &c3_u0, 1, 3, 0U, 0, 0U, 0);
  c3_y = c3_u0;
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void c3_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c3_mxArrayInData, const char_T *c3_varName, void *c3_outData)
{
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c3_outData = c3_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_mxArrayInData), c3_varName);
  sf_mex_destroy(&c3_mxArrayInData);
}

static const mxArray *c3_e_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance)
{
  const mxArray *c3_y;
  int32_T c3_i1;
  boolean_T c3_bv0[8];
  c3_y = NULL;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_createcellmatrix(11, 1), false);
  sf_mex_setcell(c3_y, 0, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_Eout));
  sf_mex_setcell(c3_y, 1, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_Iin));
  sf_mex_setcell(c3_y, 2, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_pout));
  sf_mex_setcell(c3_y, 3, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_qout));
  sf_mex_setcell(c3_y, 4, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_C));
  sf_mex_setcell(c3_y, 5, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_L));
  sf_mex_setcell(c3_y, 6, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_p));
  sf_mex_setcell(c3_y, 7, c3_emlrt_marshallOut(chartInstance,
    *chartInstance->c3_q));
  sf_mex_setcell(c3_y, 8, c3_d_emlrt_marshallOut(chartInstance,
    chartInstance->c3_is_active_c3_Boost_converter_V03));
  sf_mex_setcell(c3_y, 9, c3_d_emlrt_marshallOut(chartInstance,
    chartInstance->c3_is_c3_Boost_converter_V03));
  for (c3_i1 = 0; c3_i1 < 8; c3_i1++) {
    c3_bv0[c3_i1] = chartInstance->c3_dataWrittenToVector[c3_i1];
  }

  sf_mex_setcell(c3_y, 10, c3_f_emlrt_marshallOut(chartInstance, c3_bv0));
  return c3_y;
}

static const mxArray *c3_f_emlrt_marshallOut
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const boolean_T c3_u[8])
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_create("y", c3_u, 11, 0U, 1U, 0U, 1, 8), false);
  return c3_y;
}

static void c3_i_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u)
{
  boolean_T c3_bv1[8];
  int32_T c3_i2;
  *chartInstance->c3_Eout = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Eout", c3_u, 0)), "Eout");
  *chartInstance->c3_Iin = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Iin", c3_u, 1)), "Iin");
  *chartInstance->c3_pout = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("pout", c3_u, 2)), "pout");
  *chartInstance->c3_qout = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("qout", c3_u, 3)), "qout");
  *chartInstance->c3_C = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("C", c3_u, 4)), "C");
  *chartInstance->c3_L = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("L", c3_u, 5)), "L");
  *chartInstance->c3_p = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("p", c3_u, 6)), "p");
  *chartInstance->c3_q = c3_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("q", c3_u, 7)), "q");
  chartInstance->c3_is_active_c3_Boost_converter_V03 = c3_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_active_c3_Boost_converter_V03",
       c3_u, 8)), "is_active_c3_Boost_converter_V03");
  chartInstance->c3_is_c3_Boost_converter_V03 = c3_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c3_Boost_converter_V03", c3_u,
       9)), "is_c3_Boost_converter_V03");
  c3_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c3_u, 10)), "dataWrittenToVector", c3_bv1);
  for (c3_i2 = 0; c3_i2 < 8; c3_i2++) {
    chartInstance->c3_dataWrittenToVector[c3_i2] = c3_bv1[c3_i2];
  }

  sf_mex_assign(&chartInstance->c3_setSimStateSideEffectsInfo,
                c3_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c3_u, 11)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c3_u);
}

static void c3_j_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_b_dataWrittenToVector, const char_T
  *c3_identifier, boolean_T c3_y[8])
{
  emlrtMsgIdentifier c3_thisId;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  c3_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c3_b_dataWrittenToVector),
                        &c3_thisId, c3_y);
  sf_mex_destroy(&c3_b_dataWrittenToVector);
}

static void c3_k_emlrt_marshallIn(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance, const mxArray *c3_u, const emlrtMsgIdentifier *c3_parentId,
  boolean_T c3_y[8])
{
  boolean_T c3_bv2[8];
  int32_T c3_i3;
  (void)chartInstance;
  sf_mex_import(c3_parentId, sf_mex_dup(c3_u), c3_bv2, 1, 11, 0U, 1, 0U, 1, 8);
  for (c3_i3 = 0; c3_i3 < 8; c3_i3++) {
    c3_y[c3_i3] = c3_bv2[c3_i3];
  }

  sf_mex_destroy(&c3_u);
}

static const mxArray *c3_l_emlrt_marshallIn
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray
   *c3_b_setSimStateSideEffectsInfo, const char_T *c3_identifier)
{
  const mxArray *c3_y = NULL;
  emlrtMsgIdentifier c3_thisId;
  c3_y = NULL;
  c3_thisId.fIdentifier = c3_identifier;
  c3_thisId.fParent = NULL;
  sf_mex_assign(&c3_y, c3_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c3_b_setSimStateSideEffectsInfo), &c3_thisId), false);
  sf_mex_destroy(&c3_b_setSimStateSideEffectsInfo);
  return c3_y;
}

static const mxArray *c3_m_emlrt_marshallIn
  (SFc3_Boost_converter_V03InstanceStruct *chartInstance, const mxArray *c3_u,
   const emlrtMsgIdentifier *c3_parentId)
{
  const mxArray *c3_y = NULL;
  (void)chartInstance;
  (void)c3_parentId;
  c3_y = NULL;
  sf_mex_assign(&c3_y, sf_mex_duplicatearraysafe(&c3_u), false);
  sf_mex_destroy(&c3_u);
  return c3_y;
}

static void init_dsm_address_info(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc3_Boost_converter_V03InstanceStruct
  *chartInstance)
{
  chartInstance->c3_S = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    0);
  chartInstance->c3_Ein = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c3_L = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 0);
  chartInstance->c3_q = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 1);
  chartInstance->c3_p = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 2);
  chartInstance->c3_C = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 3);
  chartInstance->c3_Eout = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c3_Iout = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c3_qout = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c3_pout = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c3_Iin = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c3_Boost_converter_V03_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1092944288U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(171458799U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1247907199U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2989700243U);
}

mxArray* sf_c3_Boost_converter_V03_get_post_codegen_info(void);
mxArray *sf_c3_Boost_converter_V03_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("1xqsbPCSuyeA14TFTWcdaF");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c3_Boost_converter_V03_get_post_codegen_info
      ();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c3_Boost_converter_V03_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c3_Boost_converter_V03_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c3_Boost_converter_V03_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c3_Boost_converter_V03_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c3_Boost_converter_V03(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[44],T\"Eout\",},{M[1],M[52],T\"Iin\",},{M[1],M[48],T\"pout\",},{M[1],M[47],T\"qout\",},{M[5],M[40],T\"C\",},{M[5],M[30],T\"L\",},{M[5],M[39],T\"p\",},{M[5],M[38],T\"q\",},{M[8],M[0],T\"is_active_c3_Boost_converter_V03\",},{M[9],M[0],T\"is_c3_Boost_converter_V03\",}}",
    "100 S'type','srcId','name','auxInfo'{{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 11, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c3_Boost_converter_V03_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc3_Boost_converter_V03InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Boost_converter_V03MachineNumber_,
           3,
           4,
           13,
           0,
           11,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Boost_converter_V03MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Boost_converter_V03MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Boost_converter_V03MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"C");
          _SFD_SET_DATA_PROPS(1,0,0,0,"L");
          _SFD_SET_DATA_PROPS(2,0,0,0,"p");
          _SFD_SET_DATA_PROPS(3,0,0,0,"q");
          _SFD_SET_DATA_PROPS(4,1,1,0,"S");
          _SFD_SET_DATA_PROPS(5,1,1,0,"Ein");
          _SFD_SET_DATA_PROPS(6,1,1,0,"Iout");
          _SFD_SET_DATA_PROPS(7,2,0,1,"Eout");
          _SFD_SET_DATA_PROPS(8,2,0,1,"qout");
          _SFD_SET_DATA_PROPS(9,2,0,1,"pout");
          _SFD_SET_DATA_PROPS(10,2,0,1,"Iin");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_CH_SUBSTATE_COUNT(4);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
        }

        _SFD_CV_INIT_CHART(4,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(12,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(10,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(12,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(3,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(3,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(3,0,0,2,8,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(3,0,1,12,18,-1,5);
        _SFD_CV_INIT_EML(5,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(5,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(5,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,2,8,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,1,12,18,-1,5);
        _SFD_CV_INIT_EML(7,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(7,0,0,1,17,1,17);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 17 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(7,0,0,2,17,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(0,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(0,0,0,1,21,1,21);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 21 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(0,0,0,2,21,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(6,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,12,1,12);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,2,12,-1,3);
        _SFD_CV_INIT_EML(8,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(8,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(8,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(8,0,0,2,8,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(8,0,1,12,18,-1,5);
        _SFD_CV_INIT_EML(10,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(10,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(10,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(9,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(9,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(9,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(9,0,0,2,8,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(9,0,1,12,18,-1,5);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,8,1,8);
        _SFD_CV_INIT_EML(11,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(11,0,0,1,7,1,7);
        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(4,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(4,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(1,0,0,1,18,1,18);

        {
          static int condStart[] = { 2, 12 };

          static int condEnd[] = { 8, 18 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(1,0,0,2,18,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c3_sf_marshallOut,(MexInFcnForType)c3_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Boost_converter_V03MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc3_Boost_converter_V03InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c3_S);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c3_Ein);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c3_L);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c3_q);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c3_p);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c3_C);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c3_Eout);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c3_Iout);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c3_qout);
        _SFD_SET_DATA_VALUE_PTR(9U, chartInstance->c3_pout);
        _SFD_SET_DATA_VALUE_PTR(10U, chartInstance->c3_Iin);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sWBeoXrBXQqceGvIjg1AByF";
}

static void sf_opaque_initialize_c3_Boost_converter_V03(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c3_Boost_converter_V03
    ((SFc3_Boost_converter_V03InstanceStruct*) chartInstanceVar);
  initialize_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c3_Boost_converter_V03(void *chartInstanceVar)
{
  enable_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c3_Boost_converter_V03(void *chartInstanceVar)
{
  disable_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c3_Boost_converter_V03(void
  *chartInstanceVar)
{
  zeroCrossings_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_derivatives_c3_Boost_converter_V03(void *chartInstanceVar)
{
  derivatives_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_outputs_c3_Boost_converter_V03(void *chartInstanceVar)
{
  outputs_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c3_Boost_converter_V03(void *chartInstanceVar)
{
  sf_gateway_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c3_Boost_converter_V03(SimStruct*
  S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c3_Boost_converter_V03
    ((SFc3_Boost_converter_V03InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c3_Boost_converter_V03(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c3_Boost_converter_V03(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc3_Boost_converter_V03InstanceStruct*) chartInstanceVar
      )->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Boost_converter_V03_optimization_info();
    }

    finalize_c3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc3_Boost_converter_V03((SFc3_Boost_converter_V03InstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c3_Boost_converter_V03(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c3_Boost_converter_V03
      ((SFc3_Boost_converter_V03InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c3_Boost_converter_V03(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Boost_converter_V03_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,3);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,3,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,3);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,3,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,3,4);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=4; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,3);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1489947036U));
  ssSetChecksum1(S,(160174326U));
  ssSetChecksum2(S,(3365650397U));
  ssSetChecksum3(S,(3538577603U));
  ssSetNumContStates(S,4);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c3_Boost_converter_V03(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c3_Boost_converter_V03(SimStruct *S)
{
  SFc3_Boost_converter_V03InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc3_Boost_converter_V03InstanceStruct *)utMalloc(sizeof
    (SFc3_Boost_converter_V03InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc3_Boost_converter_V03InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c3_Boost_converter_V03;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c3_Boost_converter_V03;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c3_Boost_converter_V03;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c3_Boost_converter_V03;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c3_Boost_converter_V03;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c3_Boost_converter_V03;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c3_Boost_converter_V03;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c3_Boost_converter_V03;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c3_Boost_converter_V03;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c3_Boost_converter_V03;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c3_Boost_converter_V03;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c3_Boost_converter_V03;
  chartInstance->chartInfo.mdlStart = mdlStart_c3_Boost_converter_V03;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c3_Boost_converter_V03;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c3_Boost_converter_V03_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c3_Boost_converter_V03(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c3_Boost_converter_V03(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c3_Boost_converter_V03(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c3_Boost_converter_V03_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
