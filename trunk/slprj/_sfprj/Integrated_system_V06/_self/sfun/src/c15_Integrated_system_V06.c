/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V06_sfun.h"
#include "c15_Integrated_system_V06.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V06_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c15_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c15_IN_Mode1                   ((uint8_T)1U)
#define c15_IN_Mode2                   ((uint8_T)2U)
#define c15_IN_Mode3                   ((uint8_T)3U)
#define c15_IN_Mode4                   ((uint8_T)4U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c15_debug_family_names[2] = { "nargin", "nargout" };

static const char * c15_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c15_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c15_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c15_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c15_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_i_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_j_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_k_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_q_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_r_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_t_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c15_u_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void initialize_params_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void enable_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void disable_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void c15_update_debugger_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void set_sim_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_st);
static void c15_set_sim_state_side_effects_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void finalize_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void sf_gateway_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void mdl_start_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void zeroCrossings_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void derivatives_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void outputs_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void initSimStructsc15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void c15_eml_ini_fcn_to_be_inlined_161
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void c15_eml_term_fcn_to_be_inlined_161
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c15_machineNumber, uint32_T
  c15_chartNumber, uint32_T c15_instanceNumber);
static const mxArray *c15_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const real_T c15_u);
static const mxArray *c15_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData);
static real_T c15_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_nargout, const char_T *c15_identifier);
static real_T c15_b_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId);
static void c15_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData);
static const mxArray *c15_b_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c15_u);
static const mxArray *c15_b_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData);
static boolean_T c15_c_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_sf_internal_predicateOutput, const char_T *c15_identifier);
static boolean_T c15_d_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_u, const emlrtMsgIdentifier *c15_parentId);
static void c15_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData);
static const mxArray *c15_c_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c15_u);
static const mxArray *c15_c_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData);
static int32_T c15_e_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_b_sfEvent, const char_T *c15_identifier);
static int32_T c15_f_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId);
static void c15_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData);
static const mxArray *c15_d_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c15_u);
static const mxArray *c15_d_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData);
static uint8_T c15_g_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_b_tp_Mode1, const char_T *c15_identifier);
static uint8_T c15_h_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId);
static void c15_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData);
static const mxArray *c15_e_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *c15_f_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c15_u[2]);
static void c15_i_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u);
static void c15_j_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_b_dataWrittenToVector, const char_T
  *c15_identifier, boolean_T c15_y[2]);
static void c15_k_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId,
  boolean_T c15_y[2]);
static const mxArray *c15_l_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_b_setSimStateSideEffectsInfo, const char_T *c15_identifier);
static const mxArray *c15_m_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_u, const emlrtMsgIdentifier *c15_parentId);
static void init_dsm_address_info(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c15_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c15_doSetSimStateSideEffects = 0U;
  chartInstance->c15_setSimStateSideEffectsInfo = NULL;
  chartInstance->c15_tp_Mode1 = 0U;
  chartInstance->c15_tp_Mode2 = 0U;
  chartInstance->c15_tp_Mode3 = 0U;
  chartInstance->c15_tp_Mode4 = 0U;
  chartInstance->c15_is_active_c15_Integrated_system_V06 = 0U;
  chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c15_update_debugger_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c15_prevAniVal;
  c15_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c15_is_active_c15_Integrated_system_V06 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 13U, chartInstance->c15_sfEvent);
  }

  if (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
  }

  if (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
  }

  if (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
  }

  if (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode4) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
  }

  _SFD_SET_ANIMATION(c15_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c15_st = NULL;
  c15_st = NULL;
  sf_mex_assign(&c15_st, c15_e_emlrt_marshallOut(chartInstance), false);
  return c15_st;
}

static void set_sim_state_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_st)
{
  c15_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c15_st));
  chartInstance->c15_doSetSimStateSideEffects = 1U;
  c15_update_debugger_state_c15_Integrated_system_V06(chartInstance);
  sf_mex_destroy(&c15_st);
}

static void c15_set_sim_state_side_effects_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (chartInstance->c15_doSetSimStateSideEffects != 0) {
    chartInstance->c15_tp_Mode1 = (uint8_T)
      (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode1);
    chartInstance->c15_tp_Mode2 = (uint8_T)
      (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode2);
    chartInstance->c15_tp_Mode3 = (uint8_T)
      (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode3);
    chartInstance->c15_tp_Mode4 = (uint8_T)
      (chartInstance->c15_is_c15_Integrated_system_V06 == c15_IN_Mode4);
    chartInstance->c15_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c15_setSimStateSideEffectsInfo);
}

static void sf_gateway_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c15_debug_family_var_map[2];
  real_T c15_nargin = 0.0;
  real_T c15_nargout = 0.0;
  uint32_T c15_b_debug_family_var_map[3];
  real_T c15_b_nargin = 0.0;
  real_T c15_b_nargout = 1.0;
  boolean_T c15_out;
  real_T c15_c_nargin = 0.0;
  real_T c15_c_nargout = 1.0;
  boolean_T c15_b_out;
  real_T c15_d_nargin = 0.0;
  real_T c15_d_nargout = 1.0;
  boolean_T c15_c_out;
  real_T c15_e_nargin = 0.0;
  real_T c15_e_nargout = 1.0;
  boolean_T c15_d_out;
  real_T c15_f_nargin = 0.0;
  real_T c15_f_nargout = 1.0;
  boolean_T c15_e_out;
  real_T c15_g_nargin = 0.0;
  real_T c15_g_nargout = 1.0;
  boolean_T c15_f_out;
  real_T c15_h_nargin = 0.0;
  real_T c15_h_nargout = 1.0;
  boolean_T c15_g_out;
  real_T c15_i_nargin = 0.0;
  real_T c15_i_nargout = 1.0;
  boolean_T c15_h_out;
  real_T c15_j_nargin = 0.0;
  real_T c15_j_nargout = 0.0;
  real_T c15_k_nargin = 0.0;
  real_T c15_k_nargout = 0.0;
  real_T c15_l_nargin = 0.0;
  real_T c15_l_nargout = 0.0;
  real_T c15_m_nargin = 0.0;
  real_T c15_m_nargout = 0.0;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  c15_set_sim_state_side_effects_c15_Integrated_system_V06(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c15_lastMajorTime = _sfTime_;
    chartInstance->c15_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 13U, chartInstance->c15_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_out, 3U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_in, 2U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_PWM_2, 1U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_PWM_1, 0U, 1U, 0U,
                          chartInstance->c15_sfEvent, false);
    chartInstance->c15_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 13U,
                 chartInstance->c15_sfEvent);
    if (chartInstance->c15_is_active_c15_Integrated_system_V06 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 13U,
                   chartInstance->c15_sfEvent);
      chartInstance->c15_stateChanged = true;
      chartInstance->c15_is_active_c15_Integrated_system_V06 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c15_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_e_debug_family_names,
        c15_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargin, 0U, c15_sf_marshallOut,
        c15_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargout, 1U, c15_sf_marshallOut,
        c15_sf_marshallIn);
      *chartInstance->c15_V_a = 0.0;
      chartInstance->c15_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 5U, 0U,
                            chartInstance->c15_sfEvent, false);
      *chartInstance->c15_I_in = 0.0;
      chartInstance->c15_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 5U, 0U,
                            chartInstance->c15_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c15_stateChanged = true;
      chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
      chartInstance->c15_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c15_is_c15_Integrated_system_V06) {
       case c15_IN_Mode1:
        CV_CHART_EVAL(13, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c15_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c15_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_f_debug_family_names,
          c15_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_out, 2U, c15_b_sf_marshallOut,
          c15_b_sf_marshallIn);
        guard8 = false;
        if (CV_EML_COND(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(1, 0, 1, CV_RELATIONAL_EVAL(5U, 1U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c15_out = true;
          } else {
            guard8 = true;
          }
        } else {
          guard8 = true;
        }

        if (guard8 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c15_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
          chartInstance->c15_stateChanged = true;
          chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode2 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                       chartInstance->c15_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_h_debug_family_names,
            c15_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard7 = false;
          if (CV_EML_COND(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(3, 0, 1, CV_RELATIONAL_EVAL(5U, 3U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(3, 0, 0, true);
              CV_EML_IF(3, 0, 0, true);
              c15_b_out = true;
            } else {
              guard7 = true;
            }
          } else {
            guard7 = true;
          }

          if (guard7 == true) {
            CV_EML_MCDC(3, 0, 0, false);
            CV_EML_IF(3, 0, 0, false);
            c15_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode1 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
            chartInstance->c15_stateChanged = true;
            chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode3;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode3 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U,
                         chartInstance->c15_sfEvent);
          }
        }
        break;

       case c15_IN_Mode2:
        CV_CHART_EVAL(13, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c15_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c15_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_g_debug_family_names,
          c15_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard6 = false;
        if (CV_EML_COND(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(2, 0, 1, CV_RELATIONAL_EVAL(5U, 2U, 1,
                *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 0.0))) {
            CV_EML_MCDC(2, 0, 0, true);
            CV_EML_IF(2, 0, 0, true);
            c15_c_out = true;
          } else {
            guard6 = true;
          }
        } else {
          guard6 = true;
        }

        if (guard6 == true) {
          CV_EML_MCDC(2, 0, 0, false);
          CV_EML_IF(2, 0, 0, false);
          c15_c_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
          chartInstance->c15_stateChanged = true;
          chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode1 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                       chartInstance->c15_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_k_debug_family_names,
            c15_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard5 = false;
          if (CV_EML_COND(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(5, 0, 1, CV_RELATIONAL_EVAL(5U, 5U, 1,
                  *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 1.0))) {
              CV_EML_MCDC(5, 0, 0, true);
              CV_EML_IF(5, 0, 0, true);
              c15_d_out = true;
            } else {
              guard5 = true;
            }
          } else {
            guard5 = true;
          }

          if (guard5 == true) {
            CV_EML_MCDC(5, 0, 0, false);
            CV_EML_IF(5, 0, 0, false);
            c15_d_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_d_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode2 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
            chartInstance->c15_stateChanged = true;
            chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode4;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode4 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U,
                         chartInstance->c15_sfEvent);
          }
        }
        break;

       case c15_IN_Mode3:
        CV_CHART_EVAL(13, 0, 3);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c15_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                     chartInstance->c15_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_m_debug_family_names,
          c15_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard4 = false;
        if (CV_EML_COND(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0,
              *chartInstance->c15_PWM_1, 1.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              1.0))) {
          if (CV_EML_COND(4, 0, 1, CV_RELATIONAL_EVAL(5U, 4U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(4, 0, 0, true);
            CV_EML_IF(4, 0, 0, true);
            c15_e_out = true;
          } else {
            guard4 = true;
          }
        } else {
          guard4 = true;
        }

        if (guard4 == true) {
          CV_EML_MCDC(4, 0, 0, false);
          CV_EML_IF(4, 0, 0, false);
          c15_e_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode3 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
          chartInstance->c15_stateChanged = true;
          chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode4;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode4 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c15_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_i_debug_family_names,
            c15_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard3 = false;
          if (CV_EML_COND(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
                *chartInstance->c15_PWM_1, 0.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 0.0))) {
            if (CV_EML_COND(6, 0, 1, CV_RELATIONAL_EVAL(5U, 6U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(6, 0, 0, true);
              CV_EML_IF(6, 0, 0, true);
              c15_f_out = true;
            } else {
              guard3 = true;
            }
          } else {
            guard3 = true;
          }

          if (guard3 == true) {
            CV_EML_MCDC(6, 0, 0, false);
            CV_EML_IF(6, 0, 0, false);
            c15_f_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_f_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode3 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
            chartInstance->c15_stateChanged = true;
            chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode1;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode1 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U,
                         chartInstance->c15_sfEvent);
          }
        }
        break;

       case c15_IN_Mode4:
        CV_CHART_EVAL(13, 0, 4);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c15_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                     chartInstance->c15_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_j_debug_family_names,
          c15_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard2 = false;
        if (CV_EML_COND(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(7, 0, 1, CV_RELATIONAL_EVAL(5U, 7U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(7, 0, 0, true);
            CV_EML_IF(7, 0, 0, true);
            c15_g_out = true;
          } else {
            guard2 = true;
          }
        } else {
          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(7, 0, 0, false);
          CV_EML_IF(7, 0, 0, false);
          c15_g_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_g_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode4 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
          chartInstance->c15_stateChanged = true;
          chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c15_sfEvent);
          chartInstance->c15_tp_Mode2 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                       chartInstance->c15_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_l_debug_family_names,
            c15_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_i_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_i_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard1 = false;
          if (CV_EML_COND(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(8, 0, 1, CV_RELATIONAL_EVAL(5U, 8U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(8, 0, 0, true);
              CV_EML_IF(8, 0, 0, true);
              c15_h_out = true;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1 == true) {
            CV_EML_MCDC(8, 0, 0, false);
            CV_EML_IF(8, 0, 0, false);
            c15_h_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_h_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode4 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c15_sfEvent);
            chartInstance->c15_stateChanged = true;
            chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_Mode3;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c15_sfEvent);
            chartInstance->c15_tp_Mode3 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U,
                         chartInstance->c15_sfEvent);
          }
        }
        break;

       default:
        CV_CHART_EVAL(13, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c15_sfEvent);
    if (chartInstance->c15_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c15_is_c15_Integrated_system_V06) {
   case c15_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_j_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_j_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = 0.0;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = 0.0;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode2:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_b_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_k_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_k_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = -*chartInstance->c15_V_in;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = -*chartInstance->c15_I_out;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode3:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_c_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_l_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_l_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = *chartInstance->c15_V_in;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = *chartInstance->c15_I_out;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode4:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_d_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_m_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_m_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = 0.0;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = 0.0;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V06MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c15_debug_family_var_map[3];
  real_T c15_nargin = 0.0;
  real_T c15_nargout = 1.0;
  boolean_T c15_out;
  real_T c15_b_nargin = 0.0;
  real_T c15_b_nargout = 1.0;
  boolean_T c15_b_out;
  real_T c15_c_nargin = 0.0;
  real_T c15_c_nargout = 1.0;
  boolean_T c15_c_out;
  real_T c15_d_nargin = 0.0;
  real_T c15_d_nargout = 1.0;
  boolean_T c15_d_out;
  real_T c15_e_nargin = 0.0;
  real_T c15_e_nargout = 1.0;
  boolean_T c15_e_out;
  real_T c15_f_nargin = 0.0;
  real_T c15_f_nargout = 1.0;
  boolean_T c15_f_out;
  real_T c15_g_nargin = 0.0;
  real_T c15_g_nargout = 1.0;
  boolean_T c15_g_out;
  real_T c15_h_nargin = 0.0;
  real_T c15_h_nargout = 1.0;
  boolean_T c15_h_out;
  real_T *c15_zcVar;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  c15_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c15_lastMajorTime == _sfTime_) {
    *c15_zcVar = -1.0;
  } else {
    chartInstance->c15_stateChanged = (boolean_T)0;
    if (chartInstance->c15_is_active_c15_Integrated_system_V06 == 0U) {
      chartInstance->c15_stateChanged = true;
    } else {
      switch (chartInstance->c15_is_c15_Integrated_system_V06) {
       case c15_IN_Mode1:
        CV_CHART_EVAL(13, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_f_debug_family_names,
          c15_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargin, 0U, c15_sf_marshallOut,
          c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_out, 2U, c15_b_sf_marshallOut,
          c15_b_sf_marshallIn);
        guard8 = false;
        if (CV_EML_COND(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(1, 0, 1, CV_RELATIONAL_EVAL(5U, 1U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c15_out = true;
          } else {
            guard8 = true;
          }
        } else {
          guard8 = true;
        }

        if (guard8 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c15_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_out) {
          chartInstance->c15_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_h_debug_family_names,
            c15_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard7 = false;
          if (CV_EML_COND(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(3, 0, 1, CV_RELATIONAL_EVAL(5U, 3U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(3, 0, 0, true);
              CV_EML_IF(3, 0, 0, true);
              c15_b_out = true;
            } else {
              guard7 = true;
            }
          } else {
            guard7 = true;
          }

          if (guard7 == true) {
            CV_EML_MCDC(3, 0, 0, false);
            CV_EML_IF(3, 0, 0, false);
            c15_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_b_out) {
            chartInstance->c15_stateChanged = true;
          }
        }
        break;

       case c15_IN_Mode2:
        CV_CHART_EVAL(13, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_g_debug_family_names,
          c15_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard6 = false;
        if (CV_EML_COND(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(2, 0, 1, CV_RELATIONAL_EVAL(5U, 2U, 1,
                *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 0.0))) {
            CV_EML_MCDC(2, 0, 0, true);
            CV_EML_IF(2, 0, 0, true);
            c15_c_out = true;
          } else {
            guard6 = true;
          }
        } else {
          guard6 = true;
        }

        if (guard6 == true) {
          CV_EML_MCDC(2, 0, 0, false);
          CV_EML_IF(2, 0, 0, false);
          c15_c_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_c_out) {
          chartInstance->c15_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_k_debug_family_names,
            c15_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard5 = false;
          if (CV_EML_COND(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(5, 0, 1, CV_RELATIONAL_EVAL(5U, 5U, 1,
                  *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 1.0))) {
              CV_EML_MCDC(5, 0, 0, true);
              CV_EML_IF(5, 0, 0, true);
              c15_d_out = true;
            } else {
              guard5 = true;
            }
          } else {
            guard5 = true;
          }

          if (guard5 == true) {
            CV_EML_MCDC(5, 0, 0, false);
            CV_EML_IF(5, 0, 0, false);
            c15_d_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_d_out) {
            chartInstance->c15_stateChanged = true;
          }
        }
        break;

       case c15_IN_Mode3:
        CV_CHART_EVAL(13, 0, 3);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_m_debug_family_names,
          c15_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_e_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard4 = false;
        if (CV_EML_COND(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0,
              *chartInstance->c15_PWM_1, 1.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              1.0))) {
          if (CV_EML_COND(4, 0, 1, CV_RELATIONAL_EVAL(5U, 4U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(4, 0, 0, true);
            CV_EML_IF(4, 0, 0, true);
            c15_e_out = true;
          } else {
            guard4 = true;
          }
        } else {
          guard4 = true;
        }

        if (guard4 == true) {
          CV_EML_MCDC(4, 0, 0, false);
          CV_EML_IF(4, 0, 0, false);
          c15_e_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_e_out) {
          chartInstance->c15_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_i_debug_family_names,
            c15_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_f_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard3 = false;
          if (CV_EML_COND(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
                *chartInstance->c15_PWM_1, 0.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 0.0))) {
            if (CV_EML_COND(6, 0, 1, CV_RELATIONAL_EVAL(5U, 6U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(6, 0, 0, true);
              CV_EML_IF(6, 0, 0, true);
              c15_f_out = true;
            } else {
              guard3 = true;
            }
          } else {
            guard3 = true;
          }

          if (guard3 == true) {
            CV_EML_MCDC(6, 0, 0, false);
            CV_EML_IF(6, 0, 0, false);
            c15_f_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_f_out) {
            chartInstance->c15_stateChanged = true;
          }
        }
        break;

       case c15_IN_Mode4:
        CV_CHART_EVAL(13, 0, 4);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_j_debug_family_names,
          c15_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_nargin, 0U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_nargout, 1U,
          c15_sf_marshallOut, c15_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_g_out, 2U,
          c15_b_sf_marshallOut, c15_b_sf_marshallIn);
        guard2 = false;
        if (CV_EML_COND(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
              *chartInstance->c15_PWM_1, 0.0, -1, 0U, *chartInstance->c15_PWM_1 ==
              0.0))) {
          if (CV_EML_COND(7, 0, 1, CV_RELATIONAL_EVAL(5U, 7U, 1,
                *chartInstance->c15_PWM_2, 1.0, -1, 0U,
                *chartInstance->c15_PWM_2 == 1.0))) {
            CV_EML_MCDC(7, 0, 0, true);
            CV_EML_IF(7, 0, 0, true);
            c15_g_out = true;
          } else {
            guard2 = true;
          }
        } else {
          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(7, 0, 0, false);
          CV_EML_IF(7, 0, 0, false);
          c15_g_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c15_g_out) {
          chartInstance->c15_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c15_l_debug_family_names,
            c15_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_nargin, 0U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_nargout, 1U,
            c15_sf_marshallOut, c15_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_h_out, 2U,
            c15_b_sf_marshallOut, c15_b_sf_marshallIn);
          guard1 = false;
          if (CV_EML_COND(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
                *chartInstance->c15_PWM_1, 1.0, -1, 0U,
                *chartInstance->c15_PWM_1 == 1.0))) {
            if (CV_EML_COND(8, 0, 1, CV_RELATIONAL_EVAL(5U, 8U, 1,
                  *chartInstance->c15_PWM_2, 0.0, -1, 0U,
                  *chartInstance->c15_PWM_2 == 0.0))) {
              CV_EML_MCDC(8, 0, 0, true);
              CV_EML_IF(8, 0, 0, true);
              c15_h_out = true;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1 == true) {
            CV_EML_MCDC(8, 0, 0, false);
            CV_EML_IF(8, 0, 0, false);
            c15_h_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c15_h_out) {
            chartInstance->c15_stateChanged = true;
          }
        }
        break;

       default:
        CV_CHART_EVAL(13, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c15_stateChanged) {
      *c15_zcVar = 1.0;
    } else {
      *c15_zcVar = -1.0;
    }
  }
}

static void derivatives_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c15_debug_family_var_map[2];
  real_T c15_nargin = 0.0;
  real_T c15_nargout = 0.0;
  real_T c15_b_nargin = 0.0;
  real_T c15_b_nargout = 0.0;
  real_T c15_c_nargin = 0.0;
  real_T c15_c_nargout = 0.0;
  real_T c15_d_nargin = 0.0;
  real_T c15_d_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c15_is_c15_Integrated_system_V06) {
   case c15_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode2:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_b_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode3:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_c_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode4:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_d_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
    break;
  }
}

static void outputs_c15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c15_debug_family_var_map[2];
  real_T c15_nargin = 0.0;
  real_T c15_nargout = 0.0;
  real_T c15_b_nargin = 0.0;
  real_T c15_b_nargout = 0.0;
  real_T c15_c_nargin = 0.0;
  real_T c15_c_nargout = 0.0;
  real_T c15_d_nargin = 0.0;
  real_T c15_d_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c15_is_c15_Integrated_system_V06) {
   case c15_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = 0.0;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = 0.0;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 0U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode2:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_b_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_b_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = -*chartInstance->c15_V_in;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = -*chartInstance->c15_I_out;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 1U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode3:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_c_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_c_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = *chartInstance->c15_V_in;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = *chartInstance->c15_I_out;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 2U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c15_sfEvent);
    break;

   case c15_IN_Mode4:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c15_d_debug_family_names,
      c15_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargin, 0U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c15_d_nargout, 1U, c15_sf_marshallOut,
      c15_sf_marshallIn);
    *chartInstance->c15_V_a = 0.0;
    chartInstance->c15_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_V_a, 4U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    *chartInstance->c15_I_in = 0.0;
    chartInstance->c15_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c15_I_in, 5U, 4U, 3U,
                          chartInstance->c15_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c15_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c15_is_c15_Integrated_system_V06 = c15_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c15_sfEvent);
    break;
  }
}

static void initSimStructsc15_Integrated_system_V06
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c15_eml_ini_fcn_to_be_inlined_161
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c15_eml_term_fcn_to_be_inlined_161
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c15_machineNumber, uint32_T
  c15_chartNumber, uint32_T c15_instanceNumber)
{
  (void)c15_machineNumber;
  (void)c15_chartNumber;
  (void)c15_instanceNumber;
}

static const mxArray *c15_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const real_T c15_u)
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_create("y", &c15_u, 0, 0U, 0U, 0U, 0), false);
  return c15_y;
}

static const mxArray *c15_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData)
{
  const mxArray *c15_mxArrayOutData = NULL;
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c15_mxArrayOutData = NULL;
  sf_mex_assign(&c15_mxArrayOutData, c15_emlrt_marshallOut(chartInstance,
    *(real_T *)c15_inData), false);
  return c15_mxArrayOutData;
}

static real_T c15_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_nargout, const char_T *c15_identifier)
{
  real_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  c15_y = c15_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c15_nargout),
    &c15_thisId);
  sf_mex_destroy(&c15_nargout);
  return c15_y;
}

static real_T c15_b_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  real_T c15_y;
  real_T c15_d0;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), &c15_d0, 1, 0, 0U, 0, 0U, 0);
  c15_y = c15_d0;
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static void c15_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData)
{
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(real_T *)c15_outData = c15_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_mxArrayInData), c15_varName);
  sf_mex_destroy(&c15_mxArrayInData);
}

static const mxArray *c15_b_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c15_u)
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_create("y", &c15_u, 11, 0U, 0U, 0U, 0), false);
  return c15_y;
}

static const mxArray *c15_b_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData)
{
  const mxArray *c15_mxArrayOutData = NULL;
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c15_mxArrayOutData = NULL;
  sf_mex_assign(&c15_mxArrayOutData, c15_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c15_inData), false);
  return c15_mxArrayOutData;
}

static boolean_T c15_c_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_sf_internal_predicateOutput, const char_T *c15_identifier)
{
  boolean_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  c15_y = c15_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_sf_internal_predicateOutput), &c15_thisId);
  sf_mex_destroy(&c15_sf_internal_predicateOutput);
  return c15_y;
}

static boolean_T c15_d_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  boolean_T c15_y;
  boolean_T c15_b0;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), &c15_b0, 1, 11, 0U, 0, 0U, 0);
  c15_y = c15_b0;
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static void c15_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData)
{
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c15_outData = c15_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_mxArrayInData), c15_varName);
  sf_mex_destroy(&c15_mxArrayInData);
}

const mxArray *sf_c15_Integrated_system_V06_get_eml_resolved_functions_info(void)
{
  const mxArray *c15_nameCaptureInfo = NULL;
  c15_nameCaptureInfo = NULL;
  sf_mex_assign(&c15_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c15_nameCaptureInfo;
}

static const mxArray *c15_c_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c15_u)
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_create("y", &c15_u, 6, 0U, 0U, 0U, 0), false);
  return c15_y;
}

static const mxArray *c15_c_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData)
{
  const mxArray *c15_mxArrayOutData = NULL;
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c15_mxArrayOutData = NULL;
  sf_mex_assign(&c15_mxArrayOutData, c15_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c15_inData), false);
  return c15_mxArrayOutData;
}

static int32_T c15_e_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_b_sfEvent, const char_T *c15_identifier)
{
  int32_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  c15_y = c15_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c15_b_sfEvent),
    &c15_thisId);
  sf_mex_destroy(&c15_b_sfEvent);
  return c15_y;
}

static int32_T c15_f_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  int32_T c15_y;
  int32_T c15_i0;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), &c15_i0, 1, 6, 0U, 0, 0U, 0);
  c15_y = c15_i0;
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static void c15_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData)
{
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c15_outData = c15_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_mxArrayInData), c15_varName);
  sf_mex_destroy(&c15_mxArrayInData);
}

static const mxArray *c15_d_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c15_u)
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_create("y", &c15_u, 3, 0U, 0U, 0U, 0), false);
  return c15_y;
}

static const mxArray *c15_d_sf_marshallOut(void *chartInstanceVoid, void
  *c15_inData)
{
  const mxArray *c15_mxArrayOutData = NULL;
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c15_mxArrayOutData = NULL;
  sf_mex_assign(&c15_mxArrayOutData, c15_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c15_inData), false);
  return c15_mxArrayOutData;
}

static uint8_T c15_g_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_b_tp_Mode1, const char_T *c15_identifier)
{
  uint8_T c15_y;
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  c15_y = c15_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c15_b_tp_Mode1),
    &c15_thisId);
  sf_mex_destroy(&c15_b_tp_Mode1);
  return c15_y;
}

static uint8_T c15_h_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  uint8_T c15_y;
  uint8_T c15_u0;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), &c15_u0, 1, 3, 0U, 0, 0U, 0);
  c15_y = c15_u0;
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static void c15_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c15_mxArrayInData, const char_T *c15_varName, void *c15_outData)
{
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c15_outData = c15_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_mxArrayInData), c15_varName);
  sf_mex_destroy(&c15_mxArrayInData);
}

static const mxArray *c15_e_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c15_y;
  int32_T c15_i1;
  boolean_T c15_bv0[2];
  c15_y = NULL;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_createcellmatrix(5, 1), false);
  sf_mex_setcell(c15_y, 0, c15_emlrt_marshallOut(chartInstance,
    *chartInstance->c15_I_in));
  sf_mex_setcell(c15_y, 1, c15_emlrt_marshallOut(chartInstance,
    *chartInstance->c15_V_a));
  sf_mex_setcell(c15_y, 2, c15_d_emlrt_marshallOut(chartInstance,
    chartInstance->c15_is_active_c15_Integrated_system_V06));
  sf_mex_setcell(c15_y, 3, c15_d_emlrt_marshallOut(chartInstance,
    chartInstance->c15_is_c15_Integrated_system_V06));
  for (c15_i1 = 0; c15_i1 < 2; c15_i1++) {
    c15_bv0[c15_i1] = chartInstance->c15_dataWrittenToVector[c15_i1];
  }

  sf_mex_setcell(c15_y, 4, c15_f_emlrt_marshallOut(chartInstance, c15_bv0));
  return c15_y;
}

static const mxArray *c15_f_emlrt_marshallOut
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c15_u[2])
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_create("y", c15_u, 11, 0U, 1U, 0U, 1, 2), false);
  return c15_y;
}

static void c15_i_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u)
{
  boolean_T c15_bv1[2];
  int32_T c15_i2;
  *chartInstance->c15_I_in = c15_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c15_u, 0)), "I_in");
  *chartInstance->c15_V_a = c15_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_a", c15_u, 1)), "V_a");
  chartInstance->c15_is_active_c15_Integrated_system_V06 =
    c15_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c15_Integrated_system_V06", c15_u, 2)),
    "is_active_c15_Integrated_system_V06");
  chartInstance->c15_is_c15_Integrated_system_V06 = c15_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c15_Integrated_system_V06",
       c15_u, 3)), "is_c15_Integrated_system_V06");
  c15_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c15_u, 4)), "dataWrittenToVector", c15_bv1);
  for (c15_i2 = 0; c15_i2 < 2; c15_i2++) {
    chartInstance->c15_dataWrittenToVector[c15_i2] = c15_bv1[c15_i2];
  }

  sf_mex_assign(&chartInstance->c15_setSimStateSideEffectsInfo,
                c15_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c15_u, 5)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c15_u);
}

static void c15_j_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_b_dataWrittenToVector, const char_T
  *c15_identifier, boolean_T c15_y[2])
{
  emlrtMsgIdentifier c15_thisId;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  c15_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c15_b_dataWrittenToVector),
    &c15_thisId, c15_y);
  sf_mex_destroy(&c15_b_dataWrittenToVector);
}

static void c15_k_emlrt_marshallIn(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c15_u, const emlrtMsgIdentifier *c15_parentId,
  boolean_T c15_y[2])
{
  boolean_T c15_bv2[2];
  int32_T c15_i3;
  (void)chartInstance;
  sf_mex_import(c15_parentId, sf_mex_dup(c15_u), c15_bv2, 1, 11, 0U, 1, 0U, 1, 2);
  for (c15_i3 = 0; c15_i3 < 2; c15_i3++) {
    c15_y[c15_i3] = c15_bv2[c15_i3];
  }

  sf_mex_destroy(&c15_u);
}

static const mxArray *c15_l_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_b_setSimStateSideEffectsInfo, const char_T *c15_identifier)
{
  const mxArray *c15_y = NULL;
  emlrtMsgIdentifier c15_thisId;
  c15_y = NULL;
  c15_thisId.fIdentifier = c15_identifier;
  c15_thisId.fParent = NULL;
  sf_mex_assign(&c15_y, c15_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c15_b_setSimStateSideEffectsInfo), &c15_thisId), false);
  sf_mex_destroy(&c15_b_setSimStateSideEffectsInfo);
  return c15_y;
}

static const mxArray *c15_m_emlrt_marshallIn
  (SFc15_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c15_u, const emlrtMsgIdentifier *c15_parentId)
{
  const mxArray *c15_y = NULL;
  (void)chartInstance;
  (void)c15_parentId;
  c15_y = NULL;
  sf_mex_assign(&c15_y, sf_mex_duplicatearraysafe(&c15_u), false);
  sf_mex_destroy(&c15_u);
  return c15_y;
}

static void init_dsm_address_info(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc15_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  chartInstance->c15_PWM_1 = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c15_PWM_2 = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c15_V_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c15_V_a = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c15_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c15_I_out = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c15_Integrated_system_V06_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(948796043U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4157722542U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(458212915U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2479239886U);
}

mxArray* sf_c15_Integrated_system_V06_get_post_codegen_info(void);
mxArray *sf_c15_Integrated_system_V06_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("kbr2ZhpWHZ1GooNuGxvICD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c15_Integrated_system_V06_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c15_Integrated_system_V06_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c15_Integrated_system_V06_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c15_Integrated_system_V06_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c15_Integrated_system_V06_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c15_Integrated_system_V06(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[35],T\"I_in\",},{M[1],M[28],T\"V_a\",},{M[8],M[0],T\"is_active_c15_Integrated_system_V06\",},{M[9],M[0],T\"is_c15_Integrated_system_V06\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c15_Integrated_system_V06_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc15_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V06MachineNumber_,
           15,
           4,
           9,
           0,
           6,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V06MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V06MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V06MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"PWM_1");
          _SFD_SET_DATA_PROPS(1,1,1,0,"PWM_2");
          _SFD_SET_DATA_PROPS(2,1,1,0,"V_in");
          _SFD_SET_DATA_PROPS(3,1,1,0,"I_out");
          _SFD_SET_DATA_PROPS(4,2,0,1,"V_a");
          _SFD_SET_DATA_PROPS(5,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_CH_SUBSTATE_COUNT(4);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
        }

        _SFD_CV_INIT_CHART(4,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(1,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(1,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(2,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(2,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(3,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(3,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(3,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(3,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(3,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(6,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(6,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(6,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(7,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(7,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(7,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(7,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(7,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(5,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(5,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(5,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(8,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(8,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(8,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(8,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(8,0,1,16,26,-1,0);
        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(4,0,0,1,26,1,26);

        {
          static int condStart[] = { 2, 16 };

          static int condEnd[] = { 12, 26 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(4,0,0,2,26,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(4,0,0,2,12,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(4,0,1,16,26,-1,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)c15_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c15_sf_marshallOut,(MexInFcnForType)c15_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V06MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc15_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c15_PWM_1);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c15_PWM_2);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c15_V_in);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c15_V_a);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c15_I_in);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c15_I_out);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "szPORTQDnc0oBwqEHxR6LYB";
}

static void sf_opaque_initialize_c15_Integrated_system_V06(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc15_Integrated_system_V06InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
  initialize_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c15_Integrated_system_V06(void *chartInstanceVar)
{
  enable_c15_Integrated_system_V06((SFc15_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c15_Integrated_system_V06(void *chartInstanceVar)
{
  disable_c15_Integrated_system_V06((SFc15_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c15_Integrated_system_V06(void
  *chartInstanceVar)
{
  zeroCrossings_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c15_Integrated_system_V06(void
  *chartInstanceVar)
{
  derivatives_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c15_Integrated_system_V06(void *chartInstanceVar)
{
  outputs_c15_Integrated_system_V06((SFc15_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c15_Integrated_system_V06(void *chartInstanceVar)
{
  sf_gateway_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c15_Integrated_system_V06
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c15_Integrated_system_V06(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c15_Integrated_system_V06(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc15_Integrated_system_V06InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V06_optimization_info();
    }

    finalize_c15_Integrated_system_V06
      ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc15_Integrated_system_V06
    ((SFc15_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c15_Integrated_system_V06(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c15_Integrated_system_V06
      ((SFc15_Integrated_system_V06InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c15_Integrated_system_V06(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V06_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      15);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,15,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,15);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,15,4);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,15,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 4; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,15);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1906047200U));
  ssSetChecksum1(S,(2030821920U));
  ssSetChecksum2(S,(2442127960U));
  ssSetChecksum3(S,(1390276512U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c15_Integrated_system_V06(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c15_Integrated_system_V06(SimStruct *S)
{
  SFc15_Integrated_system_V06InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc15_Integrated_system_V06InstanceStruct *)utMalloc(sizeof
    (SFc15_Integrated_system_V06InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc15_Integrated_system_V06InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c15_Integrated_system_V06;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c15_Integrated_system_V06;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c15_Integrated_system_V06;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c15_Integrated_system_V06;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c15_Integrated_system_V06;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c15_Integrated_system_V06;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c15_Integrated_system_V06;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c15_Integrated_system_V06;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c15_Integrated_system_V06;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c15_Integrated_system_V06;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c15_Integrated_system_V06;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c15_Integrated_system_V06;
  chartInstance->chartInfo.mdlStart = mdlStart_c15_Integrated_system_V06;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c15_Integrated_system_V06;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c15_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c15_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c15_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c15_Integrated_system_V06(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c15_Integrated_system_V06_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
