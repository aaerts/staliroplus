/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V06_sfun.h"
#include "c9_Integrated_system_V06.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V06_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c9_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c9_IN_Mode1                    ((uint8_T)1U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c9_debug_family_names[2] = { "nargin", "nargout" };

static const char * c9_b_debug_family_names[2] = { "nargin", "nargout" };

/* Function Declarations */
static void initialize_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void initialize_params_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void enable_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void disable_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void c9_update_debugger_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void set_sim_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c9_st);
static void c9_set_sim_state_side_effects_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void finalize_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void sf_gateway_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void mdl_start_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void zeroCrossings_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void derivatives_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void outputs_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void initSimStructsc9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void c9_eml_ini_fcn_to_be_inlined_126
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static void c9_eml_term_fcn_to_be_inlined_126
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static real_T c9_mrdivide(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, real_T c9_A, real_T c9_B);
static real_T c9_rdivide(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c9_x, real_T c9_y);
static void c9_isBuiltInNumeric(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance);
static void c9_eml_scalexp_compatible(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance);
static real_T c9_eml_div(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c9_x, real_T c9_y);
static real_T c9_div(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
                     real_T c9_x, real_T c9_y);
static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber);
static const mxArray *c9_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const real_T c9_u);
static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData);
static real_T c9_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_nargout, const char_T *c9_identifier);
static real_T c9_b_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_b_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c9_u);
static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static int32_T c9_c_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_sfEvent, const char_T *c9_identifier);
static int32_T c9_d_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_c_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c9_u);
static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData);
static uint8_T c9_e_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_tp_Mode1, const char_T *c9_identifier);
static uint8_T c9_f_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId);
static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData);
static const mxArray *c9_d_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *c9_e_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c9_u[4]);
static void c9_g_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u);
static void c9_h_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_dataWrittenToVector, const char_T
  *c9_identifier, boolean_T c9_y[4]);
static void c9_i_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId,
  boolean_T c9_y[4]);
static const mxArray *c9_j_emlrt_marshallIn
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c9_b_setSimStateSideEffectsInfo, const char_T *c9_identifier);
static const mxArray *c9_k_emlrt_marshallIn
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c9_u,
   const emlrtMsgIdentifier *c9_parentId);
static void init_dsm_address_info(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c9_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c9_doSetSimStateSideEffects = 0U;
  chartInstance->c9_setSimStateSideEffectsInfo = NULL;
  chartInstance->c9_tp_Mode1 = 0U;
  chartInstance->c9_is_active_c9_Integrated_system_V06 = 0U;
  chartInstance->c9_is_c9_Integrated_system_V06 = c9_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c9_update_debugger_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c9_prevAniVal;
  c9_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c9_is_active_c9_Integrated_system_V06 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 8U, chartInstance->c9_sfEvent);
  }

  if (chartInstance->c9_is_c9_Integrated_system_V06 == c9_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
  }

  _SFD_SET_ANIMATION(c9_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c9_st = NULL;
  c9_st = NULL;
  sf_mex_assign(&c9_st, c9_d_emlrt_marshallOut(chartInstance), false);
  return c9_st;
}

static void set_sim_state_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c9_st)
{
  c9_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_st));
  chartInstance->c9_doSetSimStateSideEffects = 1U;
  c9_update_debugger_state_c9_Integrated_system_V06(chartInstance);
  sf_mex_destroy(&c9_st);
}

static void c9_set_sim_state_side_effects_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (chartInstance->c9_doSetSimStateSideEffects != 0) {
    chartInstance->c9_tp_Mode1 = (uint8_T)
      (chartInstance->c9_is_c9_Integrated_system_V06 == c9_IN_Mode1);
    chartInstance->c9_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c9_setSimStateSideEffectsInfo);
}

static void sf_gateway_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  real_T c9_b_nargin = 0.0;
  real_T c9_b_nargout = 0.0;
  c9_set_sim_state_side_effects_c9_Integrated_system_V06(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c9_lastMajorTime = _sfTime_;
    chartInstance->c9_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c9_R, 1U, 1U, 0U,
                          chartInstance->c9_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c9_K, 0U, 1U, 0U,
                          chartInstance->c9_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c9_I_in, 4U, 1U, 0U,
                          chartInstance->c9_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c9_V_out, 3U, 1U, 0U,
                          chartInstance->c9_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c9_V_in, 2U, 1U, 0U,
                          chartInstance->c9_sfEvent, false);
    chartInstance->c9_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
    if (chartInstance->c9_is_active_c9_Integrated_system_V06 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
      chartInstance->c9_stateChanged = true;
      chartInstance->c9_is_active_c9_Integrated_system_V06 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_b_debug_family_names,
        c9_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
        c9_sf_marshallIn);
      chartInstance->c9_K = 2.0;
      chartInstance->c9_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c9_K, 0U, 5U, 0U,
                            chartInstance->c9_sfEvent, false);
      if (!chartInstance->c9_dataWrittenToVector[2U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 0U, chartInstance->c9_sfEvent,
          false);
      }

      sf_mex_printf("%s =\\n", "K");
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                        c9_emlrt_marshallOut(chartInstance, chartInstance->c9_K));
      chartInstance->c9_R = 50.0;
      chartInstance->c9_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c9_R, 1U, 5U, 0U,
                            chartInstance->c9_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c9_stateChanged = true;
      chartInstance->c9_is_c9_Integrated_system_V06 = c9_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c9_sfEvent);
      chartInstance->c9_tp_Mode1 = 1U;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                   chartInstance->c9_sfEvent);
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c9_sfEvent);
    if (chartInstance->c9_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_b_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  *chartInstance->c9_V_out = c9_mrdivide(chartInstance, *chartInstance->c9_V_in,
    chartInstance->c9_K);
  chartInstance->c9_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_V_out, 3U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  if (!chartInstance->c9_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  *chartInstance->c9_I_in = c9_mrdivide(chartInstance, *chartInstance->c9_V_in,
    chartInstance->c9_K * chartInstance->c9_R);
  chartInstance->c9_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_I_in, 4U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V06MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  real_T *c9_zcVar;
  c9_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c9_lastMajorTime == _sfTime_) {
    *c9_zcVar = -1.0;
  } else {
    chartInstance->c9_stateChanged = (boolean_T)0;
    if (chartInstance->c9_is_active_c9_Integrated_system_V06 == 0U) {
      chartInstance->c9_stateChanged = true;
    }

    if (chartInstance->c9_stateChanged) {
      *c9_zcVar = 1.0;
    } else {
      *c9_zcVar = -1.0;
    }
  }
}

static void derivatives_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  chartInstance->c9_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_V_out, 3U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  if (!chartInstance->c9_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  chartInstance->c9_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_I_in, 4U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
}

static void outputs_c9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c9_debug_family_var_map[2];
  real_T c9_nargin = 0.0;
  real_T c9_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c9_debug_family_names,
    c9_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargin, 0U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c9_nargout, 1U, c9_sf_marshallOut,
    c9_sf_marshallIn);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  *chartInstance->c9_V_out = c9_mrdivide(chartInstance, *chartInstance->c9_V_in,
    chartInstance->c9_K);
  chartInstance->c9_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_V_out, 3U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  if (!chartInstance->c9_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  if (!chartInstance->c9_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c9_sfEvent,
      false);
  }

  *chartInstance->c9_I_in = c9_mrdivide(chartInstance, *chartInstance->c9_V_in,
    chartInstance->c9_K * chartInstance->c9_R);
  chartInstance->c9_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c9_I_in, 4U, 4U, 0U,
                        chartInstance->c9_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c9_sfEvent);
}

static void initSimStructsc9_Integrated_system_V06
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c9_eml_ini_fcn_to_be_inlined_126
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c9_eml_term_fcn_to_be_inlined_126
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c9_mrdivide(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, real_T c9_A, real_T c9_B)
{
  return c9_rdivide(chartInstance, c9_A, c9_B);
}

static real_T c9_rdivide(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c9_x, real_T c9_y)
{
  return c9_eml_div(chartInstance, c9_x, c9_y);
}

static void c9_isBuiltInNumeric(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c9_eml_scalexp_compatible(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static real_T c9_eml_div(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c9_x, real_T c9_y)
{
  return c9_div(chartInstance, c9_x, c9_y);
}

static real_T c9_div(SFc9_Integrated_system_V06InstanceStruct *chartInstance,
                     real_T c9_x, real_T c9_y)
{
  (void)chartInstance;
  return c9_x / c9_y;
}

static void init_script_number_translation(uint32_T c9_machineNumber, uint32_T
  c9_chartNumber, uint32_T c9_instanceNumber)
{
  (void)c9_machineNumber;
  (void)c9_chartNumber;
  (void)c9_instanceNumber;
}

static const mxArray *c9_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const real_T c9_u)
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 0, 0U, 0U, 0U, 0), false);
  return c9_y;
}

static const mxArray *c9_sf_marshallOut(void *chartInstanceVoid, void *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  sf_mex_assign(&c9_mxArrayOutData, c9_emlrt_marshallOut(chartInstance, *(real_T
    *)c9_inData), false);
  return c9_mxArrayOutData;
}

static real_T c9_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_nargout, const char_T *c9_identifier)
{
  real_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_nargout), &c9_thisId);
  sf_mex_destroy(&c9_nargout);
  return c9_y;
}

static real_T c9_b_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  real_T c9_y;
  real_T c9_d0;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_d0, 1, 0, 0U, 0, 0U, 0);
  c9_y = c9_d0;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(real_T *)c9_outData = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_mxArrayInData), c9_varName);
  sf_mex_destroy(&c9_mxArrayInData);
}

const mxArray *sf_c9_Integrated_system_V06_get_eml_resolved_functions_info(void)
{
  const mxArray *c9_nameCaptureInfo = NULL;
  c9_nameCaptureInfo = NULL;
  sf_mex_assign(&c9_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c9_nameCaptureInfo;
}

static const mxArray *c9_b_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c9_u)
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 6, 0U, 0U, 0U, 0), false);
  return c9_y;
}

static const mxArray *c9_b_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  sf_mex_assign(&c9_mxArrayOutData, c9_b_emlrt_marshallOut(chartInstance,
    *(int32_T *)c9_inData), false);
  return c9_mxArrayOutData;
}

static int32_T c9_c_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_sfEvent, const char_T *c9_identifier)
{
  int32_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_sfEvent),
    &c9_thisId);
  sf_mex_destroy(&c9_b_sfEvent);
  return c9_y;
}

static int32_T c9_d_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  int32_T c9_y;
  int32_T c9_i0;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_i0, 1, 6, 0U, 0, 0U, 0);
  c9_y = c9_i0;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c9_outData = c9_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_mxArrayInData), c9_varName);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_c_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c9_u)
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", &c9_u, 3, 0U, 0U, 0U, 0), false);
  return c9_y;
}

static const mxArray *c9_c_sf_marshallOut(void *chartInstanceVoid, void
  *c9_inData)
{
  const mxArray *c9_mxArrayOutData = NULL;
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c9_mxArrayOutData = NULL;
  sf_mex_assign(&c9_mxArrayOutData, c9_c_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c9_inData), false);
  return c9_mxArrayOutData;
}

static uint8_T c9_e_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_tp_Mode1, const char_T *c9_identifier)
{
  uint8_T c9_y;
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_y = c9_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_tp_Mode1),
    &c9_thisId);
  sf_mex_destroy(&c9_b_tp_Mode1);
  return c9_y;
}

static uint8_T c9_f_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId)
{
  uint8_T c9_y;
  uint8_T c9_u0;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), &c9_u0, 1, 3, 0U, 0, 0U, 0);
  c9_y = c9_u0;
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void c9_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c9_mxArrayInData, const char_T *c9_varName, void *c9_outData)
{
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c9_outData = c9_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_mxArrayInData), c9_varName);
  sf_mex_destroy(&c9_mxArrayInData);
}

static const mxArray *c9_d_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c9_y;
  int32_T c9_i1;
  boolean_T c9_bv0[4];
  c9_y = NULL;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_createcellmatrix(7, 1), false);
  sf_mex_setcell(c9_y, 0, c9_emlrt_marshallOut(chartInstance,
    *chartInstance->c9_I_in));
  sf_mex_setcell(c9_y, 1, c9_emlrt_marshallOut(chartInstance,
    *chartInstance->c9_V_out));
  sf_mex_setcell(c9_y, 2, c9_emlrt_marshallOut(chartInstance,
    chartInstance->c9_K));
  sf_mex_setcell(c9_y, 3, c9_emlrt_marshallOut(chartInstance,
    chartInstance->c9_R));
  sf_mex_setcell(c9_y, 4, c9_c_emlrt_marshallOut(chartInstance,
    chartInstance->c9_is_active_c9_Integrated_system_V06));
  sf_mex_setcell(c9_y, 5, c9_c_emlrt_marshallOut(chartInstance,
    chartInstance->c9_is_c9_Integrated_system_V06));
  for (c9_i1 = 0; c9_i1 < 4; c9_i1++) {
    c9_bv0[c9_i1] = chartInstance->c9_dataWrittenToVector[c9_i1];
  }

  sf_mex_setcell(c9_y, 6, c9_e_emlrt_marshallOut(chartInstance, c9_bv0));
  return c9_y;
}

static const mxArray *c9_e_emlrt_marshallOut
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c9_u[4])
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_create("y", c9_u, 11, 0U, 1U, 0U, 1, 4), false);
  return c9_y;
}

static void c9_g_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u)
{
  boolean_T c9_bv1[4];
  int32_T c9_i2;
  *chartInstance->c9_I_in = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c9_u, 0)), "I_in");
  *chartInstance->c9_V_out = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_out", c9_u, 1)), "V_out");
  chartInstance->c9_K = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("K", c9_u, 2)), "K");
  chartInstance->c9_R = c9_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("R", c9_u, 3)), "R");
  chartInstance->c9_is_active_c9_Integrated_system_V06 = c9_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c9_Integrated_system_V06", c9_u, 4)),
     "is_active_c9_Integrated_system_V06");
  chartInstance->c9_is_c9_Integrated_system_V06 = c9_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c9_Integrated_system_V06",
       c9_u, 5)), "is_c9_Integrated_system_V06");
  c9_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c9_u, 6)), "dataWrittenToVector", c9_bv1);
  for (c9_i2 = 0; c9_i2 < 4; c9_i2++) {
    chartInstance->c9_dataWrittenToVector[c9_i2] = c9_bv1[c9_i2];
  }

  sf_mex_assign(&chartInstance->c9_setSimStateSideEffectsInfo,
                c9_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c9_u, 7)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c9_u);
}

static void c9_h_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_b_dataWrittenToVector, const char_T
  *c9_identifier, boolean_T c9_y[4])
{
  emlrtMsgIdentifier c9_thisId;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  c9_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c9_b_dataWrittenToVector),
                        &c9_thisId, c9_y);
  sf_mex_destroy(&c9_b_dataWrittenToVector);
}

static void c9_i_emlrt_marshallIn(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c9_u, const emlrtMsgIdentifier *c9_parentId,
  boolean_T c9_y[4])
{
  boolean_T c9_bv2[4];
  int32_T c9_i3;
  (void)chartInstance;
  sf_mex_import(c9_parentId, sf_mex_dup(c9_u), c9_bv2, 1, 11, 0U, 1, 0U, 1, 4);
  for (c9_i3 = 0; c9_i3 < 4; c9_i3++) {
    c9_y[c9_i3] = c9_bv2[c9_i3];
  }

  sf_mex_destroy(&c9_u);
}

static const mxArray *c9_j_emlrt_marshallIn
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c9_b_setSimStateSideEffectsInfo, const char_T *c9_identifier)
{
  const mxArray *c9_y = NULL;
  emlrtMsgIdentifier c9_thisId;
  c9_y = NULL;
  c9_thisId.fIdentifier = c9_identifier;
  c9_thisId.fParent = NULL;
  sf_mex_assign(&c9_y, c9_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c9_b_setSimStateSideEffectsInfo), &c9_thisId), false);
  sf_mex_destroy(&c9_b_setSimStateSideEffectsInfo);
  return c9_y;
}

static const mxArray *c9_k_emlrt_marshallIn
  (SFc9_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c9_u,
   const emlrtMsgIdentifier *c9_parentId)
{
  const mxArray *c9_y = NULL;
  (void)chartInstance;
  (void)c9_parentId;
  c9_y = NULL;
  sf_mex_assign(&c9_y, sf_mex_duplicatearraysafe(&c9_u), false);
  sf_mex_destroy(&c9_u);
  return c9_y;
}

static void init_dsm_address_info(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc9_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  chartInstance->c9_V_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c9_V_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c9_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c9_Integrated_system_V06_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2508146223U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2292862229U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1168474935U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(733904636U);
}

mxArray* sf_c9_Integrated_system_V06_get_post_codegen_info(void);
mxArray *sf_c9_Integrated_system_V06_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("JOTvO4rNqzJTlzqT6EdIdE");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c9_Integrated_system_V06_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c9_Integrated_system_V06_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c9_Integrated_system_V06_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c9_Integrated_system_V06_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c9_Integrated_system_V06_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c9_Integrated_system_V06(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[33],T\"I_in\",},{M[1],M[37],T\"V_out\",},{M[3],M[35],T\"K\",},{M[3],M[47],T\"R\",},{M[8],M[0],T\"is_active_c9_Integrated_system_V06\",},{M[9],M[0],T\"is_c9_Integrated_system_V06\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c9_Integrated_system_V06_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc9_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V06MachineNumber_,
           9,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V06MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V06MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V06MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"K");
          _SFD_SET_DATA_PROPS(1,0,0,0,"R");
          _SFD_SET_DATA_PROPS(2,1,1,0,"V_in");
          _SFD_SET_DATA_PROPS(3,2,0,1,"V_out");
          _SFD_SET_DATA_PROPS(4,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_CH_SUBSTATE_COUNT(1);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_ST_SUBSTATE_COUNT(0,0);
        }

        _SFD_CV_INIT_CHART(1,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)c9_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)c9_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)c9_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c9_sf_marshallOut,(MexInFcnForType)c9_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V06MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc9_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c9_V_in);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c9_V_out);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c9_I_in);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c9_K);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c9_R);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "spiPRlRYAZ6GJD8kFBHWefD";
}

static void sf_opaque_initialize_c9_Integrated_system_V06(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c9_Integrated_system_V06
    ((SFc9_Integrated_system_V06InstanceStruct*) chartInstanceVar);
  initialize_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c9_Integrated_system_V06(void *chartInstanceVar)
{
  enable_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c9_Integrated_system_V06(void *chartInstanceVar)
{
  disable_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c9_Integrated_system_V06(void
  *chartInstanceVar)
{
  zeroCrossings_c9_Integrated_system_V06
    ((SFc9_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c9_Integrated_system_V06(void
  *chartInstanceVar)
{
  derivatives_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_outputs_c9_Integrated_system_V06(void *chartInstanceVar)
{
  outputs_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c9_Integrated_system_V06(void *chartInstanceVar)
{
  sf_gateway_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c9_Integrated_system_V06(SimStruct*
  S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c9_Integrated_system_V06
    ((SFc9_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c9_Integrated_system_V06(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c9_Integrated_system_V06
    ((SFc9_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c9_Integrated_system_V06(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc9_Integrated_system_V06InstanceStruct*) chartInstanceVar)
      ->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V06_optimization_info();
    }

    finalize_c9_Integrated_system_V06((SFc9_Integrated_system_V06InstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc9_Integrated_system_V06
    ((SFc9_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c9_Integrated_system_V06(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c9_Integrated_system_V06
      ((SFc9_Integrated_system_V06InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c9_Integrated_system_V06(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V06_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,9);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,9,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,9);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,9,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,9,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,9);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2546893326U));
  ssSetChecksum1(S,(1252986062U));
  ssSetChecksum2(S,(529738588U));
  ssSetChecksum3(S,(2564137070U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c9_Integrated_system_V06(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c9_Integrated_system_V06(SimStruct *S)
{
  SFc9_Integrated_system_V06InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc9_Integrated_system_V06InstanceStruct *)utMalloc(sizeof
    (SFc9_Integrated_system_V06InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc9_Integrated_system_V06InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c9_Integrated_system_V06;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c9_Integrated_system_V06;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c9_Integrated_system_V06;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c9_Integrated_system_V06;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c9_Integrated_system_V06;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c9_Integrated_system_V06;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c9_Integrated_system_V06;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c9_Integrated_system_V06;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c9_Integrated_system_V06;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c9_Integrated_system_V06;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c9_Integrated_system_V06;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c9_Integrated_system_V06;
  chartInstance->chartInfo.mdlStart = mdlStart_c9_Integrated_system_V06;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c9_Integrated_system_V06;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c9_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c9_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c9_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c9_Integrated_system_V06(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c9_Integrated_system_V06_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
