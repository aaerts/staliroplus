#ifndef __c12_Integrated_system_V06_h__
#define __c12_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc12_Integrated_system_V06InstanceStruct
#define typedef_SFc12_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c12_sfEvent;
  uint8_T c12_tp_Mode1;
  uint8_T c12_tp_Mode2;
  boolean_T c12_isStable;
  boolean_T c12_stateChanged;
  real_T c12_lastMajorTime;
  uint8_T c12_is_active_c12_Integrated_system_V06;
  uint8_T c12_is_c12_Integrated_system_V06;
  boolean_T c12_dataWrittenToVector[4];
  uint8_T c12_doSetSimStateSideEffects;
  const mxArray *c12_setSimStateSideEffectsInfo;
  real_T *c12_V_out;
  real_T *c12_q;
  real_T *c12_I_out;
  real_T *c12_q_min;
  real_T *c12_q_out;
} SFc12_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc12_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c12_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c12_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c12_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
