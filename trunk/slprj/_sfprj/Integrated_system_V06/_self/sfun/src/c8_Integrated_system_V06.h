#ifndef __c8_Integrated_system_V06_h__
#define __c8_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc8_Integrated_system_V06InstanceStruct
#define typedef_SFc8_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c8_sfEvent;
  uint8_T c8_tp_Mode1;
  uint8_T c8_tp_Mode2;
  boolean_T c8_isStable;
  boolean_T c8_stateChanged;
  real_T c8_lastMajorTime;
  uint8_T c8_is_active_c8_Integrated_system_V06;
  uint8_T c8_is_c8_Integrated_system_V06;
  boolean_T c8_dataWrittenToVector[2];
  uint8_T c8_doSetSimStateSideEffects;
  const mxArray *c8_setSimStateSideEffectsInfo;
  real_T *c8_PWM;
  real_T *c8_V_C;
  real_T *c8_V_S;
  real_T *c8_I_in;
  real_T *c8_I_out;
} SFc8_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc8_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c8_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c8_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c8_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
