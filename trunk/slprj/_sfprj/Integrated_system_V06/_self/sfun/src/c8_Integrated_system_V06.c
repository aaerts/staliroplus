/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V06_sfun.h"
#include "c8_Integrated_system_V06.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V06_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c8_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c8_IN_Mode1                    ((uint8_T)1U)
#define c8_IN_Mode2                    ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c8_debug_family_names[2] = { "nargin", "nargout" };

static const char * c8_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c8_c_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c8_d_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c8_e_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c8_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void initialize_params_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void enable_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void disable_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void c8_update_debugger_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void set_sim_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c8_st);
static void c8_set_sim_state_side_effects_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void finalize_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void sf_gateway_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void mdl_start_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void zeroCrossings_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void derivatives_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void outputs_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void initSimStructsc8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void c8_eml_ini_fcn_to_be_inlined_115
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void c8_eml_term_fcn_to_be_inlined_115
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c8_machineNumber, uint32_T
  c8_chartNumber, uint32_T c8_instanceNumber);
static const mxArray *c8_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const real_T c8_u);
static const mxArray *c8_sf_marshallOut(void *chartInstanceVoid, void *c8_inData);
static real_T c8_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_nargout, const char_T *c8_identifier);
static real_T c8_b_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_b_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T c8_u);
static const mxArray *c8_b_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static boolean_T c8_c_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c8_sf_internal_predicateOutput, const char_T
  *c8_identifier);
static boolean_T c8_d_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_c_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c8_u);
static const mxArray *c8_c_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static int32_T c8_e_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_sfEvent, const char_T *c8_identifier);
static int32_T c8_f_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_d_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c8_u);
static const mxArray *c8_d_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData);
static uint8_T c8_g_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_tp_Mode1, const char_T *c8_identifier);
static uint8_T c8_h_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId);
static void c8_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData);
static const mxArray *c8_e_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *c8_f_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c8_u[2]);
static void c8_i_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u);
static void c8_j_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_dataWrittenToVector, const char_T
  *c8_identifier, boolean_T c8_y[2]);
static void c8_k_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId,
  boolean_T c8_y[2]);
static const mxArray *c8_l_emlrt_marshallIn
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c8_b_setSimStateSideEffectsInfo, const char_T *c8_identifier);
static const mxArray *c8_m_emlrt_marshallIn
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c8_u,
   const emlrtMsgIdentifier *c8_parentId);
static void init_dsm_address_info(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c8_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c8_doSetSimStateSideEffects = 0U;
  chartInstance->c8_setSimStateSideEffectsInfo = NULL;
  chartInstance->c8_tp_Mode1 = 0U;
  chartInstance->c8_tp_Mode2 = 0U;
  chartInstance->c8_is_active_c8_Integrated_system_V06 = 0U;
  chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c8_update_debugger_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c8_prevAniVal;
  c8_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c8_is_active_c8_Integrated_system_V06 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 7U, chartInstance->c8_sfEvent);
  }

  if (chartInstance->c8_is_c8_Integrated_system_V06 == c8_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
  }

  if (chartInstance->c8_is_c8_Integrated_system_V06 == c8_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c8_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c8_sfEvent);
  }

  _SFD_SET_ANIMATION(c8_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c8_st = NULL;
  c8_st = NULL;
  sf_mex_assign(&c8_st, c8_e_emlrt_marshallOut(chartInstance), false);
  return c8_st;
}

static void set_sim_state_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c8_st)
{
  c8_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_st));
  chartInstance->c8_doSetSimStateSideEffects = 1U;
  c8_update_debugger_state_c8_Integrated_system_V06(chartInstance);
  sf_mex_destroy(&c8_st);
}

static void c8_set_sim_state_side_effects_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (chartInstance->c8_doSetSimStateSideEffects != 0) {
    chartInstance->c8_tp_Mode1 = (uint8_T)
      (chartInstance->c8_is_c8_Integrated_system_V06 == c8_IN_Mode1);
    chartInstance->c8_tp_Mode2 = (uint8_T)
      (chartInstance->c8_is_c8_Integrated_system_V06 == c8_IN_Mode2);
    chartInstance->c8_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c8_setSimStateSideEffectsInfo);
}

static void sf_gateway_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c8_debug_family_var_map[3];
  real_T c8_nargin = 0.0;
  real_T c8_nargout = 1.0;
  boolean_T c8_out;
  real_T c8_b_nargin = 0.0;
  real_T c8_b_nargout = 1.0;
  boolean_T c8_b_out;
  uint32_T c8_b_debug_family_var_map[2];
  real_T c8_c_nargin = 0.0;
  real_T c8_c_nargout = 0.0;
  real_T c8_d_nargin = 0.0;
  real_T c8_d_nargout = 0.0;
  c8_set_sim_state_side_effects_c8_Integrated_system_V06(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c8_lastMajorTime = _sfTime_;
    chartInstance->c8_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 7U, chartInstance->c8_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_out, 2U, 1U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 1U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 1U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_C, 1U, 1U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_PWM, 0U, 1U, 0U,
                          chartInstance->c8_sfEvent, false);
    chartInstance->c8_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 7U, chartInstance->c8_sfEvent);
    if (chartInstance->c8_is_active_c8_Integrated_system_V06 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 7U, chartInstance->c8_sfEvent);
      chartInstance->c8_stateChanged = true;
      chartInstance->c8_is_active_c8_Integrated_system_V06 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c8_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
      chartInstance->c8_stateChanged = true;
      chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
      chartInstance->c8_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c8_is_c8_Integrated_system_V06) {
       case c8_IN_Mode1:
        CV_CHART_EVAL(7, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c8_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c8_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c8_c_debug_family_names,
          c8_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargin, 0U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargout, 1U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_out, 2U, c8_b_sf_marshallOut,
          c8_b_sf_marshallIn);
        c8_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c8_PWM, 1.0, -1, 5U, *chartInstance->c8_PWM >= 1.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c8_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c8_sfEvent);
          chartInstance->c8_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
          chartInstance->c8_stateChanged = true;
          chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c8_sfEvent);
          chartInstance->c8_tp_Mode2 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
        }
        break;

       case c8_IN_Mode2:
        CV_CHART_EVAL(7, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c8_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c8_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c8_d_debug_family_names,
          c8_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargin, 0U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargout, 1U,
          c8_sf_marshallOut, c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_out, 2U, c8_b_sf_marshallOut,
          c8_b_sf_marshallIn);
        c8_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c8_PWM, 0.0, -1, 3U, *chartInstance->c8_PWM <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c8_b_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c8_sfEvent);
          chartInstance->c8_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c8_sfEvent);
          chartInstance->c8_stateChanged = true;
          chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
          chartInstance->c8_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(7, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c8_sfEvent);
    if (chartInstance->c8_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c8_is_c8_Integrated_system_V06) {
   case c8_IN_Mode1:
    CV_CHART_EVAL(7, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_debug_family_names,
      c8_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_c_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_c_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    *chartInstance->c8_V_S = 0.0;
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    *chartInstance->c8_I_in = 0.0;
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    break;

   case c8_IN_Mode2:
    CV_CHART_EVAL(7, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_b_debug_family_names,
      c8_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_d_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_d_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    *chartInstance->c8_V_S = *chartInstance->c8_V_C;
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    *chartInstance->c8_I_in = *chartInstance->c8_I_out;
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    break;

   default:
    CV_CHART_EVAL(7, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V06MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c8_debug_family_var_map[3];
  real_T c8_nargin = 0.0;
  real_T c8_nargout = 1.0;
  boolean_T c8_out;
  real_T c8_b_nargin = 0.0;
  real_T c8_b_nargout = 1.0;
  boolean_T c8_b_out;
  real_T *c8_zcVar;
  c8_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c8_lastMajorTime == _sfTime_) {
    *c8_zcVar = -1.0;
  } else {
    chartInstance->c8_stateChanged = (boolean_T)0;
    if (chartInstance->c8_is_active_c8_Integrated_system_V06 == 0U) {
      chartInstance->c8_stateChanged = true;
    } else {
      switch (chartInstance->c8_is_c8_Integrated_system_V06) {
       case c8_IN_Mode1:
        CV_CHART_EVAL(7, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c8_c_debug_family_names,
          c8_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargin, 0U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargout, 1U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_out, 2U, c8_b_sf_marshallOut,
          c8_b_sf_marshallIn);
        c8_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c8_PWM, 1.0, -1, 5U, *chartInstance->c8_PWM >= 1.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c8_out) {
          chartInstance->c8_stateChanged = true;
        }
        break;

       case c8_IN_Mode2:
        CV_CHART_EVAL(7, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c8_d_debug_family_names,
          c8_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargin, 0U, c8_sf_marshallOut,
          c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargout, 1U,
          c8_sf_marshallOut, c8_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_out, 2U, c8_b_sf_marshallOut,
          c8_b_sf_marshallIn);
        c8_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c8_PWM, 0.0, -1, 3U, *chartInstance->c8_PWM <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c8_b_out) {
          chartInstance->c8_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(7, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c8_stateChanged) {
      *c8_zcVar = 1.0;
    } else {
      *c8_zcVar = -1.0;
    }
  }
}

static void derivatives_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c8_debug_family_var_map[2];
  real_T c8_nargin = 0.0;
  real_T c8_nargout = 0.0;
  real_T c8_b_nargin = 0.0;
  real_T c8_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c8_is_c8_Integrated_system_V06) {
   case c8_IN_Mode1:
    CV_CHART_EVAL(7, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_debug_family_names,
      c8_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    break;

   case c8_IN_Mode2:
    CV_CHART_EVAL(7, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_b_debug_family_names,
      c8_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    break;

   default:
    CV_CHART_EVAL(7, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
    break;
  }
}

static void outputs_c8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c8_debug_family_var_map[2];
  real_T c8_nargin = 0.0;
  real_T c8_nargout = 0.0;
  real_T c8_b_nargin = 0.0;
  real_T c8_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c8_is_c8_Integrated_system_V06) {
   case c8_IN_Mode1:
    CV_CHART_EVAL(7, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_debug_family_names,
      c8_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    *chartInstance->c8_V_S = 0.0;
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    *chartInstance->c8_I_in = 0.0;
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 0U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c8_sfEvent);
    break;

   case c8_IN_Mode2:
    CV_CHART_EVAL(7, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c8_b_debug_family_names,
      c8_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargin, 0U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c8_b_nargout, 1U, c8_sf_marshallOut,
      c8_sf_marshallIn);
    *chartInstance->c8_V_S = *chartInstance->c8_V_C;
    chartInstance->c8_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_V_S, 3U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    *chartInstance->c8_I_in = *chartInstance->c8_I_out;
    chartInstance->c8_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c8_I_in, 4U, 4U, 1U,
                          chartInstance->c8_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c8_sfEvent);
    break;

   default:
    CV_CHART_EVAL(7, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c8_is_c8_Integrated_system_V06 = c8_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c8_sfEvent);
    break;
  }
}

static void initSimStructsc8_Integrated_system_V06
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c8_eml_ini_fcn_to_be_inlined_115
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c8_eml_term_fcn_to_be_inlined_115
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c8_machineNumber, uint32_T
  c8_chartNumber, uint32_T c8_instanceNumber)
{
  (void)c8_machineNumber;
  (void)c8_chartNumber;
  (void)c8_instanceNumber;
}

static const mxArray *c8_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const real_T c8_u)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 0, 0U, 0U, 0U, 0), false);
  return c8_y;
}

static const mxArray *c8_sf_marshallOut(void *chartInstanceVoid, void *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  sf_mex_assign(&c8_mxArrayOutData, c8_emlrt_marshallOut(chartInstance, *(real_T
    *)c8_inData), false);
  return c8_mxArrayOutData;
}

static real_T c8_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_nargout, const char_T *c8_identifier)
{
  real_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_nargout), &c8_thisId);
  sf_mex_destroy(&c8_nargout);
  return c8_y;
}

static real_T c8_b_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  real_T c8_y;
  real_T c8_d0;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_d0, 1, 0, 0U, 0, 0U, 0);
  c8_y = c8_d0;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(real_T *)c8_outData = c8_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_mxArrayInData), c8_varName);
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_b_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T c8_u)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 11, 0U, 0U, 0U, 0), false);
  return c8_y;
}

static const mxArray *c8_b_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  sf_mex_assign(&c8_mxArrayOutData, c8_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c8_inData), false);
  return c8_mxArrayOutData;
}

static boolean_T c8_c_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c8_sf_internal_predicateOutput, const char_T
  *c8_identifier)
{
  boolean_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_sf_internal_predicateOutput), &c8_thisId);
  sf_mex_destroy(&c8_sf_internal_predicateOutput);
  return c8_y;
}

static boolean_T c8_d_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  boolean_T c8_y;
  boolean_T c8_b0;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_b0, 1, 11, 0U, 0, 0U, 0);
  c8_y = c8_b0;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c8_outData = c8_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_mxArrayInData), c8_varName);
  sf_mex_destroy(&c8_mxArrayInData);
}

const mxArray *sf_c8_Integrated_system_V06_get_eml_resolved_functions_info(void)
{
  const mxArray *c8_nameCaptureInfo = NULL;
  c8_nameCaptureInfo = NULL;
  sf_mex_assign(&c8_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c8_nameCaptureInfo;
}

static const mxArray *c8_c_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c8_u)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 6, 0U, 0U, 0U, 0), false);
  return c8_y;
}

static const mxArray *c8_c_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  sf_mex_assign(&c8_mxArrayOutData, c8_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c8_inData), false);
  return c8_mxArrayOutData;
}

static int32_T c8_e_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_sfEvent, const char_T *c8_identifier)
{
  int32_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_sfEvent),
    &c8_thisId);
  sf_mex_destroy(&c8_b_sfEvent);
  return c8_y;
}

static int32_T c8_f_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  int32_T c8_y;
  int32_T c8_i0;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_i0, 1, 6, 0U, 0, 0U, 0);
  c8_y = c8_i0;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c8_outData = c8_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_mxArrayInData), c8_varName);
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_d_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c8_u)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", &c8_u, 3, 0U, 0U, 0U, 0), false);
  return c8_y;
}

static const mxArray *c8_d_sf_marshallOut(void *chartInstanceVoid, void
  *c8_inData)
{
  const mxArray *c8_mxArrayOutData = NULL;
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c8_mxArrayOutData = NULL;
  sf_mex_assign(&c8_mxArrayOutData, c8_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c8_inData), false);
  return c8_mxArrayOutData;
}

static uint8_T c8_g_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_tp_Mode1, const char_T *c8_identifier)
{
  uint8_T c8_y;
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_y = c8_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_tp_Mode1),
    &c8_thisId);
  sf_mex_destroy(&c8_b_tp_Mode1);
  return c8_y;
}

static uint8_T c8_h_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId)
{
  uint8_T c8_y;
  uint8_T c8_u0;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), &c8_u0, 1, 3, 0U, 0, 0U, 0);
  c8_y = c8_u0;
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void c8_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c8_mxArrayInData, const char_T *c8_varName, void *c8_outData)
{
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c8_outData = c8_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_mxArrayInData), c8_varName);
  sf_mex_destroy(&c8_mxArrayInData);
}

static const mxArray *c8_e_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c8_y;
  int32_T c8_i1;
  boolean_T c8_bv0[2];
  c8_y = NULL;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_createcellmatrix(5, 1), false);
  sf_mex_setcell(c8_y, 0, c8_emlrt_marshallOut(chartInstance,
    *chartInstance->c8_I_in));
  sf_mex_setcell(c8_y, 1, c8_emlrt_marshallOut(chartInstance,
    *chartInstance->c8_V_S));
  sf_mex_setcell(c8_y, 2, c8_d_emlrt_marshallOut(chartInstance,
    chartInstance->c8_is_active_c8_Integrated_system_V06));
  sf_mex_setcell(c8_y, 3, c8_d_emlrt_marshallOut(chartInstance,
    chartInstance->c8_is_c8_Integrated_system_V06));
  for (c8_i1 = 0; c8_i1 < 2; c8_i1++) {
    c8_bv0[c8_i1] = chartInstance->c8_dataWrittenToVector[c8_i1];
  }

  sf_mex_setcell(c8_y, 4, c8_f_emlrt_marshallOut(chartInstance, c8_bv0));
  return c8_y;
}

static const mxArray *c8_f_emlrt_marshallOut
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c8_u[2])
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_create("y", c8_u, 11, 0U, 1U, 0U, 1, 2), false);
  return c8_y;
}

static void c8_i_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u)
{
  boolean_T c8_bv1[2];
  int32_T c8_i2;
  *chartInstance->c8_I_in = c8_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c8_u, 0)), "I_in");
  *chartInstance->c8_V_S = c8_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_S", c8_u, 1)), "V_S");
  chartInstance->c8_is_active_c8_Integrated_system_V06 = c8_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c8_Integrated_system_V06", c8_u, 2)),
     "is_active_c8_Integrated_system_V06");
  chartInstance->c8_is_c8_Integrated_system_V06 = c8_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c8_Integrated_system_V06",
       c8_u, 3)), "is_c8_Integrated_system_V06");
  c8_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c8_u, 4)), "dataWrittenToVector", c8_bv1);
  for (c8_i2 = 0; c8_i2 < 2; c8_i2++) {
    chartInstance->c8_dataWrittenToVector[c8_i2] = c8_bv1[c8_i2];
  }

  sf_mex_assign(&chartInstance->c8_setSimStateSideEffectsInfo,
                c8_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c8_u, 5)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c8_u);
}

static void c8_j_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_b_dataWrittenToVector, const char_T
  *c8_identifier, boolean_T c8_y[2])
{
  emlrtMsgIdentifier c8_thisId;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  c8_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c8_b_dataWrittenToVector),
                        &c8_thisId, c8_y);
  sf_mex_destroy(&c8_b_dataWrittenToVector);
}

static void c8_k_emlrt_marshallIn(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c8_u, const emlrtMsgIdentifier *c8_parentId,
  boolean_T c8_y[2])
{
  boolean_T c8_bv2[2];
  int32_T c8_i3;
  (void)chartInstance;
  sf_mex_import(c8_parentId, sf_mex_dup(c8_u), c8_bv2, 1, 11, 0U, 1, 0U, 1, 2);
  for (c8_i3 = 0; c8_i3 < 2; c8_i3++) {
    c8_y[c8_i3] = c8_bv2[c8_i3];
  }

  sf_mex_destroy(&c8_u);
}

static const mxArray *c8_l_emlrt_marshallIn
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c8_b_setSimStateSideEffectsInfo, const char_T *c8_identifier)
{
  const mxArray *c8_y = NULL;
  emlrtMsgIdentifier c8_thisId;
  c8_y = NULL;
  c8_thisId.fIdentifier = c8_identifier;
  c8_thisId.fParent = NULL;
  sf_mex_assign(&c8_y, c8_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c8_b_setSimStateSideEffectsInfo), &c8_thisId), false);
  sf_mex_destroy(&c8_b_setSimStateSideEffectsInfo);
  return c8_y;
}

static const mxArray *c8_m_emlrt_marshallIn
  (SFc8_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c8_u,
   const emlrtMsgIdentifier *c8_parentId)
{
  const mxArray *c8_y = NULL;
  (void)chartInstance;
  (void)c8_parentId;
  c8_y = NULL;
  sf_mex_assign(&c8_y, sf_mex_duplicatearraysafe(&c8_u), false);
  sf_mex_destroy(&c8_u);
  return c8_y;
}

static void init_dsm_address_info(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc8_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  chartInstance->c8_PWM = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c8_V_C = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_V_S = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c8_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c8_I_out = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c8_Integrated_system_V06_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2090317647U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3065905880U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2177892711U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(481870227U);
}

mxArray* sf_c8_Integrated_system_V06_get_post_codegen_info(void);
mxArray *sf_c8_Integrated_system_V06_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("1NMY9ee5UvWB5YEt3NHP3");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c8_Integrated_system_V06_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c8_Integrated_system_V06_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c8_Integrated_system_V06_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c8_Integrated_system_V06_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c8_Integrated_system_V06_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c8_Integrated_system_V06(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[35],T\"I_in\",},{M[1],M[28],T\"V_S\",},{M[8],M[0],T\"is_active_c8_Integrated_system_V06\",},{M[9],M[0],T\"is_c8_Integrated_system_V06\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c8_Integrated_system_V06_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc8_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V06MachineNumber_,
           8,
           2,
           3,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V06MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V06MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V06MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"PWM");
          _SFD_SET_DATA_PROPS(1,1,1,0,"V_C");
          _SFD_SET_DATA_PROPS(2,1,1,0,"I_out");
          _SFD_SET_DATA_PROPS(3,2,0,1,"V_S");
          _SFD_SET_DATA_PROPS(4,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,2,10,-1,5);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,11,1,11);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,3,11,-1,3);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_sf_marshallOut,(MexInFcnForType)c8_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c8_sf_marshallOut,(MexInFcnForType)c8_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V06MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc8_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c8_PWM);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c8_V_C);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c8_V_S);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c8_I_in);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c8_I_out);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "s217vIBoC5B3DEwFE0BsnqG";
}

static void sf_opaque_initialize_c8_Integrated_system_V06(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c8_Integrated_system_V06
    ((SFc8_Integrated_system_V06InstanceStruct*) chartInstanceVar);
  initialize_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c8_Integrated_system_V06(void *chartInstanceVar)
{
  enable_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c8_Integrated_system_V06(void *chartInstanceVar)
{
  disable_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c8_Integrated_system_V06(void
  *chartInstanceVar)
{
  zeroCrossings_c8_Integrated_system_V06
    ((SFc8_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c8_Integrated_system_V06(void
  *chartInstanceVar)
{
  derivatives_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_outputs_c8_Integrated_system_V06(void *chartInstanceVar)
{
  outputs_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c8_Integrated_system_V06(void *chartInstanceVar)
{
  sf_gateway_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c8_Integrated_system_V06(SimStruct*
  S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c8_Integrated_system_V06
    ((SFc8_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c8_Integrated_system_V06(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c8_Integrated_system_V06
    ((SFc8_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c8_Integrated_system_V06(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc8_Integrated_system_V06InstanceStruct*) chartInstanceVar)
      ->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V06_optimization_info();
    }

    finalize_c8_Integrated_system_V06((SFc8_Integrated_system_V06InstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc8_Integrated_system_V06
    ((SFc8_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c8_Integrated_system_V06(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c8_Integrated_system_V06
      ((SFc8_Integrated_system_V06InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c8_Integrated_system_V06(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V06_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,8);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,8,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,8);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,8,3);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,8,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 3; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,8);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(70238077U));
  ssSetChecksum1(S,(401376423U));
  ssSetChecksum2(S,(1874699001U));
  ssSetChecksum3(S,(619758477U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c8_Integrated_system_V06(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c8_Integrated_system_V06(SimStruct *S)
{
  SFc8_Integrated_system_V06InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc8_Integrated_system_V06InstanceStruct *)utMalloc(sizeof
    (SFc8_Integrated_system_V06InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc8_Integrated_system_V06InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c8_Integrated_system_V06;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c8_Integrated_system_V06;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c8_Integrated_system_V06;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c8_Integrated_system_V06;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c8_Integrated_system_V06;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c8_Integrated_system_V06;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c8_Integrated_system_V06;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c8_Integrated_system_V06;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c8_Integrated_system_V06;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c8_Integrated_system_V06;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c8_Integrated_system_V06;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c8_Integrated_system_V06;
  chartInstance->chartInfo.mdlStart = mdlStart_c8_Integrated_system_V06;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c8_Integrated_system_V06;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c8_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c8_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c8_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c8_Integrated_system_V06(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c8_Integrated_system_V06_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
