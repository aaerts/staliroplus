/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V06_sfun.h"
#include "c6_Integrated_system_V06.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V06_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c6_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c6_IN_Mode1                    ((uint8_T)1U)
#define c6_IN_Mode2                    ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c6_debug_family_names[2] = { "nargin", "nargout" };

static const char * c6_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c6_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c6_d_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c6_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c6_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c6_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c6_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void initialize_params_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void enable_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void disable_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void c6_update_debugger_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void set_sim_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c6_st);
static void c6_set_sim_state_side_effects_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void finalize_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void sf_gateway_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void mdl_start_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void zeroCrossings_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void derivatives_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void outputs_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void initSimStructsc6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void c6_eml_ini_fcn_to_be_inlined_91
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static void c6_eml_term_fcn_to_be_inlined_91
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static real_T c6_mrdivide(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, real_T c6_A, real_T c6_B);
static real_T c6_rdivide(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c6_x, real_T c6_y);
static void c6_isBuiltInNumeric(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance);
static void c6_eml_scalexp_compatible(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance);
static real_T c6_eml_div(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c6_x, real_T c6_y);
static real_T c6_div(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
                     real_T c6_x, real_T c6_y);
static void init_script_number_translation(uint32_T c6_machineNumber, uint32_T
  c6_chartNumber, uint32_T c6_instanceNumber);
static const mxArray *c6_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const real_T c6_u);
static const mxArray *c6_sf_marshallOut(void *chartInstanceVoid, void *c6_inData);
static real_T c6_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_nargout, const char_T *c6_identifier);
static real_T c6_b_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId);
static void c6_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData);
static const mxArray *c6_b_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T c6_u);
static const mxArray *c6_b_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData);
static boolean_T c6_c_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c6_sf_internal_predicateOutput, const char_T
  *c6_identifier);
static boolean_T c6_d_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId);
static void c6_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData);
static const mxArray *c6_c_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c6_u);
static const mxArray *c6_c_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData);
static int32_T c6_e_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_sfEvent, const char_T *c6_identifier);
static int32_T c6_f_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId);
static void c6_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData);
static const mxArray *c6_d_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c6_u);
static const mxArray *c6_d_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData);
static uint8_T c6_g_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_tp_Mode1, const char_T *c6_identifier);
static uint8_T c6_h_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId);
static void c6_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData);
static const mxArray *c6_e_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance);
static const mxArray *c6_f_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c6_u[4]);
static void c6_i_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u);
static void c6_j_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_dataWrittenToVector, const char_T
  *c6_identifier, boolean_T c6_y[4]);
static void c6_k_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId,
  boolean_T c6_y[4]);
static const mxArray *c6_l_emlrt_marshallIn
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c6_b_setSimStateSideEffectsInfo, const char_T *c6_identifier);
static const mxArray *c6_m_emlrt_marshallIn
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c6_u,
   const emlrtMsgIdentifier *c6_parentId);
static void init_dsm_address_info(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c6_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c6_doSetSimStateSideEffects = 0U;
  chartInstance->c6_setSimStateSideEffectsInfo = NULL;
  chartInstance->c6_tp_Mode1 = 0U;
  chartInstance->c6_tp_Mode2 = 0U;
  chartInstance->c6_is_active_c6_Integrated_system_V06 = 0U;
  chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c6_update_debugger_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c6_prevAniVal;
  c6_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c6_is_active_c6_Integrated_system_V06 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 5U, chartInstance->c6_sfEvent);
  }

  if (chartInstance->c6_is_c6_Integrated_system_V06 == c6_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
  }

  if (chartInstance->c6_is_c6_Integrated_system_V06 == c6_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c6_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c6_sfEvent);
  }

  _SFD_SET_ANIMATION(c6_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c6_st = NULL;
  c6_st = NULL;
  sf_mex_assign(&c6_st, c6_e_emlrt_marshallOut(chartInstance), false);
  return c6_st;
}

static void set_sim_state_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c6_st)
{
  c6_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c6_st));
  chartInstance->c6_doSetSimStateSideEffects = 1U;
  c6_update_debugger_state_c6_Integrated_system_V06(chartInstance);
  sf_mex_destroy(&c6_st);
}

static void c6_set_sim_state_side_effects_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  if (chartInstance->c6_doSetSimStateSideEffects != 0) {
    chartInstance->c6_tp_Mode1 = (uint8_T)
      (chartInstance->c6_is_c6_Integrated_system_V06 == c6_IN_Mode1);
    chartInstance->c6_tp_Mode2 = (uint8_T)
      (chartInstance->c6_is_c6_Integrated_system_V06 == c6_IN_Mode2);
    chartInstance->c6_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c6_setSimStateSideEffectsInfo);
}

static void sf_gateway_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c6_debug_family_var_map[2];
  real_T c6_nargin = 0.0;
  real_T c6_nargout = 0.0;
  uint32_T c6_b_debug_family_var_map[3];
  real_T c6_b_nargin = 0.0;
  real_T c6_b_nargout = 1.0;
  boolean_T c6_out;
  real_T c6_c_nargin = 0.0;
  real_T c6_c_nargout = 0.0;
  real_T c6_d_nargin = 0.0;
  real_T c6_d_nargout = 1.0;
  boolean_T c6_b_out;
  real_T c6_e_nargin = 0.0;
  real_T c6_e_nargout = 0.0;
  real_T c6_f_nargin = 0.0;
  real_T c6_f_nargout = 0.0;
  c6_set_sim_state_side_effects_c6_Integrated_system_V06(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c6_lastMajorTime = _sfTime_;
    chartInstance->c6_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 5U, chartInstance->c6_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c6_C, 0U, 1U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_q, 2U, 1U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 1U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c6_R, 1U, 1U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_I_in, 3U, 1U, 0U,
                          chartInstance->c6_sfEvent, false);
    chartInstance->c6_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c6_sfEvent);
    if (chartInstance->c6_is_active_c6_Integrated_system_V06 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 5U, chartInstance->c6_sfEvent);
      chartInstance->c6_stateChanged = true;
      chartInstance->c6_is_active_c6_Integrated_system_V06 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c6_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_c_debug_family_names,
        c6_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargin, 0U, c6_sf_marshallOut,
        c6_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargout, 1U, c6_sf_marshallOut,
        c6_sf_marshallIn);
      *chartInstance->c6_q = 1.128;
      chartInstance->c6_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c6_q, 2U, 5U, 0U,
                            chartInstance->c6_sfEvent, false);
      chartInstance->c6_R = 1.0E+9;
      chartInstance->c6_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c6_R, 1U, 5U, 0U,
                            chartInstance->c6_sfEvent, false);
      chartInstance->c6_C = 0.0047;
      chartInstance->c6_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c6_C, 0U, 5U, 0U,
                            chartInstance->c6_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c6_stateChanged = true;
      chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
      chartInstance->c6_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c6_is_c6_Integrated_system_V06) {
       case c6_IN_Mode1:
        CV_CHART_EVAL(5, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c6_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c6_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c6_d_debug_family_names,
          c6_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargin, 0U, c6_sf_marshallOut,
          c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargout, 1U,
          c6_sf_marshallOut, c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_out, 2U, c6_b_sf_marshallOut,
          c6_b_sf_marshallIn);
        if (!chartInstance->c6_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 1U,
            chartInstance->c6_sfEvent, false);
        }

        c6_out = CV_EML_IF(1, 0, 0, *chartInstance->c6_q <= 0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c6_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c6_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_e_debug_family_names,
            c6_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_c_nargin, 0U,
            c6_sf_marshallOut, c6_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_c_nargout, 1U,
            c6_sf_marshallOut, c6_sf_marshallIn);
          *chartInstance->c6_q = 0.0;
          chartInstance->c6_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c6_q, 2U, 5U, 1U,
                                chartInstance->c6_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c6_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
          chartInstance->c6_stateChanged = true;
          chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c6_sfEvent);
          chartInstance->c6_tp_Mode2 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
        }
        break;

       case c6_IN_Mode2:
        CV_CHART_EVAL(5, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c6_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c6_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c6_f_debug_family_names,
          c6_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_d_nargin, 0U, c6_sf_marshallOut,
          c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_d_nargout, 1U,
          c6_sf_marshallOut, c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_out, 2U, c6_b_sf_marshallOut,
          c6_b_sf_marshallIn);
        if (!chartInstance->c6_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c6_sfEvent, false);
        }

        c6_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c6_q, 0.0, -1, 4U, *chartInstance->c6_q > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c6_b_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c6_sfEvent);
          chartInstance->c6_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c6_sfEvent);
          chartInstance->c6_stateChanged = true;
          chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
          chartInstance->c6_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(5, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c6_sfEvent);
    if (chartInstance->c6_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c6_is_c6_Integrated_system_V06) {
   case c6_IN_Mode1:
    CV_CHART_EVAL(5, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_e_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_e_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    *chartInstance->c6_V_C = c6_mrdivide(chartInstance, *chartInstance->c6_q,
      chartInstance->c6_C);
    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    break;

   case c6_IN_Mode2:
    CV_CHART_EVAL(5, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_b_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_f_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_f_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    *chartInstance->c6_V_C = 0.0;
    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 1U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    break;

   default:
    CV_CHART_EVAL(5, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V06MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c6_debug_family_var_map[3];
  real_T c6_nargin = 0.0;
  real_T c6_nargout = 1.0;
  boolean_T c6_out;
  real_T c6_b_nargin = 0.0;
  real_T c6_b_nargout = 1.0;
  boolean_T c6_b_out;
  real_T *c6_zcVar;
  c6_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c6_lastMajorTime == _sfTime_) {
    *c6_zcVar = -1.0;
  } else {
    chartInstance->c6_stateChanged = (boolean_T)0;
    if (chartInstance->c6_is_active_c6_Integrated_system_V06 == 0U) {
      chartInstance->c6_stateChanged = true;
    } else {
      switch (chartInstance->c6_is_c6_Integrated_system_V06) {
       case c6_IN_Mode1:
        CV_CHART_EVAL(5, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c6_d_debug_family_names,
          c6_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargin, 0U, c6_sf_marshallOut,
          c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargout, 1U, c6_sf_marshallOut,
          c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_out, 2U, c6_b_sf_marshallOut,
          c6_b_sf_marshallIn);
        if (!chartInstance->c6_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 1U,
            chartInstance->c6_sfEvent, false);
        }

        c6_out = CV_EML_IF(1, 0, 0, *chartInstance->c6_q <= 0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c6_out) {
          chartInstance->c6_stateChanged = true;
        }
        break;

       case c6_IN_Mode2:
        CV_CHART_EVAL(5, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c6_f_debug_family_names,
          c6_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargin, 0U, c6_sf_marshallOut,
          c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargout, 1U,
          c6_sf_marshallOut, c6_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_out, 2U, c6_b_sf_marshallOut,
          c6_b_sf_marshallIn);
        if (!chartInstance->c6_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c6_sfEvent, false);
        }

        c6_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c6_q, 0.0, -1, 4U, *chartInstance->c6_q > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c6_b_out) {
          chartInstance->c6_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(5, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c6_stateChanged) {
      *c6_zcVar = 1.0;
    } else {
      *c6_zcVar = -1.0;
    }
  }
}

static void derivatives_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c6_debug_family_var_map[2];
  real_T c6_nargin = 0.0;
  real_T c6_nargout = 0.0;
  real_T c6_b_nargin = 0.0;
  real_T c6_b_nargout = 0.0;
  real_T *c6_q_dot;
  c6_q_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 0);
  *c6_q_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c6_q_dot, 2U, 1U, 5U, chartInstance->c6_sfEvent, false);
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c6_is_c6_Integrated_system_V06) {
   case c6_IN_Mode1:
    CV_CHART_EVAL(5, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    *c6_q_dot = *chartInstance->c6_I_in - c6_mrdivide(chartInstance,
      *chartInstance->c6_q, chartInstance->c6_R * chartInstance->c6_C);
    _SFD_DATA_RANGE_CHECK(*c6_q_dot, 2U, 4U, 0U, chartInstance->c6_sfEvent,
                          false);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    break;

   case c6_IN_Mode2:
    CV_CHART_EVAL(5, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_b_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    *c6_q_dot = *chartInstance->c6_I_in - c6_mrdivide(chartInstance,
      *chartInstance->c6_q, chartInstance->c6_R * chartInstance->c6_C);
    _SFD_DATA_RANGE_CHECK(*c6_q_dot, 2U, 4U, 1U, chartInstance->c6_sfEvent,
                          false);
    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 1U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    break;

   default:
    CV_CHART_EVAL(5, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
    break;
  }
}

static void outputs_c6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  uint32_T c6_debug_family_var_map[2];
  real_T c6_nargin = 0.0;
  real_T c6_nargout = 0.0;
  real_T c6_b_nargin = 0.0;
  real_T c6_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c6_is_c6_Integrated_system_V06) {
   case c6_IN_Mode1:
    CV_CHART_EVAL(5, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c6_sfEvent,
        false);
    }

    *chartInstance->c6_V_C = c6_mrdivide(chartInstance, *chartInstance->c6_q,
      chartInstance->c6_C);
    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 0U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c6_sfEvent);
    break;

   case c6_IN_Mode2:
    CV_CHART_EVAL(5, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c6_b_debug_family_names,
      c6_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargin, 0U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c6_b_nargout, 1U, c6_sf_marshallOut,
      c6_sf_marshallIn);
    if (!chartInstance->c6_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    if (!chartInstance->c6_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c6_sfEvent,
        false);
    }

    *chartInstance->c6_V_C = 0.0;
    chartInstance->c6_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c6_V_C, 4U, 4U, 1U,
                          chartInstance->c6_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c6_sfEvent);
    break;

   default:
    CV_CHART_EVAL(5, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c6_is_c6_Integrated_system_V06 = c6_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c6_sfEvent);
    break;
  }
}

static void initSimStructsc6_Integrated_system_V06
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c6_eml_ini_fcn_to_be_inlined_91
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c6_eml_term_fcn_to_be_inlined_91
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c6_mrdivide(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, real_T c6_A, real_T c6_B)
{
  return c6_rdivide(chartInstance, c6_A, c6_B);
}

static real_T c6_rdivide(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c6_x, real_T c6_y)
{
  return c6_eml_div(chartInstance, c6_x, c6_y);
}

static void c6_isBuiltInNumeric(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c6_eml_scalexp_compatible(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static real_T c6_eml_div(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
  real_T c6_x, real_T c6_y)
{
  return c6_div(chartInstance, c6_x, c6_y);
}

static real_T c6_div(SFc6_Integrated_system_V06InstanceStruct *chartInstance,
                     real_T c6_x, real_T c6_y)
{
  (void)chartInstance;
  return c6_x / c6_y;
}

static void init_script_number_translation(uint32_T c6_machineNumber, uint32_T
  c6_chartNumber, uint32_T c6_instanceNumber)
{
  (void)c6_machineNumber;
  (void)c6_chartNumber;
  (void)c6_instanceNumber;
}

static const mxArray *c6_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const real_T c6_u)
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_create("y", &c6_u, 0, 0U, 0U, 0U, 0), false);
  return c6_y;
}

static const mxArray *c6_sf_marshallOut(void *chartInstanceVoid, void *c6_inData)
{
  const mxArray *c6_mxArrayOutData = NULL;
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c6_mxArrayOutData = NULL;
  sf_mex_assign(&c6_mxArrayOutData, c6_emlrt_marshallOut(chartInstance, *(real_T
    *)c6_inData), false);
  return c6_mxArrayOutData;
}

static real_T c6_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_nargout, const char_T *c6_identifier)
{
  real_T c6_y;
  emlrtMsgIdentifier c6_thisId;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  c6_y = c6_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c6_nargout), &c6_thisId);
  sf_mex_destroy(&c6_nargout);
  return c6_y;
}

static real_T c6_b_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId)
{
  real_T c6_y;
  real_T c6_d0;
  (void)chartInstance;
  sf_mex_import(c6_parentId, sf_mex_dup(c6_u), &c6_d0, 1, 0, 0U, 0, 0U, 0);
  c6_y = c6_d0;
  sf_mex_destroy(&c6_u);
  return c6_y;
}

static void c6_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData)
{
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(real_T *)c6_outData = c6_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_mxArrayInData), c6_varName);
  sf_mex_destroy(&c6_mxArrayInData);
}

static const mxArray *c6_b_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T c6_u)
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_create("y", &c6_u, 11, 0U, 0U, 0U, 0), false);
  return c6_y;
}

static const mxArray *c6_b_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData)
{
  const mxArray *c6_mxArrayOutData = NULL;
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c6_mxArrayOutData = NULL;
  sf_mex_assign(&c6_mxArrayOutData, c6_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c6_inData), false);
  return c6_mxArrayOutData;
}

static boolean_T c6_c_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c6_sf_internal_predicateOutput, const char_T
  *c6_identifier)
{
  boolean_T c6_y;
  emlrtMsgIdentifier c6_thisId;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  c6_y = c6_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_sf_internal_predicateOutput), &c6_thisId);
  sf_mex_destroy(&c6_sf_internal_predicateOutput);
  return c6_y;
}

static boolean_T c6_d_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct *
  chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId)
{
  boolean_T c6_y;
  boolean_T c6_b0;
  (void)chartInstance;
  sf_mex_import(c6_parentId, sf_mex_dup(c6_u), &c6_b0, 1, 11, 0U, 0, 0U, 0);
  c6_y = c6_b0;
  sf_mex_destroy(&c6_u);
  return c6_y;
}

static void c6_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData)
{
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c6_outData = c6_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_mxArrayInData), c6_varName);
  sf_mex_destroy(&c6_mxArrayInData);
}

const mxArray *sf_c6_Integrated_system_V06_get_eml_resolved_functions_info(void)
{
  const mxArray *c6_nameCaptureInfo = NULL;
  c6_nameCaptureInfo = NULL;
  sf_mex_assign(&c6_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c6_nameCaptureInfo;
}

static const mxArray *c6_c_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const int32_T c6_u)
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_create("y", &c6_u, 6, 0U, 0U, 0U, 0), false);
  return c6_y;
}

static const mxArray *c6_c_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData)
{
  const mxArray *c6_mxArrayOutData = NULL;
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c6_mxArrayOutData = NULL;
  sf_mex_assign(&c6_mxArrayOutData, c6_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c6_inData), false);
  return c6_mxArrayOutData;
}

static int32_T c6_e_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_sfEvent, const char_T *c6_identifier)
{
  int32_T c6_y;
  emlrtMsgIdentifier c6_thisId;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  c6_y = c6_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c6_b_sfEvent),
    &c6_thisId);
  sf_mex_destroy(&c6_b_sfEvent);
  return c6_y;
}

static int32_T c6_f_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId)
{
  int32_T c6_y;
  int32_T c6_i0;
  (void)chartInstance;
  sf_mex_import(c6_parentId, sf_mex_dup(c6_u), &c6_i0, 1, 6, 0U, 0, 0U, 0);
  c6_y = c6_i0;
  sf_mex_destroy(&c6_u);
  return c6_y;
}

static void c6_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData)
{
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c6_outData = c6_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_mxArrayInData), c6_varName);
  sf_mex_destroy(&c6_mxArrayInData);
}

static const mxArray *c6_d_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const uint8_T c6_u)
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_create("y", &c6_u, 3, 0U, 0U, 0U, 0), false);
  return c6_y;
}

static const mxArray *c6_d_sf_marshallOut(void *chartInstanceVoid, void
  *c6_inData)
{
  const mxArray *c6_mxArrayOutData = NULL;
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  c6_mxArrayOutData = NULL;
  sf_mex_assign(&c6_mxArrayOutData, c6_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c6_inData), false);
  return c6_mxArrayOutData;
}

static uint8_T c6_g_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_tp_Mode1, const char_T *c6_identifier)
{
  uint8_T c6_y;
  emlrtMsgIdentifier c6_thisId;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  c6_y = c6_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c6_b_tp_Mode1),
    &c6_thisId);
  sf_mex_destroy(&c6_b_tp_Mode1);
  return c6_y;
}

static uint8_T c6_h_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId)
{
  uint8_T c6_y;
  uint8_T c6_u0;
  (void)chartInstance;
  sf_mex_import(c6_parentId, sf_mex_dup(c6_u), &c6_u0, 1, 3, 0U, 0, 0U, 0);
  c6_y = c6_u0;
  sf_mex_destroy(&c6_u);
  return c6_y;
}

static void c6_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c6_mxArrayInData, const char_T *c6_varName, void *c6_outData)
{
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c6_outData = c6_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_mxArrayInData), c6_varName);
  sf_mex_destroy(&c6_mxArrayInData);
}

static const mxArray *c6_e_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance)
{
  const mxArray *c6_y;
  int32_T c6_i1;
  boolean_T c6_bv0[4];
  c6_y = NULL;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_createcellmatrix(7, 1), false);
  sf_mex_setcell(c6_y, 0, c6_emlrt_marshallOut(chartInstance,
    *chartInstance->c6_V_C));
  sf_mex_setcell(c6_y, 1, c6_emlrt_marshallOut(chartInstance,
    chartInstance->c6_C));
  sf_mex_setcell(c6_y, 2, c6_emlrt_marshallOut(chartInstance,
    chartInstance->c6_R));
  sf_mex_setcell(c6_y, 3, c6_emlrt_marshallOut(chartInstance,
    *chartInstance->c6_q));
  sf_mex_setcell(c6_y, 4, c6_d_emlrt_marshallOut(chartInstance,
    chartInstance->c6_is_active_c6_Integrated_system_V06));
  sf_mex_setcell(c6_y, 5, c6_d_emlrt_marshallOut(chartInstance,
    chartInstance->c6_is_c6_Integrated_system_V06));
  for (c6_i1 = 0; c6_i1 < 4; c6_i1++) {
    c6_bv0[c6_i1] = chartInstance->c6_dataWrittenToVector[c6_i1];
  }

  sf_mex_setcell(c6_y, 6, c6_f_emlrt_marshallOut(chartInstance, c6_bv0));
  return c6_y;
}

static const mxArray *c6_f_emlrt_marshallOut
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const boolean_T
   c6_u[4])
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_create("y", c6_u, 11, 0U, 1U, 0U, 1, 4), false);
  return c6_y;
}

static void c6_i_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u)
{
  boolean_T c6_bv1[4];
  int32_T c6_i2;
  *chartInstance->c6_V_C = c6_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_C", c6_u, 0)), "V_C");
  chartInstance->c6_C = c6_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("C", c6_u, 1)), "C");
  chartInstance->c6_R = c6_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("R", c6_u, 2)), "R");
  *chartInstance->c6_q = c6_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("q", c6_u, 3)), "q");
  chartInstance->c6_is_active_c6_Integrated_system_V06 = c6_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c6_Integrated_system_V06", c6_u, 4)),
     "is_active_c6_Integrated_system_V06");
  chartInstance->c6_is_c6_Integrated_system_V06 = c6_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c6_Integrated_system_V06",
       c6_u, 5)), "is_c6_Integrated_system_V06");
  c6_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c6_u, 6)), "dataWrittenToVector", c6_bv1);
  for (c6_i2 = 0; c6_i2 < 4; c6_i2++) {
    chartInstance->c6_dataWrittenToVector[c6_i2] = c6_bv1[c6_i2];
  }

  sf_mex_assign(&chartInstance->c6_setSimStateSideEffectsInfo,
                c6_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c6_u, 7)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c6_u);
}

static void c6_j_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_b_dataWrittenToVector, const char_T
  *c6_identifier, boolean_T c6_y[4])
{
  emlrtMsgIdentifier c6_thisId;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  c6_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c6_b_dataWrittenToVector),
                        &c6_thisId, c6_y);
  sf_mex_destroy(&c6_b_dataWrittenToVector);
}

static void c6_k_emlrt_marshallIn(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance, const mxArray *c6_u, const emlrtMsgIdentifier *c6_parentId,
  boolean_T c6_y[4])
{
  boolean_T c6_bv2[4];
  int32_T c6_i3;
  (void)chartInstance;
  sf_mex_import(c6_parentId, sf_mex_dup(c6_u), c6_bv2, 1, 11, 0U, 1, 0U, 1, 4);
  for (c6_i3 = 0; c6_i3 < 4; c6_i3++) {
    c6_y[c6_i3] = c6_bv2[c6_i3];
  }

  sf_mex_destroy(&c6_u);
}

static const mxArray *c6_l_emlrt_marshallIn
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray
   *c6_b_setSimStateSideEffectsInfo, const char_T *c6_identifier)
{
  const mxArray *c6_y = NULL;
  emlrtMsgIdentifier c6_thisId;
  c6_y = NULL;
  c6_thisId.fIdentifier = c6_identifier;
  c6_thisId.fParent = NULL;
  sf_mex_assign(&c6_y, c6_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c6_b_setSimStateSideEffectsInfo), &c6_thisId), false);
  sf_mex_destroy(&c6_b_setSimStateSideEffectsInfo);
  return c6_y;
}

static const mxArray *c6_m_emlrt_marshallIn
  (SFc6_Integrated_system_V06InstanceStruct *chartInstance, const mxArray *c6_u,
   const emlrtMsgIdentifier *c6_parentId)
{
  const mxArray *c6_y = NULL;
  (void)chartInstance;
  (void)c6_parentId;
  c6_y = NULL;
  sf_mex_assign(&c6_y, sf_mex_duplicatearraysafe(&c6_u), false);
  sf_mex_destroy(&c6_u);
  return c6_y;
}

static void init_dsm_address_info(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc6_Integrated_system_V06InstanceStruct
  *chartInstance)
{
  chartInstance->c6_I_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c6_V_C = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c6_q = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 0);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c6_Integrated_system_V06_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1594419643U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3293061697U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3497871247U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1095328602U);
}

mxArray* sf_c6_Integrated_system_V06_get_post_codegen_info(void);
mxArray *sf_c6_Integrated_system_V06_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("76tBcJdqd6FZQZvfLmOyI");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c6_Integrated_system_V06_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c6_Integrated_system_V06_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c6_Integrated_system_V06_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c6_Integrated_system_V06_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c6_Integrated_system_V06_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c6_Integrated_system_V06(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[10],T\"V_C\",},{M[3],M[17],T\"C\",},{M[3],M[8],T\"R\",},{M[5],M[16],T\"q\",},{M[8],M[0],T\"is_active_c6_Integrated_system_V06\",},{M[9],M[0],T\"is_c6_Integrated_system_V06\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c6_Integrated_system_V06_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc6_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V06MachineNumber_,
           6,
           2,
           3,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V06MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V06MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V06MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"C");
          _SFD_SET_DATA_PROPS(1,0,0,0,"R");
          _SFD_SET_DATA_PROPS(2,0,0,0,"q");
          _SFD_SET_DATA_PROPS(3,1,1,0,"I_in");
          _SFD_SET_DATA_PROPS(4,2,0,1,"V_C");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,8,1,8);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,7,-1,4);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c6_sf_marshallOut,(MexInFcnForType)c6_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c6_sf_marshallOut,(MexInFcnForType)c6_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c6_sf_marshallOut,(MexInFcnForType)c6_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c6_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c6_sf_marshallOut,(MexInFcnForType)c6_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V06MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc6_Integrated_system_V06InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c6_I_in);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c6_R);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c6_V_C);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c6_q);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c6_C);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sONcdvKhiEywLLyhUnDk3r";
}

static void sf_opaque_initialize_c6_Integrated_system_V06(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c6_Integrated_system_V06
    ((SFc6_Integrated_system_V06InstanceStruct*) chartInstanceVar);
  initialize_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c6_Integrated_system_V06(void *chartInstanceVar)
{
  enable_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c6_Integrated_system_V06(void *chartInstanceVar)
{
  disable_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c6_Integrated_system_V06(void
  *chartInstanceVar)
{
  zeroCrossings_c6_Integrated_system_V06
    ((SFc6_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c6_Integrated_system_V06(void
  *chartInstanceVar)
{
  derivatives_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_outputs_c6_Integrated_system_V06(void *chartInstanceVar)
{
  outputs_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c6_Integrated_system_V06(void *chartInstanceVar)
{
  sf_gateway_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c6_Integrated_system_V06(SimStruct*
  S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c6_Integrated_system_V06
    ((SFc6_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c6_Integrated_system_V06(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c6_Integrated_system_V06
    ((SFc6_Integrated_system_V06InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c6_Integrated_system_V06(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc6_Integrated_system_V06InstanceStruct*) chartInstanceVar)
      ->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V06_optimization_info();
    }

    finalize_c6_Integrated_system_V06((SFc6_Integrated_system_V06InstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc6_Integrated_system_V06
    ((SFc6_Integrated_system_V06InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c6_Integrated_system_V06(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c6_Integrated_system_V06
      ((SFc6_Integrated_system_V06InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c6_Integrated_system_V06(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V06_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,6);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,6,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,6);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,6,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,6,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,6);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1140596471U));
  ssSetChecksum1(S,(1788720823U));
  ssSetChecksum2(S,(906496495U));
  ssSetChecksum3(S,(2177703189U));
  ssSetNumContStates(S,1);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c6_Integrated_system_V06(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c6_Integrated_system_V06(SimStruct *S)
{
  SFc6_Integrated_system_V06InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc6_Integrated_system_V06InstanceStruct *)utMalloc(sizeof
    (SFc6_Integrated_system_V06InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc6_Integrated_system_V06InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c6_Integrated_system_V06;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c6_Integrated_system_V06;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c6_Integrated_system_V06;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c6_Integrated_system_V06;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c6_Integrated_system_V06;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c6_Integrated_system_V06;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c6_Integrated_system_V06;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c6_Integrated_system_V06;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c6_Integrated_system_V06;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c6_Integrated_system_V06;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c6_Integrated_system_V06;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c6_Integrated_system_V06;
  chartInstance->chartInfo.mdlStart = mdlStart_c6_Integrated_system_V06;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c6_Integrated_system_V06;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c6_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c6_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c6_Integrated_system_V06(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c6_Integrated_system_V06(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c6_Integrated_system_V06_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
