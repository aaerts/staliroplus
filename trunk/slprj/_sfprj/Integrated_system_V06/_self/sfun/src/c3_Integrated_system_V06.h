#ifndef __c3_Integrated_system_V06_h__
#define __c3_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc3_Integrated_system_V06InstanceStruct
#define typedef_SFc3_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c3_sfEvent;
  uint8_T c3_tp_Mode1;
  boolean_T c3_isStable;
  boolean_T c3_stateChanged;
  real_T c3_lastMajorTime;
  uint8_T c3_is_active_c3_Integrated_system_V06;
  uint8_T c3_is_c3_Integrated_system_V06;
  real_T c3_L;
  real_T c3_R;
  real_T c3_Ke;
  real_T c3_Kt;
  boolean_T c3_dataWrittenToVector[7];
  uint8_T c3_doSetSimStateSideEffects;
  const mxArray *c3_setSimStateSideEffectsInfo;
  real_T *c3_Tem;
  real_T *c3_I_in;
  real_T *c3_I;
  real_T *c3_w;
  real_T *c3_Va;
} SFc3_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc3_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c3_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c3_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c3_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
