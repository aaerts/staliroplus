#ifndef __c6_Integrated_system_V06_h__
#define __c6_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc6_Integrated_system_V06InstanceStruct
#define typedef_SFc6_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c6_sfEvent;
  uint8_T c6_tp_Mode1;
  uint8_T c6_tp_Mode2;
  boolean_T c6_isStable;
  boolean_T c6_stateChanged;
  real_T c6_lastMajorTime;
  uint8_T c6_is_active_c6_Integrated_system_V06;
  uint8_T c6_is_c6_Integrated_system_V06;
  real_T c6_R;
  real_T c6_C;
  boolean_T c6_dataWrittenToVector[4];
  uint8_T c6_doSetSimStateSideEffects;
  const mxArray *c6_setSimStateSideEffectsInfo;
  real_T *c6_I_in;
  real_T *c6_V_C;
  real_T *c6_q;
} SFc6_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc6_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c6_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c6_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c6_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
