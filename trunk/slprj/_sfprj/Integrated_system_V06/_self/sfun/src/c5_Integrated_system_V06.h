#ifndef __c5_Integrated_system_V06_h__
#define __c5_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc5_Integrated_system_V06InstanceStruct
#define typedef_SFc5_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c5_sfEvent;
  uint8_T c5_tp_Mode1;
  uint8_T c5_tp_Mode2;
  boolean_T c5_isStable;
  boolean_T c5_stateChanged;
  real_T c5_lastMajorTime;
  uint8_T c5_is_active_c5_Integrated_system_V06;
  uint8_T c5_is_c5_Integrated_system_V06;
  real_T c5_R;
  real_T c5_V_trigger;
  boolean_T c5_dataWrittenToVector[3];
  uint8_T c5_doSetSimStateSideEffects;
  const mxArray *c5_setSimStateSideEffectsInfo;
  real_T *c5_V_source;
  real_T *c5_V_C;
  real_T *c5_I_in;
} SFc5_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc5_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c5_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c5_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
