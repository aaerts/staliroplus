#ifndef __c10_Integrated_system_V06_h__
#define __c10_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc10_Integrated_system_V06InstanceStruct
#define typedef_SFc10_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c10_sfEvent;
  uint8_T c10_tp_Mode1;
  boolean_T c10_isStable;
  boolean_T c10_stateChanged;
  real_T c10_lastMajorTime;
  uint8_T c10_is_active_c10_Integrated_system_V06;
  uint8_T c10_is_c10_Integrated_system_V06;
  real_T c10_V_range;
  real_T c10_V_offset;
  real_T c10_a_max;
  boolean_T c10_dataWrittenToVector[4];
  uint8_T c10_doSetSimStateSideEffects;
  const mxArray *c10_setSimStateSideEffectsInfo;
  real_T *c10_lever_angle;
  real_T *c10_a_lever;
} SFc10_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc10_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c10_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c10_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c10_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
