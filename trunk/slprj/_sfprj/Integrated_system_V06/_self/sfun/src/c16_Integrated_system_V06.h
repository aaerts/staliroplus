#ifndef __c16_Integrated_system_V06_h__
#define __c16_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "rtw_capi.h"
#include "rtw_modelmap.h"

/* Type Definitions */
#ifndef typedef_SFc16_Integrated_system_V06InstanceStruct
#define typedef_SFc16_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c16_sfEvent;
  uint8_T c16_tp_Mode3;
  uint8_T c16_tp_Mode1;
  uint8_T c16_tp_Mode2;
  uint8_T c16_tp_Mode4;
  uint8_T c16_tp_Mode5;
  uint8_T c16_tp_Mode6;
  uint8_T c16_tp_Mode10;
  uint8_T c16_tp_Mode8;
  uint8_T c16_tp_Mode9;
  uint8_T c16_tp_Mode7;
  boolean_T c16_isStable;
  boolean_T c16_stateChanged;
  real_T c16_lastMajorTime;
  uint8_T c16_is_active_c16_Integrated_system_V06;
  uint8_T c16_is_c16_Integrated_system_V06;
  real_T c16_ball_shoot;
  real_T c16_ball_threshold;
  real_T c16_ball_catching;
  real_T c16_delta;
  real_T c16_temporalCounter_i1;
  real_T c16_presentTime;
  real_T c16_elapsedTime;
  real_T c16_previousTime;
  boolean_T c16_dataWrittenToVector[9];
  uint8_T c16_doSetSimStateSideEffects;
  const mxArray *c16_setSimStateSideEffectsInfo;
  void *c16_dataSetLogObjVector[14];
  rtwCAPI_ModelMappingInfo c16_testPointMappingInfo;
  void *c16_testPointAddrMap[14];
  real_T *c16_user_shoot_power;
  real_T *c16_user_shoot_button;
  real_T *c16_user_max_power;
  real_T *c16_sensor_ball_distance;
  real_T *c16_sensor_lever_angle;
  real_T *c16_sensor_capacitor_charge;
  real_T *c16_IGBT_PWM;
  real_T *c16_bridge_PWM_1;
  real_T *c16_bridge_PWM_2;
  real_T *c16_mode;
  real_T *c16_V_in;
} SFc16_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc16_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c16_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c16_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c16_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
