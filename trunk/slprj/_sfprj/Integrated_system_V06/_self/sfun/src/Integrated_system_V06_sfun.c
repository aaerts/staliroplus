/* Include files */

#include "Integrated_system_V06_sfun.h"
#include "Integrated_system_V06_sfun_debug_macros.h"
#include "c1_Integrated_system_V06.h"
#include "c2_Integrated_system_V06.h"
#include "c3_Integrated_system_V06.h"
#include "c4_Integrated_system_V06.h"
#include "c5_Integrated_system_V06.h"
#include "c6_Integrated_system_V06.h"
#include "c7_Integrated_system_V06.h"
#include "c8_Integrated_system_V06.h"
#include "c9_Integrated_system_V06.h"
#include "c10_Integrated_system_V06.h"
#include "c11_Integrated_system_V06.h"
#include "c12_Integrated_system_V06.h"
#include "c13_Integrated_system_V06.h"
#include "c15_Integrated_system_V06.h"
#include "c16_Integrated_system_V06.h"

/* Type Definitions */

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */
uint32_T _Integrated_system_V06MachineNumber_;

/* Function Declarations */

/* Function Definitions */
void Integrated_system_V06_initializer(void)
{
}

void Integrated_system_V06_terminator(void)
{
}

/* SFunction Glue Code */
unsigned int sf_Integrated_system_V06_method_dispatcher(SimStruct *simstructPtr,
  unsigned int chartFileNumber, const char* specsCksum, int_T method, void *data)
{
  if (chartFileNumber==1) {
    c1_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==2) {
    c2_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==3) {
    c3_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==4) {
    c4_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==5) {
    c5_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==6) {
    c6_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==7) {
    c7_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==8) {
    c8_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==9) {
    c9_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==10) {
    c10_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==11) {
    c11_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==12) {
    c12_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==13) {
    c13_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==15) {
    c15_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  if (chartFileNumber==16) {
    c16_Integrated_system_V06_method_dispatcher(simstructPtr, method, data);
    return 1;
  }

  return 0;
}

extern void sf_Integrated_system_V06_uses_exported_functions(int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[])
{
  plhs[0] = mxCreateLogicalScalar(0);
}

unsigned int sf_Integrated_system_V06_process_check_sum_call( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[20];
  if (nrhs<1 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the checksum */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"sf_get_check_sum"))
    return 0;
  plhs[0] = mxCreateDoubleMatrix( 1,4,mxREAL);
  if (nrhs>1 && mxIsChar(prhs[1])) {
    mxGetString(prhs[1], commandName,sizeof(commandName)/sizeof(char));
    commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
    if (!strcmp(commandName,"machine")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3262952822U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2233993944U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3079777639U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2596028137U);
    } else if (!strcmp(commandName,"exportedFcn")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0U);
    } else if (nrhs==3 && !strcmp(commandName,"chart")) {
      unsigned int chartFileNumber;
      chartFileNumber = (unsigned int)mxGetScalar(prhs[2]);
      switch (chartFileNumber) {
       case 1:
        {
          extern void sf_c1_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c1_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 2:
        {
          extern void sf_c2_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c2_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 3:
        {
          extern void sf_c3_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c3_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 4:
        {
          extern void sf_c4_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c4_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 5:
        {
          extern void sf_c5_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c5_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 6:
        {
          extern void sf_c6_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c6_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 7:
        {
          extern void sf_c7_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c7_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 8:
        {
          extern void sf_c8_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c8_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 9:
        {
          extern void sf_c9_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c9_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 10:
        {
          extern void sf_c10_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c10_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 11:
        {
          extern void sf_c11_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c11_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 12:
        {
          extern void sf_c12_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c12_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 13:
        {
          extern void sf_c13_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c13_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 15:
        {
          extern void sf_c15_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c15_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       case 16:
        {
          extern void sf_c16_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
          sf_c16_Integrated_system_V06_get_check_sum(plhs);
          break;
        }

       default:
        ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(0.0);
        ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(0.0);
      }
    } else if (!strcmp(commandName,"target")) {
      ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3267489760U);
      ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3330274261U);
      ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2714568270U);
      ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(344490432U);
    } else {
      return 0;
    }
  } else {
    ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1686840916U);
    ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2713438415U);
    ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3012180657U);
    ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2091413176U);
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_Integrated_system_V06_autoinheritance_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[32];
  char aiChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]) )
    return 0;

  /* Possible call to get the autoinheritance_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_autoinheritance_info"))
    return 0;
  mxGetString(prhs[2], aiChksum,sizeof(aiChksum)/sizeof(char));
  aiChksum[(sizeof(aiChksum)/sizeof(char)-1)] = '\0';

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(aiChksum, "5DK94UApRvpUyLOahYFCWF") == 0) {
          extern mxArray *sf_c1_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c1_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 2:
      {
        if (strcmp(aiChksum, "TjPN7jtAL3NoZBGqkyJ8dG") == 0) {
          extern mxArray *sf_c2_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c2_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 3:
      {
        if (strcmp(aiChksum, "JsWJqzLDedJqP6aUKbRXAF") == 0) {
          extern mxArray *sf_c3_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c3_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 4:
      {
        if (strcmp(aiChksum, "APqulMxwgJ5946ZoCHMqAB") == 0) {
          extern mxArray *sf_c4_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c4_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 5:
      {
        if (strcmp(aiChksum, "MuEvS0oesSHeXiUuLvaXiG") == 0) {
          extern mxArray *sf_c5_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c5_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 6:
      {
        if (strcmp(aiChksum, "76tBcJdqd6FZQZvfLmOyI") == 0) {
          extern mxArray *sf_c6_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c6_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 7:
      {
        if (strcmp(aiChksum, "9kfUwieQLfB2YWGPYROV3F") == 0) {
          extern mxArray *sf_c7_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c7_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 8:
      {
        if (strcmp(aiChksum, "1NMY9ee5UvWB5YEt3NHP3") == 0) {
          extern mxArray *sf_c8_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c8_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 9:
      {
        if (strcmp(aiChksum, "JOTvO4rNqzJTlzqT6EdIdE") == 0) {
          extern mxArray *sf_c9_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c9_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 10:
      {
        if (strcmp(aiChksum, "cg6ZzK62ipV9xOt2Mm59WC") == 0) {
          extern mxArray *sf_c10_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c10_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 11:
      {
        if (strcmp(aiChksum, "92q8fkLCHeUr5GW4k92BNH") == 0) {
          extern mxArray *sf_c11_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c11_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 12:
      {
        if (strcmp(aiChksum, "HuczkW8Andlhg6Udm9xvdD") == 0) {
          extern mxArray *sf_c12_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c12_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 13:
      {
        if (strcmp(aiChksum, "nOkdPP7oQTAyK6zPaXnmI") == 0) {
          extern mxArray *sf_c13_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c13_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 15:
      {
        if (strcmp(aiChksum, "kbr2ZhpWHZ1GooNuGxvICD") == 0) {
          extern mxArray *sf_c15_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c15_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     case 16:
      {
        if (strcmp(aiChksum, "e41Xcc8IDoKFvuFpVzcT2E") == 0) {
          extern mxArray *sf_c16_Integrated_system_V06_get_autoinheritance_info
            (void);
          plhs[0] = sf_c16_Integrated_system_V06_get_autoinheritance_info();
          break;
        }

        plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_Integrated_system_V06_get_eml_resolved_functions_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{

#ifdef MATLAB_MEX_FILE

  char commandName[64];
  if (nrhs<2 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the get_eml_resolved_functions_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_eml_resolved_functions_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        extern const mxArray
          *sf_c1_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c1_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 2:
      {
        extern const mxArray
          *sf_c2_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c2_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 3:
      {
        extern const mxArray
          *sf_c3_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c3_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 4:
      {
        extern const mxArray
          *sf_c4_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c4_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 5:
      {
        extern const mxArray
          *sf_c5_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c5_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 6:
      {
        extern const mxArray
          *sf_c6_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c6_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 7:
      {
        extern const mxArray
          *sf_c7_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c7_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 8:
      {
        extern const mxArray
          *sf_c8_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c8_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 9:
      {
        extern const mxArray
          *sf_c9_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c9_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 10:
      {
        extern const mxArray
          *sf_c10_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c10_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 11:
      {
        extern const mxArray
          *sf_c11_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c11_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 12:
      {
        extern const mxArray
          *sf_c12_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c12_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 13:
      {
        extern const mxArray
          *sf_c13_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c13_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 15:
      {
        extern const mxArray
          *sf_c15_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c15_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     case 16:
      {
        extern const mxArray
          *sf_c16_Integrated_system_V06_get_eml_resolved_functions_info(void);
        mxArray *persistentMxArray = (mxArray *)
          sf_c16_Integrated_system_V06_get_eml_resolved_functions_info();
        plhs[0] = mxDuplicateArray(persistentMxArray);
        mxDestroyArray(persistentMxArray);
        break;
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;

#else

  return 0;

#endif

}

unsigned int sf_Integrated_system_V06_third_party_uses_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the third_party_uses_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_third_party_uses_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "se7DQo775KMK83rDBBYeaZG") == 0) {
          extern mxArray *sf_c1_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c1_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sd0HYjY2KpNS08ZoJ4BOuEE") == 0) {
          extern mxArray *sf_c2_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c2_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "slRFzTBQdgWHSteLSsqf6GD") == 0) {
          extern mxArray *sf_c3_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c3_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "svH61tJpo9uxCMwBYlkFjxE") == 0) {
          extern mxArray *sf_c4_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c4_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "sOUu7EDj4IItPMiHWHS6ssE") == 0) {
          extern mxArray *sf_c5_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c5_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "sONcdvKhiEywLLyhUnDk3r") == 0) {
          extern mxArray *sf_c6_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c6_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sYCLAjyBpfcAzbvnEmWAGxC") == 0) {
          extern mxArray *sf_c7_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c7_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s217vIBoC5B3DEwFE0BsnqG") == 0) {
          extern mxArray *sf_c8_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c8_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "spiPRlRYAZ6GJD8kFBHWefD") == 0) {
          extern mxArray *sf_c9_Integrated_system_V06_third_party_uses_info(void);
          plhs[0] = sf_c9_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s6MrrgAEtKGgXn5BPCfCUdH") == 0) {
          extern mxArray *sf_c10_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c10_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "shIf8WE8kC7Y3cvsgiYNafD") == 0) {
          extern mxArray *sf_c11_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c11_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "sulc4dH4xCHSS7ju8riAg1D") == 0) {
          extern mxArray *sf_c12_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c12_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "sv6QuFM9pK8byvFfsEUpw3") == 0) {
          extern mxArray *sf_c13_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c13_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "szPORTQDnc0oBwqEHxR6LYB") == 0) {
          extern mxArray *sf_c15_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c15_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "svpeSAKrZB1XZsBnluwN2vG") == 0) {
          extern mxArray *sf_c16_Integrated_system_V06_third_party_uses_info
            (void);
          plhs[0] = sf_c16_Integrated_system_V06_third_party_uses_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_Integrated_system_V06_jit_fallback_info( int nlhs, mxArray *
  plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the jit_fallback_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_jit_fallback_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "se7DQo775KMK83rDBBYeaZG") == 0) {
          extern mxArray *sf_c1_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c1_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sd0HYjY2KpNS08ZoJ4BOuEE") == 0) {
          extern mxArray *sf_c2_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c2_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "slRFzTBQdgWHSteLSsqf6GD") == 0) {
          extern mxArray *sf_c3_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c3_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "svH61tJpo9uxCMwBYlkFjxE") == 0) {
          extern mxArray *sf_c4_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c4_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "sOUu7EDj4IItPMiHWHS6ssE") == 0) {
          extern mxArray *sf_c5_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c5_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "sONcdvKhiEywLLyhUnDk3r") == 0) {
          extern mxArray *sf_c6_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c6_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sYCLAjyBpfcAzbvnEmWAGxC") == 0) {
          extern mxArray *sf_c7_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c7_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s217vIBoC5B3DEwFE0BsnqG") == 0) {
          extern mxArray *sf_c8_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c8_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "spiPRlRYAZ6GJD8kFBHWefD") == 0) {
          extern mxArray *sf_c9_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c9_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s6MrrgAEtKGgXn5BPCfCUdH") == 0) {
          extern mxArray *sf_c10_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c10_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "shIf8WE8kC7Y3cvsgiYNafD") == 0) {
          extern mxArray *sf_c11_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c11_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "sulc4dH4xCHSS7ju8riAg1D") == 0) {
          extern mxArray *sf_c12_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c12_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "sv6QuFM9pK8byvFfsEUpw3") == 0) {
          extern mxArray *sf_c13_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c13_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "szPORTQDnc0oBwqEHxR6LYB") == 0) {
          extern mxArray *sf_c15_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c15_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "svpeSAKrZB1XZsBnluwN2vG") == 0) {
          extern mxArray *sf_c16_Integrated_system_V06_jit_fallback_info(void);
          plhs[0] = sf_c16_Integrated_system_V06_jit_fallback_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

unsigned int sf_Integrated_system_V06_updateBuildInfo_args_info( int nlhs,
  mxArray * plhs[], int nrhs, const mxArray * prhs[] )
{
  char commandName[64];
  char tpChksum[64];
  if (nrhs<3 || !mxIsChar(prhs[0]))
    return 0;

  /* Possible call to get the updateBuildInfo_args_info */
  mxGetString(prhs[0], commandName,sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  mxGetString(prhs[2], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName,"get_updateBuildInfo_args_info"))
    return 0;

  {
    unsigned int chartFileNumber;
    chartFileNumber = (unsigned int)mxGetScalar(prhs[1]);
    switch (chartFileNumber) {
     case 1:
      {
        if (strcmp(tpChksum, "se7DQo775KMK83rDBBYeaZG") == 0) {
          extern mxArray *sf_c1_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c1_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 2:
      {
        if (strcmp(tpChksum, "sd0HYjY2KpNS08ZoJ4BOuEE") == 0) {
          extern mxArray *sf_c2_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c2_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 3:
      {
        if (strcmp(tpChksum, "slRFzTBQdgWHSteLSsqf6GD") == 0) {
          extern mxArray *sf_c3_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c3_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 4:
      {
        if (strcmp(tpChksum, "svH61tJpo9uxCMwBYlkFjxE") == 0) {
          extern mxArray *sf_c4_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c4_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 5:
      {
        if (strcmp(tpChksum, "sOUu7EDj4IItPMiHWHS6ssE") == 0) {
          extern mxArray *sf_c5_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c5_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 6:
      {
        if (strcmp(tpChksum, "sONcdvKhiEywLLyhUnDk3r") == 0) {
          extern mxArray *sf_c6_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c6_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 7:
      {
        if (strcmp(tpChksum, "sYCLAjyBpfcAzbvnEmWAGxC") == 0) {
          extern mxArray *sf_c7_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c7_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 8:
      {
        if (strcmp(tpChksum, "s217vIBoC5B3DEwFE0BsnqG") == 0) {
          extern mxArray *sf_c8_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c8_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 9:
      {
        if (strcmp(tpChksum, "spiPRlRYAZ6GJD8kFBHWefD") == 0) {
          extern mxArray *sf_c9_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c9_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 10:
      {
        if (strcmp(tpChksum, "s6MrrgAEtKGgXn5BPCfCUdH") == 0) {
          extern mxArray *sf_c10_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c10_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 11:
      {
        if (strcmp(tpChksum, "shIf8WE8kC7Y3cvsgiYNafD") == 0) {
          extern mxArray *sf_c11_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c11_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 12:
      {
        if (strcmp(tpChksum, "sulc4dH4xCHSS7ju8riAg1D") == 0) {
          extern mxArray *sf_c12_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c12_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 13:
      {
        if (strcmp(tpChksum, "sv6QuFM9pK8byvFfsEUpw3") == 0) {
          extern mxArray *sf_c13_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c13_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 15:
      {
        if (strcmp(tpChksum, "szPORTQDnc0oBwqEHxR6LYB") == 0) {
          extern mxArray *sf_c15_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c15_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     case 16:
      {
        if (strcmp(tpChksum, "svpeSAKrZB1XZsBnluwN2vG") == 0) {
          extern mxArray *sf_c16_Integrated_system_V06_updateBuildInfo_args_info
            (void);
          plhs[0] = sf_c16_Integrated_system_V06_updateBuildInfo_args_info();
          break;
        }
      }

     default:
      plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
    }
  }

  return 1;
}

void sf_Integrated_system_V06_get_post_codegen_info( int nlhs, mxArray * plhs[],
  int nrhs, const mxArray * prhs[] )
{
  unsigned int chartFileNumber = (unsigned int) mxGetScalar(prhs[0]);
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  switch (chartFileNumber) {
   case 1:
    {
      if (strcmp(tpChksum, "se7DQo775KMK83rDBBYeaZG") == 0) {
        extern mxArray *sf_c1_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c1_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 2:
    {
      if (strcmp(tpChksum, "sd0HYjY2KpNS08ZoJ4BOuEE") == 0) {
        extern mxArray *sf_c2_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c2_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 3:
    {
      if (strcmp(tpChksum, "slRFzTBQdgWHSteLSsqf6GD") == 0) {
        extern mxArray *sf_c3_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c3_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 4:
    {
      if (strcmp(tpChksum, "svH61tJpo9uxCMwBYlkFjxE") == 0) {
        extern mxArray *sf_c4_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c4_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 5:
    {
      if (strcmp(tpChksum, "sOUu7EDj4IItPMiHWHS6ssE") == 0) {
        extern mxArray *sf_c5_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c5_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 6:
    {
      if (strcmp(tpChksum, "sONcdvKhiEywLLyhUnDk3r") == 0) {
        extern mxArray *sf_c6_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c6_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 7:
    {
      if (strcmp(tpChksum, "sYCLAjyBpfcAzbvnEmWAGxC") == 0) {
        extern mxArray *sf_c7_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c7_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 8:
    {
      if (strcmp(tpChksum, "s217vIBoC5B3DEwFE0BsnqG") == 0) {
        extern mxArray *sf_c8_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c8_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 9:
    {
      if (strcmp(tpChksum, "spiPRlRYAZ6GJD8kFBHWefD") == 0) {
        extern mxArray *sf_c9_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c9_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 10:
    {
      if (strcmp(tpChksum, "s6MrrgAEtKGgXn5BPCfCUdH") == 0) {
        extern mxArray *sf_c10_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c10_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 11:
    {
      if (strcmp(tpChksum, "shIf8WE8kC7Y3cvsgiYNafD") == 0) {
        extern mxArray *sf_c11_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c11_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 12:
    {
      if (strcmp(tpChksum, "sulc4dH4xCHSS7ju8riAg1D") == 0) {
        extern mxArray *sf_c12_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c12_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 13:
    {
      if (strcmp(tpChksum, "sv6QuFM9pK8byvFfsEUpw3") == 0) {
        extern mxArray *sf_c13_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c13_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 15:
    {
      if (strcmp(tpChksum, "szPORTQDnc0oBwqEHxR6LYB") == 0) {
        extern mxArray *sf_c15_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c15_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   case 16:
    {
      if (strcmp(tpChksum, "svpeSAKrZB1XZsBnluwN2vG") == 0) {
        extern mxArray *sf_c16_Integrated_system_V06_get_post_codegen_info(void);
        plhs[0] = sf_c16_Integrated_system_V06_get_post_codegen_info();
        return;
      }
    }
    break;

   default:
    break;
  }

  plhs[0] = mxCreateDoubleMatrix(0,0,mxREAL);
}

void Integrated_system_V06_debug_initialize(struct SfDebugInstanceStruct*
  debugInstance)
{
  _Integrated_system_V06MachineNumber_ = sf_debug_initialize_machine
    (debugInstance,"Integrated_system_V06","sfun",0,15,0,0,0);
  sf_debug_set_machine_event_thresholds(debugInstance,
    _Integrated_system_V06MachineNumber_,0,0);
  sf_debug_set_machine_data_thresholds(debugInstance,
    _Integrated_system_V06MachineNumber_,0);
}

void Integrated_system_V06_register_exported_symbols(SimStruct* S)
{
}

static mxArray* sRtwOptimizationInfoStruct= NULL;
mxArray* load_Integrated_system_V06_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct==NULL) {
    sRtwOptimizationInfoStruct = sf_load_rtw_optimization_info(
      "Integrated_system_V06", "Integrated_system_V06");
    mexMakeArrayPersistent(sRtwOptimizationInfoStruct);
  }

  return(sRtwOptimizationInfoStruct);
}

void unload_Integrated_system_V06_optimization_info(void)
{
  if (sRtwOptimizationInfoStruct!=NULL) {
    mxDestroyArray(sRtwOptimizationInfoStruct);
    sRtwOptimizationInfoStruct = NULL;
  }
}
