#ifndef __c15_Integrated_system_V06_h__
#define __c15_Integrated_system_V06_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc15_Integrated_system_V06InstanceStruct
#define typedef_SFc15_Integrated_system_V06InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c15_sfEvent;
  uint8_T c15_tp_Mode1;
  uint8_T c15_tp_Mode2;
  uint8_T c15_tp_Mode3;
  uint8_T c15_tp_Mode4;
  boolean_T c15_isStable;
  boolean_T c15_stateChanged;
  real_T c15_lastMajorTime;
  uint8_T c15_is_active_c15_Integrated_system_V06;
  uint8_T c15_is_c15_Integrated_system_V06;
  boolean_T c15_dataWrittenToVector[2];
  uint8_T c15_doSetSimStateSideEffects;
  const mxArray *c15_setSimStateSideEffectsInfo;
  real_T *c15_PWM_1;
  real_T *c15_PWM_2;
  real_T *c15_V_in;
  real_T *c15_V_a;
  real_T *c15_I_in;
  real_T *c15_I_out;
} SFc15_Integrated_system_V06InstanceStruct;

#endif                                 /*typedef_SFc15_Integrated_system_V06InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c15_Integrated_system_V06_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c15_Integrated_system_V06_get_check_sum(mxArray *plhs[]);
extern void c15_Integrated_system_V06_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
