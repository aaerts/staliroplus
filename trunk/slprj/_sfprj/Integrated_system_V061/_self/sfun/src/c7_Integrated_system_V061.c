/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c7_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c7_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c7_IN_Mode1                    ((uint8_T)1U)
#define c7_IN_Mode2                    ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c7_debug_family_names[2] = { "nargin", "nargout" };

static const char * c7_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c7_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c7_d_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c7_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c7_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c7_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c7_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void c7_update_debugger_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c7_st);
static void c7_set_sim_state_side_effects_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void c7_eml_ini_fcn_to_be_inlined_102
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static void c7_eml_term_fcn_to_be_inlined_102
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c7_mrdivide(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_A, real_T c7_B);
static real_T c7_rdivide(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_x, real_T c7_y);
static void c7_isBuiltInNumeric(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c7_eml_scalexp_compatible(SFc7_Integrated_system_V061InstanceStruct *
  chartInstance);
static real_T c7_eml_div(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_x, real_T c7_y);
static real_T c7_div(SFc7_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c7_x, real_T c7_y);
static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber);
static const mxArray *c7_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const real_T c7_u);
static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData);
static real_T c7_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_nargout, const char_T *c7_identifier);
static real_T c7_b_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_b_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c7_u);
static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static boolean_T c7_c_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_sf_internal_predicateOutput, const char_T
  *c7_identifier);
static boolean_T c7_d_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_c_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c7_u);
static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static int32_T c7_e_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_sfEvent, const char_T *c7_identifier);
static int32_T c7_f_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_d_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c7_u);
static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static uint8_T c7_g_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_tp_Mode1, const char_T *c7_identifier);
static uint8_T c7_h_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_e_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c7_f_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c7_u[5]);
static void c7_i_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u);
static void c7_j_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_dataWrittenToVector, const char_T
  *c7_identifier, boolean_T c7_y[5]);
static void c7_k_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  boolean_T c7_y[5]);
static const mxArray *c7_l_emlrt_marshallIn
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c7_b_setSimStateSideEffectsInfo, const char_T *c7_identifier);
static const mxArray *c7_m_emlrt_marshallIn
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c7_u,
   const emlrtMsgIdentifier *c7_parentId);
static void init_dsm_address_info(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_doSetSimStateSideEffects = 0U;
  chartInstance->c7_setSimStateSideEffectsInfo = NULL;
  chartInstance->c7_tp_Mode1 = 0U;
  chartInstance->c7_tp_Mode2 = 0U;
  chartInstance->c7_is_active_c7_Integrated_system_V061 = 0U;
  chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_debugger_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c7_prevAniVal;
  c7_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c7_is_active_c7_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 6U, chartInstance->c7_sfEvent);
  }

  if (chartInstance->c7_is_c7_Integrated_system_V061 == c7_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
  }

  if (chartInstance->c7_is_c7_Integrated_system_V061 == c7_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c7_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c7_sfEvent);
  }

  _SFD_SET_ANIMATION(c7_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c7_st = NULL;
  c7_st = NULL;
  sf_mex_assign(&c7_st, c7_e_emlrt_marshallOut(chartInstance), false);
  return c7_st;
}

static void set_sim_state_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c7_st)
{
  c7_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_st));
  chartInstance->c7_doSetSimStateSideEffects = 1U;
  c7_update_debugger_state_c7_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c7_st);
}

static void c7_set_sim_state_side_effects_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c7_doSetSimStateSideEffects != 0) {
    chartInstance->c7_tp_Mode1 = (uint8_T)
      (chartInstance->c7_is_c7_Integrated_system_V061 == c7_IN_Mode1);
    chartInstance->c7_tp_Mode2 = (uint8_T)
      (chartInstance->c7_is_c7_Integrated_system_V061 == c7_IN_Mode2);
    chartInstance->c7_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c7_setSimStateSideEffectsInfo);
}

static void sf_gateway_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c7_debug_family_var_map[2];
  real_T c7_nargin = 0.0;
  real_T c7_nargout = 0.0;
  uint32_T c7_b_debug_family_var_map[3];
  real_T c7_b_nargin = 0.0;
  real_T c7_b_nargout = 1.0;
  boolean_T c7_out;
  real_T c7_c_nargin = 0.0;
  real_T c7_c_nargout = 0.0;
  real_T c7_d_nargin = 0.0;
  real_T c7_d_nargout = 1.0;
  boolean_T c7_b_out;
  real_T c7_e_nargin = 0.0;
  real_T c7_e_nargout = 0.0;
  real_T c7_f_nargin = 0.0;
  real_T c7_f_nargout = 0.0;
  c7_set_sim_state_side_effects_c7_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c7_lastMajorTime = _sfTime_;
    chartInstance->c7_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c7_I_max, 0U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c7_R, 2U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_V_in, 4U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c7_K, 1U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_V_C, 3U, 1U, 0U,
                          chartInstance->c7_sfEvent, false);
    chartInstance->c7_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
    if (chartInstance->c7_is_active_c7_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
      chartInstance->c7_stateChanged = true;
      chartInstance->c7_is_active_c7_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_c_debug_family_names,
        c7_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_sf_marshallOut,
        c7_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_sf_marshallOut,
        c7_sf_marshallIn);
      chartInstance->c7_K = 10.0;
      chartInstance->c7_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c7_K, 1U, 5U, 0U,
                            chartInstance->c7_sfEvent, false);
      chartInstance->c7_R = 80.0;
      chartInstance->c7_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c7_R, 2U, 5U, 0U,
                            chartInstance->c7_sfEvent, false);
      chartInstance->c7_I_max = 3.0;
      chartInstance->c7_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c7_I_max, 0U, 5U, 0U,
                            chartInstance->c7_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c7_stateChanged = true;
      chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
      chartInstance->c7_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c7_is_c7_Integrated_system_V061) {
       case c7_IN_Mode1:
        CV_CHART_EVAL(6, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c7_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c7_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c7_d_debug_family_names,
          c7_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargin, 0U, c7_sf_marshallOut,
          c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargout, 1U,
          c7_sf_marshallOut, c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_out, 2U, c7_b_sf_marshallOut,
          c7_b_sf_marshallIn);
        if (!chartInstance->c7_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        c7_out = CV_EML_IF(1, 0, 0, c7_mrdivide(chartInstance,
          *chartInstance->c7_V_in * chartInstance->c7_K - *chartInstance->c7_V_C,
          chartInstance->c7_R) >= chartInstance->c7_I_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c7_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c7_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_e_debug_family_names,
            c7_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_c_nargin, 0U,
            c7_sf_marshallOut, c7_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_c_nargout, 1U,
            c7_sf_marshallOut, c7_sf_marshallIn);
          if (!chartInstance->c7_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
              chartInstance->c7_sfEvent, false);
          }

          *chartInstance->c7_I_C = chartInstance->c7_I_max;
          chartInstance->c7_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 5U, 1U,
                                chartInstance->c7_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c7_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
          chartInstance->c7_stateChanged = true;
          chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c7_sfEvent);
          chartInstance->c7_tp_Mode2 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
        }
        break;

       case c7_IN_Mode2:
        CV_CHART_EVAL(6, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c7_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c7_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c7_f_debug_family_names,
          c7_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_d_nargin, 0U, c7_sf_marshallOut,
          c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_d_nargout, 1U,
          c7_sf_marshallOut, c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_out, 2U, c7_b_sf_marshallOut,
          c7_b_sf_marshallIn);
        if (!chartInstance->c7_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        c7_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0, c7_mrdivide
          (chartInstance, *chartInstance->c7_V_in * chartInstance->c7_K -
           *chartInstance->c7_V_C, chartInstance->c7_R), chartInstance->c7_I_max,
          -1, 2U, c7_mrdivide(chartInstance, *chartInstance->c7_V_in *
                              chartInstance->c7_K - *chartInstance->c7_V_C,
                              chartInstance->c7_R) < chartInstance->c7_I_max));
        _SFD_SYMBOL_SCOPE_POP();
        if (c7_b_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c7_sfEvent);
          chartInstance->c7_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c7_sfEvent);
          chartInstance->c7_stateChanged = true;
          chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
          chartInstance->c7_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(6, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
    if (chartInstance->c7_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c7_is_c7_Integrated_system_V061) {
   case c7_IN_Mode1:
    CV_CHART_EVAL(6, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_e_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_e_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_C = c7_mrdivide(chartInstance, *chartInstance->c7_V_in *
      chartInstance->c7_K - *chartInstance->c7_V_C, chartInstance->c7_R);
    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_in = chartInstance->c7_K * c7_mrdivide(chartInstance,
      *chartInstance->c7_V_in * chartInstance->c7_K - *chartInstance->c7_V_C,
      chartInstance->c7_R);
    chartInstance->c7_dataWrittenToVector[2U] = true;
    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    break;

   case c7_IN_Mode2:
    CV_CHART_EVAL(6, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_b_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_f_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_f_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_C = chartInstance->c7_I_max;
    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_in = chartInstance->c7_K * chartInstance->c7_I_max;
    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    break;

   default:
    CV_CHART_EVAL(6, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c7_debug_family_var_map[3];
  real_T c7_nargin = 0.0;
  real_T c7_nargout = 1.0;
  boolean_T c7_out;
  real_T c7_b_nargin = 0.0;
  real_T c7_b_nargout = 1.0;
  boolean_T c7_b_out;
  real_T *c7_zcVar;
  c7_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c7_lastMajorTime == _sfTime_) {
    *c7_zcVar = -1.0;
  } else {
    chartInstance->c7_stateChanged = (boolean_T)0;
    if (chartInstance->c7_is_active_c7_Integrated_system_V061 == 0U) {
      chartInstance->c7_stateChanged = true;
    } else {
      switch (chartInstance->c7_is_c7_Integrated_system_V061) {
       case c7_IN_Mode1:
        CV_CHART_EVAL(6, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c7_d_debug_family_names,
          c7_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_sf_marshallOut,
          c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_sf_marshallOut,
          c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_out, 2U, c7_b_sf_marshallOut,
          c7_b_sf_marshallIn);
        if (!chartInstance->c7_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
            chartInstance->c7_sfEvent, false);
        }

        c7_out = CV_EML_IF(1, 0, 0, c7_mrdivide(chartInstance,
          *chartInstance->c7_V_in * chartInstance->c7_K - *chartInstance->c7_V_C,
          chartInstance->c7_R) >= chartInstance->c7_I_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c7_out) {
          chartInstance->c7_stateChanged = true;
        }
        break;

       case c7_IN_Mode2:
        CV_CHART_EVAL(6, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c7_f_debug_family_names,
          c7_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargin, 0U, c7_sf_marshallOut,
          c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargout, 1U,
          c7_sf_marshallOut, c7_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_out, 2U, c7_b_sf_marshallOut,
          c7_b_sf_marshallIn);
        if (!chartInstance->c7_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        if (!chartInstance->c7_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 2U,
            chartInstance->c7_sfEvent, false);
        }

        c7_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0, c7_mrdivide
          (chartInstance, *chartInstance->c7_V_in * chartInstance->c7_K -
           *chartInstance->c7_V_C, chartInstance->c7_R), chartInstance->c7_I_max,
          -1, 2U, c7_mrdivide(chartInstance, *chartInstance->c7_V_in *
                              chartInstance->c7_K - *chartInstance->c7_V_C,
                              chartInstance->c7_R) < chartInstance->c7_I_max));
        _SFD_SYMBOL_SCOPE_POP();
        if (c7_b_out) {
          chartInstance->c7_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(6, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c7_stateChanged) {
      *c7_zcVar = 1.0;
    } else {
      *c7_zcVar = -1.0;
    }
  }
}

static void derivatives_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c7_debug_family_var_map[2];
  real_T c7_nargin = 0.0;
  real_T c7_nargout = 0.0;
  real_T c7_b_nargin = 0.0;
  real_T c7_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c7_is_c7_Integrated_system_V061) {
   case c7_IN_Mode1:
    CV_CHART_EVAL(6, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    chartInstance->c7_dataWrittenToVector[2U] = true;
    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    break;

   case c7_IN_Mode2:
    CV_CHART_EVAL(6, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_b_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    break;

   default:
    CV_CHART_EVAL(6, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
    break;
  }
}

static void outputs_c7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c7_debug_family_var_map[2];
  real_T c7_nargin = 0.0;
  real_T c7_nargout = 0.0;
  real_T c7_b_nargin = 0.0;
  real_T c7_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c7_is_c7_Integrated_system_V061) {
   case c7_IN_Mode1:
    CV_CHART_EVAL(6, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_C = c7_mrdivide(chartInstance, *chartInstance->c7_V_in *
      chartInstance->c7_K - *chartInstance->c7_V_C, chartInstance->c7_R);
    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_in = chartInstance->c7_K * c7_mrdivide(chartInstance,
      *chartInstance->c7_V_in * chartInstance->c7_K - *chartInstance->c7_V_C,
      chartInstance->c7_R);
    chartInstance->c7_dataWrittenToVector[2U] = true;
    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 0U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c7_sfEvent);
    break;

   case c7_IN_Mode2:
    CV_CHART_EVAL(6, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c7_b_debug_family_names,
      c7_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargin, 0U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargout, 1U, c7_sf_marshallOut,
      c7_sf_marshallIn);
    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_C = chartInstance->c7_I_max;
    chartInstance->c7_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_C, 5U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    if (!chartInstance->c7_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    if (!chartInstance->c7_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c7_sfEvent,
        false);
    }

    *chartInstance->c7_I_in = chartInstance->c7_K * chartInstance->c7_I_max;
    chartInstance->c7_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c7_I_in, 6U, 4U, 1U,
                          chartInstance->c7_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c7_sfEvent);
    break;

   default:
    CV_CHART_EVAL(6, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c7_is_c7_Integrated_system_V061 = c7_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c7_sfEvent);
    break;
  }
}

static void initSimStructsc7_Integrated_system_V061
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c7_eml_ini_fcn_to_be_inlined_102
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c7_eml_term_fcn_to_be_inlined_102
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c7_mrdivide(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_A, real_T c7_B)
{
  return c7_rdivide(chartInstance, c7_A, c7_B);
}

static real_T c7_rdivide(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_x, real_T c7_y)
{
  return c7_eml_div(chartInstance, c7_x, c7_y);
}

static void c7_isBuiltInNumeric(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c7_eml_scalexp_compatible(SFc7_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static real_T c7_eml_div(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c7_x, real_T c7_y)
{
  return c7_div(chartInstance, c7_x, c7_y);
}

static real_T c7_div(SFc7_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c7_x, real_T c7_y)
{
  (void)chartInstance;
  return c7_x / c7_y;
}

static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber)
{
  (void)c7_machineNumber;
  (void)c7_chartNumber;
  (void)c7_instanceNumber;
}

static const mxArray *c7_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const real_T c7_u)
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  return c7_y;
}

static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  sf_mex_assign(&c7_mxArrayOutData, c7_emlrt_marshallOut(chartInstance, *(real_T
    *)c7_inData), false);
  return c7_mxArrayOutData;
}

static real_T c7_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_nargout, const char_T *c7_identifier)
{
  real_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_nargout), &c7_thisId);
  sf_mex_destroy(&c7_nargout);
  return c7_y;
}

static real_T c7_b_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  real_T c7_y;
  real_T c7_d0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_d0, 1, 0, 0U, 0, 0U, 0);
  c7_y = c7_d0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(real_T *)c7_outData = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_mxArrayInData), c7_varName);
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_b_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c7_u)
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 11, 0U, 0U, 0U, 0), false);
  return c7_y;
}

static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  sf_mex_assign(&c7_mxArrayOutData, c7_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c7_inData), false);
  return c7_mxArrayOutData;
}

static boolean_T c7_c_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_sf_internal_predicateOutput, const char_T
  *c7_identifier)
{
  boolean_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_sf_internal_predicateOutput), &c7_thisId);
  sf_mex_destroy(&c7_sf_internal_predicateOutput);
  return c7_y;
}

static boolean_T c7_d_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  boolean_T c7_y;
  boolean_T c7_b0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_b0, 1, 11, 0U, 0, 0U, 0);
  c7_y = c7_b0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c7_outData = c7_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_mxArrayInData), c7_varName);
  sf_mex_destroy(&c7_mxArrayInData);
}

const mxArray *sf_c7_Integrated_system_V061_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  c7_nameCaptureInfo = NULL;
  sf_mex_assign(&c7_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c7_nameCaptureInfo;
}

static const mxArray *c7_c_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c7_u)
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 6, 0U, 0U, 0U, 0), false);
  return c7_y;
}

static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  sf_mex_assign(&c7_mxArrayOutData, c7_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c7_inData), false);
  return c7_mxArrayOutData;
}

static int32_T c7_e_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_sfEvent, const char_T *c7_identifier)
{
  int32_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_sfEvent),
    &c7_thisId);
  sf_mex_destroy(&c7_b_sfEvent);
  return c7_y;
}

static int32_T c7_f_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int32_T c7_y;
  int32_T c7_i0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i0, 1, 6, 0U, 0, 0U, 0);
  c7_y = c7_i0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c7_outData = c7_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_mxArrayInData), c7_varName);
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_d_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c7_u)
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 3, 0U, 0U, 0U, 0), false);
  return c7_y;
}

static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData = NULL;
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  sf_mex_assign(&c7_mxArrayOutData, c7_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c7_inData), false);
  return c7_mxArrayOutData;
}

static uint8_T c7_g_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_tp_Mode1, const char_T *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_y = c7_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_tp_Mode1),
    &c7_thisId);
  sf_mex_destroy(&c7_b_tp_Mode1);
  return c7_y;
}

static uint8_T c7_h_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_u0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_u0, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_u0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c7_outData = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_mxArrayInData), c7_varName);
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_e_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c7_y;
  int32_T c7_i1;
  boolean_T c7_bv0[5];
  c7_y = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(8, 1), false);
  sf_mex_setcell(c7_y, 0, c7_emlrt_marshallOut(chartInstance,
    *chartInstance->c7_I_C));
  sf_mex_setcell(c7_y, 1, c7_emlrt_marshallOut(chartInstance,
    *chartInstance->c7_I_in));
  sf_mex_setcell(c7_y, 2, c7_emlrt_marshallOut(chartInstance,
    chartInstance->c7_I_max));
  sf_mex_setcell(c7_y, 3, c7_emlrt_marshallOut(chartInstance,
    chartInstance->c7_K));
  sf_mex_setcell(c7_y, 4, c7_emlrt_marshallOut(chartInstance,
    chartInstance->c7_R));
  sf_mex_setcell(c7_y, 5, c7_d_emlrt_marshallOut(chartInstance,
    chartInstance->c7_is_active_c7_Integrated_system_V061));
  sf_mex_setcell(c7_y, 6, c7_d_emlrt_marshallOut(chartInstance,
    chartInstance->c7_is_c7_Integrated_system_V061));
  for (c7_i1 = 0; c7_i1 < 5; c7_i1++) {
    c7_bv0[c7_i1] = chartInstance->c7_dataWrittenToVector[c7_i1];
  }

  sf_mex_setcell(c7_y, 7, c7_f_emlrt_marshallOut(chartInstance, c7_bv0));
  return c7_y;
}

static const mxArray *c7_f_emlrt_marshallOut
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c7_u[5])
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 11, 0U, 1U, 0U, 1, 5), false);
  return c7_y;
}

static void c7_i_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u)
{
  boolean_T c7_bv1[5];
  int32_T c7_i2;
  *chartInstance->c7_I_C = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_C", c7_u, 0)), "I_C");
  *chartInstance->c7_I_in = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c7_u, 1)), "I_in");
  chartInstance->c7_I_max = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_max", c7_u, 2)), "I_max");
  chartInstance->c7_K = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("K", c7_u, 3)), "K");
  chartInstance->c7_R = c7_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("R", c7_u, 4)), "R");
  chartInstance->c7_is_active_c7_Integrated_system_V061 = c7_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c7_Integrated_system_V061", c7_u, 5)),
     "is_active_c7_Integrated_system_V061");
  chartInstance->c7_is_c7_Integrated_system_V061 = c7_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c7_Integrated_system_V061",
       c7_u, 6)), "is_c7_Integrated_system_V061");
  c7_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c7_u, 7)), "dataWrittenToVector", c7_bv1);
  for (c7_i2 = 0; c7_i2 < 5; c7_i2++) {
    chartInstance->c7_dataWrittenToVector[c7_i2] = c7_bv1[c7_i2];
  }

  sf_mex_assign(&chartInstance->c7_setSimStateSideEffectsInfo,
                c7_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c7_u, 8)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c7_u);
}

static void c7_j_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_b_dataWrittenToVector, const char_T
  *c7_identifier, boolean_T c7_y[5])
{
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  c7_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_dataWrittenToVector),
                        &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_dataWrittenToVector);
}

static void c7_k_emlrt_marshallIn(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  boolean_T c7_y[5])
{
  boolean_T c7_bv2[5];
  int32_T c7_i3;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_bv2, 1, 11, 0U, 1, 0U, 1, 5);
  for (c7_i3 = 0; c7_i3 < 5; c7_i3++) {
    c7_y[c7_i3] = c7_bv2[c7_i3];
  }

  sf_mex_destroy(&c7_u);
}

static const mxArray *c7_l_emlrt_marshallIn
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c7_b_setSimStateSideEffectsInfo, const char_T *c7_identifier)
{
  const mxArray *c7_y = NULL;
  emlrtMsgIdentifier c7_thisId;
  c7_y = NULL;
  c7_thisId.fIdentifier = c7_identifier;
  c7_thisId.fParent = NULL;
  sf_mex_assign(&c7_y, c7_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_b_setSimStateSideEffectsInfo), &c7_thisId), false);
  sf_mex_destroy(&c7_b_setSimStateSideEffectsInfo);
  return c7_y;
}

static const mxArray *c7_m_emlrt_marshallIn
  (SFc7_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c7_u,
   const emlrtMsgIdentifier *c7_parentId)
{
  const mxArray *c7_y = NULL;
  (void)chartInstance;
  (void)c7_parentId;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_duplicatearraysafe(&c7_u), false);
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void init_dsm_address_info(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc7_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  chartInstance->c7_V_C = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c7_I_C = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c7_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c7_V_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c7_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3109274615U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(4038497191U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(20893822U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3037939926U);
}

mxArray* sf_c7_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c7_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("9kfUwieQLfB2YWGPYROV3F");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c7_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c7_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c7_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c7_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c7_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x8'type','srcId','name','auxInfo'{{M[1],M[34],T\"I_C\",},{M[1],M[33],T\"I_in\",},{M[3],M[46],T\"I_max\",},{M[3],M[35],T\"K\",},{M[3],M[38],T\"R\",},{M[8],M[0],T\"is_active_c7_Integrated_system_V061\",},{M[9],M[0],T\"is_c7_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 8, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           7,
           2,
           3,
           0,
           7,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"I_max");
          _SFD_SET_DATA_PROPS(1,0,0,0,"K");
          _SFD_SET_DATA_PROPS(2,0,0,0,"R");
          _SFD_SET_DATA_PROPS(3,1,1,0,"V_C");
          _SFD_SET_DATA_PROPS(4,1,1,0,"V_in");
          _SFD_SET_DATA_PROPS(5,2,0,1,"I_C");
          _SFD_SET_DATA_PROPS(6,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,37,1,37);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,36,1,36);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,36,-1,2);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)c7_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c7_V_C);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c7_I_C);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c7_I_in);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c7_K);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c7_V_in);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c7_R);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c7_I_max);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sYCLAjyBpfcAzbvnEmWAGxC";
}

static void sf_opaque_initialize_c7_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc7_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c7_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c7_Integrated_system_V061((SFc7_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c7_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c7_Integrated_system_V061((SFc7_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c7_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c7_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c7_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c7_Integrated_system_V061((SFc7_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c7_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c7_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c7_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c7_Integrated_system_V061(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c7_Integrated_system_V061
      ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_Integrated_system_V061
    ((SFc7_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c7_Integrated_system_V061
      ((SFc7_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c7_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,7);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,7,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,7);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,7,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,7,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,7);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3142214119U));
  ssSetChecksum1(S,(4107526563U));
  ssSetChecksum2(S,(1712780545U));
  ssSetChecksum3(S,(3510950343U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c7_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c7_Integrated_system_V061(SimStruct *S)
{
  SFc7_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc7_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc7_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc7_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c7_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c7_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c7_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c7_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c7_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c7_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c7_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c7_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c7_Integrated_system_V061;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c7_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c7_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c7_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c7_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c7_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c7_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
