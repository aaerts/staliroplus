#ifndef __c2_Integrated_system_V061_h__
#define __c2_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_Integrated_system_V061InstanceStruct
#define typedef_SFc2_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  uint8_T c2_tp_Mode3;
  uint8_T c2_tp_Mode1;
  uint8_T c2_tp_Mode2;
  boolean_T c2_isStable;
  boolean_T c2_stateChanged;
  real_T c2_lastMajorTime;
  uint8_T c2_is_active_c2_Integrated_system_V061;
  uint8_T c2_is_c2_Integrated_system_V061;
  real_T c2_l;
  real_T c2_g;
  real_T c2_J;
  real_T c2_m;
  real_T c2_a_max;
  boolean_T c2_dataWrittenToVector[10];
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
  real_T *c2_Tem;
  real_T *c2_w_out;
  real_T *c2_a_out;
  real_T *c2_w;
  real_T *c2_a;
  real_T *c2_lever_trigger;
  real_T *c2_lever_trigger_out;
} SFc2_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc2_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c2_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c2_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
