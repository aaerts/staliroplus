#ifndef __c13_Integrated_system_V061_h__
#define __c13_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc13_Integrated_system_V061InstanceStruct
#define typedef_SFc13_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c13_sfEvent;
  uint8_T c13_tp_Mode1;
  boolean_T c13_isStable;
  boolean_T c13_stateChanged;
  real_T c13_lastMajorTime;
  uint8_T c13_is_active_c13_Integrated_system_V061;
  uint8_T c13_is_c13_Integrated_system_V061;
  real_T c13_K;
  boolean_T c13_dataWrittenToVector[3];
  uint8_T c13_doSetSimStateSideEffects;
  const mxArray *c13_setSimStateSideEffectsInfo;
  real_T *c13_V_in;
  real_T *c13_V_out;
  real_T *c13_I_out;
  real_T *c13_I_in;
} SFc13_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc13_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c13_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c13_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c13_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
