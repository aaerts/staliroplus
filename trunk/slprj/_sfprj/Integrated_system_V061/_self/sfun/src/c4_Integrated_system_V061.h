#ifndef __c4_Integrated_system_V061_h__
#define __c4_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc4_Integrated_system_V061InstanceStruct
#define typedef_SFc4_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c4_sfEvent;
  uint8_T c4_tp_Mode1;
  boolean_T c4_isStable;
  boolean_T c4_stateChanged;
  real_T c4_lastMajorTime;
  uint8_T c4_is_active_c4_Integrated_system_V061;
  uint8_T c4_is_c4_Integrated_system_V061;
  real_T c4_R;
  real_T c4_V_max;
  real_T c4_V_offset;
  real_T c4_V_range;
  boolean_T c4_dataWrittenToVector[6];
  uint8_T c4_doSetSimStateSideEffects;
  const mxArray *c4_setSimStateSideEffectsInfo;
  real_T *c4_I_in;
  real_T *c4_capacitor_charge;
  real_T *c4_V_C;
} SFc4_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc4_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c4_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c4_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c4_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
