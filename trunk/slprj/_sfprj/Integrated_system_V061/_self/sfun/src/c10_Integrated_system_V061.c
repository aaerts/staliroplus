/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c10_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c10_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c10_IN_Mode1                   ((uint8_T)1U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c10_debug_family_names[2] = { "nargin", "nargout" };

static const char * c10_b_debug_family_names[2] = { "nargin", "nargout" };

/* Function Declarations */
static void initialize_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void c10_update_debugger_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_st);
static void c10_set_sim_state_side_effects_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void c10_eml_ini_fcn_to_be_inlined_134
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static void c10_eml_term_fcn_to_be_inlined_134
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c10_mrdivide(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_A, real_T c10_B);
static real_T c10_rdivide(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_x, real_T c10_y);
static void c10_isBuiltInNumeric(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c10_eml_scalexp_compatible
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c10_eml_div(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_x, real_T c10_y);
static real_T c10_div(SFc10_Integrated_system_V061InstanceStruct *chartInstance,
                      real_T c10_x, real_T c10_y);
static void init_script_number_translation(uint32_T c10_machineNumber, uint32_T
  c10_chartNumber, uint32_T c10_instanceNumber);
static const mxArray *c10_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const real_T c10_u);
static const mxArray *c10_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static real_T c10_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_nargout, const char_T *c10_identifier);
static real_T c10_b_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_b_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c10_u);
static const mxArray *c10_b_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static int32_T c10_c_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_sfEvent, const char_T *c10_identifier);
static int32_T c10_d_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_c_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c10_u);
static const mxArray *c10_c_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData);
static uint8_T c10_e_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_tp_Mode1, const char_T *c10_identifier);
static uint8_T c10_f_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void c10_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData);
static const mxArray *c10_d_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c10_e_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c10_u[4]);
static void c10_g_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u);
static void c10_h_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_dataWrittenToVector, const char_T
  *c10_identifier, boolean_T c10_y[4]);
static void c10_i_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId,
  boolean_T c10_y[4]);
static const mxArray *c10_j_emlrt_marshallIn
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_b_setSimStateSideEffectsInfo, const char_T *c10_identifier);
static const mxArray *c10_k_emlrt_marshallIn
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_u, const emlrtMsgIdentifier *c10_parentId);
static void init_dsm_address_info(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc10_Integrated_system_V061InstanceStruct *
  chartInstance);

/* Function Definitions */
static void initialize_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c10_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c10_doSetSimStateSideEffects = 0U;
  chartInstance->c10_setSimStateSideEffectsInfo = NULL;
  chartInstance->c10_tp_Mode1 = 0U;
  chartInstance->c10_is_active_c10_Integrated_system_V061 = 0U;
  chartInstance->c10_is_c10_Integrated_system_V061 = c10_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c10_update_debugger_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c10_prevAniVal;
  c10_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c10_is_active_c10_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 9U, chartInstance->c10_sfEvent);
  }

  if (chartInstance->c10_is_c10_Integrated_system_V061 == c10_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c10_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c10_sfEvent);
  }

  _SFD_SET_ANIMATION(c10_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c10_st = NULL;
  c10_st = NULL;
  sf_mex_assign(&c10_st, c10_d_emlrt_marshallOut(chartInstance), false);
  return c10_st;
}

static void set_sim_state_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_st)
{
  c10_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_st));
  chartInstance->c10_doSetSimStateSideEffects = 1U;
  c10_update_debugger_state_c10_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c10_st);
}

static void c10_set_sim_state_side_effects_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c10_doSetSimStateSideEffects != 0) {
    chartInstance->c10_tp_Mode1 = (uint8_T)
      (chartInstance->c10_is_c10_Integrated_system_V061 == c10_IN_Mode1);
    chartInstance->c10_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c10_setSimStateSideEffectsInfo);
}

static void sf_gateway_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c10_debug_family_var_map[2];
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 0.0;
  real_T c10_b_nargin = 0.0;
  real_T c10_b_nargout = 0.0;
  c10_set_sim_state_side_effects_c10_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c10_lastMajorTime = _sfTime_;
    chartInstance->c10_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 9U, chartInstance->c10_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c10_a_max, 2U, 1U, 0U,
                          chartInstance->c10_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c10_V_offset, 0U, 1U, 0U,
                          chartInstance->c10_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c10_V_range, 1U, 1U, 0U,
                          chartInstance->c10_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c10_a_lever, 3U, 1U, 0U,
                          chartInstance->c10_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c10_lever_angle, 4U, 1U, 0U,
                          chartInstance->c10_sfEvent, false);
    chartInstance->c10_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 9U, chartInstance->c10_sfEvent);
    if (chartInstance->c10_is_active_c10_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 9U,
                   chartInstance->c10_sfEvent);
      chartInstance->c10_stateChanged = true;
      chartInstance->c10_is_active_c10_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c10_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c10_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_b_debug_family_names,
        c10_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 0U, c10_sf_marshallOut,
        c10_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 1U, c10_sf_marshallOut,
        c10_sf_marshallIn);
      chartInstance->c10_V_offset = 0.0;
      chartInstance->c10_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c10_V_offset, 0U, 5U, 0U,
                            chartInstance->c10_sfEvent, false);
      chartInstance->c10_V_range = 5.0;
      chartInstance->c10_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c10_V_range, 1U, 5U, 0U,
                            chartInstance->c10_sfEvent, false);
      chartInstance->c10_a_max = 90.0;
      chartInstance->c10_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c10_a_max, 2U, 5U, 0U,
                            chartInstance->c10_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c10_stateChanged = true;
      chartInstance->c10_is_c10_Integrated_system_V061 = c10_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c10_sfEvent);
      chartInstance->c10_tp_Mode1 = 1U;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                   chartInstance->c10_sfEvent);
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c10_sfEvent);
    if (chartInstance->c10_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_debug_family_names,
    c10_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_b_nargin, 0U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_b_nargout, 1U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  if (!chartInstance->c10_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  *chartInstance->c10_lever_angle = chartInstance->c10_V_offset + c10_mrdivide
    (chartInstance, *chartInstance->c10_a_lever, chartInstance->c10_a_max) *
    chartInstance->c10_V_range;
  chartInstance->c10_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c10_lever_angle, 4U, 4U, 0U,
                        chartInstance->c10_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  real_T *c10_zcVar;
  c10_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c10_lastMajorTime == _sfTime_) {
    *c10_zcVar = -1.0;
  } else {
    chartInstance->c10_stateChanged = (boolean_T)0;
    if (chartInstance->c10_is_active_c10_Integrated_system_V061 == 0U) {
      chartInstance->c10_stateChanged = true;
    }

    if (chartInstance->c10_stateChanged) {
      *c10_zcVar = 1.0;
    } else {
      *c10_zcVar = -1.0;
    }
  }
}

static void derivatives_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c10_debug_family_var_map[2];
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_debug_family_names,
    c10_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 0U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 1U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  if (!chartInstance->c10_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  chartInstance->c10_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c10_lever_angle, 4U, 4U, 0U,
                        chartInstance->c10_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
}

static void outputs_c10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c10_debug_family_var_map[2];
  real_T c10_nargin = 0.0;
  real_T c10_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c10_debug_family_names,
    c10_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargin, 0U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c10_nargout, 1U, c10_sf_marshallOut,
    c10_sf_marshallIn);
  if (!chartInstance->c10_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  if (!chartInstance->c10_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c10_sfEvent,
      false);
  }

  *chartInstance->c10_lever_angle = chartInstance->c10_V_offset + c10_mrdivide
    (chartInstance, *chartInstance->c10_a_lever, chartInstance->c10_a_max) *
    chartInstance->c10_V_range;
  chartInstance->c10_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c10_lever_angle, 4U, 4U, 0U,
                        chartInstance->c10_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c10_sfEvent);
}

static void initSimStructsc10_Integrated_system_V061
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c10_eml_ini_fcn_to_be_inlined_134
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c10_eml_term_fcn_to_be_inlined_134
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c10_mrdivide(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_A, real_T c10_B)
{
  return c10_rdivide(chartInstance, c10_A, c10_B);
}

static real_T c10_rdivide(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_x, real_T c10_y)
{
  return c10_eml_div(chartInstance, c10_x, c10_y);
}

static void c10_isBuiltInNumeric(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c10_eml_scalexp_compatible
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c10_eml_div(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c10_x, real_T c10_y)
{
  return c10_div(chartInstance, c10_x, c10_y);
}

static real_T c10_div(SFc10_Integrated_system_V061InstanceStruct *chartInstance,
                      real_T c10_x, real_T c10_y)
{
  (void)chartInstance;
  return c10_x / c10_y;
}

static void init_script_number_translation(uint32_T c10_machineNumber, uint32_T
  c10_chartNumber, uint32_T c10_instanceNumber)
{
  (void)c10_machineNumber;
  (void)c10_chartNumber;
  (void)c10_instanceNumber;
}

static const mxArray *c10_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const real_T c10_u)
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 0, 0U, 0U, 0U, 0), false);
  return c10_y;
}

static const mxArray *c10_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  sf_mex_assign(&c10_mxArrayOutData, c10_emlrt_marshallOut(chartInstance,
    *(real_T *)c10_inData), false);
  return c10_mxArrayOutData;
}

static real_T c10_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_nargout, const char_T *c10_identifier)
{
  real_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_nargout),
    &c10_thisId);
  sf_mex_destroy(&c10_nargout);
  return c10_y;
}

static real_T c10_b_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  real_T c10_y;
  real_T c10_d0;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_d0, 1, 0, 0U, 0, 0U, 0);
  c10_y = c10_d0;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(real_T *)c10_outData = c10_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_mxArrayInData), c10_varName);
  sf_mex_destroy(&c10_mxArrayInData);
}

const mxArray *sf_c10_Integrated_system_V061_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c10_nameCaptureInfo = NULL;
  c10_nameCaptureInfo = NULL;
  sf_mex_assign(&c10_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c10_nameCaptureInfo;
}

static const mxArray *c10_b_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c10_u)
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 6, 0U, 0U, 0U, 0), false);
  return c10_y;
}

static const mxArray *c10_b_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  sf_mex_assign(&c10_mxArrayOutData, c10_b_emlrt_marshallOut(chartInstance,
    *(int32_T *)c10_inData), false);
  return c10_mxArrayOutData;
}

static int32_T c10_c_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_sfEvent, const char_T *c10_identifier)
{
  int32_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_b_sfEvent),
    &c10_thisId);
  sf_mex_destroy(&c10_b_sfEvent);
  return c10_y;
}

static int32_T c10_d_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  int32_T c10_y;
  int32_T c10_i0;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_i0, 1, 6, 0U, 0, 0U, 0);
  c10_y = c10_i0;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(int32_T *)c10_outData = c10_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_mxArrayInData), c10_varName);
  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_c_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c10_u)
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", &c10_u, 3, 0U, 0U, 0U, 0), false);
  return c10_y;
}

static const mxArray *c10_c_sf_marshallOut(void *chartInstanceVoid, void
  *c10_inData)
{
  const mxArray *c10_mxArrayOutData = NULL;
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c10_mxArrayOutData = NULL;
  sf_mex_assign(&c10_mxArrayOutData, c10_c_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c10_inData), false);
  return c10_mxArrayOutData;
}

static uint8_T c10_e_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_tp_Mode1, const char_T *c10_identifier)
{
  uint8_T c10_y;
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_y = c10_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_b_tp_Mode1),
    &c10_thisId);
  sf_mex_destroy(&c10_b_tp_Mode1);
  return c10_y;
}

static uint8_T c10_f_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  uint8_T c10_y;
  uint8_T c10_u0;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), &c10_u0, 1, 3, 0U, 0, 0U, 0);
  c10_y = c10_u0;
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void c10_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c10_mxArrayInData, const char_T *c10_varName, void *c10_outData)
{
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(uint8_T *)c10_outData = c10_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_mxArrayInData), c10_varName);
  sf_mex_destroy(&c10_mxArrayInData);
}

static const mxArray *c10_d_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c10_y;
  int32_T c10_i1;
  boolean_T c10_bv0[4];
  c10_y = NULL;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_createcellmatrix(7, 1), false);
  sf_mex_setcell(c10_y, 0, c10_emlrt_marshallOut(chartInstance,
    *chartInstance->c10_lever_angle));
  sf_mex_setcell(c10_y, 1, c10_emlrt_marshallOut(chartInstance,
    chartInstance->c10_V_offset));
  sf_mex_setcell(c10_y, 2, c10_emlrt_marshallOut(chartInstance,
    chartInstance->c10_V_range));
  sf_mex_setcell(c10_y, 3, c10_emlrt_marshallOut(chartInstance,
    chartInstance->c10_a_max));
  sf_mex_setcell(c10_y, 4, c10_c_emlrt_marshallOut(chartInstance,
    chartInstance->c10_is_active_c10_Integrated_system_V061));
  sf_mex_setcell(c10_y, 5, c10_c_emlrt_marshallOut(chartInstance,
    chartInstance->c10_is_c10_Integrated_system_V061));
  for (c10_i1 = 0; c10_i1 < 4; c10_i1++) {
    c10_bv0[c10_i1] = chartInstance->c10_dataWrittenToVector[c10_i1];
  }

  sf_mex_setcell(c10_y, 6, c10_e_emlrt_marshallOut(chartInstance, c10_bv0));
  return c10_y;
}

static const mxArray *c10_e_emlrt_marshallOut
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c10_u[4])
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_create("y", c10_u, 11, 0U, 1U, 0U, 1, 4), false);
  return c10_y;
}

static void c10_g_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u)
{
  boolean_T c10_bv1[4];
  int32_T c10_i2;
  *chartInstance->c10_lever_angle = c10_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("lever_angle", c10_u, 0)), "lever_angle");
  chartInstance->c10_V_offset = c10_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_offset", c10_u, 1)), "V_offset");
  chartInstance->c10_V_range = c10_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_range", c10_u, 2)), "V_range");
  chartInstance->c10_a_max = c10_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("a_max", c10_u, 3)), "a_max");
  chartInstance->c10_is_active_c10_Integrated_system_V061 =
    c10_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c10_Integrated_system_V061", c10_u, 4)),
    "is_active_c10_Integrated_system_V061");
  chartInstance->c10_is_c10_Integrated_system_V061 = c10_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c10_Integrated_system_V061",
       c10_u, 5)), "is_c10_Integrated_system_V061");
  c10_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c10_u, 6)), "dataWrittenToVector", c10_bv1);
  for (c10_i2 = 0; c10_i2 < 4; c10_i2++) {
    chartInstance->c10_dataWrittenToVector[c10_i2] = c10_bv1[c10_i2];
  }

  sf_mex_assign(&chartInstance->c10_setSimStateSideEffectsInfo,
                c10_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c10_u, 7)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c10_u);
}

static void c10_h_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_b_dataWrittenToVector, const char_T
  *c10_identifier, boolean_T c10_y[4])
{
  emlrtMsgIdentifier c10_thisId;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  c10_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c10_b_dataWrittenToVector),
    &c10_thisId, c10_y);
  sf_mex_destroy(&c10_b_dataWrittenToVector);
}

static void c10_i_emlrt_marshallIn(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c10_u, const emlrtMsgIdentifier *c10_parentId,
  boolean_T c10_y[4])
{
  boolean_T c10_bv2[4];
  int32_T c10_i3;
  (void)chartInstance;
  sf_mex_import(c10_parentId, sf_mex_dup(c10_u), c10_bv2, 1, 11, 0U, 1, 0U, 1, 4);
  for (c10_i3 = 0; c10_i3 < 4; c10_i3++) {
    c10_y[c10_i3] = c10_bv2[c10_i3];
  }

  sf_mex_destroy(&c10_u);
}

static const mxArray *c10_j_emlrt_marshallIn
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_b_setSimStateSideEffectsInfo, const char_T *c10_identifier)
{
  const mxArray *c10_y = NULL;
  emlrtMsgIdentifier c10_thisId;
  c10_y = NULL;
  c10_thisId.fIdentifier = c10_identifier;
  c10_thisId.fParent = NULL;
  sf_mex_assign(&c10_y, c10_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c10_b_setSimStateSideEffectsInfo), &c10_thisId), false);
  sf_mex_destroy(&c10_b_setSimStateSideEffectsInfo);
  return c10_y;
}

static const mxArray *c10_k_emlrt_marshallIn
  (SFc10_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c10_u, const emlrtMsgIdentifier *c10_parentId)
{
  const mxArray *c10_y = NULL;
  (void)chartInstance;
  (void)c10_parentId;
  c10_y = NULL;
  sf_mex_assign(&c10_y, sf_mex_duplicatearraysafe(&c10_u), false);
  sf_mex_destroy(&c10_u);
  return c10_y;
}

static void init_dsm_address_info(SFc10_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc10_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  chartInstance->c10_lever_angle = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c10_a_lever = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c10_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(707329110U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1873222973U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2626973509U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2468233768U);
}

mxArray* sf_c10_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c10_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("cg6ZzK62ipV9xOt2Mm59WC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c10_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c10_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c10_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c10_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c10_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c10_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x7'type','srcId','name','auxInfo'{{M[1],M[10],T\"lever_angle\",},{M[3],M[26],T\"V_offset\",},{M[3],M[21],T\"V_range\",},{M[3],M[27],T\"a_max\",},{M[8],M[0],T\"is_active_c10_Integrated_system_V061\",},{M[9],M[0],T\"is_c10_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 7, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c10_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc10_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           10,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"V_offset");
          _SFD_SET_DATA_PROPS(1,0,0,0,"V_range");
          _SFD_SET_DATA_PROPS(2,0,0,0,"a_max");
          _SFD_SET_DATA_PROPS(3,1,1,0,"a_lever");
          _SFD_SET_DATA_PROPS(4,2,0,1,"lever_angle");
          _SFD_STATE_INFO(0,0,0);
          _SFD_CH_SUBSTATE_COUNT(1);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_ST_SUBSTATE_COUNT(0,0);
        }

        _SFD_CV_INIT_CHART(1,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_sf_marshallOut,(MexInFcnForType)c10_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_sf_marshallOut,(MexInFcnForType)c10_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_sf_marshallOut,(MexInFcnForType)c10_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c10_sf_marshallOut,(MexInFcnForType)c10_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc10_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c10_lever_angle);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c10_a_lever);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c10_V_range);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c10_V_offset);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c10_a_max);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "s6MrrgAEtKGgXn5BPCfCUdH";
}

static void sf_opaque_initialize_c10_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc10_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c10_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c10_Integrated_system_V061((SFc10_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c10_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c10_Integrated_system_V061((SFc10_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c10_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c10_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c10_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c10_Integrated_system_V061((SFc10_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c10_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c10_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c10_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c10_Integrated_system_V061(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc10_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c10_Integrated_system_V061
      ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc10_Integrated_system_V061
    ((SFc10_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c10_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c10_Integrated_system_V061
      ((SFc10_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c10_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      10);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,10,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,10);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,10,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,10,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,10);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1944476725U));
  ssSetChecksum1(S,(1995837394U));
  ssSetChecksum2(S,(2159622227U));
  ssSetChecksum3(S,(2671647551U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c10_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c10_Integrated_system_V061(SimStruct *S)
{
  SFc10_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc10_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc10_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc10_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c10_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c10_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c10_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c10_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c10_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c10_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c10_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c10_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c10_Integrated_system_V061;
  chartInstance->chartInfo.outputs =
    sf_opaque_outputs_c10_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c10_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c10_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c10_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c10_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c10_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c10_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c10_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c10_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c10_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
