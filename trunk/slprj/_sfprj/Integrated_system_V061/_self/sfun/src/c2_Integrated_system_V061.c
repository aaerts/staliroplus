/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c2_Integrated_system_V061.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c2_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c2_IN_Mode1                    ((uint8_T)1U)
#define c2_IN_Mode2                    ((uint8_T)2U)
#define c2_IN_Mode3                    ((uint8_T)3U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c2_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_e_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_j_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_k_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_m_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void c2_update_debugger_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c2_st);
static void c2_set_sim_state_side_effects_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void c2_eml_ini_fcn_to_be_inlined_38
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static void c2_eml_term_fcn_to_be_inlined_38
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c2_mrdivide(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_A, real_T c2_B);
static real_T c2_rdivide(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x, real_T c2_y);
static void c2_isBuiltInNumeric(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c2_eml_scalexp_compatible(SFc2_Integrated_system_V061InstanceStruct *
  chartInstance);
static real_T c2_eml_div(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x, real_T c2_y);
static real_T c2_div(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c2_x, real_T c2_y);
static real_T c2_cos(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c2_x);
static real_T c2_eml_scalar_cos(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber);
static const mxArray *c2_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const real_T c2_u);
static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData);
static real_T c2_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_nargout, const char_T *c2_identifier);
static real_T c2_b_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_b_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c2_u);
static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static boolean_T c2_c_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_sf_internal_predicateOutput, const char_T
  *c2_identifier);
static boolean_T c2_d_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_c_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c2_u);
static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static int32_T c2_e_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_sfEvent, const char_T *c2_identifier);
static int32_T c2_f_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_d_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c2_u);
static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static uint8_T c2_g_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_tp_Mode3, const char_T *c2_identifier);
static uint8_T c2_h_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_e_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c2_f_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c2_u[10]);
static void c2_i_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u);
static void c2_j_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_dataWrittenToVector, const char_T
  *c2_identifier, boolean_T c2_y[10]);
static void c2_k_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId,
  boolean_T c2_y[10]);
static const mxArray *c2_l_emlrt_marshallIn
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c2_b_setSimStateSideEffectsInfo, const char_T *c2_identifier);
static const mxArray *c2_m_emlrt_marshallIn
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c2_u,
   const emlrtMsgIdentifier *c2_parentId);
static void c2_b_cos(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T *c2_x);
static void c2_b_eml_scalar_cos(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T *c2_x);
static void init_dsm_address_info(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_doSetSimStateSideEffects = 0U;
  chartInstance->c2_setSimStateSideEffectsInfo = NULL;
  chartInstance->c2_tp_Mode1 = 0U;
  chartInstance->c2_tp_Mode2 = 0U;
  chartInstance->c2_tp_Mode3 = 0U;
  chartInstance->c2_is_active_c2_Integrated_system_V061 = 0U;
  chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_lever_trigger = 0.0;
}

static void initialize_params_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c2_update_debugger_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c2_prevAniVal;
  c2_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c2_is_active_c2_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
  }

  if (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
  }

  if (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
  }

  if (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
  }

  _SFD_SET_ANIMATION(c2_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c2_st = NULL;
  c2_st = NULL;
  sf_mex_assign(&c2_st, c2_e_emlrt_marshallOut(chartInstance), false);
  return c2_st;
}

static void set_sim_state_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c2_st)
{
  c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_st));
  chartInstance->c2_doSetSimStateSideEffects = 1U;
  c2_update_debugger_state_c2_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void c2_set_sim_state_side_effects_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c2_doSetSimStateSideEffects != 0) {
    chartInstance->c2_tp_Mode1 = (uint8_T)
      (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode1);
    chartInstance->c2_tp_Mode2 = (uint8_T)
      (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode2);
    chartInstance->c2_tp_Mode3 = (uint8_T)
      (chartInstance->c2_is_c2_Integrated_system_V061 == c2_IN_Mode3);
    chartInstance->c2_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c2_setSimStateSideEffectsInfo);
}

static void sf_gateway_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  uint32_T c2_b_debug_family_var_map[3];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_d_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_e_nargin = 0.0;
  real_T c2_e_nargout = 0.0;
  real_T c2_f_nargin = 0.0;
  real_T c2_f_nargout = 1.0;
  boolean_T c2_c_out;
  real_T c2_d0;
  real_T c2_d1;
  real_T c2_g_nargin = 0.0;
  real_T c2_g_nargout = 1.0;
  boolean_T c2_d_out;
  real_T c2_d2;
  real_T c2_h_nargin = 0.0;
  real_T c2_h_nargout = 0.0;
  real_T c2_i_nargin = 0.0;
  real_T c2_i_nargout = 0.0;
  real_T c2_j_nargin = 0.0;
  real_T c2_j_nargout = 0.0;
  real_T c2_k_nargin = 0.0;
  real_T c2_k_nargout = 0.0;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  c2_set_sim_state_side_effects_c2_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c2_lastMajorTime = _sfTime_;
    chartInstance->c2_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger, 10U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c2_a_max, 2U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a, 1U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c2_m, 5U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c2_J, 0U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c2_g, 3U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w, 6U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Tem, 7U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c2_l, 4U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
    chartInstance->c2_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    if (chartInstance->c2_is_active_c2_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
      chartInstance->c2_stateChanged = true;
      chartInstance->c2_is_active_c2_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_d_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      *chartInstance->c2_w = 0.0;
      chartInstance->c2_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w, 6U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      *chartInstance->c2_a = 0.0;
      chartInstance->c2_dataWrittenToVector[7U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a, 1U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      chartInstance->c2_m = 1.0;
      chartInstance->c2_dataWrittenToVector[6U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c2_m, 5U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      chartInstance->c2_g = 9.81;
      chartInstance->c2_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c2_g, 3U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      chartInstance->c2_J = 1.0;
      chartInstance->c2_dataWrittenToVector[5U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c2_J, 0U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      chartInstance->c2_l = 0.5;
      chartInstance->c2_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c2_l, 4U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      chartInstance->c2_a_max = 90.0;
      chartInstance->c2_dataWrittenToVector[8U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c2_a_max, 2U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      *chartInstance->c2_lever_trigger = 0.0;
      chartInstance->c2_dataWrittenToVector[9U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger, 10U, 5U, 0U,
                            chartInstance->c2_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c2_stateChanged = true;
      chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_Mode3;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
      chartInstance->c2_tp_Mode3 = 1U;
    } else {
      switch (chartInstance->c2_is_c2_Integrated_system_V061) {
       case c2_IN_Mode1:
        CV_CHART_EVAL(1, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c2_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_e_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c2_sfEvent, false);
        }

        guard2 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c2_a * 57.295779513082323 <=
                        0.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c2_w < 0.0)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c2_out = true;
          } else {
            guard2 = true;
          }
        } else {
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 5U, 1U,
              chartInstance->c2_sfEvent, false);
          }

          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c2_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c2_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_f_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_a = 0.0;
          chartInstance->c2_dataWrittenToVector[7U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a, 1U, 5U, 1U,
                                chartInstance->c2_sfEvent, false);
          *chartInstance->c2_w = 0.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w, 6U, 5U, 1U,
                                chartInstance->c2_sfEvent, false);
          *chartInstance->c2_lever_trigger = 1.0;
          chartInstance->c2_dataWrittenToVector[9U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger, 10U, 5U, 1U,
                                chartInstance->c2_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c2_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
          chartInstance->c2_stateChanged = true;
          chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
          chartInstance->c2_tp_Mode3 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                       chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_j_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          if (!chartInstance->c2_dataWrittenToVector[7U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
              chartInstance->c2_sfEvent, false);
          }

          if (!chartInstance->c2_dataWrittenToVector[8U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
              chartInstance->c2_sfEvent, false);
          }

          guard1 = false;
          if (CV_EML_COND(2, 0, 0, *chartInstance->c2_a * 57.295779513082323 >=
                          chartInstance->c2_a_max)) {
            if (CV_EML_COND(2, 0, 1, *chartInstance->c2_w > 0.0)) {
              CV_EML_MCDC(2, 0, 0, true);
              CV_EML_IF(2, 0, 0, true);
              c2_b_out = true;
            } else {
              guard1 = true;
            }
          } else {
            if (!chartInstance->c2_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 5U, 2U,
                chartInstance->c2_sfEvent, false);
            }

            guard1 = true;
          }

          if (guard1 == true) {
            CV_EML_MCDC(2, 0, 0, false);
            CV_EML_IF(2, 0, 0, false);
            c2_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c2_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_k_debug_family_names,
              c2_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            if (!chartInstance->c2_dataWrittenToVector[8U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
                chartInstance->c2_sfEvent, false);
            }

            *chartInstance->c2_a = c2_mrdivide(chartInstance,
              chartInstance->c2_a_max, 360.0) * 6.2831853071795862;
            chartInstance->c2_dataWrittenToVector[7U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a, 1U, 5U, 2U,
                                  chartInstance->c2_sfEvent, false);
            *chartInstance->c2_w = 0.0;
            chartInstance->c2_dataWrittenToVector[3U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w, 6U, 5U, 2U,
                                  chartInstance->c2_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c2_tp_Mode1 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
            chartInstance->c2_stateChanged = true;
            chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_Mode2;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
            chartInstance->c2_tp_Mode2 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
          }
        }
        break;

       case c2_IN_Mode2:
        CV_CHART_EVAL(1, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c2_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                     chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_g_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        c2_d0 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d0);
        c2_d1 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d1);
        c2_c_out = CV_EML_IF(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0, c2_mrdivide
          (chartInstance, 1.0, chartInstance->c2_J) * (*chartInstance->c2_Tem -
          chartInstance->c2_m * chartInstance->c2_g * chartInstance->c2_l *
          c2_d0), 0.0, -1, 2U, c2_mrdivide(chartInstance, 1.0,
          chartInstance->c2_J) * (*chartInstance->c2_Tem - chartInstance->c2_m *
          chartInstance->c2_g * chartInstance->c2_l * c2_d1) < 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c2_sfEvent);
          chartInstance->c2_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c2_sfEvent);
          chartInstance->c2_stateChanged = true;
          chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
          chartInstance->c2_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
        }
        break;

       case c2_IN_Mode3:
        CV_CHART_EVAL(1, 0, 3);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c2_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                     chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_h_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        c2_d2 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d2);
        c2_d_out = CV_EML_IF(3, 0, 0, c2_mrdivide(chartInstance, 1.0,
          chartInstance->c2_J) * (*chartInstance->c2_Tem - chartInstance->c2_m *
          chartInstance->c2_g * chartInstance->c2_l * c2_d2) > 0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_i_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_lever_trigger = 0.0;
          chartInstance->c2_dataWrittenToVector[9U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger, 10U, 5U, 3U,
                                chartInstance->c2_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c2_tp_Mode3 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c2_sfEvent);
          chartInstance->c2_stateChanged = true;
          chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
          chartInstance->c2_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(1, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    if (chartInstance->c2_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c2_is_c2_Integrated_system_V061) {
   case c2_IN_Mode1:
    CV_CHART_EVAL(1, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_b_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[6U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode2:
    CV_CHART_EVAL(1, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_c_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_j_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_j_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode3:
    CV_CHART_EVAL(1, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_k_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_k_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    break;

   default:
    CV_CHART_EVAL(1, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
    break;
  }

  if (!chartInstance->c2_dataWrittenToVector[9U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(10U, 1U, 1U, chartInstance->c2_sfEvent,
      false);
  }

  *chartInstance->c2_lever_trigger_out = *chartInstance->c2_lever_trigger;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger_out, 10U, 1U, 1U,
                        chartInstance->c2_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  real_T c2_d3;
  real_T c2_d4;
  real_T c2_d_nargin = 0.0;
  real_T c2_d_nargout = 1.0;
  boolean_T c2_d_out;
  real_T c2_d5;
  real_T *c2_zcVar;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  c2_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c2_lastMajorTime == _sfTime_) {
    *c2_zcVar = -1.0;
  } else {
    chartInstance->c2_stateChanged = (boolean_T)0;
    if (chartInstance->c2_is_active_c2_Integrated_system_V061 == 0U) {
      chartInstance->c2_stateChanged = true;
    } else {
      switch (chartInstance->c2_is_c2_Integrated_system_V061) {
       case c2_IN_Mode1:
        CV_CHART_EVAL(1, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_e_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c2_sfEvent, false);
        }

        guard2 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c2_a * 57.295779513082323 <=
                        0.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c2_w < 0.0)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c2_out = true;
          } else {
            guard2 = true;
          }
        } else {
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 5U, 1U,
              chartInstance->c2_sfEvent, false);
          }

          guard2 = true;
        }

        if (guard2 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c2_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c2_out) {
          chartInstance->c2_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_j_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          if (!chartInstance->c2_dataWrittenToVector[7U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
              chartInstance->c2_sfEvent, false);
          }

          if (!chartInstance->c2_dataWrittenToVector[8U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
              chartInstance->c2_sfEvent, false);
          }

          guard1 = false;
          if (CV_EML_COND(2, 0, 0, *chartInstance->c2_a * 57.295779513082323 >=
                          chartInstance->c2_a_max)) {
            if (CV_EML_COND(2, 0, 1, *chartInstance->c2_w > 0.0)) {
              CV_EML_MCDC(2, 0, 0, true);
              CV_EML_IF(2, 0, 0, true);
              c2_b_out = true;
            } else {
              guard1 = true;
            }
          } else {
            if (!chartInstance->c2_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 5U, 2U,
                chartInstance->c2_sfEvent, false);
            }

            guard1 = true;
          }

          if (guard1 == true) {
            CV_EML_MCDC(2, 0, 0, false);
            CV_EML_IF(2, 0, 0, false);
            c2_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c2_b_out) {
            chartInstance->c2_stateChanged = true;
          }
        }
        break;

       case c2_IN_Mode2:
        CV_CHART_EVAL(1, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_g_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 4U,
            chartInstance->c2_sfEvent, false);
        }

        c2_d3 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d3);
        c2_d4 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d4);
        c2_c_out = CV_EML_IF(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0, c2_mrdivide
          (chartInstance, 1.0, chartInstance->c2_J) * (*chartInstance->c2_Tem -
          chartInstance->c2_m * chartInstance->c2_g * chartInstance->c2_l *
          c2_d3), 0.0, -1, 2U, c2_mrdivide(chartInstance, 1.0,
          chartInstance->c2_J) * (*chartInstance->c2_Tem - chartInstance->c2_m *
          chartInstance->c2_g * chartInstance->c2_l * c2_d4) < 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          chartInstance->c2_stateChanged = true;
        }
        break;

       case c2_IN_Mode3:
        CV_CHART_EVAL(1, 0, 3);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_h_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        if (!chartInstance->c2_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 3U,
            chartInstance->c2_sfEvent, false);
        }

        c2_d5 = *chartInstance->c2_a;
        c2_b_cos(chartInstance, &c2_d5);
        c2_d_out = CV_EML_IF(3, 0, 0, c2_mrdivide(chartInstance, 1.0,
          chartInstance->c2_J) * (*chartInstance->c2_Tem - chartInstance->c2_m *
          chartInstance->c2_g * chartInstance->c2_l * c2_d5) > 0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_d_out) {
          chartInstance->c2_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(1, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c2_stateChanged) {
      *c2_zcVar = 1.0;
    } else {
      *c2_zcVar = -1.0;
    }
  }
}

static void derivatives_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_d6;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 0.0;
  real_T *c2_w_dot;
  real_T *c2_a_dot;
  real_T *c2_lever_trigger_dot;
  c2_lever_trigger_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 2);
  c2_a_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 1);
  c2_w_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 0);
  *c2_w_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c2_w_dot, 6U, 1U, 1U, chartInstance->c2_sfEvent, false);
  *c2_a_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c2_a_dot, 1U, 1U, 1U, chartInstance->c2_sfEvent, false);
  *c2_lever_trigger_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c2_lever_trigger_dot, 10U, 1U, 1U,
                        chartInstance->c2_sfEvent, false);
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c2_is_c2_Integrated_system_V061) {
   case c2_IN_Mode1:
    CV_CHART_EVAL(1, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_b_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[6U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    c2_d6 = *chartInstance->c2_a;
    c2_b_cos(chartInstance, &c2_d6);
    *c2_w_dot = c2_mrdivide(chartInstance, 1.0, chartInstance->c2_J) *
      (*chartInstance->c2_Tem - chartInstance->c2_m * chartInstance->c2_g *
       chartInstance->c2_l * c2_d6);
    _SFD_DATA_RANGE_CHECK(*c2_w_dot, 6U, 4U, 0U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    *c2_a_dot = *chartInstance->c2_w;
    _SFD_DATA_RANGE_CHECK(*c2_a_dot, 1U, 4U, 0U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode2:
    CV_CHART_EVAL(1, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_c_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *c2_w_dot = 0.0;
    _SFD_DATA_RANGE_CHECK(*c2_w_dot, 6U, 4U, 1U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    *c2_a_dot = *chartInstance->c2_w;
    _SFD_DATA_RANGE_CHECK(*c2_a_dot, 1U, 4U, 1U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode3:
    CV_CHART_EVAL(1, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *c2_w_dot = 0.0;
    _SFD_DATA_RANGE_CHECK(*c2_w_dot, 6U, 4U, 2U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    *c2_a_dot = *chartInstance->c2_w;
    _SFD_DATA_RANGE_CHECK(*c2_a_dot, 1U, 4U, 2U, chartInstance->c2_sfEvent,
                          false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    break;

   default:
    CV_CHART_EVAL(1, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
    break;
  }

  if (!chartInstance->c2_dataWrittenToVector[9U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(10U, 1U, 1U, chartInstance->c2_sfEvent,
      false);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger_out, 10U, 1U, 1U,
                        chartInstance->c2_sfEvent, false);
}

static void outputs_c2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c2_is_c2_Integrated_system_V061) {
   case c2_IN_Mode1:
    CV_CHART_EVAL(1, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_b_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[6U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 0U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 0U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode2:
    CV_CHART_EVAL(1, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_c_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 1U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
    break;

   case c2_IN_Mode3:
    CV_CHART_EVAL(1, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(6U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_w_out = *chartInstance->c2_w;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_w_out, 8U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[7U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 2U, chartInstance->c2_sfEvent,
        false);
    }

    *chartInstance->c2_a_out = *chartInstance->c2_a * 57.295779513082323;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_a_out, 9U, 4U, 2U,
                          chartInstance->c2_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c2_sfEvent);
    break;

   default:
    CV_CHART_EVAL(1, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c2_is_c2_Integrated_system_V061 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c2_sfEvent);
    break;
  }

  if (!chartInstance->c2_dataWrittenToVector[9U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(10U, 1U, 1U, chartInstance->c2_sfEvent,
      false);
  }

  *chartInstance->c2_lever_trigger_out = *chartInstance->c2_lever_trigger;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_lever_trigger_out, 10U, 1U, 1U,
                        chartInstance->c2_sfEvent, false);
}

static void initSimStructsc2_Integrated_system_V061
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_eml_ini_fcn_to_be_inlined_38
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_eml_term_fcn_to_be_inlined_38
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c2_mrdivide(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_A, real_T c2_B)
{
  return c2_rdivide(chartInstance, c2_A, c2_B);
}

static real_T c2_rdivide(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x, real_T c2_y)
{
  return c2_eml_div(chartInstance, c2_x, c2_y);
}

static void c2_isBuiltInNumeric(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c2_eml_scalexp_compatible(SFc2_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static real_T c2_eml_div(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x, real_T c2_y)
{
  return c2_div(chartInstance, c2_x, c2_y);
}

static real_T c2_div(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c2_x, real_T c2_y)
{
  (void)chartInstance;
  return c2_x / c2_y;
}

static real_T c2_cos(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c2_x)
{
  real_T c2_b_x;
  c2_b_x = c2_x;
  c2_b_cos(chartInstance, &c2_b_x);
  return c2_b_x;
}

static real_T c2_eml_scalar_cos(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c2_x)
{
  real_T c2_b_x;
  c2_b_x = c2_x;
  c2_b_eml_scalar_cos(chartInstance, &c2_b_x);
  return c2_b_x;
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber)
{
  (void)c2_machineNumber;
  (void)c2_chartNumber;
  (void)c2_instanceNumber;
}

static const mxArray *c2_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const real_T c2_u)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 0, 0U, 0U, 0U, 0), false);
  return c2_y;
}

static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  sf_mex_assign(&c2_mxArrayOutData, c2_emlrt_marshallOut(chartInstance, *(real_T
    *)c2_inData), false);
  return c2_mxArrayOutData;
}

static real_T c2_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_nargout, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  return c2_y;
}

static real_T c2_b_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d7;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d7, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d7;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(real_T *)c2_outData = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_mxArrayInData), c2_varName);
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_b_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c2_u)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 11, 0U, 0U, 0U, 0), false);
  return c2_y;
}

static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  sf_mex_assign(&c2_mxArrayOutData, c2_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c2_inData), false);
  return c2_mxArrayOutData;
}

static boolean_T c2_c_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_sf_internal_predicateOutput, const char_T
  *c2_identifier)
{
  boolean_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_sf_internal_predicateOutput), &c2_thisId);
  sf_mex_destroy(&c2_sf_internal_predicateOutput);
  return c2_y;
}

static boolean_T c2_d_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  boolean_T c2_y;
  boolean_T c2_b0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_b0, 1, 11, 0U, 0, 0U, 0);
  c2_y = c2_b0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c2_outData = c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_mxArrayInData), c2_varName);
  sf_mex_destroy(&c2_mxArrayInData);
}

const mxArray *sf_c2_Integrated_system_V061_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c2_nameCaptureInfo;
}

static const mxArray *c2_c_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c2_u)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 6, 0U, 0U, 0U, 0), false);
  return c2_y;
}

static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  sf_mex_assign(&c2_mxArrayOutData, c2_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c2_inData), false);
  return c2_mxArrayOutData;
}

static int32_T c2_e_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_sfEvent, const char_T *c2_identifier)
{
  int32_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_sfEvent),
    &c2_thisId);
  sf_mex_destroy(&c2_b_sfEvent);
  return c2_y;
}

static int32_T c2_f_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  int32_T c2_y;
  int32_T c2_i0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_i0, 1, 6, 0U, 0, 0U, 0);
  c2_y = c2_i0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c2_outData = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_mxArrayInData), c2_varName);
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_d_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c2_u)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 3, 0U, 0U, 0U, 0), false);
  return c2_y;
}

static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  sf_mex_assign(&c2_mxArrayOutData, c2_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c2_inData), false);
  return c2_mxArrayOutData;
}

static uint8_T c2_g_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_tp_Mode3, const char_T *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_tp_Mode3),
    &c2_thisId);
  sf_mex_destroy(&c2_b_tp_Mode3);
  return c2_y;
}

static uint8_T c2_h_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c2_outData = c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_mxArrayInData), c2_varName);
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_e_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c2_y;
  int32_T c2_i1;
  boolean_T c2_bv0[10];
  c2_y = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(13, 1), false);
  sf_mex_setcell(c2_y, 0, c2_emlrt_marshallOut(chartInstance,
    *chartInstance->c2_a_out));
  sf_mex_setcell(c2_y, 1, c2_emlrt_marshallOut(chartInstance,
    *chartInstance->c2_w_out));
  sf_mex_setcell(c2_y, 2, c2_emlrt_marshallOut(chartInstance,
    chartInstance->c2_J));
  sf_mex_setcell(c2_y, 3, c2_emlrt_marshallOut(chartInstance,
    chartInstance->c2_a_max));
  sf_mex_setcell(c2_y, 4, c2_emlrt_marshallOut(chartInstance,
    chartInstance->c2_g));
  sf_mex_setcell(c2_y, 5, c2_emlrt_marshallOut(chartInstance,
    chartInstance->c2_l));
  sf_mex_setcell(c2_y, 6, c2_emlrt_marshallOut(chartInstance,
    chartInstance->c2_m));
  sf_mex_setcell(c2_y, 7, c2_emlrt_marshallOut(chartInstance,
    *chartInstance->c2_a));
  sf_mex_setcell(c2_y, 8, c2_emlrt_marshallOut(chartInstance,
    *chartInstance->c2_lever_trigger));
  sf_mex_setcell(c2_y, 9, c2_emlrt_marshallOut(chartInstance,
    *chartInstance->c2_w));
  sf_mex_setcell(c2_y, 10, c2_d_emlrt_marshallOut(chartInstance,
    chartInstance->c2_is_active_c2_Integrated_system_V061));
  sf_mex_setcell(c2_y, 11, c2_d_emlrt_marshallOut(chartInstance,
    chartInstance->c2_is_c2_Integrated_system_V061));
  for (c2_i1 = 0; c2_i1 < 10; c2_i1++) {
    c2_bv0[c2_i1] = chartInstance->c2_dataWrittenToVector[c2_i1];
  }

  sf_mex_setcell(c2_y, 12, c2_f_emlrt_marshallOut(chartInstance, c2_bv0));
  return c2_y;
}

static const mxArray *c2_f_emlrt_marshallOut
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c2_u[10])
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_u, 11, 0U, 1U, 0U, 1, 10), false);
  return c2_y;
}

static void c2_i_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u)
{
  boolean_T c2_bv1[10];
  int32_T c2_i2;
  *chartInstance->c2_a_out = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("a_out", c2_u, 0)), "a_out");
  *chartInstance->c2_w_out = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("w_out", c2_u, 1)), "w_out");
  chartInstance->c2_J = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("J", c2_u, 2)), "J");
  chartInstance->c2_a_max = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("a_max", c2_u, 3)), "a_max");
  chartInstance->c2_g = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("g", c2_u, 4)), "g");
  chartInstance->c2_l = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("l", c2_u, 5)), "l");
  chartInstance->c2_m = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("m", c2_u, 6)), "m");
  *chartInstance->c2_a = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("a", c2_u, 7)), "a");
  *chartInstance->c2_lever_trigger = c2_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("lever_trigger", c2_u, 8)), "lever_trigger");
  *chartInstance->c2_w = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("w", c2_u, 9)), "w");
  chartInstance->c2_is_active_c2_Integrated_system_V061 = c2_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c2_Integrated_system_V061", c2_u, 10)),
     "is_active_c2_Integrated_system_V061");
  chartInstance->c2_is_c2_Integrated_system_V061 = c2_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c2_Integrated_system_V061",
       c2_u, 11)), "is_c2_Integrated_system_V061");
  c2_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c2_u, 12)), "dataWrittenToVector", c2_bv1);
  for (c2_i2 = 0; c2_i2 < 10; c2_i2++) {
    chartInstance->c2_dataWrittenToVector[c2_i2] = c2_bv1[c2_i2];
  }

  sf_mex_assign(&chartInstance->c2_setSimStateSideEffectsInfo,
                c2_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c2_u, 13)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c2_u);
}

static void c2_j_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_b_dataWrittenToVector, const char_T
  *c2_identifier, boolean_T c2_y[10])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_dataWrittenToVector),
                        &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_dataWrittenToVector);
}

static void c2_k_emlrt_marshallIn(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId,
  boolean_T c2_y[10])
{
  boolean_T c2_bv2[10];
  int32_T c2_i3;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), c2_bv2, 1, 11, 0U, 1, 0U, 1, 10);
  for (c2_i3 = 0; c2_i3 < 10; c2_i3++) {
    c2_y[c2_i3] = c2_bv2[c2_i3];
  }

  sf_mex_destroy(&c2_u);
}

static const mxArray *c2_l_emlrt_marshallIn
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c2_b_setSimStateSideEffectsInfo, const char_T *c2_identifier)
{
  const mxArray *c2_y = NULL;
  emlrtMsgIdentifier c2_thisId;
  c2_y = NULL;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  sf_mex_assign(&c2_y, c2_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_setSimStateSideEffectsInfo), &c2_thisId), false);
  sf_mex_destroy(&c2_b_setSimStateSideEffectsInfo);
  return c2_y;
}

static const mxArray *c2_m_emlrt_marshallIn
  (SFc2_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c2_u,
   const emlrtMsgIdentifier *c2_parentId)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  (void)c2_parentId;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_duplicatearraysafe(&c2_u), false);
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_b_cos(SFc2_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T *c2_x)
{
  c2_b_eml_scalar_cos(chartInstance, c2_x);
}

static void c2_b_eml_scalar_cos(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance, real_T *c2_x)
{
  (void)chartInstance;
  *c2_x = muDoubleScalarCos(*c2_x);
}

static void init_dsm_address_info(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc2_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  chartInstance->c2_Tem = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_w_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_a_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c2_w = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 0);
  chartInstance->c2_a = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 1);
  chartInstance->c2_lever_trigger = (real_T *)(ssGetContStates_wrapper
    (chartInstance->S) + 2);
  chartInstance->c2_lever_trigger_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c2_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2038693527U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3199295264U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(81298661U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3018256837U);
}

mxArray* sf_c2_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c2_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("TjPN7jtAL3NoZBGqkyJ8dG");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,7,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c2_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c2_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c2_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[11],T\"a_out\",},{M[1],M[10],T\"w_out\",},{M[3],M[20],T\"J\",},{M[3],M[25],T\"a_max\",},{M[3],M[19],T\"g\",},{M[3],M[8],T\"l\",},{M[3],M[21],T\"m\",},{M[5],M[22],T\"a\",},{M[5],M[28],T\"lever_trigger\",},{M[5],M[16],T\"w\",}}",
    "100 S1x3'type','srcId','name','auxInfo'{{M[8],M[0],T\"is_active_c2_Integrated_system_V061\",},{M[9],M[0],T\"is_c2_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 13, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           2,
           3,
           5,
           0,
           11,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"J");
          _SFD_SET_DATA_PROPS(1,0,0,0,"a");
          _SFD_SET_DATA_PROPS(2,0,0,0,"a_max");
          _SFD_SET_DATA_PROPS(3,0,0,0,"g");
          _SFD_SET_DATA_PROPS(4,0,0,0,"l");
          _SFD_SET_DATA_PROPS(5,0,0,0,"m");
          _SFD_SET_DATA_PROPS(6,0,0,0,"w");
          _SFD_SET_DATA_PROPS(7,1,1,0,"Tem");
          _SFD_SET_DATA_PROPS(8,2,0,1,"w_out");
          _SFD_SET_DATA_PROPS(9,2,0,1,"a_out");
          _SFD_SET_DATA_PROPS(10,2,0,1,"lever_trigger");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_CH_SUBSTATE_COUNT(3);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
        }

        _SFD_CV_INIT_CHART(3,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(1,0,0,1,32,1,32);

        {
          static int condStart[] = { 1, 26 };

          static int condEnd[] = { 22, 32 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(1,0,0,1,32,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,1,46,1,46);
        _SFD_CV_INIT_EML_RELATIONAL(4,0,0,2,46,-1,2);
        _SFD_CV_INIT_EML(3,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(3,0,0,1,46,1,46);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(2,0,0,1,36,1,36);

        {
          static int condStart[] = { 2, 31 };

          static int condEnd[] = { 27, 36 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(2,0,0,2,36,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(4U, &chartInstance->c2_l);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c2_Tem);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c2_w_out);
        _SFD_SET_DATA_VALUE_PTR(9U, chartInstance->c2_a_out);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c2_w);
        _SFD_SET_DATA_VALUE_PTR(3U, &chartInstance->c2_g);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c2_J);
        _SFD_SET_DATA_VALUE_PTR(5U, &chartInstance->c2_m);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c2_a);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c2_a_max);
        _SFD_SET_DATA_VALUE_PTR(10U, chartInstance->c2_lever_trigger);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sd0HYjY2KpNS08ZoJ4BOuEE";
}

static void sf_opaque_initialize_c2_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc2_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c2_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c2_Integrated_system_V061((SFc2_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c2_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c2_Integrated_system_V061((SFc2_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c2_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c2_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c2_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c2_Integrated_system_V061((SFc2_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c2_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c2_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c2_Integrated_system_V061(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c2_Integrated_system_V061
      ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_Integrated_system_V061
    ((SFc2_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c2_Integrated_system_V061
      ((SFc2_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c2_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,2,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3558995340U));
  ssSetChecksum1(S,(4038434329U));
  ssSetChecksum2(S,(3652584990U));
  ssSetChecksum3(S,(1766060455U));
  ssSetNumContStates(S,3);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c2_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c2_Integrated_system_V061(SimStruct *S)
{
  SFc2_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc2_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc2_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc2_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c2_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c2_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c2_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c2_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c2_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c2_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c2_Integrated_system_V061;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c2_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c2_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c2_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c2_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c2_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
