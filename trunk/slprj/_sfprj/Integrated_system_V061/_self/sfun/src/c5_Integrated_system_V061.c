/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c5_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c5_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c5_IN_Mode1                    ((uint8_T)1U)
#define c5_IN_Mode2                    ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c5_debug_family_names[2] = { "nargin", "nargout" };

static const char * c5_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c5_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c5_d_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c5_e_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c5_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c5_g_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void c5_update_debugger_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c5_st);
static void c5_set_sim_state_side_effects_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void c5_eml_ini_fcn_to_be_inlined_80
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static void c5_eml_term_fcn_to_be_inlined_80
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c5_mrdivide(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_A, real_T c5_B);
static real_T c5_rdivide(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_x, real_T c5_y);
static void c5_isBuiltInNumeric(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c5_eml_scalexp_compatible(SFc5_Integrated_system_V061InstanceStruct *
  chartInstance);
static real_T c5_eml_div(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_x, real_T c5_y);
static real_T c5_div(SFc5_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c5_x, real_T c5_y);
static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber);
static const mxArray *c5_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const real_T c5_u);
static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData);
static real_T c5_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_nargout, const char_T *c5_identifier);
static real_T c5_b_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_b_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c5_u);
static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static boolean_T c5_c_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_sf_internal_predicateOutput, const char_T
  *c5_identifier);
static boolean_T c5_d_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_c_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c5_u);
static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static int32_T c5_e_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_sfEvent, const char_T *c5_identifier);
static int32_T c5_f_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_d_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c5_u);
static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData);
static uint8_T c5_g_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_tp_Mode1, const char_T *c5_identifier);
static uint8_T c5_h_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId);
static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData);
static const mxArray *c5_e_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c5_f_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c5_u[3]);
static void c5_i_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u);
static void c5_j_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_dataWrittenToVector, const char_T
  *c5_identifier, boolean_T c5_y[3]);
static void c5_k_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId,
  boolean_T c5_y[3]);
static const mxArray *c5_l_emlrt_marshallIn
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c5_b_setSimStateSideEffectsInfo, const char_T *c5_identifier);
static const mxArray *c5_m_emlrt_marshallIn
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c5_u,
   const emlrtMsgIdentifier *c5_parentId);
static void init_dsm_address_info(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c5_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c5_doSetSimStateSideEffects = 0U;
  chartInstance->c5_setSimStateSideEffectsInfo = NULL;
  chartInstance->c5_tp_Mode1 = 0U;
  chartInstance->c5_tp_Mode2 = 0U;
  chartInstance->c5_is_active_c5_Integrated_system_V061 = 0U;
  chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c5_update_debugger_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c5_prevAniVal;
  c5_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c5_is_active_c5_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 4U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_Integrated_system_V061 == c5_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
  }

  if (chartInstance->c5_is_c5_Integrated_system_V061 == c5_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
  }

  _SFD_SET_ANIMATION(c5_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c5_st = NULL;
  c5_st = NULL;
  sf_mex_assign(&c5_st, c5_e_emlrt_marshallOut(chartInstance), false);
  return c5_st;
}

static void set_sim_state_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c5_st)
{
  c5_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_st));
  chartInstance->c5_doSetSimStateSideEffects = 1U;
  c5_update_debugger_state_c5_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c5_st);
}

static void c5_set_sim_state_side_effects_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c5_doSetSimStateSideEffects != 0) {
    chartInstance->c5_tp_Mode1 = (uint8_T)
      (chartInstance->c5_is_c5_Integrated_system_V061 == c5_IN_Mode1);
    chartInstance->c5_tp_Mode2 = (uint8_T)
      (chartInstance->c5_is_c5_Integrated_system_V061 == c5_IN_Mode2);
    chartInstance->c5_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c5_setSimStateSideEffectsInfo);
}

static void sf_gateway_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c5_debug_family_var_map[2];
  real_T c5_nargin = 0.0;
  real_T c5_nargout = 0.0;
  uint32_T c5_b_debug_family_var_map[3];
  real_T c5_b_nargin = 0.0;
  real_T c5_b_nargout = 1.0;
  boolean_T c5_out;
  real_T c5_c_nargin = 0.0;
  real_T c5_c_nargout = 1.0;
  boolean_T c5_b_out;
  real_T c5_d_nargin = 0.0;
  real_T c5_d_nargout = 0.0;
  real_T c5_e_nargin = 0.0;
  real_T c5_e_nargout = 0.0;
  c5_set_sim_state_side_effects_c5_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c5_lastMajorTime = _sfTime_;
    chartInstance->c5_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c5_V_trigger, 1U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c5_R, 0U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_V_C, 3U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_V_source, 2U, 1U, 0U,
                          chartInstance->c5_sfEvent, false);
    chartInstance->c5_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
    if (chartInstance->c5_is_active_c5_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
      chartInstance->c5_stateChanged = true;
      chartInstance->c5_is_active_c5_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_c_debug_family_names,
        c5_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
        c5_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
        c5_sf_marshallIn);
      chartInstance->c5_R = 660.0;
      chartInstance->c5_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c5_R, 0U, 5U, 0U,
                            chartInstance->c5_sfEvent, false);
      chartInstance->c5_V_trigger = 0.0;
      chartInstance->c5_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c5_V_trigger, 1U, 5U, 0U,
                            chartInstance->c5_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c5_stateChanged = true;
      chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
      chartInstance->c5_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c5_is_c5_Integrated_system_V061) {
       case c5_IN_Mode1:
        CV_CHART_EVAL(4, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c5_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c5_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c5_d_debug_family_names,
          c5_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargin, 0U, c5_sf_marshallOut,
          c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargout, 1U,
          c5_sf_marshallOut, c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_out, 2U, c5_b_sf_marshallOut,
          c5_b_sf_marshallIn);
        if (!chartInstance->c5_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c5_sfEvent, false);
        }

        c5_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c5_V_source, chartInstance->c5_V_trigger, -1, 3U,
          *chartInstance->c5_V_source <= chartInstance->c5_V_trigger));
        _SFD_SYMBOL_SCOPE_POP();
        if (c5_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
          chartInstance->c5_stateChanged = true;
          chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_Mode2 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
        }
        break;

       case c5_IN_Mode2:
        CV_CHART_EVAL(4, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c5_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c5_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c5_e_debug_family_names,
          c5_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_c_nargin, 0U, c5_sf_marshallOut,
          c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_c_nargout, 1U,
          c5_sf_marshallOut, c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_out, 2U, c5_b_sf_marshallOut,
          c5_b_sf_marshallIn);
        if (!chartInstance->c5_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c5_sfEvent, false);
        }

        c5_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c5_V_source, chartInstance->c5_V_trigger, -1, 4U,
          *chartInstance->c5_V_source > chartInstance->c5_V_trigger));
        _SFD_SYMBOL_SCOPE_POP();
        if (c5_b_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c5_sfEvent);
          chartInstance->c5_stateChanged = true;
          chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
          chartInstance->c5_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(4, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c5_sfEvent);
    if (chartInstance->c5_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c5_is_c5_Integrated_system_V061) {
   case c5_IN_Mode1:
    CV_CHART_EVAL(4, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_d_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_d_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    *chartInstance->c5_I_in = 0.0;
    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    break;

   case c5_IN_Mode2:
    CV_CHART_EVAL(4, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_b_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_e_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_e_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    if (!chartInstance->c5_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c5_sfEvent,
        false);
    }

    *chartInstance->c5_I_in = c5_mrdivide(chartInstance, *chartInstance->c5_V_C,
      chartInstance->c5_R);
    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 1U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    break;

   default:
    CV_CHART_EVAL(4, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c5_debug_family_var_map[3];
  real_T c5_nargin = 0.0;
  real_T c5_nargout = 1.0;
  boolean_T c5_out;
  real_T c5_b_nargin = 0.0;
  real_T c5_b_nargout = 1.0;
  boolean_T c5_b_out;
  real_T *c5_zcVar;
  c5_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c5_lastMajorTime == _sfTime_) {
    *c5_zcVar = -1.0;
  } else {
    chartInstance->c5_stateChanged = (boolean_T)0;
    if (chartInstance->c5_is_active_c5_Integrated_system_V061 == 0U) {
      chartInstance->c5_stateChanged = true;
    } else {
      switch (chartInstance->c5_is_c5_Integrated_system_V061) {
       case c5_IN_Mode1:
        CV_CHART_EVAL(4, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c5_d_debug_family_names,
          c5_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
          c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
          c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_out, 2U, c5_b_sf_marshallOut,
          c5_b_sf_marshallIn);
        if (!chartInstance->c5_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c5_sfEvent, false);
        }

        c5_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c5_V_source, chartInstance->c5_V_trigger, -1, 3U,
          *chartInstance->c5_V_source <= chartInstance->c5_V_trigger));
        _SFD_SYMBOL_SCOPE_POP();
        if (c5_out) {
          chartInstance->c5_stateChanged = true;
        }
        break;

       case c5_IN_Mode2:
        CV_CHART_EVAL(4, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c5_e_debug_family_names,
          c5_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargin, 0U, c5_sf_marshallOut,
          c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargout, 1U,
          c5_sf_marshallOut, c5_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_out, 2U, c5_b_sf_marshallOut,
          c5_b_sf_marshallIn);
        if (!chartInstance->c5_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c5_sfEvent, false);
        }

        c5_b_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c5_V_source, chartInstance->c5_V_trigger, -1, 4U,
          *chartInstance->c5_V_source > chartInstance->c5_V_trigger));
        _SFD_SYMBOL_SCOPE_POP();
        if (c5_b_out) {
          chartInstance->c5_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(4, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c5_stateChanged) {
      *c5_zcVar = 1.0;
    } else {
      *c5_zcVar = -1.0;
    }
  }
}

static void derivatives_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c5_debug_family_var_map[2];
  real_T c5_nargin = 0.0;
  real_T c5_nargout = 0.0;
  real_T c5_b_nargin = 0.0;
  real_T c5_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c5_is_c5_Integrated_system_V061) {
   case c5_IN_Mode1:
    CV_CHART_EVAL(4, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    break;

   case c5_IN_Mode2:
    CV_CHART_EVAL(4, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_b_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    if (!chartInstance->c5_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c5_sfEvent,
        false);
    }

    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 1U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    break;

   default:
    CV_CHART_EVAL(4, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
    break;
  }
}

static void outputs_c5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c5_debug_family_var_map[2];
  real_T c5_nargin = 0.0;
  real_T c5_nargout = 0.0;
  real_T c5_b_nargin = 0.0;
  real_T c5_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c5_is_c5_Integrated_system_V061) {
   case c5_IN_Mode1:
    CV_CHART_EVAL(4, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    *chartInstance->c5_I_in = 0.0;
    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 0U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c5_sfEvent);
    break;

   case c5_IN_Mode2:
    CV_CHART_EVAL(4, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c5_b_debug_family_names,
      c5_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargin, 0U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c5_b_nargout, 1U, c5_sf_marshallOut,
      c5_sf_marshallIn);
    if (!chartInstance->c5_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c5_sfEvent,
        false);
    }

    *chartInstance->c5_I_in = c5_mrdivide(chartInstance, *chartInstance->c5_V_C,
      chartInstance->c5_R);
    chartInstance->c5_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c5_I_in, 4U, 4U, 1U,
                          chartInstance->c5_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c5_sfEvent);
    break;

   default:
    CV_CHART_EVAL(4, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c5_is_c5_Integrated_system_V061 = c5_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c5_sfEvent);
    break;
  }
}

static void initSimStructsc5_Integrated_system_V061
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c5_eml_ini_fcn_to_be_inlined_80
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c5_eml_term_fcn_to_be_inlined_80
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c5_mrdivide(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_A, real_T c5_B)
{
  return c5_rdivide(chartInstance, c5_A, c5_B);
}

static real_T c5_rdivide(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_x, real_T c5_y)
{
  return c5_eml_div(chartInstance, c5_x, c5_y);
}

static void c5_isBuiltInNumeric(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c5_eml_scalexp_compatible(SFc5_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static real_T c5_eml_div(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c5_x, real_T c5_y)
{
  return c5_div(chartInstance, c5_x, c5_y);
}

static real_T c5_div(SFc5_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c5_x, real_T c5_y)
{
  (void)chartInstance;
  return c5_x / c5_y;
}

static void init_script_number_translation(uint32_T c5_machineNumber, uint32_T
  c5_chartNumber, uint32_T c5_instanceNumber)
{
  (void)c5_machineNumber;
  (void)c5_chartNumber;
  (void)c5_instanceNumber;
}

static const mxArray *c5_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const real_T c5_u)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 0, 0U, 0U, 0U, 0), false);
  return c5_y;
}

static const mxArray *c5_sf_marshallOut(void *chartInstanceVoid, void *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  sf_mex_assign(&c5_mxArrayOutData, c5_emlrt_marshallOut(chartInstance, *(real_T
    *)c5_inData), false);
  return c5_mxArrayOutData;
}

static real_T c5_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_nargout, const char_T *c5_identifier)
{
  real_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_nargout), &c5_thisId);
  sf_mex_destroy(&c5_nargout);
  return c5_y;
}

static real_T c5_b_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  real_T c5_y;
  real_T c5_d0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_d0, 1, 0, 0U, 0, 0U, 0);
  c5_y = c5_d0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(real_T *)c5_outData = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_mxArrayInData), c5_varName);
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_b_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c5_u)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 11, 0U, 0U, 0U, 0), false);
  return c5_y;
}

static const mxArray *c5_b_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  sf_mex_assign(&c5_mxArrayOutData, c5_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c5_inData), false);
  return c5_mxArrayOutData;
}

static boolean_T c5_c_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_sf_internal_predicateOutput, const char_T
  *c5_identifier)
{
  boolean_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_sf_internal_predicateOutput), &c5_thisId);
  sf_mex_destroy(&c5_sf_internal_predicateOutput);
  return c5_y;
}

static boolean_T c5_d_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  boolean_T c5_y;
  boolean_T c5_b0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_b0, 1, 11, 0U, 0, 0U, 0);
  c5_y = c5_b0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c5_outData = c5_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_mxArrayInData), c5_varName);
  sf_mex_destroy(&c5_mxArrayInData);
}

const mxArray *sf_c5_Integrated_system_V061_get_eml_resolved_functions_info(void)
{
  const mxArray *c5_nameCaptureInfo = NULL;
  c5_nameCaptureInfo = NULL;
  sf_mex_assign(&c5_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c5_nameCaptureInfo;
}

static const mxArray *c5_c_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c5_u)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 6, 0U, 0U, 0U, 0), false);
  return c5_y;
}

static const mxArray *c5_c_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  sf_mex_assign(&c5_mxArrayOutData, c5_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c5_inData), false);
  return c5_mxArrayOutData;
}

static int32_T c5_e_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_sfEvent, const char_T *c5_identifier)
{
  int32_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_sfEvent),
    &c5_thisId);
  sf_mex_destroy(&c5_b_sfEvent);
  return c5_y;
}

static int32_T c5_f_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  int32_T c5_y;
  int32_T c5_i0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_i0, 1, 6, 0U, 0, 0U, 0);
  c5_y = c5_i0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c5_outData = c5_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_mxArrayInData), c5_varName);
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_d_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c5_u)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", &c5_u, 3, 0U, 0U, 0U, 0), false);
  return c5_y;
}

static const mxArray *c5_d_sf_marshallOut(void *chartInstanceVoid, void
  *c5_inData)
{
  const mxArray *c5_mxArrayOutData = NULL;
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c5_mxArrayOutData = NULL;
  sf_mex_assign(&c5_mxArrayOutData, c5_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c5_inData), false);
  return c5_mxArrayOutData;
}

static uint8_T c5_g_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_tp_Mode1, const char_T *c5_identifier)
{
  uint8_T c5_y;
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_y = c5_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_tp_Mode1),
    &c5_thisId);
  sf_mex_destroy(&c5_b_tp_Mode1);
  return c5_y;
}

static uint8_T c5_h_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId)
{
  uint8_T c5_y;
  uint8_T c5_u0;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), &c5_u0, 1, 3, 0U, 0, 0U, 0);
  c5_y = c5_u0;
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void c5_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c5_mxArrayInData, const char_T *c5_varName, void *c5_outData)
{
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c5_outData = c5_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_mxArrayInData), c5_varName);
  sf_mex_destroy(&c5_mxArrayInData);
}

static const mxArray *c5_e_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c5_y;
  int32_T c5_i1;
  boolean_T c5_bv0[3];
  c5_y = NULL;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_createcellmatrix(6, 1), false);
  sf_mex_setcell(c5_y, 0, c5_emlrt_marshallOut(chartInstance,
    *chartInstance->c5_I_in));
  sf_mex_setcell(c5_y, 1, c5_emlrt_marshallOut(chartInstance,
    chartInstance->c5_R));
  sf_mex_setcell(c5_y, 2, c5_emlrt_marshallOut(chartInstance,
    chartInstance->c5_V_trigger));
  sf_mex_setcell(c5_y, 3, c5_d_emlrt_marshallOut(chartInstance,
    chartInstance->c5_is_active_c5_Integrated_system_V061));
  sf_mex_setcell(c5_y, 4, c5_d_emlrt_marshallOut(chartInstance,
    chartInstance->c5_is_c5_Integrated_system_V061));
  for (c5_i1 = 0; c5_i1 < 3; c5_i1++) {
    c5_bv0[c5_i1] = chartInstance->c5_dataWrittenToVector[c5_i1];
  }

  sf_mex_setcell(c5_y, 5, c5_f_emlrt_marshallOut(chartInstance, c5_bv0));
  return c5_y;
}

static const mxArray *c5_f_emlrt_marshallOut
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c5_u[3])
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_create("y", c5_u, 11, 0U, 1U, 0U, 1, 3), false);
  return c5_y;
}

static void c5_i_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u)
{
  boolean_T c5_bv1[3];
  int32_T c5_i2;
  *chartInstance->c5_I_in = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c5_u, 0)), "I_in");
  chartInstance->c5_R = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("R", c5_u, 1)), "R");
  chartInstance->c5_V_trigger = c5_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_trigger", c5_u, 2)), "V_trigger");
  chartInstance->c5_is_active_c5_Integrated_system_V061 = c5_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c5_Integrated_system_V061", c5_u, 3)),
     "is_active_c5_Integrated_system_V061");
  chartInstance->c5_is_c5_Integrated_system_V061 = c5_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c5_Integrated_system_V061",
       c5_u, 4)), "is_c5_Integrated_system_V061");
  c5_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c5_u, 5)), "dataWrittenToVector", c5_bv1);
  for (c5_i2 = 0; c5_i2 < 3; c5_i2++) {
    chartInstance->c5_dataWrittenToVector[c5_i2] = c5_bv1[c5_i2];
  }

  sf_mex_assign(&chartInstance->c5_setSimStateSideEffectsInfo,
                c5_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c5_u, 6)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c5_u);
}

static void c5_j_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_b_dataWrittenToVector, const char_T
  *c5_identifier, boolean_T c5_y[3])
{
  emlrtMsgIdentifier c5_thisId;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  c5_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c5_b_dataWrittenToVector),
                        &c5_thisId, c5_y);
  sf_mex_destroy(&c5_b_dataWrittenToVector);
}

static void c5_k_emlrt_marshallIn(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c5_u, const emlrtMsgIdentifier *c5_parentId,
  boolean_T c5_y[3])
{
  boolean_T c5_bv2[3];
  int32_T c5_i3;
  (void)chartInstance;
  sf_mex_import(c5_parentId, sf_mex_dup(c5_u), c5_bv2, 1, 11, 0U, 1, 0U, 1, 3);
  for (c5_i3 = 0; c5_i3 < 3; c5_i3++) {
    c5_y[c5_i3] = c5_bv2[c5_i3];
  }

  sf_mex_destroy(&c5_u);
}

static const mxArray *c5_l_emlrt_marshallIn
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c5_b_setSimStateSideEffectsInfo, const char_T *c5_identifier)
{
  const mxArray *c5_y = NULL;
  emlrtMsgIdentifier c5_thisId;
  c5_y = NULL;
  c5_thisId.fIdentifier = c5_identifier;
  c5_thisId.fParent = NULL;
  sf_mex_assign(&c5_y, c5_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c5_b_setSimStateSideEffectsInfo), &c5_thisId), false);
  sf_mex_destroy(&c5_b_setSimStateSideEffectsInfo);
  return c5_y;
}

static const mxArray *c5_m_emlrt_marshallIn
  (SFc5_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c5_u,
   const emlrtMsgIdentifier *c5_parentId)
{
  const mxArray *c5_y = NULL;
  (void)chartInstance;
  (void)c5_parentId;
  c5_y = NULL;
  sf_mex_assign(&c5_y, sf_mex_duplicatearraysafe(&c5_u), false);
  sf_mex_destroy(&c5_u);
  return c5_y;
}

static void init_dsm_address_info(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc5_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  chartInstance->c5_V_source = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c5_V_C = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c5_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c5_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3584792780U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1354988131U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2406641830U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3192674209U);
}

mxArray* sf_c5_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c5_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("MuEvS0oesSHeXiUuLvaXiG");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c5_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c5_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c5_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c5_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c5_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c5_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x6'type','srcId','name','auxInfo'{{M[1],M[28],T\"I_in\",},{M[3],M[29],T\"R\",},{M[3],M[34],T\"V_trigger\",},{M[8],M[0],T\"is_active_c5_Integrated_system_V061\",},{M[9],M[0],T\"is_c5_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 6, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c5_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           5,
           2,
           3,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"R");
          _SFD_SET_DATA_PROPS(1,0,0,0,"V_trigger");
          _SFD_SET_DATA_PROPS(2,1,1,0,"V_source");
          _SFD_SET_DATA_PROPS(3,1,1,0,"V_C");
          _SFD_SET_DATA_PROPS(4,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,23,1,23);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,2,23,-1,3);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,22,1,22);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,22,-1,4);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c5_sf_marshallOut,(MexInFcnForType)c5_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc5_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c5_V_source);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c5_V_C);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c5_I_in);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c5_R);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c5_V_trigger);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sOUu7EDj4IItPMiHWHS6ssE";
}

static void sf_opaque_initialize_c5_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc5_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c5_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c5_Integrated_system_V061((SFc5_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c5_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c5_Integrated_system_V061((SFc5_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c5_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c5_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c5_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c5_Integrated_system_V061((SFc5_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c5_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c5_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c5_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c5_Integrated_system_V061(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc5_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c5_Integrated_system_V061
      ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc5_Integrated_system_V061
    ((SFc5_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c5_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c5_Integrated_system_V061
      ((SFc5_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c5_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,5);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,5,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,5);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,5,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,5,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,5);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1821706065U));
  ssSetChecksum1(S,(3196909785U));
  ssSetChecksum2(S,(4019506915U));
  ssSetChecksum3(S,(1061669821U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c5_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c5_Integrated_system_V061(SimStruct *S)
{
  SFc5_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc5_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc5_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc5_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c5_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c5_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c5_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c5_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c5_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c5_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c5_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c5_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c5_Integrated_system_V061;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c5_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c5_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c5_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c5_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c5_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c5_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c5_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c5_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c5_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c5_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
