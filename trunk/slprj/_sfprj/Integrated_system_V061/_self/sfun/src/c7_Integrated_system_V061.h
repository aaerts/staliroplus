#ifndef __c7_Integrated_system_V061_h__
#define __c7_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc7_Integrated_system_V061InstanceStruct
#define typedef_SFc7_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c7_sfEvent;
  uint8_T c7_tp_Mode1;
  uint8_T c7_tp_Mode2;
  boolean_T c7_isStable;
  boolean_T c7_stateChanged;
  real_T c7_lastMajorTime;
  uint8_T c7_is_active_c7_Integrated_system_V061;
  uint8_T c7_is_c7_Integrated_system_V061;
  real_T c7_K;
  real_T c7_R;
  real_T c7_I_max;
  boolean_T c7_dataWrittenToVector[5];
  uint8_T c7_doSetSimStateSideEffects;
  const mxArray *c7_setSimStateSideEffectsInfo;
  real_T *c7_V_C;
  real_T *c7_I_C;
  real_T *c7_I_in;
  real_T *c7_V_in;
} SFc7_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc7_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c7_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c7_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c7_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
