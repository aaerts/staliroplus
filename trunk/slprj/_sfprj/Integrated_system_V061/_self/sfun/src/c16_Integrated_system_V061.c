/* Include files */

#include "simstruc.h"
#include "fixedpoint.h"
#include "simtarget/slLogLoadBlocksSfcnBridge.h"
#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c16_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c16_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c16_IN_Mode1                   ((uint8_T)1U)
#define c16_IN_Mode10                  ((uint8_T)2U)
#define c16_IN_Mode2                   ((uint8_T)3U)
#define c16_IN_Mode3                   ((uint8_T)4U)
#define c16_IN_Mode4                   ((uint8_T)5U)
#define c16_IN_Mode5                   ((uint8_T)6U)
#define c16_IN_Mode6                   ((uint8_T)7U)
#define c16_IN_Mode7                   ((uint8_T)8U)
#define c16_IN_Mode8                   ((uint8_T)9U)
#define c16_IN_Mode9                   ((uint8_T)10U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c16_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_h_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_j_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_k_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_m_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_o_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_q_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_r_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_t_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_u_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_v_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_w_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_x_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_y_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ab_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_bb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_cb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_db_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_eb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_fb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_gb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_hb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_ib_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_jb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_kb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_lb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_mb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_nb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ob_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_pb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_qb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_rb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_sb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_tb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_ub_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_vb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_wb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_xb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_yb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ac_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_bc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_cc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_dc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ec_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_fc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_gc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_hc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ic_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_jc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_kc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_lc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_mc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_nc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_oc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_pc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_qc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_rc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_sc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_tc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_uc_debug_family_names[2] = { "nargin", "nargout" };

static const char * c16_vc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_wc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_xc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_yc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ad_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_bd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_cd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_dd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ed_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_fd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_gd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_hd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_id_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_jd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_kd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ld_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_md_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_nd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_od_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_pd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_qd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_rd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_sd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_td_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ud_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_vd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_wd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_xd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_yd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ae_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_be_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ce_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_de_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ee_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_fe_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ge_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_he_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_ie_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c16_je_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void c16_update_debugger_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_st);
static void c16_set_sim_state_side_effects_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void c16_eml_ini_fcn_to_be_inlined_181
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void c16_eml_term_fcn_to_be_inlined_181
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c16_machineNumber, uint32_T
  c16_chartNumber, uint32_T c16_instanceNumber);
static const mxArray *c16_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const real_T c16_u);
static const mxArray *c16_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static real_T c16_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_nargout, const char_T *c16_identifier);
static real_T c16_b_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_b_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c16_u);
static const mxArray *c16_b_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static boolean_T c16_c_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_sf_internal_predicateOutput, const char_T *c16_identifier);
static boolean_T c16_d_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_c_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c16_u);
static const mxArray *c16_c_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static int32_T c16_e_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_sfEvent, const char_T *c16_identifier);
static int32_T c16_f_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_d_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c16_u);
static const mxArray *c16_d_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData);
static uint8_T c16_g_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_tp_Mode3, const char_T *c16_identifier);
static uint8_T c16_h_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void c16_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData);
static const mxArray *c16_e_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c16_f_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c16_u[9]);
static void c16_i_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u);
static void c16_j_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_dataWrittenToVector, const char_T
  *c16_identifier, boolean_T c16_y[9]);
static void c16_k_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId,
  boolean_T c16_y[9]);
static const mxArray *c16_l_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_b_setSimStateSideEffectsInfo, const char_T *c16_identifier);
static const mxArray *c16_m_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_u, const emlrtMsgIdentifier *c16_parentId);
static void init_test_point_addr_map(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance);
static void **get_test_point_address_map
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void **get_dataset_logging_obj_vector
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance);
static void init_dsm_address_info(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance);

/* Function Definitions */
static void initialize_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c16_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c16_doSetSimStateSideEffects = 0U;
  chartInstance->c16_setSimStateSideEffectsInfo = NULL;
  chartInstance->c16_tp_Mode1 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode1);
  chartInstance->c16_tp_Mode10 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[10], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode10);
  chartInstance->c16_temporalCounter_i1 = 0.0;
  chartInstance->c16_tp_Mode2 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode2);
  chartInstance->c16_tp_Mode3 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode3);
  chartInstance->c16_tp_Mode4 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode4);
  chartInstance->c16_tp_Mode5 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode5);
  chartInstance->c16_tp_Mode6 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode6);
  chartInstance->c16_tp_Mode7 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode7);
  chartInstance->c16_tp_Mode8 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode8);
  chartInstance->c16_tp_Mode9 = 0U;
  ssLoggerUpdateTimeseries(chartInstance->S,
    chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T *)
    &chartInstance->c16_tp_Mode9);
  chartInstance->c16_is_active_c16_Integrated_system_V061 = 0U;
  chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_NO_ACTIVE_CHILD;
  chartInstance->c16_presentTime = 0.0;
  chartInstance->c16_elapsedTime = 0.0;
  chartInstance->c16_previousTime = 0.0;
}

static void initialize_params_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c16_presentTime = _sfTime_;
  chartInstance->c16_previousTime = chartInstance->c16_presentTime;
}

static void disable_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c16_presentTime = _sfTime_;
  chartInstance->c16_elapsedTime = chartInstance->c16_presentTime -
    chartInstance->c16_previousTime;
  chartInstance->c16_previousTime = chartInstance->c16_presentTime;
  chartInstance->c16_temporalCounter_i1 += chartInstance->c16_elapsedTime;
}

static void c16_update_debugger_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c16_prevAniVal;
  c16_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c16_is_active_c16_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 13U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode4) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode5) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode6) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode10) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode8) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode9) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
  }

  if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode7) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
  }

  _SFD_SET_ANIMATION(c16_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c16_st = NULL;
  c16_st = NULL;
  sf_mex_assign(&c16_st, c16_e_emlrt_marshallOut(chartInstance), false);
  return c16_st;
}

static void set_sim_state_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_st)
{
  c16_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_st));
  chartInstance->c16_doSetSimStateSideEffects = 1U;
  c16_update_debugger_state_c16_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c16_st);
}

static void c16_set_sim_state_side_effects_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c16_doSetSimStateSideEffects != 0) {
    chartInstance->c16_tp_Mode1 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode1);
    if (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode10) {
      chartInstance->c16_tp_Mode10 = 1U;
      if (sf_mex_sub(chartInstance->c16_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 3) == 0.0) {
        chartInstance->c16_temporalCounter_i1 = 0.0;
      }
    } else {
      chartInstance->c16_tp_Mode10 = 0U;
    }

    chartInstance->c16_tp_Mode2 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode2);
    chartInstance->c16_tp_Mode3 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode3);
    chartInstance->c16_tp_Mode4 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode4);
    chartInstance->c16_tp_Mode5 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode5);
    chartInstance->c16_tp_Mode6 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode6);
    chartInstance->c16_tp_Mode7 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode7);
    chartInstance->c16_tp_Mode8 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode8);
    chartInstance->c16_tp_Mode9 = (uint8_T)
      (chartInstance->c16_is_c16_Integrated_system_V061 == c16_IN_Mode9);
    chartInstance->c16_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c16_setSimStateSideEffectsInfo);
}

static void sf_gateway_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c16_debug_family_var_map[2];
  real_T c16_nargin = 0.0;
  real_T c16_nargout = 0.0;
  real_T c16_b_nargin = 0.0;
  real_T c16_b_nargout = 0.0;
  uint32_T c16_b_debug_family_var_map[3];
  real_T c16_c_nargin = 0.0;
  real_T c16_c_nargout = 1.0;
  boolean_T c16_out;
  real_T c16_d_nargin = 0.0;
  real_T c16_d_nargout = 0.0;
  real_T c16_e_nargin = 0.0;
  real_T c16_e_nargout = 1.0;
  boolean_T c16_b_out;
  real_T c16_f_nargin = 0.0;
  real_T c16_f_nargout = 0.0;
  real_T c16_g_nargin = 0.0;
  real_T c16_g_nargout = 0.0;
  real_T c16_h_nargin = 0.0;
  real_T c16_h_nargout = 1.0;
  boolean_T c16_c_out;
  real_T c16_i_nargin = 0.0;
  real_T c16_i_nargout = 0.0;
  real_T c16_j_nargin = 0.0;
  real_T c16_j_nargout = 0.0;
  real_T c16_k_nargin = 0.0;
  real_T c16_k_nargout = 1.0;
  boolean_T c16_d_out;
  real_T c16_l_nargin = 0.0;
  real_T c16_l_nargout = 0.0;
  real_T c16_m_nargin = 0.0;
  real_T c16_m_nargout = 1.0;
  boolean_T c16_e_out;
  real_T c16_n_nargin = 0.0;
  real_T c16_n_nargout = 0.0;
  real_T c16_o_nargin = 0.0;
  real_T c16_o_nargout = 0.0;
  real_T c16_p_nargin = 0.0;
  real_T c16_p_nargout = 1.0;
  boolean_T c16_f_out;
  real_T c16_q_nargin = 0.0;
  real_T c16_q_nargout = 0.0;
  real_T c16_r_nargin = 0.0;
  real_T c16_r_nargout = 0.0;
  real_T c16_s_nargin = 0.0;
  real_T c16_s_nargout = 1.0;
  boolean_T c16_g_out;
  real_T c16_t_nargin = 0.0;
  real_T c16_t_nargout = 0.0;
  real_T c16_u_nargin = 0.0;
  real_T c16_u_nargout = 0.0;
  real_T c16_v_nargin = 0.0;
  real_T c16_v_nargout = 1.0;
  boolean_T c16_h_out;
  real_T c16_w_nargin = 0.0;
  real_T c16_w_nargout = 0.0;
  real_T c16_x_nargin = 0.0;
  real_T c16_x_nargout = 1.0;
  boolean_T c16_i_out;
  real_T c16_y_nargin = 0.0;
  real_T c16_y_nargout = 0.0;
  real_T c16_ab_nargin = 0.0;
  real_T c16_ab_nargout = 0.0;
  real_T c16_bb_nargin = 0.0;
  real_T c16_bb_nargout = 1.0;
  boolean_T c16_j_out;
  real_T c16_cb_nargin = 0.0;
  real_T c16_cb_nargout = 0.0;
  real_T c16_db_nargin = 0.0;
  real_T c16_db_nargout = 1.0;
  boolean_T c16_k_out;
  real_T c16_eb_nargin = 0.0;
  real_T c16_eb_nargout = 0.0;
  real_T c16_fb_nargin = 0.0;
  real_T c16_fb_nargout = 1.0;
  boolean_T c16_l_out;
  real_T c16_gb_nargin = 0.0;
  real_T c16_gb_nargout = 0.0;
  real_T c16_hb_nargin = 0.0;
  real_T c16_hb_nargout = 1.0;
  boolean_T c16_m_out;
  real_T c16_ib_nargin = 0.0;
  real_T c16_ib_nargout = 0.0;
  real_T c16_jb_nargin = 0.0;
  real_T c16_jb_nargout = 1.0;
  boolean_T c16_n_out;
  real_T c16_kb_nargin = 0.0;
  real_T c16_kb_nargout = 0.0;
  real_T c16_lb_nargin = 0.0;
  real_T c16_lb_nargout = 0.0;
  real_T c16_mb_nargin = 0.0;
  real_T c16_mb_nargout = 1.0;
  boolean_T c16_o_out;
  real_T c16_nb_nargin = 0.0;
  real_T c16_nb_nargout = 0.0;
  real_T c16_ob_nargin = 0.0;
  real_T c16_ob_nargout = 0.0;
  real_T c16_pb_nargin = 0.0;
  real_T c16_pb_nargout = 1.0;
  boolean_T c16_p_out;
  real_T c16_qb_nargin = 0.0;
  real_T c16_qb_nargout = 0.0;
  real_T c16_rb_nargin = 0.0;
  real_T c16_rb_nargout = 1.0;
  boolean_T c16_q_out;
  real_T c16_sb_nargin = 0.0;
  real_T c16_sb_nargout = 0.0;
  real_T c16_tb_nargin = 0.0;
  real_T c16_tb_nargout = 1.0;
  boolean_T c16_r_out;
  real_T c16_ub_nargin = 0.0;
  real_T c16_ub_nargout = 0.0;
  real_T c16_vb_nargin = 0.0;
  real_T c16_vb_nargout = 0.0;
  real_T c16_wb_nargin = 0.0;
  real_T c16_wb_nargout = 1.0;
  boolean_T c16_s_out;
  real_T c16_xb_nargin = 0.0;
  real_T c16_xb_nargout = 0.0;
  real_T c16_yb_nargin = 0.0;
  real_T c16_yb_nargout = 0.0;
  real_T c16_ac_nargin = 0.0;
  real_T c16_ac_nargout = 1.0;
  boolean_T c16_t_out;
  real_T c16_bc_nargin = 0.0;
  real_T c16_bc_nargout = 0.0;
  real_T c16_cc_nargin = 0.0;
  real_T c16_cc_nargout = 1.0;
  boolean_T c16_u_out;
  real_T c16_dc_nargin = 0.0;
  real_T c16_dc_nargout = 0.0;
  real_T c16_ec_nargin = 0.0;
  real_T c16_ec_nargout = 0.0;
  real_T c16_fc_nargin = 0.0;
  real_T c16_fc_nargout = 1.0;
  boolean_T c16_v_out;
  real_T c16_gc_nargin = 0.0;
  real_T c16_gc_nargout = 0.0;
  real_T c16_hc_nargin = 0.0;
  real_T c16_hc_nargout = 1.0;
  boolean_T c16_w_out;
  real_T c16_ic_nargin = 0.0;
  real_T c16_ic_nargout = 0.0;
  real_T c16_jc_nargin = 0.0;
  real_T c16_jc_nargout = 1.0;
  boolean_T c16_x_out;
  real_T c16_kc_nargin = 0.0;
  real_T c16_kc_nargout = 0.0;
  real_T c16_lc_nargin = 0.0;
  real_T c16_lc_nargout = 0.0;
  real_T c16_mc_nargin = 0.0;
  real_T c16_mc_nargout = 1.0;
  boolean_T c16_y_out;
  real_T c16_nc_nargin = 0.0;
  real_T c16_nc_nargout = 0.0;
  real_T c16_oc_nargin = 0.0;
  real_T c16_oc_nargout = 0.0;
  real_T c16_pc_nargin = 0.0;
  real_T c16_pc_nargout = 1.0;
  boolean_T c16_ab_out;
  real_T c16_qc_nargin = 0.0;
  real_T c16_qc_nargout = 0.0;
  real_T c16_rc_nargin = 0.0;
  real_T c16_rc_nargout = 1.0;
  boolean_T c16_bb_out;
  real_T c16_sc_nargin = 0.0;
  real_T c16_sc_nargout = 0.0;
  real_T c16_tc_nargin = 0.0;
  real_T c16_tc_nargout = 1.0;
  boolean_T c16_cb_out;
  real_T c16_uc_nargin = 0.0;
  real_T c16_uc_nargout = 0.0;
  real_T c16_vc_nargin = 0.0;
  real_T c16_vc_nargout = 0.0;
  real_T c16_wc_nargin = 0.0;
  real_T c16_wc_nargout = 1.0;
  boolean_T c16_db_out;
  real_T c16_xc_nargin = 0.0;
  real_T c16_xc_nargout = 0.0;
  real_T c16_yc_nargin = 0.0;
  real_T c16_yc_nargout = 0.0;
  real_T c16_ad_nargin = 0.0;
  real_T c16_ad_nargout = 1.0;
  boolean_T c16_eb_out;
  real_T c16_bd_nargin = 0.0;
  real_T c16_bd_nargout = 0.0;
  real_T c16_cd_nargin = 0.0;
  real_T c16_cd_nargout = 0.0;
  real_T c16_dd_nargin = 0.0;
  real_T c16_dd_nargout = 1.0;
  boolean_T c16_fb_out;
  real_T c16_ed_nargin = 0.0;
  real_T c16_ed_nargout = 0.0;
  real_T c16_fd_nargin = 0.0;
  real_T c16_fd_nargout = 1.0;
  boolean_T c16_gb_out;
  real_T c16_gd_nargin = 0.0;
  real_T c16_gd_nargout = 0.0;
  real_T c16_hd_nargin = 0.0;
  real_T c16_hd_nargout = 0.0;
  real_T c16_id_nargin = 0.0;
  real_T c16_id_nargout = 1.0;
  boolean_T c16_hb_out;
  real_T c16_jd_nargin = 0.0;
  real_T c16_jd_nargout = 0.0;
  real_T c16_kd_nargin = 0.0;
  real_T c16_kd_nargout = 1.0;
  boolean_T c16_ib_out;
  real_T c16_ld_nargin = 0.0;
  real_T c16_ld_nargout = 0.0;
  real_T c16_md_nargin = 0.0;
  real_T c16_md_nargout = 0.0;
  real_T c16_nd_nargin = 0.0;
  real_T c16_nd_nargout = 1.0;
  boolean_T c16_jb_out;
  real_T c16_od_nargin = 0.0;
  real_T c16_od_nargout = 0.0;
  real_T c16_pd_nargin = 0.0;
  real_T c16_pd_nargout = 1.0;
  boolean_T c16_kb_out;
  real_T c16_qd_nargin = 0.0;
  real_T c16_qd_nargout = 0.0;
  real_T c16_rd_nargin = 0.0;
  real_T c16_rd_nargout = 0.0;
  real_T c16_sd_nargin = 0.0;
  real_T c16_sd_nargout = 1.0;
  boolean_T c16_lb_out;
  real_T c16_td_nargin = 0.0;
  real_T c16_td_nargout = 0.0;
  real_T c16_ud_nargin = 0.0;
  real_T c16_ud_nargout = 0.0;
  real_T c16_vd_nargin = 0.0;
  real_T c16_vd_nargout = 1.0;
  boolean_T c16_mb_out;
  real_T c16_wd_nargin = 0.0;
  real_T c16_wd_nargout = 0.0;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  boolean_T guard9 = false;
  boolean_T guard10 = false;
  boolean_T guard11 = false;
  boolean_T guard12 = false;
  boolean_T guard13 = false;
  boolean_T guard14 = false;
  c16_set_sim_state_side_effects_c16_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c16_lastMajorTime = _sfTime_;
    chartInstance->c16_stateChanged = (boolean_T)0;
    chartInstance->c16_presentTime = _sfTime_;
    chartInstance->c16_elapsedTime = chartInstance->c16_presentTime -
      chartInstance->c16_previousTime;
    chartInstance->c16_previousTime = chartInstance->c16_presentTime;
    chartInstance->c16_temporalCounter_i1 += chartInstance->c16_elapsedTime;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 13U, chartInstance->c16_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_V_in, 8U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c16_delta, 3U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_threshold, 2U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_sensor_capacitor_charge, 7U, 1U,
                          0U, chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_sensor_lever_angle, 6U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_sensor_ball_distance, 5U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_user_shoot_button, 4U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_user_shoot_power, 9U, 1U, 0U,
                          chartInstance->c16_sfEvent, false);
    chartInstance->c16_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 13U,
                 chartInstance->c16_sfEvent);
    if (chartInstance->c16_is_active_c16_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 13U,
                   chartInstance->c16_sfEvent);
      chartInstance->c16_stateChanged = true;
      chartInstance->c16_is_active_c16_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c16_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_k_debug_family_names,
        c16_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargin, 0U, c16_sf_marshallOut,
        c16_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargout, 1U, c16_sf_marshallOut,
        c16_sf_marshallIn);
      chartInstance->c16_ball_shoot = 0.0;
      chartInstance->c16_dataWrittenToVector[1U] = true;
      ssLoggerUpdateTimeseries(chartInstance->S,
        chartInstance->c16_dataSetLogObjVector[0], 0, _sfTime_, (char_T *)
        &chartInstance->c16_ball_shoot);
      chartInstance->c16_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      chartInstance->c16_ball_threshold = 0.25;
      chartInstance->c16_dataWrittenToVector[6U] = true;
      ssLoggerUpdateTimeseries(chartInstance->S,
        chartInstance->c16_dataSetLogObjVector[1], 0, _sfTime_, (char_T *)
        &chartInstance->c16_ball_threshold);
      chartInstance->c16_dataWrittenToVector[6U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_threshold, 2U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      chartInstance->c16_ball_catching = 1.0;
      chartInstance->c16_dataWrittenToVector[7U] = true;
      ssLoggerUpdateTimeseries(chartInstance->S,
        chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
        &chartInstance->c16_ball_catching);
      chartInstance->c16_dataWrittenToVector[7U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      *chartInstance->c16_bridge_PWM_1 = 1.0;
      chartInstance->c16_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      *chartInstance->c16_bridge_PWM_2 = 0.0;
      chartInstance->c16_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      *chartInstance->c16_IGBT_PWM = 0.0;
      chartInstance->c16_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      *chartInstance->c16_user_shoot_power = 0.0;
      chartInstance->c16_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_user_shoot_power, 9U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      *chartInstance->c16_mode = 1.0;
      chartInstance->c16_dataWrittenToVector[5U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 5U, 0U,
                            chartInstance->c16_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c16_stateChanged = true;
      chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
      chartInstance->c16_tp_Mode1 = 1U;
      ssLoggerUpdateTimeseries(chartInstance->S,
        chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
        &chartInstance->c16_tp_Mode1);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_b_debug_family_names,
        c16_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_nargin, 0U, c16_sf_marshallOut,
        c16_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_nargout, 1U,
        c16_sf_marshallOut, c16_sf_marshallIn);
      *chartInstance->c16_mode = 1.0;
      chartInstance->c16_dataWrittenToVector[5U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 0U,
                            chartInstance->c16_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
    } else {
      switch (chartInstance->c16_is_c16_Integrated_system_V061) {
       case c16_IN_Mode1:
        CV_CHART_EVAL(13, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ab_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_out, 2U, c16_b_sf_marshallOut,
          c16_b_sf_marshallIn);
        c16_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
          *chartInstance->c16_sensor_capacitor_charge > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode1 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode1);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode4;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode4 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode4);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_d_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 4.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 4U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 28U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_l_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 28U,
              chartInstance->c16_sfEvent, false);
          }

          guard14 = false;
          if (CV_EML_COND(28, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(28, 0, 1, chartInstance->c16_ball_catching == 2.0))
            {
              CV_EML_MCDC(28, 0, 0, true);
              CV_EML_IF(28, 0, 0, true);
              c16_b_out = true;
            } else {
              guard14 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 28U,
                chartInstance->c16_sfEvent, false);
            }

            guard14 = true;
          }

          if (guard14 == true) {
            CV_EML_MCDC(28, 0, 0, false);
            CV_EML_IF(28, 0, 0, false);
            c16_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 28U, chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_m_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            chartInstance->c16_ball_catching = 3.0;
            chartInstance->c16_dataWrittenToVector[7U] = true;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
              &chartInstance->c16_ball_catching);
            chartInstance->c16_dataWrittenToVector[7U] = true;
            _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 28U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c16_tp_Mode1 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode1);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode1;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode1 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode1);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_b_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 1.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 0U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 29U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_v_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[6U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 29U,
                chartInstance->c16_sfEvent, false);
            }

            guard13 = false;
            if (CV_EML_COND(29, 0, 0, *chartInstance->c16_sensor_ball_distance >
                            chartInstance->c16_ball_threshold)) {
              if (CV_EML_COND(29, 0, 1, chartInstance->c16_ball_catching == 2.0))
              {
                CV_EML_MCDC(29, 0, 0, true);
                CV_EML_IF(29, 0, 0, true);
                c16_c_out = true;
              } else {
                guard13 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 29U,
                  chartInstance->c16_sfEvent, false);
              }

              guard13 = true;
            }

            if (guard13 == true) {
              CV_EML_MCDC(29, 0, 0, false);
              CV_EML_IF(29, 0, 0, false);
              c16_c_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_c_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 29U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_w_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              chartInstance->c16_ball_catching = 1.0;
              chartInstance->c16_dataWrittenToVector[7U] = true;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
                &chartInstance->c16_ball_catching);
              chartInstance->c16_dataWrittenToVector[7U] = true;
              _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                    29U, chartInstance->c16_sfEvent, false);
              *chartInstance->c16_bridge_PWM_1 = 1.0;
              chartInstance->c16_dataWrittenToVector[3U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U,
                                    29U, chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode1 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode1);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode1;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode1 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode1);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_b_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 1.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 0U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 16U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_s_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_d_out = CV_EML_IF(16, 0, 0, CV_RELATIONAL_EVAL(5U, 16U, 0,
                *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
                *chartInstance->c16_sensor_lever_angle > 0.01));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_d_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 16U,
                             chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode1 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode1);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode2;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode2 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode2);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_c_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 2.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 2U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U,
                             chartInstance->c16_sfEvent);
              }
            }
          }
        }
        break;

       case c16_IN_Mode10:
        CV_CHART_EVAL(13, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 38U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_qb_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_e_out = CV_EML_IF(38, 0, 0, chartInstance->c16_temporalCounter_i1 +
                              (_sfTime_ - chartInstance->c16_previousTime) >=
                              0.03);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 38U, chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_rb_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_IGBT_PWM = 0.0;
          chartInstance->c16_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 5U, 38U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c16_tp_Mode10 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[10], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode10);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode6;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode6 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode6);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_f_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 6.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
        }
        break;

       case c16_IN_Mode2:
        CV_CHART_EVAL(13, 0, 3);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_p_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_f_out = CV_EML_IF(8, 0, 0, *chartInstance->c16_sensor_lever_angle <=
                              0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_f_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_q_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_bridge_PWM_2 = 0.0;
          chartInstance->c16_dataWrittenToVector[4U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U, 8U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c16_tp_Mode2 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode2);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode1 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode1);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_b_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 1.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 0U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 15U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_t_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_g_out = CV_EML_IF(15, 0, 0, *chartInstance->c16_sensor_lever_angle
                                >= 5.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_g_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 15U, chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_u_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_bridge_PWM_1 = 0.0;
            chartInstance->c16_dataWrittenToVector[3U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U, 15U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c16_tp_Mode2 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode2);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode3;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode3 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode3);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 3.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 3U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 18U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_cb_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_h_out = CV_EML_IF(18, 0, 0, CV_RELATIONAL_EVAL(5U, 18U, 0,
              *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
              *chartInstance->c16_sensor_capacitor_charge > 0.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_h_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 18U,
                           chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode2 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode2);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode5;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode5 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode5);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_e_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 5.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 5U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U,
                           chartInstance->c16_sfEvent);
            }
          }
        }
        break;

       case c16_IN_Mode3:
        CV_CHART_EVAL(13, 0, 4);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 25U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_n_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        if (!chartInstance->c16_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 25U,
            chartInstance->c16_sfEvent, false);
        }

        guard12 = false;
        if (CV_EML_COND(25, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                        chartInstance->c16_ball_threshold)) {
          if (CV_EML_COND(25, 0, 1, chartInstance->c16_ball_catching == 1.0)) {
            CV_EML_MCDC(25, 0, 0, true);
            CV_EML_IF(25, 0, 0, true);
            c16_i_out = true;
          } else {
            guard12 = true;
          }
        } else {
          if (!chartInstance->c16_dataWrittenToVector[7U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 25U,
              chartInstance->c16_sfEvent, false);
          }

          guard12 = true;
        }

        if (guard12 == true) {
          CV_EML_MCDC(25, 0, 0, false);
          CV_EML_IF(25, 0, 0, false);
          c16_i_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c16_i_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 25U, chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_o_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          chartInstance->c16_ball_catching = 2.0;
          chartInstance->c16_dataWrittenToVector[7U] = true;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
            &chartInstance->c16_ball_catching);
          chartInstance->c16_dataWrittenToVector[7U] = true;
          _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 25U,
                                chartInstance->c16_sfEvent, false);
          *chartInstance->c16_bridge_PWM_2 = 1.0;
          chartInstance->c16_dataWrittenToVector[4U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U, 25U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c16_tp_Mode3 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode3);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode3 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode3);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 3.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 3U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_r_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_j_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
            *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
            *chartInstance->c16_sensor_lever_angle < 5.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_j_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode3 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode3);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode2;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode2 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode2);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_c_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 2.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 2U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 14U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_x_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_k_out = CV_EML_IF(14, 0, 0, CV_RELATIONAL_EVAL(5U, 14U, 0,
              *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
              *chartInstance->c16_sensor_capacitor_charge > 0.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_k_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U,
                           chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode3 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode3);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode6;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode6 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode6);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_f_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 6.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U,
                           chartInstance->c16_sfEvent);
            }
          }
        }
        break;

       case c16_IN_Mode4:
        CV_CHART_EVAL(13, 0, 5);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_wb_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_l_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
          *chartInstance->c16_sensor_capacitor_charge >= 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_l_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode4 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode4);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode7;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode7 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode7);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_j_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 7.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 7U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 22U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_nb_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_m_out = CV_EML_IF(22, 0, 0, CV_RELATIONAL_EVAL(5U, 22U, 0,
            *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
            *chartInstance->c16_sensor_lever_angle > 0.01));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_m_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 22U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode4 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode4);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode5;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode5 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode5);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_e_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 5.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 5U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 31U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ib_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[6U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 31U,
                chartInstance->c16_sfEvent, false);
            }

            guard11 = false;
            if (CV_EML_COND(31, 0, 0, *chartInstance->c16_sensor_ball_distance >
                            chartInstance->c16_ball_threshold)) {
              if (CV_EML_COND(31, 0, 1, chartInstance->c16_ball_catching == 2.0))
              {
                CV_EML_MCDC(31, 0, 0, true);
                CV_EML_IF(31, 0, 0, true);
                c16_n_out = true;
              } else {
                guard11 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 31U,
                  chartInstance->c16_sfEvent, false);
              }

              guard11 = true;
            }

            if (guard11 == true) {
              CV_EML_MCDC(31, 0, 0, false);
              CV_EML_IF(31, 0, 0, false);
              c16_n_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_n_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 31U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_jb_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              chartInstance->c16_ball_catching = 1.0;
              chartInstance->c16_dataWrittenToVector[7U] = true;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
                &chartInstance->c16_ball_catching);
              chartInstance->c16_dataWrittenToVector[7U] = true;
              _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                    31U, chartInstance->c16_sfEvent, false);
              *chartInstance->c16_bridge_PWM_1 = 1.0;
              chartInstance->c16_dataWrittenToVector[3U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U,
                                    31U, chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode4 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode4);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode4;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode4 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode4);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_d_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 4.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 4U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 30U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_eb_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[6U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 30U,
                  chartInstance->c16_sfEvent, false);
              }

              guard10 = false;
              if (CV_EML_COND(30, 0, 0, *chartInstance->c16_sensor_ball_distance
                              <= chartInstance->c16_ball_threshold)) {
                if (CV_EML_COND(30, 0, 1, chartInstance->c16_ball_catching ==
                                2.0)) {
                  CV_EML_MCDC(30, 0, 0, true);
                  CV_EML_IF(30, 0, 0, true);
                  c16_o_out = true;
                } else {
                  guard10 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[7U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 30U,
                    chartInstance->c16_sfEvent, false);
                }

                guard10 = true;
              }

              if (guard10 == true) {
                CV_EML_MCDC(30, 0, 0, false);
                CV_EML_IF(30, 0, 0, false);
                c16_o_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_o_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 30U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_fb_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nb_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nb_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                chartInstance->c16_ball_catching = 3.0;
                chartInstance->c16_dataWrittenToVector[7U] = true;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_ball_catching);
                chartInstance->c16_dataWrittenToVector[7U] = true;
                _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                      30U, chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
                chartInstance->c16_tp_Mode4 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode4);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode4;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode4 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode4);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_d_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ob_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ob_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 4.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 4U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_bb_debug_family_names,
                  c16_b_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pb_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pb_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                c16_p_out = CV_EML_IF(9, 0, 0, CV_RELATIONAL_EVAL(5U, 9U, 0,
                  *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
                  *chartInstance->c16_sensor_capacitor_charge <= 0.0));
                _SFD_SYMBOL_SCOPE_POP();
                if (c16_p_out) {
                  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U,
                               chartInstance->c16_sfEvent);
                  chartInstance->c16_tp_Mode4 = 0U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode4);
                  _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U,
                               chartInstance->c16_sfEvent);
                  chartInstance->c16_stateChanged = true;
                  chartInstance->c16_is_c16_Integrated_system_V061 =
                    c16_IN_Mode1;
                  _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
                  chartInstance->c16_tp_Mode1 = 1U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[5], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode1);
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U,
                    c16_b_debug_family_names, c16_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qb_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qb_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  *chartInstance->c16_mode = 1.0;
                  chartInstance->c16_dataWrittenToVector[5U] = true;
                  _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 0U,
                                        chartInstance->c16_sfEvent, false);
                  _SFD_SYMBOL_SCOPE_POP();
                } else {
                  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U,
                               chartInstance->c16_sfEvent);
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode5:
        CV_CHART_EVAL(13, 0, 6);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 17U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_yb_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rb_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rb_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_q_out = CV_EML_IF(17, 0, 0, CV_RELATIONAL_EVAL(5U, 17U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
          *chartInstance->c16_sensor_capacitor_charge >= 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_q_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 17U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode5 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode5);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode8;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode8 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode8);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_h_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 8.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 8U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 21U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_kb_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_tb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_tb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_r_out = CV_EML_IF(21, 0, 0, *chartInstance->c16_sensor_lever_angle
                                <= 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_r_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 21U, chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_lb_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ub_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ub_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_bridge_PWM_2 = 0.0;
            chartInstance->c16_dataWrittenToVector[4U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U, 21U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c16_tp_Mode5 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode5);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode4;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode4 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode4);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_d_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vb_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vb_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 4.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 4U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 24U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ob_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wb_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wb_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_s_out = CV_EML_IF(24, 0, 0,
                                  *chartInstance->c16_sensor_lever_angle >= 5.0);
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_s_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 24U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_pb_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_xb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_xb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_bridge_PWM_1 = 0.0;
              chartInstance->c16_dataWrittenToVector[3U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U,
                                    24U, chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode5 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode5);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode6;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode6 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode6);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_f_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_yb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_yb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 6.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 19U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_db_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ac_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ac_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_t_out = CV_EML_IF(19, 0, 0, CV_RELATIONAL_EVAL(5U, 19U, 0,
                *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
                *chartInstance->c16_sensor_capacitor_charge <= 0.0));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_t_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 19U,
                             chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode5 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode5);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode2;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode2 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[6], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode2);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_c_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bc_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bc_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 2.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 2U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U,
                             chartInstance->c16_sfEvent);
              }
            }
          }
        }
        break;

       case c16_IN_Mode6:
        CV_CHART_EVAL(13, 0, 7);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 36U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_sb_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cc_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cc_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        if (!chartInstance->c16_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 36U,
            chartInstance->c16_sfEvent, false);
        }

        guard9 = false;
        if (CV_EML_COND(36, 0, 0, chartInstance->c16_ball_catching == 3.0)) {
          if (CV_EML_COND(36, 0, 1, chartInstance->c16_ball_shoot == 1.0)) {
            CV_EML_MCDC(36, 0, 0, true);
            CV_EML_IF(36, 0, 0, true);
            c16_u_out = true;
          } else {
            guard9 = true;
          }
        } else {
          if (!chartInstance->c16_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 36U,
              chartInstance->c16_sfEvent, false);
          }

          guard9 = true;
        }

        if (guard9 == true) {
          CV_EML_MCDC(36, 0, 0, false);
          CV_EML_IF(36, 0, 0, false);
          c16_u_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c16_u_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 36U, chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_tb_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_dc_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_dc_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          chartInstance->c16_ball_shoot = 0.0;
          chartInstance->c16_dataWrittenToVector[1U] = true;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[0], 0, _sfTime_, (char_T *)
            &chartInstance->c16_ball_shoot);
          chartInstance->c16_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 5U, 36U,
                                chartInstance->c16_sfEvent, false);
          chartInstance->c16_ball_catching = 1.0;
          chartInstance->c16_dataWrittenToVector[7U] = true;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
            &chartInstance->c16_ball_catching);
          chartInstance->c16_dataWrittenToVector[7U] = true;
          _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 36U,
                                chartInstance->c16_sfEvent, false);
          *chartInstance->c16_IGBT_PWM = 1.0;
          chartInstance->c16_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 5U, 36U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c16_tp_Mode6 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode6);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode6;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode6 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode6);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_f_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ec_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ec_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 6.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_y_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fc_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fc_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_v_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
            *chartInstance->c16_sensor_capacitor_charge <= 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_v_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode6 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode6);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode3;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode3 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[4], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode3);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gc_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gc_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 3.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 3U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 23U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_mb_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hc_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hc_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_w_out = CV_EML_IF(23, 0, 0, CV_RELATIONAL_EVAL(5U, 23U, 0,
              *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
              *chartInstance->c16_sensor_lever_angle < 5.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_w_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 23U,
                           chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode6 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode6);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode5;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode5 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
                &chartInstance->c16_tp_Mode5);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_e_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ic_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ic_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 5.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 5U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 39U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_bc_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jc_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jc_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 39U,
                  chartInstance->c16_sfEvent, false);
              }

              guard8 = false;
              if (CV_EML_COND(39, 0, 0, chartInstance->c16_ball_catching == 3.0))
              {
                if (CV_EML_COND(39, 0, 1, chartInstance->c16_ball_shoot == 1.0))
                {
                  CV_EML_MCDC(39, 0, 0, true);
                  CV_EML_IF(39, 0, 0, true);
                  c16_x_out = true;
                } else {
                  guard8 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[1U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 39U,
                    chartInstance->c16_sfEvent, false);
                }

                guard8 = true;
              }

              if (guard8 == true) {
                CV_EML_MCDC(39, 0, 0, false);
                CV_EML_IF(39, 0, 0, false);
                c16_x_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_x_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 39U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_cc_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kc_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kc_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                chartInstance->c16_ball_shoot = 0.0;
                chartInstance->c16_dataWrittenToVector[1U] = true;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[0], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_ball_shoot);
                chartInstance->c16_dataWrittenToVector[1U] = true;
                _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 5U, 39U,
                                      chartInstance->c16_sfEvent, false);
                chartInstance->c16_ball_catching = 1.0;
                chartInstance->c16_dataWrittenToVector[7U] = true;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_ball_catching);
                chartInstance->c16_dataWrittenToVector[7U] = true;
                _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                      39U, chartInstance->c16_sfEvent, false);
                *chartInstance->c16_IGBT_PWM = 1.0;
                chartInstance->c16_dataWrittenToVector[2U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 5U, 39U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
                chartInstance->c16_tp_Mode6 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode6);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode10;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
                chartInstance->c16_temporalCounter_i1 = 0.0;
                chartInstance->c16_tp_Mode10 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[10], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode10);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_g_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lc_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lc_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 10.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 1U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 26U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_gb_debug_family_names,
                  c16_b_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mc_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mc_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                if (!chartInstance->c16_dataWrittenToVector[6U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 26U,
                    chartInstance->c16_sfEvent, false);
                }

                guard7 = false;
                if (CV_EML_COND(26, 0, 0,
                                *chartInstance->c16_sensor_ball_distance <=
                                chartInstance->c16_ball_threshold)) {
                  if (CV_EML_COND(26, 0, 1, chartInstance->c16_ball_catching ==
                                  1.0)) {
                    CV_EML_MCDC(26, 0, 0, true);
                    CV_EML_IF(26, 0, 0, true);
                    c16_y_out = true;
                  } else {
                    guard7 = true;
                  }
                } else {
                  if (!chartInstance->c16_dataWrittenToVector[7U]) {
                    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 26U,
                      chartInstance->c16_sfEvent, false);
                  }

                  guard7 = true;
                }

                if (guard7 == true) {
                  CV_EML_MCDC(26, 0, 0, false);
                  CV_EML_IF(26, 0, 0, false);
                  c16_y_out = false;
                }

                _SFD_SYMBOL_SCOPE_POP();
                if (c16_y_out) {
                  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 26U,
                               chartInstance->c16_sfEvent);
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U,
                    c16_hb_debug_family_names, c16_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nc_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nc_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  chartInstance->c16_ball_catching = 2.0;
                  chartInstance->c16_dataWrittenToVector[7U] = true;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_ball_catching);
                  chartInstance->c16_dataWrittenToVector[7U] = true;
                  _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                        26U, chartInstance->c16_sfEvent, false);
                  *chartInstance->c16_bridge_PWM_2 = 1.0;
                  chartInstance->c16_dataWrittenToVector[4U] = true;
                  _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U,
                                        5U, 26U, chartInstance->c16_sfEvent,
                                        false);
                  _SFD_SYMBOL_SCOPE_POP();
                  chartInstance->c16_tp_Mode6 = 0U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode6);
                  _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U,
                               chartInstance->c16_sfEvent);
                  chartInstance->c16_stateChanged = true;
                  chartInstance->c16_is_c16_Integrated_system_V061 =
                    c16_IN_Mode6;
                  _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
                  chartInstance->c16_tp_Mode6 = 1U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode6);
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U,
                    c16_f_debug_family_names, c16_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_oc_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_oc_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  *chartInstance->c16_mode = 6.0;
                  chartInstance->c16_dataWrittenToVector[5U] = true;
                  _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                        chartInstance->c16_sfEvent, false);
                  _SFD_SYMBOL_SCOPE_POP();
                } else {
                  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 13U,
                               chartInstance->c16_sfEvent);
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U,
                    c16_dc_debug_family_names, c16_b_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pc_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pc_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_out, 2U,
                    c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                  c16_ab_out = CV_EML_IF(13, 0, 0, CV_RELATIONAL_EVAL(5U, 13U, 0,
                    *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
                    *chartInstance->c16_sensor_capacitor_charge >= 4.99));
                  _SFD_SYMBOL_SCOPE_POP();
                  if (c16_ab_out) {
                    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U,
                                 chartInstance->c16_sfEvent);
                    chartInstance->c16_tp_Mode6 = 0U;
                    ssLoggerUpdateTimeseries(chartInstance->S,
                      chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_,
                      (char_T *)&chartInstance->c16_tp_Mode6);
                    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U,
                                 chartInstance->c16_sfEvent);
                    chartInstance->c16_stateChanged = true;
                    chartInstance->c16_is_c16_Integrated_system_V061 =
                      c16_IN_Mode9;
                    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U,
                                 chartInstance->c16_sfEvent);
                    chartInstance->c16_tp_Mode9 = 1U;
                    ssLoggerUpdateTimeseries(chartInstance->S,
                      chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_,
                      (char_T *)&chartInstance->c16_tp_Mode9);
                    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U,
                      c16_i_debug_family_names, c16_debug_family_var_map);
                    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qc_nargin, 0U,
                      c16_sf_marshallOut, c16_sf_marshallIn);
                    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qc_nargout, 1U,
                      c16_sf_marshallOut, c16_sf_marshallIn);
                    *chartInstance->c16_mode = 9.0;
                    chartInstance->c16_dataWrittenToVector[5U] = true;
                    _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 9U,
                                          chartInstance->c16_sfEvent, false);
                    _SFD_SYMBOL_SCOPE_POP();
                  } else {
                    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U,
                                 chartInstance->c16_sfEvent);
                  }
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode7:
        CV_CHART_EVAL(13, 0, 8);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_qc_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rc_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rc_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_bb_out = CV_EML_IF(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
          *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
          *chartInstance->c16_sensor_lever_angle > 0.01));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_bb_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode7 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode7);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode8;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode8 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode8);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_h_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sc_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sc_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 8.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 8U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 32U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_hc_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_tc_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_tc_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 32U,
              chartInstance->c16_sfEvent, false);
          }

          guard6 = false;
          if (CV_EML_COND(32, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(32, 0, 1, chartInstance->c16_ball_catching == 2.0))
            {
              CV_EML_MCDC(32, 0, 0, true);
              CV_EML_IF(32, 0, 0, true);
              c16_cb_out = true;
            } else {
              guard6 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 32U,
                chartInstance->c16_sfEvent, false);
            }

            guard6 = true;
          }

          if (guard6 == true) {
            CV_EML_MCDC(32, 0, 0, false);
            CV_EML_IF(32, 0, 0, false);
            c16_cb_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_cb_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 32U, chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_ic_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_uc_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_uc_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            chartInstance->c16_ball_catching = 3.0;
            chartInstance->c16_dataWrittenToVector[7U] = true;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
              &chartInstance->c16_ball_catching);
            chartInstance->c16_dataWrittenToVector[7U] = true;
            _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 32U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c16_tp_Mode7 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode7);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode7;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode7 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode7);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_j_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vc_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vc_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 7.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 7U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 34U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_tc_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wc_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wc_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 34U,
                chartInstance->c16_sfEvent, false);
            }

            guard4 = false;
            guard5 = false;
            if (CV_EML_COND(34, 0, 0, chartInstance->c16_ball_catching == 3.0))
            {
              if (CV_EML_COND(34, 0, 1, *chartInstance->c16_user_shoot_button ==
                              1.0)) {
                if (CV_EML_COND(34, 0, 2, chartInstance->c16_ball_shoot == 0.0))
                {
                  CV_EML_MCDC(34, 0, 0, true);
                  CV_EML_IF(34, 0, 0, true);
                  c16_db_out = true;
                } else {
                  guard4 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[1U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 34U,
                    chartInstance->c16_sfEvent, false);
                }

                guard5 = true;
              }
            } else {
              guard5 = true;
            }

            if (guard5 == true) {
              guard4 = true;
            }

            if (guard4 == true) {
              CV_EML_MCDC(34, 0, 0, false);
              CV_EML_IF(34, 0, 0, false);
              c16_db_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_db_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 34U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_uc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_xc_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_xc_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              chartInstance->c16_ball_shoot = 1.0;
              chartInstance->c16_dataWrittenToVector[1U] = true;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[0], 0, _sfTime_, (char_T *)
                &chartInstance->c16_ball_shoot);
              chartInstance->c16_dataWrittenToVector[1U] = true;
              _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 5U, 34U,
                                    chartInstance->c16_sfEvent, false);
              *chartInstance->c16_bridge_PWM_1 = 1.0;
              chartInstance->c16_dataWrittenToVector[3U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U,
                                    34U, chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode7 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode7);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode7;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode7 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode7);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_j_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_yc_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_yc_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 7.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 7U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 33U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_rc_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ad_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ad_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[6U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 33U,
                  chartInstance->c16_sfEvent, false);
              }

              guard3 = false;
              if (CV_EML_COND(33, 0, 0, *chartInstance->c16_sensor_ball_distance
                              > chartInstance->c16_ball_threshold)) {
                if (CV_EML_COND(33, 0, 1, chartInstance->c16_ball_catching ==
                                2.0)) {
                  CV_EML_MCDC(33, 0, 0, true);
                  CV_EML_IF(33, 0, 0, true);
                  c16_eb_out = true;
                } else {
                  guard3 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[7U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 33U,
                    chartInstance->c16_sfEvent, false);
                }

                guard3 = true;
              }

              if (guard3 == true) {
                CV_EML_MCDC(33, 0, 0, false);
                CV_EML_IF(33, 0, 0, false);
                c16_eb_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_eb_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 33U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_sc_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bd_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bd_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                chartInstance->c16_ball_catching = 1.0;
                chartInstance->c16_dataWrittenToVector[7U] = true;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_ball_catching);
                chartInstance->c16_dataWrittenToVector[7U] = true;
                _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                      33U, chartInstance->c16_sfEvent, false);
                *chartInstance->c16_bridge_PWM_1 = 1.0;
                chartInstance->c16_dataWrittenToVector[3U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U,
                                      33U, chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
                chartInstance->c16_tp_Mode7 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode7);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode7;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode7 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode7);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_j_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cd_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cd_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 7.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 7U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 10U,
                             chartInstance->c16_sfEvent);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_xb_debug_family_names,
                  c16_b_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_dd_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_dd_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                c16_fb_out = CV_EML_IF(10, 0, 0, CV_RELATIONAL_EVAL(5U, 10U, 0, *
                  chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
                  *chartInstance->c16_sensor_capacitor_charge < 4.99));
                _SFD_SYMBOL_SCOPE_POP();
                if (c16_fb_out) {
                  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U,
                               chartInstance->c16_sfEvent);
                  chartInstance->c16_tp_Mode7 = 0U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode7);
                  _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U,
                               chartInstance->c16_sfEvent);
                  chartInstance->c16_stateChanged = true;
                  chartInstance->c16_is_c16_Integrated_system_V061 =
                    c16_IN_Mode4;
                  _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
                  chartInstance->c16_tp_Mode4 = 1U;
                  ssLoggerUpdateTimeseries(chartInstance->S,
                    chartInstance->c16_dataSetLogObjVector[7], 0, _sfTime_,
                    (char_T *)&chartInstance->c16_tp_Mode4);
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U,
                    c16_d_debug_family_names, c16_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ed_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ed_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  *chartInstance->c16_mode = 4.0;
                  chartInstance->c16_dataWrittenToVector[5U] = true;
                  _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 4U,
                                        chartInstance->c16_sfEvent, false);
                  _SFD_SYMBOL_SCOPE_POP();
                } else {
                  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U,
                               chartInstance->c16_sfEvent);
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode8:
        CV_CHART_EVAL(13, 0, 9);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_oc_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fd_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fd_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_gb_out = CV_EML_IF(4, 0, 0, *chartInstance->c16_sensor_lever_angle >=
          5.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_gb_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_pc_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gd_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gd_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_bridge_PWM_1 = 0.0;
          chartInstance->c16_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_1, 11U, 5U, 4U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c16_tp_Mode8 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode8);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode9;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode9 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode9);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_i_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hd_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hd_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 9.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 9U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 20U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ac_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_id_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_id_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_hb_out = CV_EML_IF(20, 0, 0, CV_RELATIONAL_EVAL(5U, 20U, 0,
            *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
            *chartInstance->c16_sensor_capacitor_charge < 4.99));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_hb_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 20U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode8 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode8);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode5;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode5 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[8], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode5);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_e_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jd_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jd_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 5.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 5U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_mc_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kd_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kd_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_ib_out = CV_EML_IF(11, 0, 0,
              *chartInstance->c16_sensor_lever_angle <= 0.0);
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_ib_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_nc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ld_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ld_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_bridge_PWM_2 = 0.0;
              chartInstance->c16_dataWrittenToVector[4U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U,
                                    11U, chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode8 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode8);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode7;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c16_sfEvent);
              chartInstance->c16_tp_Mode7 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[13], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode7);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_j_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_md_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_md_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 7.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 7U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U,
                           chartInstance->c16_sfEvent);
            }
          }
        }
        break;

       case c16_IN_Mode9:
        CV_CHART_EVAL(13, 0, 10);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U,
                     chartInstance->c16_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                     chartInstance->c16_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ec_debug_family_names,
          c16_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nd_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nd_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_jb_out = CV_EML_IF(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
          *chartInstance->c16_sensor_capacitor_charge < 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_jb_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode9 = 0U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode9);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
          chartInstance->c16_stateChanged = true;
          chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode6;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c16_sfEvent);
          chartInstance->c16_tp_Mode6 = 1U;
          ssLoggerUpdateTimeseries(chartInstance->S,
            chartInstance->c16_dataSetLogObjVector[9], 0, _sfTime_, (char_T *)
            &chartInstance->c16_tp_Mode6);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_f_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_od_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_od_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          *chartInstance->c16_mode = 6.0;
          chartInstance->c16_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 6U,
                                chartInstance->c16_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 27U,
                       chartInstance->c16_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_jc_debug_family_names,
            c16_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pd_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_pd_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 27U,
              chartInstance->c16_sfEvent, false);
          }

          guard2 = false;
          if (CV_EML_COND(27, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(27, 0, 1, chartInstance->c16_ball_catching == 1.0))
            {
              CV_EML_MCDC(27, 0, 0, true);
              CV_EML_IF(27, 0, 0, true);
              c16_kb_out = true;
            } else {
              guard2 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 27U,
                chartInstance->c16_sfEvent, false);
            }

            guard2 = true;
          }

          if (guard2 == true) {
            CV_EML_MCDC(27, 0, 0, false);
            CV_EML_IF(27, 0, 0, false);
            c16_kb_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_kb_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 27U, chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_kc_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qd_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_qd_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            chartInstance->c16_ball_catching = 2.0;
            chartInstance->c16_dataWrittenToVector[7U] = true;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
              &chartInstance->c16_ball_catching);
            chartInstance->c16_dataWrittenToVector[7U] = true;
            _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U, 27U,
                                  chartInstance->c16_sfEvent, false);
            *chartInstance->c16_bridge_PWM_2 = 1.0;
            chartInstance->c16_dataWrittenToVector[4U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_bridge_PWM_2, 12U, 5U, 27U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
            chartInstance->c16_tp_Mode9 = 0U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode9);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
            chartInstance->c16_stateChanged = true;
            chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode9;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
            chartInstance->c16_tp_Mode9 = 1U;
            ssLoggerUpdateTimeseries(chartInstance->S,
              chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T *)
              &chartInstance->c16_tp_Mode9);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_i_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rd_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_rd_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            *chartInstance->c16_mode = 9.0;
            chartInstance->c16_dataWrittenToVector[5U] = true;
            _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 9U,
                                  chartInstance->c16_sfEvent, false);
            _SFD_SYMBOL_SCOPE_POP();
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 35U,
                         chartInstance->c16_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_fc_debug_family_names,
              c16_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sd_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_sd_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 35U,
                chartInstance->c16_sfEvent, false);
            }

            guard1 = false;
            if (CV_EML_COND(35, 0, 0, chartInstance->c16_ball_catching == 3.0))
            {
              if (CV_EML_COND(35, 0, 1, chartInstance->c16_ball_shoot == 1.0)) {
                CV_EML_MCDC(35, 0, 0, true);
                CV_EML_IF(35, 0, 0, true);
                c16_lb_out = true;
              } else {
                guard1 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 35U,
                  chartInstance->c16_sfEvent, false);
              }

              guard1 = true;
            }

            if (guard1 == true) {
              CV_EML_MCDC(35, 0, 0, false);
              CV_EML_IF(35, 0, 0, false);
              c16_lb_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_lb_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 35U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_gc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_td_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_td_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              chartInstance->c16_ball_shoot = 0.0;
              chartInstance->c16_dataWrittenToVector[1U] = true;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[0], 0, _sfTime_, (char_T *)
                &chartInstance->c16_ball_shoot);
              chartInstance->c16_dataWrittenToVector[1U] = true;
              _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_shoot, 1U, 5U, 35U,
                                    chartInstance->c16_sfEvent, false);
              chartInstance->c16_ball_catching = 1.0;
              chartInstance->c16_dataWrittenToVector[7U] = true;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[2], 0, _sfTime_, (char_T *)
                &chartInstance->c16_ball_catching);
              chartInstance->c16_dataWrittenToVector[7U] = true;
              _SFD_DATA_RANGE_CHECK(chartInstance->c16_ball_catching, 0U, 5U,
                                    35U, chartInstance->c16_sfEvent, false);
              *chartInstance->c16_IGBT_PWM = 1.0;
              chartInstance->c16_dataWrittenToVector[2U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_IGBT_PWM, 10U, 5U, 35U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c16_tp_Mode9 = 0U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode9);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
              chartInstance->c16_stateChanged = true;
              chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode10;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c16_sfEvent);
              chartInstance->c16_temporalCounter_i1 = 0.0;
              chartInstance->c16_tp_Mode10 = 1U;
              ssLoggerUpdateTimeseries(chartInstance->S,
                chartInstance->c16_dataSetLogObjVector[10], 0, _sfTime_, (char_T
                *)&chartInstance->c16_tp_Mode10);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_g_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ud_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ud_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              *chartInstance->c16_mode = 10.0;
              chartInstance->c16_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 1U,
                                    chartInstance->c16_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 12U,
                           chartInstance->c16_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_lc_debug_family_names,
                c16_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vd_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_vd_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_mb_out = CV_EML_IF(12, 0, 0, CV_RELATIONAL_EVAL(5U, 12U, 0,
                *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
                *chartInstance->c16_sensor_lever_angle < 5.0));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_mb_out) {
                _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U,
                             chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode9 = 0U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[12], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode9);
                _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c16_sfEvent);
                chartInstance->c16_stateChanged = true;
                chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_Mode8;
                _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c16_sfEvent);
                chartInstance->c16_tp_Mode8 = 1U;
                ssLoggerUpdateTimeseries(chartInstance->S,
                  chartInstance->c16_dataSetLogObjVector[11], 0, _sfTime_,
                  (char_T *)&chartInstance->c16_tp_Mode8);
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c16_h_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wd_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_wd_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                *chartInstance->c16_mode = 8.0;
                chartInstance->c16_dataWrittenToVector[5U] = true;
                _SFD_DATA_RANGE_CHECK(*chartInstance->c16_mode, 13U, 4U, 8U,
                                      chartInstance->c16_sfEvent, false);
                _SFD_SYMBOL_SCOPE_POP();
              } else {
                _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U,
                             chartInstance->c16_sfEvent);
              }
            }
          }
        }
        break;

       default:
        CV_CHART_EVAL(13, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c16_is_c16_Integrated_system_V061 =
          c16_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c16_sfEvent);
    if (chartInstance->c16_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c16_is_c16_Integrated_system_V061) {
   case c16_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode10:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode2:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode3:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode4:
    CV_CHART_EVAL(13, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode5:
    CV_CHART_EVAL(13, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode6:
    CV_CHART_EVAL(13, 0, 7);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode7:
    CV_CHART_EVAL(13, 0, 8);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode8:
    CV_CHART_EVAL(13, 0, 9);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode9:
    CV_CHART_EVAL(13, 0, 10);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c16_debug_family_var_map[3];
  real_T c16_nargin = 0.0;
  real_T c16_nargout = 1.0;
  boolean_T c16_out;
  real_T c16_b_nargin = 0.0;
  real_T c16_b_nargout = 1.0;
  boolean_T c16_b_out;
  real_T c16_c_nargin = 0.0;
  real_T c16_c_nargout = 1.0;
  boolean_T c16_c_out;
  real_T c16_d_nargin = 0.0;
  real_T c16_d_nargout = 1.0;
  boolean_T c16_d_out;
  real_T c16_e_nargin = 0.0;
  real_T c16_e_nargout = 1.0;
  boolean_T c16_e_out;
  real_T c16_f_nargin = 0.0;
  real_T c16_f_nargout = 1.0;
  boolean_T c16_f_out;
  real_T c16_g_nargin = 0.0;
  real_T c16_g_nargout = 1.0;
  boolean_T c16_g_out;
  real_T c16_h_nargin = 0.0;
  real_T c16_h_nargout = 1.0;
  boolean_T c16_h_out;
  real_T c16_i_nargin = 0.0;
  real_T c16_i_nargout = 1.0;
  boolean_T c16_i_out;
  real_T c16_j_nargin = 0.0;
  real_T c16_j_nargout = 1.0;
  boolean_T c16_j_out;
  real_T c16_k_nargin = 0.0;
  real_T c16_k_nargout = 1.0;
  boolean_T c16_k_out;
  real_T c16_l_nargin = 0.0;
  real_T c16_l_nargout = 1.0;
  boolean_T c16_l_out;
  real_T c16_m_nargin = 0.0;
  real_T c16_m_nargout = 1.0;
  boolean_T c16_m_out;
  real_T c16_n_nargin = 0.0;
  real_T c16_n_nargout = 1.0;
  boolean_T c16_n_out;
  real_T c16_o_nargin = 0.0;
  real_T c16_o_nargout = 1.0;
  boolean_T c16_o_out;
  real_T c16_p_nargin = 0.0;
  real_T c16_p_nargout = 1.0;
  boolean_T c16_p_out;
  real_T c16_q_nargin = 0.0;
  real_T c16_q_nargout = 1.0;
  boolean_T c16_q_out;
  real_T c16_r_nargin = 0.0;
  real_T c16_r_nargout = 1.0;
  boolean_T c16_r_out;
  real_T c16_s_nargin = 0.0;
  real_T c16_s_nargout = 1.0;
  boolean_T c16_s_out;
  real_T c16_t_nargin = 0.0;
  real_T c16_t_nargout = 1.0;
  boolean_T c16_t_out;
  real_T c16_u_nargin = 0.0;
  real_T c16_u_nargout = 1.0;
  boolean_T c16_u_out;
  real_T c16_v_nargin = 0.0;
  real_T c16_v_nargout = 1.0;
  boolean_T c16_v_out;
  real_T c16_w_nargin = 0.0;
  real_T c16_w_nargout = 1.0;
  boolean_T c16_w_out;
  real_T c16_x_nargin = 0.0;
  real_T c16_x_nargout = 1.0;
  boolean_T c16_x_out;
  real_T c16_y_nargin = 0.0;
  real_T c16_y_nargout = 1.0;
  boolean_T c16_y_out;
  real_T c16_ab_nargin = 0.0;
  real_T c16_ab_nargout = 1.0;
  boolean_T c16_ab_out;
  real_T c16_bb_nargin = 0.0;
  real_T c16_bb_nargout = 1.0;
  boolean_T c16_bb_out;
  real_T c16_cb_nargin = 0.0;
  real_T c16_cb_nargout = 1.0;
  boolean_T c16_cb_out;
  real_T c16_db_nargin = 0.0;
  real_T c16_db_nargout = 1.0;
  boolean_T c16_db_out;
  real_T c16_eb_nargin = 0.0;
  real_T c16_eb_nargout = 1.0;
  boolean_T c16_eb_out;
  real_T c16_fb_nargin = 0.0;
  real_T c16_fb_nargout = 1.0;
  boolean_T c16_fb_out;
  real_T c16_gb_nargin = 0.0;
  real_T c16_gb_nargout = 1.0;
  boolean_T c16_gb_out;
  real_T c16_hb_nargin = 0.0;
  real_T c16_hb_nargout = 1.0;
  boolean_T c16_hb_out;
  real_T c16_ib_nargin = 0.0;
  real_T c16_ib_nargout = 1.0;
  boolean_T c16_ib_out;
  real_T c16_jb_nargin = 0.0;
  real_T c16_jb_nargout = 1.0;
  boolean_T c16_jb_out;
  real_T c16_kb_nargin = 0.0;
  real_T c16_kb_nargout = 1.0;
  boolean_T c16_kb_out;
  real_T c16_lb_nargin = 0.0;
  real_T c16_lb_nargout = 1.0;
  boolean_T c16_lb_out;
  real_T c16_mb_nargin = 0.0;
  real_T c16_mb_nargout = 1.0;
  boolean_T c16_mb_out;
  real_T *c16_zcVar;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T guard5 = false;
  boolean_T guard6 = false;
  boolean_T guard7 = false;
  boolean_T guard8 = false;
  boolean_T guard9 = false;
  boolean_T guard10 = false;
  boolean_T guard11 = false;
  boolean_T guard12 = false;
  boolean_T guard13 = false;
  boolean_T guard14 = false;
  c16_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c16_lastMajorTime == _sfTime_) {
    *c16_zcVar = -1.0;
  } else {
    chartInstance->c16_stateChanged = (boolean_T)0;
    if (chartInstance->c16_is_active_c16_Integrated_system_V061 == 0U) {
      chartInstance->c16_stateChanged = true;
    } else {
      switch (chartInstance->c16_is_c16_Integrated_system_V061) {
       case c16_IN_Mode1:
        CV_CHART_EVAL(13, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ab_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargin, 0U, c16_sf_marshallOut,
          c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_out, 2U, c16_b_sf_marshallOut,
          c16_b_sf_marshallIn);
        c16_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
          *chartInstance->c16_sensor_capacitor_charge > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_l_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_b_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 28U,
              chartInstance->c16_sfEvent, false);
          }

          guard14 = false;
          if (CV_EML_COND(28, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(28, 0, 1, chartInstance->c16_ball_catching == 2.0))
            {
              CV_EML_MCDC(28, 0, 0, true);
              CV_EML_IF(28, 0, 0, true);
              c16_b_out = true;
            } else {
              guard14 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 28U,
                chartInstance->c16_sfEvent, false);
            }

            guard14 = true;
          }

          if (guard14 == true) {
            CV_EML_MCDC(28, 0, 0, false);
            CV_EML_IF(28, 0, 0, false);
            c16_b_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_b_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_v_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_c_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[6U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 29U,
                chartInstance->c16_sfEvent, false);
            }

            guard13 = false;
            if (CV_EML_COND(29, 0, 0, *chartInstance->c16_sensor_ball_distance >
                            chartInstance->c16_ball_threshold)) {
              if (CV_EML_COND(29, 0, 1, chartInstance->c16_ball_catching == 2.0))
              {
                CV_EML_MCDC(29, 0, 0, true);
                CV_EML_IF(29, 0, 0, true);
                c16_c_out = true;
              } else {
                guard13 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 29U,
                  chartInstance->c16_sfEvent, false);
              }

              guard13 = true;
            }

            if (guard13 == true) {
              CV_EML_MCDC(29, 0, 0, false);
              CV_EML_IF(29, 0, 0, false);
              c16_c_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_c_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_s_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_d_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_d_out = CV_EML_IF(16, 0, 0, CV_RELATIONAL_EVAL(5U, 16U, 0,
                *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
                *chartInstance->c16_sensor_lever_angle > 0.01));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_d_out) {
                chartInstance->c16_stateChanged = true;
              }
            }
          }
        }
        break;

       case c16_IN_Mode10:
        CV_CHART_EVAL(13, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_qb_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_e_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_e_out = CV_EML_IF(38, 0, 0, chartInstance->c16_temporalCounter_i1 +
                              (_sfTime_ - chartInstance->c16_previousTime) >=
                              0.03);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_e_out) {
          chartInstance->c16_stateChanged = true;
        }
        break;

       case c16_IN_Mode2:
        CV_CHART_EVAL(13, 0, 3);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_p_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_f_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_f_out = CV_EML_IF(8, 0, 0, *chartInstance->c16_sensor_lever_angle <=
                              0.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_f_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_t_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_g_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_g_out = CV_EML_IF(15, 0, 0, *chartInstance->c16_sensor_lever_angle
                                >= 5.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_g_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_cb_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_h_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_h_out = CV_EML_IF(18, 0, 0, CV_RELATIONAL_EVAL(5U, 18U, 0,
              *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
              *chartInstance->c16_sensor_capacitor_charge > 0.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_h_out) {
              chartInstance->c16_stateChanged = true;
            }
          }
        }
        break;

       case c16_IN_Mode3:
        CV_CHART_EVAL(13, 0, 4);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_n_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_i_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        if (!chartInstance->c16_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 25U,
            chartInstance->c16_sfEvent, false);
        }

        guard12 = false;
        if (CV_EML_COND(25, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                        chartInstance->c16_ball_threshold)) {
          if (CV_EML_COND(25, 0, 1, chartInstance->c16_ball_catching == 1.0)) {
            CV_EML_MCDC(25, 0, 0, true);
            CV_EML_IF(25, 0, 0, true);
            c16_i_out = true;
          } else {
            guard12 = true;
          }
        } else {
          if (!chartInstance->c16_dataWrittenToVector[7U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 25U,
              chartInstance->c16_sfEvent, false);
          }

          guard12 = true;
        }

        if (guard12 == true) {
          CV_EML_MCDC(25, 0, 0, false);
          CV_EML_IF(25, 0, 0, false);
          c16_i_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c16_i_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_r_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_j_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_j_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
            *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
            *chartInstance->c16_sensor_lever_angle < 5.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_j_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_x_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_k_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_k_out = CV_EML_IF(14, 0, 0, CV_RELATIONAL_EVAL(5U, 14U, 0,
              *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 4U,
              *chartInstance->c16_sensor_capacitor_charge > 0.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_k_out) {
              chartInstance->c16_stateChanged = true;
            }
          }
        }
        break;

       case c16_IN_Mode4:
        CV_CHART_EVAL(13, 0, 5);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_wb_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_l_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_l_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
          *chartInstance->c16_sensor_capacitor_charge >= 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_l_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_nb_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_m_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_m_out = CV_EML_IF(22, 0, 0, CV_RELATIONAL_EVAL(5U, 22U, 0,
            *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
            *chartInstance->c16_sensor_lever_angle > 0.01));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_m_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ib_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_n_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[6U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 31U,
                chartInstance->c16_sfEvent, false);
            }

            guard11 = false;
            if (CV_EML_COND(31, 0, 0, *chartInstance->c16_sensor_ball_distance >
                            chartInstance->c16_ball_threshold)) {
              if (CV_EML_COND(31, 0, 1, chartInstance->c16_ball_catching == 2.0))
              {
                CV_EML_MCDC(31, 0, 0, true);
                CV_EML_IF(31, 0, 0, true);
                c16_n_out = true;
              } else {
                guard11 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 31U,
                  chartInstance->c16_sfEvent, false);
              }

              guard11 = true;
            }

            if (guard11 == true) {
              CV_EML_MCDC(31, 0, 0, false);
              CV_EML_IF(31, 0, 0, false);
              c16_n_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_n_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_eb_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_o_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[6U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 30U,
                  chartInstance->c16_sfEvent, false);
              }

              guard10 = false;
              if (CV_EML_COND(30, 0, 0, *chartInstance->c16_sensor_ball_distance
                              <= chartInstance->c16_ball_threshold)) {
                if (CV_EML_COND(30, 0, 1, chartInstance->c16_ball_catching ==
                                2.0)) {
                  CV_EML_MCDC(30, 0, 0, true);
                  CV_EML_IF(30, 0, 0, true);
                  c16_o_out = true;
                } else {
                  guard10 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[7U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 30U,
                    chartInstance->c16_sfEvent, false);
                }

                guard10 = true;
              }

              if (guard10 == true) {
                CV_EML_MCDC(30, 0, 0, false);
                CV_EML_IF(30, 0, 0, false);
                c16_o_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_o_out) {
                chartInstance->c16_stateChanged = true;
              } else {
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_bb_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_p_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                c16_p_out = CV_EML_IF(9, 0, 0, CV_RELATIONAL_EVAL(5U, 9U, 0,
                  *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
                  *chartInstance->c16_sensor_capacitor_charge <= 0.0));
                _SFD_SYMBOL_SCOPE_POP();
                if (c16_p_out) {
                  chartInstance->c16_stateChanged = true;
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode5:
        CV_CHART_EVAL(13, 0, 6);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_yb_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_q_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_q_out = CV_EML_IF(17, 0, 0, CV_RELATIONAL_EVAL(5U, 17U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
          *chartInstance->c16_sensor_capacitor_charge >= 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_q_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_kb_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_r_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_r_out = CV_EML_IF(21, 0, 0, *chartInstance->c16_sensor_lever_angle
                                <= 0.0);
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_r_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ob_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_s_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_s_out = CV_EML_IF(24, 0, 0,
                                  *chartInstance->c16_sensor_lever_angle >= 5.0);
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_s_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_db_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_t_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_t_out = CV_EML_IF(19, 0, 0, CV_RELATIONAL_EVAL(5U, 19U, 0,
                *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
                *chartInstance->c16_sensor_capacitor_charge <= 0.0));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_t_out) {
                chartInstance->c16_stateChanged = true;
              }
            }
          }
        }
        break;

       case c16_IN_Mode6:
        CV_CHART_EVAL(13, 0, 7);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_sb_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_u_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        if (!chartInstance->c16_dataWrittenToVector[7U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 36U,
            chartInstance->c16_sfEvent, false);
        }

        guard9 = false;
        if (CV_EML_COND(36, 0, 0, chartInstance->c16_ball_catching == 3.0)) {
          if (CV_EML_COND(36, 0, 1, chartInstance->c16_ball_shoot == 1.0)) {
            CV_EML_MCDC(36, 0, 0, true);
            CV_EML_IF(36, 0, 0, true);
            c16_u_out = true;
          } else {
            guard9 = true;
          }
        } else {
          if (!chartInstance->c16_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 36U,
              chartInstance->c16_sfEvent, false);
          }

          guard9 = true;
        }

        if (guard9 == true) {
          CV_EML_MCDC(36, 0, 0, false);
          CV_EML_IF(36, 0, 0, false);
          c16_u_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c16_u_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_y_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_v_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_v_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            *chartInstance->c16_sensor_capacitor_charge, 0.0, -1, 3U,
            *chartInstance->c16_sensor_capacitor_charge <= 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_v_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_mb_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_w_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_w_out = CV_EML_IF(23, 0, 0, CV_RELATIONAL_EVAL(5U, 23U, 0,
              *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
              *chartInstance->c16_sensor_lever_angle < 5.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_w_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_bc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_x_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[7U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 39U,
                  chartInstance->c16_sfEvent, false);
              }

              guard8 = false;
              if (CV_EML_COND(39, 0, 0, chartInstance->c16_ball_catching == 3.0))
              {
                if (CV_EML_COND(39, 0, 1, chartInstance->c16_ball_shoot == 1.0))
                {
                  CV_EML_MCDC(39, 0, 0, true);
                  CV_EML_IF(39, 0, 0, true);
                  c16_x_out = true;
                } else {
                  guard8 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[1U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 39U,
                    chartInstance->c16_sfEvent, false);
                }

                guard8 = true;
              }

              if (guard8 == true) {
                CV_EML_MCDC(39, 0, 0, false);
                CV_EML_IF(39, 0, 0, false);
                c16_x_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_x_out) {
                chartInstance->c16_stateChanged = true;
              } else {
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_gb_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_y_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                if (!chartInstance->c16_dataWrittenToVector[6U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 26U,
                    chartInstance->c16_sfEvent, false);
                }

                guard7 = false;
                if (CV_EML_COND(26, 0, 0,
                                *chartInstance->c16_sensor_ball_distance <=
                                chartInstance->c16_ball_threshold)) {
                  if (CV_EML_COND(26, 0, 1, chartInstance->c16_ball_catching ==
                                  1.0)) {
                    CV_EML_MCDC(26, 0, 0, true);
                    CV_EML_IF(26, 0, 0, true);
                    c16_y_out = true;
                  } else {
                    guard7 = true;
                  }
                } else {
                  if (!chartInstance->c16_dataWrittenToVector[7U]) {
                    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 26U,
                      chartInstance->c16_sfEvent, false);
                  }

                  guard7 = true;
                }

                if (guard7 == true) {
                  CV_EML_MCDC(26, 0, 0, false);
                  CV_EML_IF(26, 0, 0, false);
                  c16_y_out = false;
                }

                _SFD_SYMBOL_SCOPE_POP();
                if (c16_y_out) {
                  chartInstance->c16_stateChanged = true;
                } else {
                  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U,
                    c16_dc_debug_family_names, c16_debug_family_var_map);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_nargin, 0U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_nargout, 1U,
                    c16_sf_marshallOut, c16_sf_marshallIn);
                  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ab_out, 2U,
                    c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                  c16_ab_out = CV_EML_IF(13, 0, 0, CV_RELATIONAL_EVAL(5U, 13U, 0,
                    *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 5U,
                    *chartInstance->c16_sensor_capacitor_charge >= 4.99));
                  _SFD_SYMBOL_SCOPE_POP();
                  if (c16_ab_out) {
                    chartInstance->c16_stateChanged = true;
                  }
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode7:
        CV_CHART_EVAL(13, 0, 8);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_qc_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_bb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_bb_out = CV_EML_IF(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
          *chartInstance->c16_sensor_lever_angle, 0.01, -1, 4U,
          *chartInstance->c16_sensor_lever_angle > 0.01));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_bb_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_hc_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_cb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 32U,
              chartInstance->c16_sfEvent, false);
          }

          guard6 = false;
          if (CV_EML_COND(32, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(32, 0, 1, chartInstance->c16_ball_catching == 2.0))
            {
              CV_EML_MCDC(32, 0, 0, true);
              CV_EML_IF(32, 0, 0, true);
              c16_cb_out = true;
            } else {
              guard6 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 32U,
                chartInstance->c16_sfEvent, false);
            }

            guard6 = true;
          }

          if (guard6 == true) {
            CV_EML_MCDC(32, 0, 0, false);
            CV_EML_IF(32, 0, 0, false);
            c16_cb_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_cb_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_tc_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_db_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 34U,
                chartInstance->c16_sfEvent, false);
            }

            guard4 = false;
            guard5 = false;
            if (CV_EML_COND(34, 0, 0, chartInstance->c16_ball_catching == 3.0))
            {
              if (CV_EML_COND(34, 0, 1, *chartInstance->c16_user_shoot_button ==
                              1.0)) {
                if (CV_EML_COND(34, 0, 2, chartInstance->c16_ball_shoot == 0.0))
                {
                  CV_EML_MCDC(34, 0, 0, true);
                  CV_EML_IF(34, 0, 0, true);
                  c16_db_out = true;
                } else {
                  guard4 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[1U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 34U,
                    chartInstance->c16_sfEvent, false);
                }

                guard5 = true;
              }
            } else {
              guard5 = true;
            }

            if (guard5 == true) {
              guard4 = true;
            }

            if (guard4 == true) {
              CV_EML_MCDC(34, 0, 0, false);
              CV_EML_IF(34, 0, 0, false);
              c16_db_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_db_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_rc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_eb_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              if (!chartInstance->c16_dataWrittenToVector[6U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 33U,
                  chartInstance->c16_sfEvent, false);
              }

              guard3 = false;
              if (CV_EML_COND(33, 0, 0, *chartInstance->c16_sensor_ball_distance
                              > chartInstance->c16_ball_threshold)) {
                if (CV_EML_COND(33, 0, 1, chartInstance->c16_ball_catching ==
                                2.0)) {
                  CV_EML_MCDC(33, 0, 0, true);
                  CV_EML_IF(33, 0, 0, true);
                  c16_eb_out = true;
                } else {
                  guard3 = true;
                }
              } else {
                if (!chartInstance->c16_dataWrittenToVector[7U]) {
                  _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 33U,
                    chartInstance->c16_sfEvent, false);
                }

                guard3 = true;
              }

              if (guard3 == true) {
                CV_EML_MCDC(33, 0, 0, false);
                CV_EML_IF(33, 0, 0, false);
                c16_eb_out = false;
              }

              _SFD_SYMBOL_SCOPE_POP();
              if (c16_eb_out) {
                chartInstance->c16_stateChanged = true;
              } else {
                _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_xb_debug_family_names,
                  c16_debug_family_var_map);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_nargin, 0U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_nargout, 1U,
                  c16_sf_marshallOut, c16_sf_marshallIn);
                _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_fb_out, 2U,
                  c16_b_sf_marshallOut, c16_b_sf_marshallIn);
                c16_fb_out = CV_EML_IF(10, 0, 0, CV_RELATIONAL_EVAL(5U, 10U, 0, *
                  chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
                  *chartInstance->c16_sensor_capacitor_charge < 4.99));
                _SFD_SYMBOL_SCOPE_POP();
                if (c16_fb_out) {
                  chartInstance->c16_stateChanged = true;
                }
              }
            }
          }
        }
        break;

       case c16_IN_Mode8:
        CV_CHART_EVAL(13, 0, 9);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_oc_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_gb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_gb_out = CV_EML_IF(4, 0, 0, *chartInstance->c16_sensor_lever_angle >=
          5.0);
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_gb_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ac_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_hb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          c16_hb_out = CV_EML_IF(20, 0, 0, CV_RELATIONAL_EVAL(5U, 20U, 0,
            *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
            *chartInstance->c16_sensor_capacitor_charge < 4.99));
          _SFD_SYMBOL_SCOPE_POP();
          if (c16_hb_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_mc_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_ib_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            c16_ib_out = CV_EML_IF(11, 0, 0,
              *chartInstance->c16_sensor_lever_angle <= 0.0);
            _SFD_SYMBOL_SCOPE_POP();
            if (c16_ib_out) {
              chartInstance->c16_stateChanged = true;
            }
          }
        }
        break;

       case c16_IN_Mode9:
        CV_CHART_EVAL(13, 0, 10);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_ec_debug_family_names,
          c16_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_nargin, 0U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_nargout, 1U,
          c16_sf_marshallOut, c16_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_jb_out, 2U,
          c16_b_sf_marshallOut, c16_b_sf_marshallIn);
        c16_jb_out = CV_EML_IF(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0,
          *chartInstance->c16_sensor_capacitor_charge, 4.99, -1, 2U,
          *chartInstance->c16_sensor_capacitor_charge < 4.99));
        _SFD_SYMBOL_SCOPE_POP();
        if (c16_jb_out) {
          chartInstance->c16_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_jc_debug_family_names,
            c16_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_nargin, 0U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_nargout, 1U,
            c16_sf_marshallOut, c16_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_kb_out, 2U,
            c16_b_sf_marshallOut, c16_b_sf_marshallIn);
          if (!chartInstance->c16_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 27U,
              chartInstance->c16_sfEvent, false);
          }

          guard2 = false;
          if (CV_EML_COND(27, 0, 0, *chartInstance->c16_sensor_ball_distance <=
                          chartInstance->c16_ball_threshold)) {
            if (CV_EML_COND(27, 0, 1, chartInstance->c16_ball_catching == 1.0))
            {
              CV_EML_MCDC(27, 0, 0, true);
              CV_EML_IF(27, 0, 0, true);
              c16_kb_out = true;
            } else {
              guard2 = true;
            }
          } else {
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 27U,
                chartInstance->c16_sfEvent, false);
            }

            guard2 = true;
          }

          if (guard2 == true) {
            CV_EML_MCDC(27, 0, 0, false);
            CV_EML_IF(27, 0, 0, false);
            c16_kb_out = false;
          }

          _SFD_SYMBOL_SCOPE_POP();
          if (c16_kb_out) {
            chartInstance->c16_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_fc_debug_family_names,
              c16_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_nargin, 0U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_nargout, 1U,
              c16_sf_marshallOut, c16_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_lb_out, 2U,
              c16_b_sf_marshallOut, c16_b_sf_marshallIn);
            if (!chartInstance->c16_dataWrittenToVector[7U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 35U,
                chartInstance->c16_sfEvent, false);
            }

            guard1 = false;
            if (CV_EML_COND(35, 0, 0, chartInstance->c16_ball_catching == 3.0))
            {
              if (CV_EML_COND(35, 0, 1, chartInstance->c16_ball_shoot == 1.0)) {
                CV_EML_MCDC(35, 0, 0, true);
                CV_EML_IF(35, 0, 0, true);
                c16_lb_out = true;
              } else {
                guard1 = true;
              }
            } else {
              if (!chartInstance->c16_dataWrittenToVector[1U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 35U,
                  chartInstance->c16_sfEvent, false);
              }

              guard1 = true;
            }

            if (guard1 == true) {
              CV_EML_MCDC(35, 0, 0, false);
              CV_EML_IF(35, 0, 0, false);
              c16_lb_out = false;
            }

            _SFD_SYMBOL_SCOPE_POP();
            if (c16_lb_out) {
              chartInstance->c16_stateChanged = true;
            } else {
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c16_lc_debug_family_names,
                c16_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_nargin, 0U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_nargout, 1U,
                c16_sf_marshallOut, c16_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c16_mb_out, 2U,
                c16_b_sf_marshallOut, c16_b_sf_marshallIn);
              c16_mb_out = CV_EML_IF(12, 0, 0, CV_RELATIONAL_EVAL(5U, 12U, 0,
                *chartInstance->c16_sensor_lever_angle, 5.0, -1, 2U,
                *chartInstance->c16_sensor_lever_angle < 5.0));
              _SFD_SYMBOL_SCOPE_POP();
              if (c16_mb_out) {
                chartInstance->c16_stateChanged = true;
              }
            }
          }
        }
        break;

       default:
        CV_CHART_EVAL(13, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c16_is_c16_Integrated_system_V061 =
          c16_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c16_stateChanged) {
      *c16_zcVar = 1.0;
    } else {
      *c16_zcVar = -1.0;
    }
  }
}

static void derivatives_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c16_is_c16_Integrated_system_V061) {
   case c16_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode10:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode2:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode3:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode4:
    CV_CHART_EVAL(13, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode5:
    CV_CHART_EVAL(13, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode6:
    CV_CHART_EVAL(13, 0, 7);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode7:
    CV_CHART_EVAL(13, 0, 8);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode8:
    CV_CHART_EVAL(13, 0, 9);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode9:
    CV_CHART_EVAL(13, 0, 10);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
    break;
  }
}

static void outputs_c16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c16_is_c16_Integrated_system_V061) {
   case c16_IN_Mode1:
    CV_CHART_EVAL(13, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode10:
    CV_CHART_EVAL(13, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode2:
    CV_CHART_EVAL(13, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode3:
    CV_CHART_EVAL(13, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode4:
    CV_CHART_EVAL(13, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode5:
    CV_CHART_EVAL(13, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode6:
    CV_CHART_EVAL(13, 0, 7);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode7:
    CV_CHART_EVAL(13, 0, 8);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode8:
    CV_CHART_EVAL(13, 0, 9);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c16_sfEvent);
    break;

   case c16_IN_Mode9:
    CV_CHART_EVAL(13, 0, 10);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c16_sfEvent);
    break;

   default:
    CV_CHART_EVAL(13, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c16_is_c16_Integrated_system_V061 = c16_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c16_sfEvent);
    break;
  }
}

static void initSimStructsc16_Integrated_system_V061
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c16_eml_ini_fcn_to_be_inlined_181
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c16_eml_term_fcn_to_be_inlined_181
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c16_machineNumber, uint32_T
  c16_chartNumber, uint32_T c16_instanceNumber)
{
  (void)c16_machineNumber;
  (void)c16_chartNumber;
  (void)c16_instanceNumber;
}

static const mxArray *c16_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const real_T c16_u)
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 0, 0U, 0U, 0U, 0), false);
  return c16_y;
}

static const mxArray *c16_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  sf_mex_assign(&c16_mxArrayOutData, c16_emlrt_marshallOut(chartInstance,
    *(real_T *)c16_inData), false);
  return c16_mxArrayOutData;
}

static real_T c16_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_nargout, const char_T *c16_identifier)
{
  real_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_nargout),
    &c16_thisId);
  sf_mex_destroy(&c16_nargout);
  return c16_y;
}

static real_T c16_b_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  real_T c16_y;
  real_T c16_d0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_d0, 1, 0, 0U, 0, 0U, 0);
  c16_y = c16_d0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(real_T *)c16_outData = c16_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_mxArrayInData), c16_varName);
  sf_mex_destroy(&c16_mxArrayInData);
}

static const mxArray *c16_b_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c16_u)
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 11, 0U, 0U, 0U, 0), false);
  return c16_y;
}

static const mxArray *c16_b_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  sf_mex_assign(&c16_mxArrayOutData, c16_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c16_inData), false);
  return c16_mxArrayOutData;
}

static boolean_T c16_c_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_sf_internal_predicateOutput, const char_T *c16_identifier)
{
  boolean_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_sf_internal_predicateOutput), &c16_thisId);
  sf_mex_destroy(&c16_sf_internal_predicateOutput);
  return c16_y;
}

static boolean_T c16_d_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  boolean_T c16_y;
  boolean_T c16_b0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_b0, 1, 11, 0U, 0, 0U, 0);
  c16_y = c16_b0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(boolean_T *)c16_outData = c16_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_mxArrayInData), c16_varName);
  sf_mex_destroy(&c16_mxArrayInData);
}

const mxArray *sf_c16_Integrated_system_V061_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c16_nameCaptureInfo = NULL;
  c16_nameCaptureInfo = NULL;
  sf_mex_assign(&c16_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c16_nameCaptureInfo;
}

static const mxArray *c16_c_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c16_u)
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 6, 0U, 0U, 0U, 0), false);
  return c16_y;
}

static const mxArray *c16_c_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  sf_mex_assign(&c16_mxArrayOutData, c16_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c16_inData), false);
  return c16_mxArrayOutData;
}

static int32_T c16_e_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_sfEvent, const char_T *c16_identifier)
{
  int32_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_sfEvent),
    &c16_thisId);
  sf_mex_destroy(&c16_b_sfEvent);
  return c16_y;
}

static int32_T c16_f_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  int32_T c16_y;
  int32_T c16_i0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_i0, 1, 6, 0U, 0, 0U, 0);
  c16_y = c16_i0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(int32_T *)c16_outData = c16_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_mxArrayInData), c16_varName);
  sf_mex_destroy(&c16_mxArrayInData);
}

static const mxArray *c16_d_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c16_u)
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", &c16_u, 3, 0U, 0U, 0U, 0), false);
  return c16_y;
}

static const mxArray *c16_d_sf_marshallOut(void *chartInstanceVoid, void
  *c16_inData)
{
  const mxArray *c16_mxArrayOutData = NULL;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c16_mxArrayOutData = NULL;
  sf_mex_assign(&c16_mxArrayOutData, c16_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c16_inData), false);
  return c16_mxArrayOutData;
}

static uint8_T c16_g_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_tp_Mode3, const char_T *c16_identifier)
{
  uint8_T c16_y;
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_y = c16_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_tp_Mode3),
    &c16_thisId);
  sf_mex_destroy(&c16_b_tp_Mode3);
  return c16_y;
}

static uint8_T c16_h_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  uint8_T c16_y;
  uint8_T c16_u0;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), &c16_u0, 1, 3, 0U, 0, 0U, 0);
  c16_y = c16_u0;
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void c16_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c16_mxArrayInData, const char_T *c16_varName, void *c16_outData)
{
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(uint8_T *)c16_outData = c16_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_mxArrayInData), c16_varName);
  sf_mex_destroy(&c16_mxArrayInData);
}

static const mxArray *c16_e_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c16_y;
  int32_T c16_i1;
  boolean_T c16_bv0[9];
  c16_y = NULL;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_createcellmatrix(14, 1), false);
  sf_mex_setcell(c16_y, 0, c16_emlrt_marshallOut(chartInstance,
    *chartInstance->c16_IGBT_PWM));
  sf_mex_setcell(c16_y, 1, c16_emlrt_marshallOut(chartInstance,
    *chartInstance->c16_bridge_PWM_1));
  sf_mex_setcell(c16_y, 2, c16_emlrt_marshallOut(chartInstance,
    *chartInstance->c16_bridge_PWM_2));
  sf_mex_setcell(c16_y, 3, c16_emlrt_marshallOut(chartInstance,
    *chartInstance->c16_mode));
  sf_mex_setcell(c16_y, 4, c16_emlrt_marshallOut(chartInstance,
    *chartInstance->c16_user_shoot_power));
  sf_mex_setcell(c16_y, 5, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_ball_catching));
  sf_mex_setcell(c16_y, 6, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_ball_shoot));
  sf_mex_setcell(c16_y, 7, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_ball_threshold));
  sf_mex_setcell(c16_y, 8, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_delta));
  sf_mex_setcell(c16_y, 9, c16_d_emlrt_marshallOut(chartInstance,
    chartInstance->c16_is_active_c16_Integrated_system_V061));
  sf_mex_setcell(c16_y, 10, c16_d_emlrt_marshallOut(chartInstance,
    chartInstance->c16_is_c16_Integrated_system_V061));
  sf_mex_setcell(c16_y, 11, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_temporalCounter_i1));
  sf_mex_setcell(c16_y, 12, c16_emlrt_marshallOut(chartInstance,
    chartInstance->c16_previousTime));
  for (c16_i1 = 0; c16_i1 < 9; c16_i1++) {
    c16_bv0[c16_i1] = chartInstance->c16_dataWrittenToVector[c16_i1];
  }

  sf_mex_setcell(c16_y, 13, c16_f_emlrt_marshallOut(chartInstance, c16_bv0));
  return c16_y;
}

static const mxArray *c16_f_emlrt_marshallOut
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c16_u[9])
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_create("y", c16_u, 11, 0U, 1U, 0U, 1, 9), false);
  return c16_y;
}

static void c16_i_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u)
{
  boolean_T c16_bv1[9];
  int32_T c16_i2;
  *chartInstance->c16_IGBT_PWM = c16_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("IGBT_PWM", c16_u, 0)), "IGBT_PWM");
  *chartInstance->c16_bridge_PWM_1 = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("bridge_PWM_1", c16_u, 1)), "bridge_PWM_1");
  *chartInstance->c16_bridge_PWM_2 = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("bridge_PWM_2", c16_u, 2)), "bridge_PWM_2");
  *chartInstance->c16_mode = c16_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("mode", c16_u, 3)), "mode");
  *chartInstance->c16_user_shoot_power = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("user_shoot_power", c16_u, 4)), "user_shoot_power");
  chartInstance->c16_ball_catching = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("ball_catching", c16_u, 5)), "ball_catching");
  chartInstance->c16_ball_shoot = c16_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("ball_shoot", c16_u, 6)), "ball_shoot");
  chartInstance->c16_ball_threshold = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("ball_threshold", c16_u, 7)), "ball_threshold");
  chartInstance->c16_delta = c16_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("delta", c16_u, 8)), "delta");
  chartInstance->c16_is_active_c16_Integrated_system_V061 =
    c16_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c16_Integrated_system_V061", c16_u, 9)),
    "is_active_c16_Integrated_system_V061");
  chartInstance->c16_is_c16_Integrated_system_V061 = c16_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c16_Integrated_system_V061",
       c16_u, 10)), "is_c16_Integrated_system_V061");
  chartInstance->c16_temporalCounter_i1 = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("temporalCounter_i1", c16_u, 11)),
    "temporalCounter_i1");
  chartInstance->c16_previousTime = c16_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("previousTime", c16_u, 12)), "previousTime");
  c16_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c16_u, 13)), "dataWrittenToVector", c16_bv1);
  for (c16_i2 = 0; c16_i2 < 9; c16_i2++) {
    chartInstance->c16_dataWrittenToVector[c16_i2] = c16_bv1[c16_i2];
  }

  sf_mex_assign(&chartInstance->c16_setSimStateSideEffectsInfo,
                c16_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c16_u, 14)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c16_u);
}

static void c16_j_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_b_dataWrittenToVector, const char_T
  *c16_identifier, boolean_T c16_y[9])
{
  emlrtMsgIdentifier c16_thisId;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  c16_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c16_b_dataWrittenToVector),
    &c16_thisId, c16_y);
  sf_mex_destroy(&c16_b_dataWrittenToVector);
}

static void c16_k_emlrt_marshallIn(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c16_u, const emlrtMsgIdentifier *c16_parentId,
  boolean_T c16_y[9])
{
  boolean_T c16_bv2[9];
  int32_T c16_i3;
  (void)chartInstance;
  sf_mex_import(c16_parentId, sf_mex_dup(c16_u), c16_bv2, 1, 11, 0U, 1, 0U, 1, 9);
  for (c16_i3 = 0; c16_i3 < 9; c16_i3++) {
    c16_y[c16_i3] = c16_bv2[c16_i3];
  }

  sf_mex_destroy(&c16_u);
}

static const mxArray *c16_l_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_b_setSimStateSideEffectsInfo, const char_T *c16_identifier)
{
  const mxArray *c16_y = NULL;
  emlrtMsgIdentifier c16_thisId;
  c16_y = NULL;
  c16_thisId.fIdentifier = c16_identifier;
  c16_thisId.fParent = NULL;
  sf_mex_assign(&c16_y, c16_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c16_b_setSimStateSideEffectsInfo), &c16_thisId), false);
  sf_mex_destroy(&c16_b_setSimStateSideEffectsInfo);
  return c16_y;
}

static const mxArray *c16_m_emlrt_marshallIn
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c16_u, const emlrtMsgIdentifier *c16_parentId)
{
  const mxArray *c16_y = NULL;
  (void)chartInstance;
  (void)c16_parentId;
  c16_y = NULL;
  sf_mex_assign(&c16_y, sf_mex_duplicatearraysafe(&c16_u), false);
  sf_mex_destroy(&c16_u);
  return c16_y;
}

static void init_test_point_addr_map(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  chartInstance->c16_testPointAddrMap[0] = &chartInstance->c16_ball_shoot;
  chartInstance->c16_testPointAddrMap[1] = &chartInstance->c16_ball_threshold;
  chartInstance->c16_testPointAddrMap[2] = &chartInstance->c16_ball_catching;
  chartInstance->c16_testPointAddrMap[3] = &chartInstance->c16_delta;
  chartInstance->c16_testPointAddrMap[4] = &chartInstance->c16_tp_Mode1;
  chartInstance->c16_testPointAddrMap[5] = &chartInstance->c16_tp_Mode10;
  chartInstance->c16_testPointAddrMap[6] = &chartInstance->c16_tp_Mode2;
  chartInstance->c16_testPointAddrMap[7] = &chartInstance->c16_tp_Mode3;
  chartInstance->c16_testPointAddrMap[8] = &chartInstance->c16_tp_Mode4;
  chartInstance->c16_testPointAddrMap[9] = &chartInstance->c16_tp_Mode5;
  chartInstance->c16_testPointAddrMap[10] = &chartInstance->c16_tp_Mode6;
  chartInstance->c16_testPointAddrMap[11] = &chartInstance->c16_tp_Mode7;
  chartInstance->c16_testPointAddrMap[12] = &chartInstance->c16_tp_Mode8;
  chartInstance->c16_testPointAddrMap[13] = &chartInstance->c16_tp_Mode9;
}

static void **get_test_point_address_map
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  return &chartInstance->c16_testPointAddrMap[0];
}

static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  return &chartInstance->c16_testPointMappingInfo;
}

static void **get_dataset_logging_obj_vector
  (SFc16_Integrated_system_V061InstanceStruct *chartInstance)
{
  return &chartInstance->c16_dataSetLogObjVector[0];
}

static void init_dsm_address_info(SFc16_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc16_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  chartInstance->c16_user_shoot_power = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c16_user_shoot_button = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c16_sensor_ball_distance = (real_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c16_sensor_lever_angle = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c16_sensor_capacitor_charge = (real_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 3);
  chartInstance->c16_IGBT_PWM = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c16_bridge_PWM_1 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c16_bridge_PWM_2 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c16_mode = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c16_V_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static void init_test_point_mapping_info(SimStruct *S);
void sf_c16_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(4226087715U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(2008777319U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1247086312U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3647025625U);
}

mxArray* sf_c16_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c16_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("4bhgFuaJDe96W4yKcaoZND");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c16_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c16_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c16_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c16_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c16_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c16_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[54],T\"IGBT_PWM\",},{M[1],M[55],T\"bridge_PWM_1\",},{M[1],M[56],T\"bridge_PWM_2\",},{M[1],M[124],T\"mode\",},{M[1],M[37],T\"user_shoot_power\",},{M[3],M[103],T\"ball_catching\",},{M[3],M[35],T\"ball_shoot\",},{M[3],M[102],T\"ball_threshold\",},{M[3],M[123],T\"delta\",},{M[8],M[0],T\"is_active_c16_Integrated_system_V061\",}}",
    "100 S1x4'type','srcId','name','auxInfo'{{M[9],M[0],T\"is_c16_Integrated_system_V061\",},{M[11],M[0],T\"temporalCounter_i1\",S'et','os','ct'{{T\"at\",M[131],M[1]}}},{M[13],M[0],T\"previousTime\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 14, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c16_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc16_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           16,
           10,
           40,
           0,
           14,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"ball_catching");
          _SFD_SET_DATA_PROPS(1,0,0,0,"ball_shoot");
          _SFD_SET_DATA_PROPS(2,0,0,0,"ball_threshold");
          _SFD_SET_DATA_PROPS(3,0,0,0,"delta");
          _SFD_SET_DATA_PROPS(4,1,1,0,"user_shoot_button");
          _SFD_SET_DATA_PROPS(5,1,1,0,"sensor_ball_distance");
          _SFD_SET_DATA_PROPS(6,1,1,0,"sensor_lever_angle");
          _SFD_SET_DATA_PROPS(7,1,1,0,"sensor_capacitor_charge");
          _SFD_SET_DATA_PROPS(8,1,1,0,"V_in");
          _SFD_SET_DATA_PROPS(9,2,0,1,"user_shoot_power");
          _SFD_SET_DATA_PROPS(10,2,0,1,"IGBT_PWM");
          _SFD_SET_DATA_PROPS(11,2,0,1,"bridge_PWM_1");
          _SFD_SET_DATA_PROPS(12,2,0,1,"bridge_PWM_2");
          _SFD_SET_DATA_PROPS(13,2,0,1,"mode");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,0);
          _SFD_STATE_INFO(8,0,0);
          _SFD_STATE_INFO(9,0,0);
          _SFD_CH_SUBSTATE_COUNT(10);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_CH_SUBSTATE_INDEX(4,4);
          _SFD_CH_SUBSTATE_INDEX(5,5);
          _SFD_CH_SUBSTATE_INDEX(6,6);
          _SFD_CH_SUBSTATE_INDEX(7,7);
          _SFD_CH_SUBSTATE_INDEX(8,8);
          _SFD_CH_SUBSTATE_INDEX(9,9);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(6,0);
          _SFD_ST_SUBSTATE_COUNT(7,0);
          _SFD_ST_SUBSTATE_COUNT(8,0);
          _SFD_ST_SUBSTATE_COUNT(9,0);
        }

        _SFD_CV_INIT_CHART(10,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(8,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(9,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(28,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(25,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(16,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(15,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(29,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(14,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(18,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(19,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(30,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(26,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(31,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(21,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(23,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(22,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(24,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(38,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(36,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(37,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(10,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(17,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(20,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(39,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(13,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(35,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(32,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(27,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(12,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(33,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(34,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(6,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(8,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(9,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(7,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(28,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(28,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(28,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(25,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(25,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(25,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(8,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(8,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(7,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(7,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(7,0,0,2,24,-1,2);
        _SFD_CV_INIT_EML(16,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(16,0,0,1,27,1,27);
        _SFD_CV_INIT_EML_RELATIONAL(16,0,0,2,27,-1,4);
        _SFD_CV_INIT_EML(15,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(15,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(29,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(29,0,0,1,61,1,48);

        {
          static int condStart[] = { 2, 43 };

          static int condEnd[] = { 39, 61 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(29,0,0,2,61,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(14,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(14,0,0,1,29,1,29);
        _SFD_CV_INIT_EML_RELATIONAL(14,0,0,2,29,-1,4);
        _SFD_CV_INIT_EML(6,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,30,1,30);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,2,30,-1,3);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,29,1,29);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,2,29,-1,4);
        _SFD_CV_INIT_EML(9,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(9,0,0,1,30,1,30);
        _SFD_CV_INIT_EML_RELATIONAL(9,0,0,2,30,-1,3);
        _SFD_CV_INIT_EML(18,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(18,0,0,1,29,1,29);
        _SFD_CV_INIT_EML_RELATIONAL(18,0,0,2,29,-1,4);
        _SFD_CV_INIT_EML(19,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(19,0,0,1,30,1,30);
        _SFD_CV_INIT_EML_RELATIONAL(19,0,0,2,30,-1,3);
        _SFD_CV_INIT_EML(30,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(30,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(30,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(26,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(26,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(26,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(31,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(31,0,0,1,61,1,48);

        {
          static int condStart[] = { 2, 43 };

          static int condEnd[] = { 39, 61 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(31,0,0,2,61,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(21,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(21,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(23,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(23,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(23,0,0,2,24,-1,2);
        _SFD_CV_INIT_EML(22,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(22,0,0,1,27,1,27);
        _SFD_CV_INIT_EML_RELATIONAL(22,0,0,2,27,-1,4);
        _SFD_CV_INIT_EML(24,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(24,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(38,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(38,0,0,1,18,1,18);
        _SFD_CV_INIT_EML(36,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(36,0,0,1,40,1,40);

        {
          static int condStart[] = { 2, 25 };

          static int condEnd[] = { 20, 40 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(36,0,0,2,40,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(37,0,0,0,1,0,0,0,0,0,3,1);
        _SFD_CV_INIT_EML_IF(37,0,0,1,65,1,48);

        {
          static int condStart[] = { 2, 24, 50 };

          static int condEnd[] = { 20, 46, 65 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3 };

          _SFD_CV_INIT_EML_MCDC(37,0,0,2,65,3,0,&(condStart[0]),&(condEnd[0]),5,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,33,1,33);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,33,-1,5);
        _SFD_CV_INIT_EML(10,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(10,0,0,1,32,1,32);
        _SFD_CV_INIT_EML_RELATIONAL(10,0,0,2,32,-1,2);
        _SFD_CV_INIT_EML(17,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(17,0,0,1,33,1,33);
        _SFD_CV_INIT_EML_RELATIONAL(17,0,0,2,33,-1,5);
        _SFD_CV_INIT_EML(20,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(20,0,0,1,32,1,32);
        _SFD_CV_INIT_EML_RELATIONAL(20,0,0,2,32,-1,2);
        _SFD_CV_INIT_EML(39,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(39,0,0,1,39,1,39);

        {
          static int condStart[] = { 2, 24 };

          static int condEnd[] = { 20, 39 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(39,0,0,2,39,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(13,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(13,0,0,1,33,1,33);
        _SFD_CV_INIT_EML_RELATIONAL(13,0,0,2,33,-1,5);
        _SFD_CV_INIT_EML(5,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(5,0,0,1,32,1,32);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,2,32,-1,2);
        _SFD_CV_INIT_EML(35,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(35,0,0,1,39,1,39);

        {
          static int condStart[] = { 2, 24 };

          static int condEnd[] = { 20, 39 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(35,0,0,2,39,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(32,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(32,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(32,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(27,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(27,0,0,1,62,1,48);

        {
          static int condStart[] = { 2, 44 };

          static int condEnd[] = { 40, 62 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(27,0,0,2,62,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(12,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(12,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(12,0,0,2,24,-1,2);
        _SFD_CV_INIT_EML(11,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(11,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,1,25,1,25);
        _SFD_CV_INIT_EML(3,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(3,0,0,1,27,1,27);
        _SFD_CV_INIT_EML_RELATIONAL(3,0,0,2,27,-1,4);
        _SFD_CV_INIT_EML(33,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(33,0,0,1,61,1,48);

        {
          static int condStart[] = { 2, 43 };

          static int condEnd[] = { 39, 61 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(33,0,0,2,61,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(34,0,0,0,1,0,0,0,0,0,3,1);
        _SFD_CV_INIT_EML_IF(34,0,0,1,65,1,48);

        {
          static int condStart[] = { 2, 24, 50 };

          static int condEnd[] = { 20, 46, 65 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3 };

          _SFD_CV_INIT_EML_MCDC(34,0,0,2,65,3,0,&(condStart[0]),&(condEnd[0]),5,
                                &(pfixExpr[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c16_sf_marshallOut,(MexInFcnForType)c16_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc16_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(9U, chartInstance->c16_user_shoot_power);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c16_ball_shoot);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c16_user_shoot_button);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c16_sensor_ball_distance);
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c16_sensor_lever_angle);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c16_sensor_capacitor_charge);
        _SFD_SET_DATA_VALUE_PTR(10U, chartInstance->c16_IGBT_PWM);
        _SFD_SET_DATA_VALUE_PTR(11U, chartInstance->c16_bridge_PWM_1);
        _SFD_SET_DATA_VALUE_PTR(12U, chartInstance->c16_bridge_PWM_2);
        _SFD_SET_DATA_VALUE_PTR(13U, chartInstance->c16_mode);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c16_ball_threshold);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c16_ball_catching);
        _SFD_SET_DATA_VALUE_PTR(3U, &chartInstance->c16_delta);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c16_V_in);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "s3G9UBNsdZIXUZiQp4ztNPE";
}

static void sf_opaque_initialize_c16_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc16_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c16_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c16_Integrated_system_V061((SFc16_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c16_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c16_Integrated_system_V061((SFc16_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c16_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c16_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c16_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c16_Integrated_system_V061((SFc16_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c16_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c16_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c16_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c16_Integrated_system_V061(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc16_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c16_Integrated_system_V061
      ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    if (!sim_mode_is_rtw_gen(S)) {
      ssSetModelMappingInfoPtr(S, NULL);
    }

    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc16_Integrated_system_V061
    ((SFc16_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c16_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c16_Integrated_system_V061
      ((SFc16_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c16_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  ssSetNeedAbsoluteTime(S,1);
  ssSetModelReferenceSampleTimeDisallowInheritance(S);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      16);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,16,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,16);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,16,5);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,16,5);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=5; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 5; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,16);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  if (sim_mode_is_modelref_sim(S)) {
    mxArray *prhs[4];
    mxArray *plhs[1];
    uint32_T newChkSum[4];
    double *inPr, *outPr;
    int i;
    int firstSlashIdx = 0;
    for (i = 0; i < 999 && S->path[i] != '\0' ; ++i) {
      if (S->path[i] == '/') {
        firstSlashIdx = i;
        break;
      }
    }

    prhs[0] = mxCreateString("Private");
    prhs[1] = mxCreateString("md5");
    prhs[2] = mxCreateDoubleMatrix(1, 4, mxREAL);
    prhs[3] = mxCreateString(S->path + firstSlashIdx + 1);
    inPr = mxGetPr(prhs[2]);
    inPr[0] = 4084002931U;
    inPr[1] = 3879323441U;
    inPr[2] = 1384298676U;
    inPr[3] = 1107209737U;
    mexCallMATLAB(1, plhs, 4, prhs, "sf");
    outPr = mxGetPr(plhs[0]);
    for (i = 0; i < 4; ++i) {
      newChkSum[i] = (uint32_T) outPr[i];
    }

    ssSetChecksum0(S, newChkSum[0]);
    ssSetChecksum1(S, newChkSum[1]);
    ssSetChecksum2(S, newChkSum[2]);
    ssSetChecksum3(S, newChkSum[3]);
    mxDestroyArray(plhs[0]);
    mxDestroyArray(prhs[0]);
    mxDestroyArray(prhs[1]);
    mxDestroyArray(prhs[2]);
    mxDestroyArray(prhs[3]);
  } else {
    ssSetChecksum0(S,(4084002931U));
    ssSetChecksum1(S,(3879323441U));
    ssSetChecksum2(S,(1384298676U));
    ssSetChecksum3(S,(1107209737U));
  }

  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c16_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c16_Integrated_system_V061(SimStruct *S)
{
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc16_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc16_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c16_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c16_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c16_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c16_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c16_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c16_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c16_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c16_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c16_Integrated_system_V061;
  chartInstance->chartInfo.outputs =
    sf_opaque_outputs_c16_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c16_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c16_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c16_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c16_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
    init_test_point_mapping_info(S);
  }

  chart_debug_initialization(S,1);
}

void c16_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c16_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c16_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c16_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c16_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

static const rtwCAPI_DataTypeMap dataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, isComplex, isPointer */
  { "real_T", "real_T", 0, 0, sizeof(real_T), SS_DOUBLE, 0, 0 },

  { "uint8_T", "uint8_T", 0, 0, sizeof(uint8_T), SS_UINT8, 0, 0 } };

static real_T sfCAPIsampleTimeZero = 0.0;
static const rtwCAPI_SampleTimeMap sampleTimeMap[] = {
  /* *period, *offset, taskId, mode */
  { &sfCAPIsampleTimeZero, &sfCAPIsampleTimeZero, 0, 0 }
};

static const rtwCAPI_DimensionMap dimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims*/
  { rtwCAPI_SCALAR, 0, 2 } };

static const rtwCAPI_Signals testPointSignals[] = {
  /* addrMapIndex, sysNum, SFRelativePath, dataName, portNumber, dataTypeIndex, dimIndex, fixPtIdx, sTimeIndex */
  { 0, 0, "StateflowChart/ball_shoot", "ball_shoot", 0, 0, 0, 0, 0 },

  { 1, 0, "StateflowChart/ball_threshold", "ball_threshold", 0, 0, 0, 0, 0 },

  { 2, 0, "StateflowChart/ball_catching", "ball_catching", 0, 0, 0, 0, 0 },

  { 3, 0, "StateflowChart/delta", "delta", 0, 0, 0, 0, 0 },

  { 4, 0, "StateflowChart/Mode1", "Mode1", 0, 1, 0, 0, 0 },

  { 5, 0, "StateflowChart/Mode10", "Mode10", 0, 1, 0, 0, 0 },

  { 6, 0, "StateflowChart/Mode2", "Mode2", 0, 1, 0, 0, 0 },

  { 7, 0, "StateflowChart/Mode3", "Mode3", 0, 1, 0, 0, 0 },

  { 8, 0, "StateflowChart/Mode4", "Mode4", 0, 1, 0, 0, 0 },

  { 9, 0, "StateflowChart/Mode5", "Mode5", 0, 1, 0, 0, 0 },

  { 10, 0, "StateflowChart/Mode6", "Mode6", 0, 1, 0, 0, 0 },

  { 11, 0, "StateflowChart/Mode7", "Mode7", 0, 1, 0, 0, 0 },

  { 12, 0, "StateflowChart/Mode8", "Mode8", 0, 1, 0, 0, 0 },

  { 13, 0, "StateflowChart/Mode9", "Mode9", 0, 1, 0, 0, 0 } };

static const rtwCAPI_FixPtMap fixedPointMap[] = {
  /* *fracSlope, *bias, scaleType, wordLength, exponent, isSigned */
  { NULL, NULL, rtwCAPI_FIX_RESERVED, 64, 0, 0 } };

static const uint_T dimensionArray[] = {
  1, 1 };

static rtwCAPI_ModelMappingStaticInfo testPointMappingStaticInfo = {
  /* block signal monitoring */
  {
    testPointSignals,                  /* Block signals Array  */
    14,                                /* Num Block IO signals */
    NULL,                              /* Root Inputs Array    */
    0,                                 /* Num root inputs      */
    NULL,                              /* Root Outputs Array */
    0                                  /* Num root outputs   */
  },

  /* parameter tuning */
  {
    NULL,                              /* Block parameters Array    */
    0,                                 /* Num block parameters      */
    NULL,                              /* Variable parameters Array */
    0                                  /* Num variable parameters   */
  },

  /* block states */
  {
    NULL,                              /* Block States array        */
    0                                  /* Num Block States          */
  },

  /* Static maps */
  {
    dataTypeMap,                       /* Data Type Map            */
    dimensionMap,                      /* Data Dimension Map       */
    fixedPointMap,                     /* Fixed Point Map          */
    NULL,                              /* Structure Element map    */
    sampleTimeMap,                     /* Sample Times Map         */
    dimensionArray                     /* Dimension Array          */
  },

  /* Target type */
  "float",

  {
    4084002931U,
    3879323441U,
    1384298676U,
    1107209737U
  }
};

static void init_signal_logging_objects(SimStruct *S, rtwCAPI_ModelMappingInfo
  *testPointMappingInfo)
{
  void ** chartDatasetLoggingObjs;
  void *pSetDescr;
  int i;
  char chartPath[1000];
  size_t lastSlashIdx = 0;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInfo->chartInstance;
  chartDatasetLoggingObjs = get_dataset_logging_obj_vector(chartInstance);

  /* compute the chart path */
  for (i = 0; i < 999 && S->path[i]; i++) {
    if (S->path[i] == '/') {
      lastSlashIdx = i;
    }
  }

  strncpy(chartPath, S->path, lastSlashIdx);
  chartPath[lastSlashIdx] = '\0';

  /* Initialize signal log vector */
  for (i = 0; i < 14; ++i) {
    chartDatasetLoggingObjs[i] = NULL;
  }

  _ssLoggerCreateDatasetDescriptWithMMI(S, testPointMappingInfo, 0, NULL,
    NULL, &pSetDescr);
  if (pSetDescr) {
    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.Data",
        "ball_shoot",
        chartPath,
        "StateflowChart/ball_shoot",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "ball_shoot",
        1,
        dimArray,
        SS_DOUBLE,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.Data",
        "ball_threshold",
        chartPath,
        "StateflowChart/ball_threshold",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "ball_threshold",
        1,
        dimArray,
        SS_DOUBLE,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.Data",
        "ball_catching",
        chartPath,
        "StateflowChart/ball_catching",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "ball_catching",
        1,
        dimArray,
        SS_DOUBLE,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.Data",
        "delta",
        chartPath,
        "StateflowChart/delta",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "delta",
        1,
        dimArray,
        SS_DOUBLE,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode3",
        chartPath,
        "StateflowChart/Mode3",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode3",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode1",
        chartPath,
        "StateflowChart/Mode1",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode1",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode2",
        chartPath,
        "StateflowChart/Mode2",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode2",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode4",
        chartPath,
        "StateflowChart/Mode4",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode4",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode5",
        chartPath,
        "StateflowChart/Mode5",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode5",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode6",
        chartPath,
        "StateflowChart/Mode6",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode6",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode10",
        chartPath,
        "StateflowChart/Mode10",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode10",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode8",
        chartPath,
        "StateflowChart/Mode8",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode8",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode9",
        chartPath,
        "StateflowChart/Mode9",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode9",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    {
      void *pElementDescr;
      int_T dimArray[] = { 1 };

      ssLoggerAddStateflowElementDescription(S, &pSetDescr,
        "Stateflow.SimulationData.State",
        "Mode7",
        chartPath,
        "StateflowChart/Mode7",
        NULL,
        &pElementDescr);
      ssLoggerAddTimeseriesDescriptionWithInterpolation(S, &pSetDescr,
        &pElementDescr,
        "Mode7",
        1,
        dimArray,
        SS_UINT8,
        0,
        SS_LINEAR_INTERPOLATION);
    }

    ssLoggerCreateElementFromDescription(S, &pSetDescr,
      chartDatasetLoggingObjs);
    if (chartDatasetLoggingObjs[0] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(35);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[0],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[1] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(102);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[1],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[2] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(103);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[2],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[3] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(123);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[3],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[4] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(58);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[4],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[5] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(3);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[5],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[6] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(57);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[6],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[7] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(59);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[7],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[8] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(60);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[8],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[9] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(61);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[9],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[10] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(131);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[10],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[11] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(63);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[11],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[12] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(64);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[12],
        "SSIdNumber",
        pVal);
    }

    if (chartDatasetLoggingObjs[13] != NULL) {
      mxArray *pVal = mxCreateDoubleScalar(62);
      ssLoggerAddElementProperty(S,
        chartDatasetLoggingObjs[13],
        "SSIdNumber",
        pVal);
    }
  }
}

static void init_test_point_mapping_info(SimStruct *S)
{
  rtwCAPI_ModelMappingInfo *testPointMappingInfo;
  void **testPointAddrMap;
  SFc16_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  chartInstance = (SFc16_Integrated_system_V061InstanceStruct *)
    chartInfo->chartInstance;
  init_test_point_addr_map(chartInstance);
  testPointMappingInfo = get_test_point_mapping_info(chartInstance);
  testPointAddrMap = get_test_point_address_map(chartInstance);
  rtwCAPI_SetStaticMap(*testPointMappingInfo, &testPointMappingStaticInfo);
  rtwCAPI_SetLoggingStaticMap(*testPointMappingInfo, NULL);
  rtwCAPI_SetInstanceLoggingInfo(*testPointMappingInfo, NULL);
  rtwCAPI_SetPath(*testPointMappingInfo, "");
  rtwCAPI_SetFullPath(*testPointMappingInfo, NULL);
  rtwCAPI_SetDataAddressMap(*testPointMappingInfo, testPointAddrMap);
  rtwCAPI_SetChildMMIArray(*testPointMappingInfo, NULL);
  rtwCAPI_SetChildMMIArrayLen(*testPointMappingInfo, 0);
  ssSetModelMappingInfoPtr(S, testPointMappingInfo);
  init_signal_logging_objects(S, testPointMappingInfo);
}
