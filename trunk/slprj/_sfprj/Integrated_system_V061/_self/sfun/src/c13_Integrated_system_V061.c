/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c13_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c13_IN_NO_ACTIVE_CHILD         ((uint8_T)0U)
#define c13_IN_Mode1                   ((uint8_T)1U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c13_debug_family_names[2] = { "nargin", "nargout" };

static const char * c13_b_debug_family_names[2] = { "nargin", "nargout" };

/* Function Declarations */
static void initialize_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void c13_update_debugger_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_st);
static void c13_set_sim_state_side_effects_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void c13_eml_ini_fcn_to_be_inlined_153
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static void c13_eml_term_fcn_to_be_inlined_153
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c13_mrdivide(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_A, real_T c13_B);
static real_T c13_rdivide(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_x, real_T c13_y);
static void c13_isBuiltInNumeric(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c13_eml_scalexp_compatible
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c13_eml_div(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_x, real_T c13_y);
static real_T c13_div(SFc13_Integrated_system_V061InstanceStruct *chartInstance,
                      real_T c13_x, real_T c13_y);
static void init_script_number_translation(uint32_T c13_machineNumber, uint32_T
  c13_chartNumber, uint32_T c13_instanceNumber);
static const mxArray *c13_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const real_T c13_u);
static const mxArray *c13_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData);
static real_T c13_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_nargout, const char_T *c13_identifier);
static real_T c13_b_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId);
static void c13_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData);
static const mxArray *c13_b_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c13_u);
static const mxArray *c13_b_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData);
static int32_T c13_c_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_sfEvent, const char_T *c13_identifier);
static int32_T c13_d_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId);
static void c13_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData);
static const mxArray *c13_c_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c13_u);
static const mxArray *c13_c_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData);
static uint8_T c13_e_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_tp_Mode1, const char_T *c13_identifier);
static uint8_T c13_f_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId);
static void c13_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData);
static const mxArray *c13_d_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c13_e_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c13_u[3]);
static void c13_g_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u);
static void c13_h_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_dataWrittenToVector, const char_T
  *c13_identifier, boolean_T c13_y[3]);
static void c13_i_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId,
  boolean_T c13_y[3]);
static const mxArray *c13_j_emlrt_marshallIn
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_b_setSimStateSideEffectsInfo, const char_T *c13_identifier);
static const mxArray *c13_k_emlrt_marshallIn
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_u, const emlrtMsgIdentifier *c13_parentId);
static void init_dsm_address_info(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc13_Integrated_system_V061InstanceStruct *
  chartInstance);

/* Function Definitions */
static void initialize_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c13_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c13_doSetSimStateSideEffects = 0U;
  chartInstance->c13_setSimStateSideEffectsInfo = NULL;
  chartInstance->c13_tp_Mode1 = 0U;
  chartInstance->c13_is_active_c13_Integrated_system_V061 = 0U;
  chartInstance->c13_is_c13_Integrated_system_V061 = c13_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c13_update_debugger_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c13_prevAniVal;
  c13_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c13_is_active_c13_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 11U, chartInstance->c13_sfEvent);
  }

  if (chartInstance->c13_is_c13_Integrated_system_V061 == c13_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c13_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c13_sfEvent);
  }

  _SFD_SET_ANIMATION(c13_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c13_st = NULL;
  c13_st = NULL;
  sf_mex_assign(&c13_st, c13_d_emlrt_marshallOut(chartInstance), false);
  return c13_st;
}

static void set_sim_state_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_st)
{
  c13_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c13_st));
  chartInstance->c13_doSetSimStateSideEffects = 1U;
  c13_update_debugger_state_c13_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c13_st);
}

static void c13_set_sim_state_side_effects_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c13_doSetSimStateSideEffects != 0) {
    chartInstance->c13_tp_Mode1 = (uint8_T)
      (chartInstance->c13_is_c13_Integrated_system_V061 == c13_IN_Mode1);
    chartInstance->c13_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c13_setSimStateSideEffectsInfo);
}

static void sf_gateway_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c13_debug_family_var_map[2];
  real_T c13_nargin = 0.0;
  real_T c13_nargout = 0.0;
  real_T c13_b_nargin = 0.0;
  real_T c13_b_nargout = 0.0;
  c13_set_sim_state_side_effects_c13_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c13_lastMajorTime = _sfTime_;
    chartInstance->c13_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 11U, chartInstance->c13_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c13_K, 0U, 1U, 0U,
                          chartInstance->c13_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c13_I_in, 4U, 1U, 0U,
                          chartInstance->c13_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c13_I_out, 2U, 1U, 0U,
                          chartInstance->c13_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c13_V_out, 3U, 1U, 0U,
                          chartInstance->c13_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c13_V_in, 1U, 1U, 0U,
                          chartInstance->c13_sfEvent, false);
    chartInstance->c13_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 11U,
                 chartInstance->c13_sfEvent);
    if (chartInstance->c13_is_active_c13_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 11U,
                   chartInstance->c13_sfEvent);
      chartInstance->c13_stateChanged = true;
      chartInstance->c13_is_active_c13_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, chartInstance->c13_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c13_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c13_b_debug_family_names,
        c13_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargin, 0U, c13_sf_marshallOut,
        c13_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargout, 1U, c13_sf_marshallOut,
        c13_sf_marshallIn);
      chartInstance->c13_K = 2.0;
      chartInstance->c13_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c13_K, 0U, 5U, 0U,
                            chartInstance->c13_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c13_stateChanged = true;
      chartInstance->c13_is_c13_Integrated_system_V061 = c13_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c13_sfEvent);
      chartInstance->c13_tp_Mode1 = 1U;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                   chartInstance->c13_sfEvent);
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, chartInstance->c13_sfEvent);
    if (chartInstance->c13_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c13_debug_family_names,
    c13_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_b_nargin, 0U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_b_nargout, 1U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  *chartInstance->c13_V_out = c13_mrdivide(chartInstance,
    *chartInstance->c13_V_in, chartInstance->c13_K);
  chartInstance->c13_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_V_out, 3U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  *chartInstance->c13_I_in = c13_mrdivide(chartInstance,
    *chartInstance->c13_I_out, chartInstance->c13_K);
  chartInstance->c13_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_I_in, 4U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  real_T *c13_zcVar;
  c13_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c13_lastMajorTime == _sfTime_) {
    *c13_zcVar = -1.0;
  } else {
    chartInstance->c13_stateChanged = (boolean_T)0;
    if (chartInstance->c13_is_active_c13_Integrated_system_V061 == 0U) {
      chartInstance->c13_stateChanged = true;
    }

    if (chartInstance->c13_stateChanged) {
      *c13_zcVar = 1.0;
    } else {
      *c13_zcVar = -1.0;
    }
  }
}

static void derivatives_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c13_debug_family_var_map[2];
  real_T c13_nargin = 0.0;
  real_T c13_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c13_debug_family_names,
    c13_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargin, 0U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargout, 1U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  chartInstance->c13_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_V_out, 3U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  chartInstance->c13_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_I_in, 4U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
}

static void outputs_c13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c13_debug_family_var_map[2];
  real_T c13_nargin = 0.0;
  real_T c13_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c13_debug_family_names,
    c13_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargin, 0U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c13_nargout, 1U, c13_sf_marshallOut,
    c13_sf_marshallIn);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  *chartInstance->c13_V_out = c13_mrdivide(chartInstance,
    *chartInstance->c13_V_in, chartInstance->c13_K);
  chartInstance->c13_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_V_out, 3U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  if (!chartInstance->c13_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c13_sfEvent,
      false);
  }

  *chartInstance->c13_I_in = c13_mrdivide(chartInstance,
    *chartInstance->c13_I_out, chartInstance->c13_K);
  chartInstance->c13_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c13_I_in, 4U, 4U, 0U,
                        chartInstance->c13_sfEvent, false);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c13_sfEvent);
}

static void initSimStructsc13_Integrated_system_V061
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c13_eml_ini_fcn_to_be_inlined_153
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c13_eml_term_fcn_to_be_inlined_153
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c13_mrdivide(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_A, real_T c13_B)
{
  return c13_rdivide(chartInstance, c13_A, c13_B);
}

static real_T c13_rdivide(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_x, real_T c13_y)
{
  return c13_eml_div(chartInstance, c13_x, c13_y);
}

static void c13_isBuiltInNumeric(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c13_eml_scalexp_compatible
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c13_eml_div(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c13_x, real_T c13_y)
{
  return c13_div(chartInstance, c13_x, c13_y);
}

static real_T c13_div(SFc13_Integrated_system_V061InstanceStruct *chartInstance,
                      real_T c13_x, real_T c13_y)
{
  (void)chartInstance;
  return c13_x / c13_y;
}

static void init_script_number_translation(uint32_T c13_machineNumber, uint32_T
  c13_chartNumber, uint32_T c13_instanceNumber)
{
  (void)c13_machineNumber;
  (void)c13_chartNumber;
  (void)c13_instanceNumber;
}

static const mxArray *c13_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const real_T c13_u)
{
  const mxArray *c13_y = NULL;
  (void)chartInstance;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_create("y", &c13_u, 0, 0U, 0U, 0U, 0), false);
  return c13_y;
}

static const mxArray *c13_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData)
{
  const mxArray *c13_mxArrayOutData = NULL;
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c13_mxArrayOutData = NULL;
  sf_mex_assign(&c13_mxArrayOutData, c13_emlrt_marshallOut(chartInstance,
    *(real_T *)c13_inData), false);
  return c13_mxArrayOutData;
}

static real_T c13_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_nargout, const char_T *c13_identifier)
{
  real_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = c13_identifier;
  c13_thisId.fParent = NULL;
  c13_y = c13_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c13_nargout),
    &c13_thisId);
  sf_mex_destroy(&c13_nargout);
  return c13_y;
}

static real_T c13_b_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct *
  chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId)
{
  real_T c13_y;
  real_T c13_d0;
  (void)chartInstance;
  sf_mex_import(c13_parentId, sf_mex_dup(c13_u), &c13_d0, 1, 0, 0U, 0, 0U, 0);
  c13_y = c13_d0;
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static void c13_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData)
{
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(real_T *)c13_outData = c13_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c13_mxArrayInData), c13_varName);
  sf_mex_destroy(&c13_mxArrayInData);
}

const mxArray *sf_c13_Integrated_system_V061_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c13_nameCaptureInfo = NULL;
  c13_nameCaptureInfo = NULL;
  sf_mex_assign(&c13_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c13_nameCaptureInfo;
}

static const mxArray *c13_b_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const int32_T
   c13_u)
{
  const mxArray *c13_y = NULL;
  (void)chartInstance;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_create("y", &c13_u, 6, 0U, 0U, 0U, 0), false);
  return c13_y;
}

static const mxArray *c13_b_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData)
{
  const mxArray *c13_mxArrayOutData = NULL;
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c13_mxArrayOutData = NULL;
  sf_mex_assign(&c13_mxArrayOutData, c13_b_emlrt_marshallOut(chartInstance,
    *(int32_T *)c13_inData), false);
  return c13_mxArrayOutData;
}

static int32_T c13_c_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_sfEvent, const char_T *c13_identifier)
{
  int32_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = c13_identifier;
  c13_thisId.fParent = NULL;
  c13_y = c13_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c13_b_sfEvent),
    &c13_thisId);
  sf_mex_destroy(&c13_b_sfEvent);
  return c13_y;
}

static int32_T c13_d_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId)
{
  int32_T c13_y;
  int32_T c13_i0;
  (void)chartInstance;
  sf_mex_import(c13_parentId, sf_mex_dup(c13_u), &c13_i0, 1, 6, 0U, 0, 0U, 0);
  c13_y = c13_i0;
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static void c13_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData)
{
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(int32_T *)c13_outData = c13_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c13_mxArrayInData), c13_varName);
  sf_mex_destroy(&c13_mxArrayInData);
}

static const mxArray *c13_c_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T
   c13_u)
{
  const mxArray *c13_y = NULL;
  (void)chartInstance;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_create("y", &c13_u, 3, 0U, 0U, 0U, 0), false);
  return c13_y;
}

static const mxArray *c13_c_sf_marshallOut(void *chartInstanceVoid, void
  *c13_inData)
{
  const mxArray *c13_mxArrayOutData = NULL;
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  c13_mxArrayOutData = NULL;
  sf_mex_assign(&c13_mxArrayOutData, c13_c_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c13_inData), false);
  return c13_mxArrayOutData;
}

static uint8_T c13_e_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_tp_Mode1, const char_T *c13_identifier)
{
  uint8_T c13_y;
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = c13_identifier;
  c13_thisId.fParent = NULL;
  c13_y = c13_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c13_b_tp_Mode1),
    &c13_thisId);
  sf_mex_destroy(&c13_b_tp_Mode1);
  return c13_y;
}

static uint8_T c13_f_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId)
{
  uint8_T c13_y;
  uint8_T c13_u0;
  (void)chartInstance;
  sf_mex_import(c13_parentId, sf_mex_dup(c13_u), &c13_u0, 1, 3, 0U, 0, 0U, 0);
  c13_y = c13_u0;
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static void c13_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c13_mxArrayInData, const char_T *c13_varName, void *c13_outData)
{
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
    chartInstanceVoid;
  *(uint8_T *)c13_outData = c13_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c13_mxArrayInData), c13_varName);
  sf_mex_destroy(&c13_mxArrayInData);
}

static const mxArray *c13_d_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c13_y;
  int32_T c13_i1;
  boolean_T c13_bv0[3];
  c13_y = NULL;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_createcellmatrix(6, 1), false);
  sf_mex_setcell(c13_y, 0, c13_emlrt_marshallOut(chartInstance,
    *chartInstance->c13_I_in));
  sf_mex_setcell(c13_y, 1, c13_emlrt_marshallOut(chartInstance,
    *chartInstance->c13_V_out));
  sf_mex_setcell(c13_y, 2, c13_emlrt_marshallOut(chartInstance,
    chartInstance->c13_K));
  sf_mex_setcell(c13_y, 3, c13_c_emlrt_marshallOut(chartInstance,
    chartInstance->c13_is_active_c13_Integrated_system_V061));
  sf_mex_setcell(c13_y, 4, c13_c_emlrt_marshallOut(chartInstance,
    chartInstance->c13_is_c13_Integrated_system_V061));
  for (c13_i1 = 0; c13_i1 < 3; c13_i1++) {
    c13_bv0[c13_i1] = chartInstance->c13_dataWrittenToVector[c13_i1];
  }

  sf_mex_setcell(c13_y, 5, c13_e_emlrt_marshallOut(chartInstance, c13_bv0));
  return c13_y;
}

static const mxArray *c13_e_emlrt_marshallOut
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c13_u[3])
{
  const mxArray *c13_y = NULL;
  (void)chartInstance;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_create("y", c13_u, 11, 0U, 1U, 0U, 1, 3), false);
  return c13_y;
}

static void c13_g_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u)
{
  boolean_T c13_bv1[3];
  int32_T c13_i2;
  *chartInstance->c13_I_in = c13_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c13_u, 0)), "I_in");
  *chartInstance->c13_V_out = c13_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("V_out", c13_u, 1)), "V_out");
  chartInstance->c13_K = c13_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("K", c13_u, 2)), "K");
  chartInstance->c13_is_active_c13_Integrated_system_V061 =
    c13_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c13_Integrated_system_V061", c13_u, 3)),
    "is_active_c13_Integrated_system_V061");
  chartInstance->c13_is_c13_Integrated_system_V061 = c13_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c13_Integrated_system_V061",
       c13_u, 4)), "is_c13_Integrated_system_V061");
  c13_h_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c13_u, 5)), "dataWrittenToVector", c13_bv1);
  for (c13_i2 = 0; c13_i2 < 3; c13_i2++) {
    chartInstance->c13_dataWrittenToVector[c13_i2] = c13_bv1[c13_i2];
  }

  sf_mex_assign(&chartInstance->c13_setSimStateSideEffectsInfo,
                c13_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c13_u, 6)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c13_u);
}

static void c13_h_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_b_dataWrittenToVector, const char_T
  *c13_identifier, boolean_T c13_y[3])
{
  emlrtMsgIdentifier c13_thisId;
  c13_thisId.fIdentifier = c13_identifier;
  c13_thisId.fParent = NULL;
  c13_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c13_b_dataWrittenToVector),
    &c13_thisId, c13_y);
  sf_mex_destroy(&c13_b_dataWrittenToVector);
}

static void c13_i_emlrt_marshallIn(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c13_u, const emlrtMsgIdentifier *c13_parentId,
  boolean_T c13_y[3])
{
  boolean_T c13_bv2[3];
  int32_T c13_i3;
  (void)chartInstance;
  sf_mex_import(c13_parentId, sf_mex_dup(c13_u), c13_bv2, 1, 11, 0U, 1, 0U, 1, 3);
  for (c13_i3 = 0; c13_i3 < 3; c13_i3++) {
    c13_y[c13_i3] = c13_bv2[c13_i3];
  }

  sf_mex_destroy(&c13_u);
}

static const mxArray *c13_j_emlrt_marshallIn
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_b_setSimStateSideEffectsInfo, const char_T *c13_identifier)
{
  const mxArray *c13_y = NULL;
  emlrtMsgIdentifier c13_thisId;
  c13_y = NULL;
  c13_thisId.fIdentifier = c13_identifier;
  c13_thisId.fParent = NULL;
  sf_mex_assign(&c13_y, c13_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c13_b_setSimStateSideEffectsInfo), &c13_thisId), false);
  sf_mex_destroy(&c13_b_setSimStateSideEffectsInfo);
  return c13_y;
}

static const mxArray *c13_k_emlrt_marshallIn
  (SFc13_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c13_u, const emlrtMsgIdentifier *c13_parentId)
{
  const mxArray *c13_y = NULL;
  (void)chartInstance;
  (void)c13_parentId;
  c13_y = NULL;
  sf_mex_assign(&c13_y, sf_mex_duplicatearraysafe(&c13_u), false);
  sf_mex_destroy(&c13_u);
  return c13_y;
}

static void init_dsm_address_info(SFc13_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc13_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  chartInstance->c13_V_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c13_V_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c13_I_out = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c13_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c13_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2320075848U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1795820720U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2496534885U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2712990426U);
}

mxArray* sf_c13_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c13_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("nOkdPP7oQTAyK6zPaXnmI");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c13_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c13_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c13_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c13_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c13_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c13_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x6'type','srcId','name','auxInfo'{{M[1],M[33],T\"I_in\",},{M[1],M[37],T\"V_out\",},{M[3],M[35],T\"K\",},{M[8],M[0],T\"is_active_c13_Integrated_system_V061\",},{M[9],M[0],T\"is_c13_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 6, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c13_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc13_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           13,
           1,
           1,
           0,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"K");
          _SFD_SET_DATA_PROPS(1,1,1,0,"V_in");
          _SFD_SET_DATA_PROPS(2,1,1,0,"I_out");
          _SFD_SET_DATA_PROPS(3,2,0,1,"V_out");
          _SFD_SET_DATA_PROPS(4,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_CH_SUBSTATE_COUNT(1);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_ST_SUBSTATE_COUNT(0,0);
        }

        _SFD_CV_INIT_CHART(1,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c13_sf_marshallOut,(MexInFcnForType)c13_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c13_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c13_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c13_sf_marshallOut,(MexInFcnForType)c13_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c13_sf_marshallOut,(MexInFcnForType)c13_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc13_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c13_V_in);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c13_V_out);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c13_I_out);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c13_I_in);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c13_K);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sv6QuFM9pK8byvFfsEUpw3";
}

static void sf_opaque_initialize_c13_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc13_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c13_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c13_Integrated_system_V061((SFc13_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c13_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c13_Integrated_system_V061((SFc13_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c13_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c13_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c13_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c13_Integrated_system_V061((SFc13_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c13_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c13_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c13_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c13_Integrated_system_V061(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc13_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c13_Integrated_system_V061
      ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc13_Integrated_system_V061
    ((SFc13_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c13_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c13_Integrated_system_V061
      ((SFc13_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c13_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,
      13);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,13,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,13);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,13,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,13,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,13);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3123104501U));
  ssSetChecksum1(S,(2136564784U));
  ssSetChecksum2(S,(4056650799U));
  ssSetChecksum3(S,(1449224665U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c13_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c13_Integrated_system_V061(SimStruct *S)
{
  SFc13_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc13_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc13_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc13_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c13_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c13_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c13_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c13_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c13_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c13_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c13_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c13_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c13_Integrated_system_V061;
  chartInstance->chartInfo.outputs =
    sf_opaque_outputs_c13_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c13_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c13_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c13_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c13_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c13_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c13_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c13_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c13_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c13_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
