#ifndef __c11_Integrated_system_V061_h__
#define __c11_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc11_Integrated_system_V061InstanceStruct
#define typedef_SFc11_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c11_sfEvent;
  uint8_T c11_tp_Mode2;
  uint8_T c11_tp_Mode1;
  boolean_T c11_isStable;
  boolean_T c11_stateChanged;
  real_T c11_lastMajorTime;
  uint8_T c11_is_active_c11_Integrated_system_V061;
  uint8_T c11_is_c11_Integrated_system_V061;
  real_T c11_tmp;
  boolean_T c11_dataWrittenToVector[2];
  uint8_T c11_doSetSimStateSideEffects;
  const mxArray *c11_setSimStateSideEffectsInfo;
  real_T *c11_sensor_ball_distance;
  real_T *c11_ball_distance;
  real_T *c11_lever_trigger;
  real_T *c11_solonoid_trigger;
} SFc11_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc11_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c11_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c11_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c11_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
