#ifndef __c9_Integrated_system_V061_h__
#define __c9_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc9_Integrated_system_V061InstanceStruct
#define typedef_SFc9_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c9_sfEvent;
  uint8_T c9_tp_Mode1;
  boolean_T c9_isStable;
  boolean_T c9_stateChanged;
  real_T c9_lastMajorTime;
  uint8_T c9_is_active_c9_Integrated_system_V061;
  uint8_T c9_is_c9_Integrated_system_V061;
  real_T c9_K;
  real_T c9_R;
  boolean_T c9_dataWrittenToVector[4];
  uint8_T c9_doSetSimStateSideEffects;
  const mxArray *c9_setSimStateSideEffectsInfo;
  real_T *c9_V_in;
  real_T *c9_V_out;
  real_T *c9_I_in;
} SFc9_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc9_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c9_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c9_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c9_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
