#ifndef __c1_Integrated_system_V061_h__
#define __c1_Integrated_system_V061_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_Integrated_system_V061InstanceStruct
#define typedef_SFc1_Integrated_system_V061InstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  uint8_T c1_tp_Mode1;
  uint8_T c1_tp_Mode2;
  boolean_T c1_isStable;
  boolean_T c1_stateChanged;
  real_T c1_lastMajorTime;
  uint8_T c1_is_active_c1_Integrated_system_V061;
  uint8_T c1_is_c1_Integrated_system_V061;
  real_T c1_R;
  real_T c1_L;
  real_T c1_Kt;
  real_T c1_I_max;
  boolean_T c1_dataWrittenToVector[9];
  uint8_T c1_doSetSimStateSideEffects;
  const mxArray *c1_setSimStateSideEffectsInfo;
  real_T *c1_F_out;
  real_T *c1_solenoid_trigger;
  real_T *c1_V_S;
  real_T *c1_I;
  real_T *c1_I_sol;
  real_T *c1_I_in;
} SFc1_Integrated_system_V061InstanceStruct;

#endif                                 /*typedef_SFc1_Integrated_system_V061InstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c1_Integrated_system_V061_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_Integrated_system_V061_get_check_sum(mxArray *plhs[]);
extern void c1_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T
  method, void *data);

#endif
