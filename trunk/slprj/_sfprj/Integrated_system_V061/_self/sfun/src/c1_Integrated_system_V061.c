/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Integrated_system_V061_sfun.h"
#include "c1_Integrated_system_V061.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Integrated_system_V061_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c1_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c1_IN_Mode1                    ((uint8_T)1U)
#define c1_IN_Mode2                    ((uint8_T)2U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_d_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_f_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_i_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void initialize_params_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void enable_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void disable_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void set_sim_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c1_st);
static void c1_set_sim_state_side_effects_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void finalize_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void sf_gateway_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void mdl_start_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void zeroCrossings_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void derivatives_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void outputs_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void initSimStructsc1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void c1_eml_ini_fcn_to_be_inlined_22
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static void c1_eml_term_fcn_to_be_inlined_22
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static real_T c1_mrdivide(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_A, real_T c1_B);
static real_T c1_rdivide(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_x, real_T c1_y);
static void c1_isBuiltInNumeric(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance);
static void c1_eml_scalexp_compatible(SFc1_Integrated_system_V061InstanceStruct *
  chartInstance);
static real_T c1_eml_div(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_x, real_T c1_y);
static real_T c1_div(SFc1_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c1_x, real_T c1_y);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const real_T c1_u);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static real_T c1_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_nargout, const char_T *c1_identifier);
static real_T c1_b_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c1_u);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static boolean_T c1_c_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_sf_internal_predicateOutput, const char_T
  *c1_identifier);
static boolean_T c1_d_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c1_u);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int32_T c1_e_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier);
static int32_T c1_f_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c1_u);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint8_T c1_g_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Mode1, const char_T *c1_identifier);
static uint8_T c1_h_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_e_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance);
static const mxArray *c1_f_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c1_u[9]);
static void c1_i_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u);
static void c1_j_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_dataWrittenToVector, const char_T
  *c1_identifier, boolean_T c1_y[9]);
static void c1_k_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId,
  boolean_T c1_y[9]);
static const mxArray *c1_l_emlrt_marshallIn
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier);
static const mxArray *c1_m_emlrt_marshallIn
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static void init_dsm_address_info(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_doSetSimStateSideEffects = 0U;
  chartInstance->c1_setSimStateSideEffectsInfo = NULL;
  chartInstance->c1_tp_Mode1 = 0U;
  chartInstance->c1_tp_Mode2 = 0U;
  chartInstance->c1_is_active_c1_Integrated_system_V061 = 0U;
  chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c1_prevAniVal;
  c1_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c1_is_active_c1_Integrated_system_V061 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Integrated_system_V061 == c1_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Integrated_system_V061 == c1_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  }

  _SFD_SET_ANIMATION(c1_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c1_st = NULL;
  c1_st = NULL;
  sf_mex_assign(&c1_st, c1_e_emlrt_marshallOut(chartInstance), false);
  return c1_st;
}

static void set_sim_state_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c1_st)
{
  c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_st));
  chartInstance->c1_doSetSimStateSideEffects = 1U;
  c1_update_debugger_state_c1_Integrated_system_V061(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void c1_set_sim_state_side_effects_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  if (chartInstance->c1_doSetSimStateSideEffects != 0) {
    chartInstance->c1_tp_Mode1 = (uint8_T)
      (chartInstance->c1_is_c1_Integrated_system_V061 == c1_IN_Mode1);
    chartInstance->c1_tp_Mode2 = (uint8_T)
      (chartInstance->c1_is_c1_Integrated_system_V061 == c1_IN_Mode2);
    chartInstance->c1_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c1_setSimStateSideEffectsInfo);
}

static void sf_gateway_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  uint32_T c1_b_debug_family_var_map[3];
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 1.0;
  boolean_T c1_out;
  real_T c1_c_nargin = 0.0;
  real_T c1_c_nargout = 0.0;
  real_T c1_d_nargin = 0.0;
  real_T c1_d_nargout = 1.0;
  boolean_T c1_b_out;
  real_T c1_e_nargin = 0.0;
  real_T c1_e_nargout = 0.0;
  real_T c1_f_nargin = 0.0;
  real_T c1_f_nargout = 0.0;
  real_T c1_g_nargin = 0.0;
  real_T c1_g_nargout = 0.0;
  boolean_T guard1 = false;
  c1_set_sim_state_side_effects_c1_Integrated_system_V061(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c1_lastMajorTime = _sfTime_;
    chartInstance->c1_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_DATA_RANGE_CHECK(chartInstance->c1_I_max, 1U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c1_Kt, 2U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c1_L, 3U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I, 0U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(chartInstance->c1_R, 4U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_V_S, 5U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_solenoid_trigger, 7U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    chartInstance->c1_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    if (chartInstance->c1_is_active_c1_Integrated_system_V061 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
      chartInstance->c1_stateChanged = true;
      chartInstance->c1_is_active_c1_Integrated_system_V061 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_c_debug_family_names,
        c1_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
        c1_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
        c1_sf_marshallIn);
      *chartInstance->c1_I = 0.0;
      chartInstance->c1_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I, 0U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      chartInstance->c1_R = 1.0;
      chartInstance->c1_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c1_R, 4U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      chartInstance->c1_L = 0.05;
      chartInstance->c1_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c1_L, 3U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      chartInstance->c1_Kt = 5.0;
      chartInstance->c1_dataWrittenToVector[5U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c1_Kt, 2U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      chartInstance->c1_I_max = 3.0;
      chartInstance->c1_dataWrittenToVector[8U] = true;
      _SFD_DATA_RANGE_CHECK(chartInstance->c1_I_max, 1U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_solenoid_trigger = 0.0;
      chartInstance->c1_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_solenoid_trigger, 7U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c1_stateChanged = true;
      chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_Mode1;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_Mode1 = 1U;
    } else {
      switch (chartInstance->c1_is_c1_Integrated_system_V061) {
       case c1_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_d_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        guard1 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c1_V_S <= 0.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c1_I >=
                          chartInstance->c1_I_max)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c1_out = true;
          } else {
            guard1 = true;
          }
        } else {
          if (!chartInstance->c1_dataWrittenToVector[8U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
              chartInstance->c1_sfEvent, false);
          }

          guard1 = true;
        }

        if (guard1 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c1_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c1_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_e_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          *chartInstance->c1_I_in = 0.0;
          chartInstance->c1_dataWrittenToVector[7U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 5U, 1U,
                                chartInstance->c1_sfEvent, false);
          *chartInstance->c1_solenoid_trigger = 1.0;
          chartInstance->c1_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c1_solenoid_trigger, 7U, 5U, 1U,
                                chartInstance->c1_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c1_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode2 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
        }
        break;

       case c1_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_f_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[8U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        c1_b_out = CV_EML_IF(2, 0, 0, *chartInstance->c1_I <
                             chartInstance->c1_I_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_b_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_g_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          *chartInstance->c1_solenoid_trigger = 0.0;
          chartInstance->c1_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c1_solenoid_trigger, 7U, 5U, 2U,
                                chartInstance->c1_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c1_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode1 = 1U;
        } else {
          _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    if (chartInstance->c1_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Integrated_system_V061) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_sol = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_in = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_F_out = *chartInstance->c1_I * chartInstance->c1_Kt;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_sol = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[8U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_in = chartInstance->c1_I_max;
    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_F_out = *chartInstance->c1_I * chartInstance->c1_Kt;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Integrated_system_V061MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[3];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 1.0;
  boolean_T c1_out;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 1.0;
  boolean_T c1_b_out;
  real_T *c1_zcVar;
  boolean_T guard1 = false;
  c1_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c1_lastMajorTime == _sfTime_) {
    *c1_zcVar = -1.0;
  } else {
    chartInstance->c1_stateChanged = (boolean_T)0;
    if (chartInstance->c1_is_active_c1_Integrated_system_V061 == 0U) {
      chartInstance->c1_stateChanged = true;
    } else {
      switch (chartInstance->c1_is_c1_Integrated_system_V061) {
       case c1_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_d_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        guard1 = false;
        if (CV_EML_COND(1, 0, 0, *chartInstance->c1_V_S <= 0.0)) {
          if (CV_EML_COND(1, 0, 1, *chartInstance->c1_I >=
                          chartInstance->c1_I_max)) {
            CV_EML_MCDC(1, 0, 0, true);
            CV_EML_IF(1, 0, 0, true);
            c1_out = true;
          } else {
            guard1 = true;
          }
        } else {
          if (!chartInstance->c1_dataWrittenToVector[8U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
              chartInstance->c1_sfEvent, false);
          }

          guard1 = true;
        }

        if (guard1 == true) {
          CV_EML_MCDC(1, 0, 0, false);
          CV_EML_IF(1, 0, 0, false);
          c1_out = false;
        }

        _SFD_SYMBOL_SCOPE_POP();
        if (c1_out) {
          chartInstance->c1_stateChanged = true;
        }
        break;

       case c1_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_f_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[8U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        c1_b_out = CV_EML_IF(2, 0, 0, *chartInstance->c1_I <
                             chartInstance->c1_I_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_b_out) {
          chartInstance->c1_stateChanged = true;
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c1_stateChanged) {
      *c1_zcVar = 1.0;
    } else {
      *c1_zcVar = -1.0;
    }
  }
}

static void derivatives_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 0.0;
  real_T *c1_I_dot;
  c1_I_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 0);
  *c1_I_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_I_dot, 0U, 1U, 0U, chartInstance->c1_sfEvent, false);
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Integrated_system_V061) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *c1_I_dot = c1_mrdivide(chartInstance, 1.0, chartInstance->c1_L) *
      (*chartInstance->c1_V_S - *chartInstance->c1_I * chartInstance->c1_R);
    _SFD_DATA_RANGE_CHECK(*c1_I_dot, 0U, 4U, 0U, chartInstance->c1_sfEvent,
                          false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *c1_I_dot = c1_mrdivide(chartInstance, 1.0, chartInstance->c1_L) *
      (*chartInstance->c1_V_S - *chartInstance->c1_I * chartInstance->c1_R);
    _SFD_DATA_RANGE_CHECK(*c1_I_dot, 0U, 4U, 1U, chartInstance->c1_sfEvent,
                          false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[8U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }
}

static void outputs_c1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Integrated_system_V061) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_sol = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_in = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 0U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_F_out = *chartInstance->c1_I * chartInstance->c1_Kt;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    if (!chartInstance->c1_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_sol = *chartInstance->c1_I;
    chartInstance->c1_dataWrittenToVector[6U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_sol, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[8U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_I_in = chartInstance->c1_I_max;
    chartInstance->c1_dataWrittenToVector[7U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_I_in, 9U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    if (!chartInstance->c1_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    if (!chartInstance->c1_dataWrittenToVector[5U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 1U, chartInstance->c1_sfEvent,
        false);
    }

    *chartInstance->c1_F_out = *chartInstance->c1_I * chartInstance->c1_Kt;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_F_out, 6U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Integrated_system_V061 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }
}

static void initSimStructsc1_Integrated_system_V061
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_eml_ini_fcn_to_be_inlined_22
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_eml_term_fcn_to_be_inlined_22
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static real_T c1_mrdivide(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_A, real_T c1_B)
{
  return c1_rdivide(chartInstance, c1_A, c1_B);
}

static real_T c1_rdivide(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_x, real_T c1_y)
{
  return c1_eml_div(chartInstance, c1_x, c1_y);
}

static void c1_isBuiltInNumeric(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c1_eml_scalexp_compatible(SFc1_Integrated_system_V061InstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static real_T c1_eml_div(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, real_T c1_x, real_T c1_y)
{
  return c1_div(chartInstance, c1_x, c1_y);
}

static real_T c1_div(SFc1_Integrated_system_V061InstanceStruct *chartInstance,
                     real_T c1_x, real_T c1_y)
{
  (void)chartInstance;
  return c1_x / c1_y;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)c1_machineNumber;
  (void)c1_chartNumber;
  (void)c1_instanceNumber;
}

static const mxArray *c1_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const real_T c1_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_emlrt_marshallOut(chartInstance, *(real_T
    *)c1_inData), false);
  return c1_mxArrayOutData;
}

static real_T c1_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_nargout, const char_T *c1_identifier)
{
  real_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_nargout), &c1_thisId);
  sf_mex_destroy(&c1_nargout);
  return c1_y;
}

static real_T c1_b_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_d0, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(real_T *)c1_outData = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c1_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 11, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static boolean_T c1_c_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_sf_internal_predicateOutput, const char_T
  *c1_identifier)
{
  boolean_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_sf_internal_predicateOutput), &c1_thisId);
  sf_mex_destroy(&c1_sf_internal_predicateOutput);
  return c1_y;
}

static boolean_T c1_d_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  boolean_T c1_y;
  boolean_T c1_b0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_b0, 1, 11, 0U, 0, 0U, 0);
  c1_y = c1_b0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c1_outData = c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_Integrated_system_V061_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static const mxArray *c1_c_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const int32_T c1_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static int32_T c1_e_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier)
{
  int32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  return c1_y;
}

static int32_T c1_f_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i0, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c1_outData = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const uint8_T c1_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 3, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static uint8_T c1_g_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Mode1, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Mode1),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Mode1);
  return c1_y;
}

static uint8_T c1_h_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c1_outData = c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_e_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance)
{
  const mxArray *c1_y;
  int32_T c1_i1;
  boolean_T c1_bv0[9];
  c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(12, 1), false);
  sf_mex_setcell(c1_y, 0, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_F_out));
  sf_mex_setcell(c1_y, 1, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_I_in));
  sf_mex_setcell(c1_y, 2, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_I_sol));
  sf_mex_setcell(c1_y, 3, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_solenoid_trigger));
  sf_mex_setcell(c1_y, 4, c1_emlrt_marshallOut(chartInstance,
    chartInstance->c1_I_max));
  sf_mex_setcell(c1_y, 5, c1_emlrt_marshallOut(chartInstance,
    chartInstance->c1_Kt));
  sf_mex_setcell(c1_y, 6, c1_emlrt_marshallOut(chartInstance,
    chartInstance->c1_L));
  sf_mex_setcell(c1_y, 7, c1_emlrt_marshallOut(chartInstance,
    chartInstance->c1_R));
  sf_mex_setcell(c1_y, 8, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_I));
  sf_mex_setcell(c1_y, 9, c1_d_emlrt_marshallOut(chartInstance,
    chartInstance->c1_is_active_c1_Integrated_system_V061));
  sf_mex_setcell(c1_y, 10, c1_d_emlrt_marshallOut(chartInstance,
    chartInstance->c1_is_c1_Integrated_system_V061));
  for (c1_i1 = 0; c1_i1 < 9; c1_i1++) {
    c1_bv0[c1_i1] = chartInstance->c1_dataWrittenToVector[c1_i1];
  }

  sf_mex_setcell(c1_y, 11, c1_f_emlrt_marshallOut(chartInstance, c1_bv0));
  return c1_y;
}

static const mxArray *c1_f_emlrt_marshallOut
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const boolean_T
   c1_u[9])
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 11, 0U, 1U, 0U, 1, 9), false);
  return c1_y;
}

static void c1_i_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u)
{
  boolean_T c1_bv1[9];
  int32_T c1_i2;
  *chartInstance->c1_F_out = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("F_out", c1_u, 0)), "F_out");
  *chartInstance->c1_I_in = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_in", c1_u, 1)), "I_in");
  *chartInstance->c1_I_sol = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_sol", c1_u, 2)), "I_sol");
  *chartInstance->c1_solenoid_trigger = c1_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("solenoid_trigger", c1_u, 3)), "solenoid_trigger");
  chartInstance->c1_I_max = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I_max", c1_u, 4)), "I_max");
  chartInstance->c1_Kt = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Kt", c1_u, 5)), "Kt");
  chartInstance->c1_L = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("L", c1_u, 6)), "L");
  chartInstance->c1_R = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("R", c1_u, 7)), "R");
  *chartInstance->c1_I = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("I", c1_u, 8)), "I");
  chartInstance->c1_is_active_c1_Integrated_system_V061 = c1_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(
       "is_active_c1_Integrated_system_V061", c1_u, 9)),
     "is_active_c1_Integrated_system_V061");
  chartInstance->c1_is_c1_Integrated_system_V061 = c1_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c1_Integrated_system_V061",
       c1_u, 10)), "is_c1_Integrated_system_V061");
  c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c1_u, 11)), "dataWrittenToVector", c1_bv1);
  for (c1_i2 = 0; c1_i2 < 9; c1_i2++) {
    chartInstance->c1_dataWrittenToVector[c1_i2] = c1_bv1[c1_i2];
  }

  sf_mex_assign(&chartInstance->c1_setSimStateSideEffectsInfo,
                c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c1_u, 12)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c1_u);
}

static void c1_j_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_b_dataWrittenToVector, const char_T
  *c1_identifier, boolean_T c1_y[9])
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_dataWrittenToVector),
                        &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_dataWrittenToVector);
}

static void c1_k_emlrt_marshallIn(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId,
  boolean_T c1_y[9])
{
  boolean_T c1_bv2[9];
  int32_T c1_i3;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), c1_bv2, 1, 11, 0U, 1, 0U, 1, 9);
  for (c1_i3 = 0; c1_i3 < 9; c1_i3++) {
    c1_y[c1_i3] = c1_bv2[c1_i3];
  }

  sf_mex_destroy(&c1_u);
}

static const mxArray *c1_l_emlrt_marshallIn
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier)
{
  const mxArray *c1_y = NULL;
  emlrtMsgIdentifier c1_thisId;
  c1_y = NULL;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  sf_mex_assign(&c1_y, c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_setSimStateSideEffectsInfo), &c1_thisId), false);
  sf_mex_destroy(&c1_b_setSimStateSideEffectsInfo);
  return c1_y;
}

static const mxArray *c1_m_emlrt_marshallIn
  (SFc1_Integrated_system_V061InstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  (void)c1_parentId;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_duplicatearraysafe(&c1_u), false);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void init_dsm_address_info(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_Integrated_system_V061InstanceStruct
  *chartInstance)
{
  chartInstance->c1_F_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_solenoid_trigger = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c1_V_S = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_I = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 0);
  chartInstance->c1_I_sol = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_I_in = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c1_Integrated_system_V061_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1110490242U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3083234063U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2931233375U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2711517060U);
}

mxArray* sf_c1_Integrated_system_V061_get_post_codegen_info(void);
mxArray *sf_c1_Integrated_system_V061_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("5DK94UApRvpUyLOahYFCWF");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c1_Integrated_system_V061_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_Integrated_system_V061_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_Integrated_system_V061_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_Integrated_system_V061_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_Integrated_system_V061_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_Integrated_system_V061(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[62],T\"F_out\",},{M[1],M[48],T\"I_in\",},{M[1],M[66],T\"I_sol\",},{M[1],M[68],T\"solenoid_trigger\",},{M[3],M[67],T\"I_max\",},{M[3],M[42],T\"Kt\",},{M[3],M[38],T\"L\",},{M[3],M[36],T\"R\",},{M[5],M[37],T\"I\",},{M[8],M[0],T\"is_active_c1_Integrated_system_V061\",}}",
    "100 S1x2'type','srcId','name','auxInfo'{{M[9],M[0],T\"is_c1_Integrated_system_V061\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 12, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_Integrated_system_V061_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Integrated_system_V061MachineNumber_,
           1,
           2,
           3,
           0,
           10,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Integrated_system_V061MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Integrated_system_V061MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Integrated_system_V061MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"I");
          _SFD_SET_DATA_PROPS(1,0,0,0,"I_max");
          _SFD_SET_DATA_PROPS(2,0,0,0,"Kt");
          _SFD_SET_DATA_PROPS(3,0,0,0,"L");
          _SFD_SET_DATA_PROPS(4,0,0,0,"R");
          _SFD_SET_DATA_PROPS(5,1,1,0,"V_S");
          _SFD_SET_DATA_PROPS(6,2,0,1,"F_out");
          _SFD_SET_DATA_PROPS(7,2,0,1,"solenoid_trigger");
          _SFD_SET_DATA_PROPS(8,2,0,1,"I_sol");
          _SFD_SET_DATA_PROPS(9,2,0,1,"I_in");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_CH_SUBSTATE_COUNT(2);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
        }

        _SFD_CV_INIT_CHART(2,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(1,0,0,1,23,1,23);

        {
          static int condStart[] = { 1, 13 };

          static int condEnd[] = { 9, 23 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(1,0,0,1,23,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,11,1,11);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Integrated_system_V061MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Integrated_system_V061InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c1_F_out);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c1_solenoid_trigger);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c1_V_S);
        _SFD_SET_DATA_VALUE_PTR(4U, &chartInstance->c1_R);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c1_I);
        _SFD_SET_DATA_VALUE_PTR(3U, &chartInstance->c1_L);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c1_Kt);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c1_I_sol);
        _SFD_SET_DATA_VALUE_PTR(9U, chartInstance->c1_I_in);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c1_I_max);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "se7DQo775KMK83rDBBYeaZG";
}

static void sf_opaque_initialize_c1_Integrated_system_V061(void
  *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_Integrated_system_V061InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
  initialize_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_Integrated_system_V061(void *chartInstanceVar)
{
  enable_c1_Integrated_system_V061((SFc1_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c1_Integrated_system_V061(void *chartInstanceVar)
{
  disable_c1_Integrated_system_V061((SFc1_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c1_Integrated_system_V061(void
  *chartInstanceVar)
{
  zeroCrossings_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_derivatives_c1_Integrated_system_V061(void
  *chartInstanceVar)
{
  derivatives_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static void sf_opaque_outputs_c1_Integrated_system_V061(void *chartInstanceVar)
{
  outputs_c1_Integrated_system_V061((SFc1_Integrated_system_V061InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c1_Integrated_system_V061(void *chartInstanceVar)
{
  sf_gateway_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_Integrated_system_V061
  (SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_Integrated_system_V061(SimStruct* S,
  const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*)chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c1_Integrated_system_V061(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_Integrated_system_V061InstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Integrated_system_V061_optimization_info();
    }

    finalize_c1_Integrated_system_V061
      ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_Integrated_system_V061
    ((SFc1_Integrated_system_V061InstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_Integrated_system_V061(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c1_Integrated_system_V061
      ((SFc1_Integrated_system_V061InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c1_Integrated_system_V061(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Integrated_system_V061_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,4);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=4; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1177406322U));
  ssSetChecksum1(S,(3734374075U));
  ssSetChecksum2(S,(2684348322U));
  ssSetChecksum3(S,(3709156236U));
  ssSetNumContStates(S,1);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_Integrated_system_V061(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c1_Integrated_system_V061(SimStruct *S)
{
  SFc1_Integrated_system_V061InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc1_Integrated_system_V061InstanceStruct *)utMalloc(sizeof
    (SFc1_Integrated_system_V061InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc1_Integrated_system_V061InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_Integrated_system_V061;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_Integrated_system_V061;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c1_Integrated_system_V061;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c1_Integrated_system_V061;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c1_Integrated_system_V061;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_Integrated_system_V061;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_Integrated_system_V061;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_Integrated_system_V061;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c1_Integrated_system_V061;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c1_Integrated_system_V061;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c1_Integrated_system_V061;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_Integrated_system_V061;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_Integrated_system_V061;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c1_Integrated_system_V061;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c1_Integrated_system_V061_method_dispatcher(SimStruct *S, int_T method,
  void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_Integrated_system_V061(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_Integrated_system_V061(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_Integrated_system_V061_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
