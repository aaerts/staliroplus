% =========================================================================
% Title         : staliro_plus_sub_V06.m
% Version       : V0.6
% Author        : Arend Aerts
% Project       : Model-based design and verification of mechatronic sys. 
% Date          : 28-04-2016
% Description   : Sub run file
%
%                                ---
% 
% V0.1          : Initial run functonality + structure
%
% V0.2          : Frequency coverage continuous signals
%
% V0.3          : First ready-to-use function
%
% V0.4          : Fequency optimization loop
%
% =========================================================================
function [stalplus_run, stalplus_run_his, stalplus] = staliro_plus_sub_V07(model,phi,preds,opt)


% Declare global variables
global staliro_opt;
staliro_opt = opt;


% INIT
% ====
decimal_round = 6;
decimal_round_timestep = 7;
stalplus_run = struct('stalplus_iter',struct('stal_run',struct('run',[],'optRobIndex',[]),'fr_select',[],'CP_select',[],'opt_rob',[],'falsified',[]),'optRob',[],'optRobIndex',[],'falsified',[],'n_tests',[],'runtime',[]);
if opt.DataExport == 1
    stalplus_run_his = struct('stalplus_iter',struct('stal_run',struct('rob',[],'samples',[],'cost',[],'YT',[],'UT',[],'T1',[])));
else
    stalplus_run_his = struct('stalplus_iter',struct('stal_run',struct('rob',[],'samples',[],'cost',[])));
end
stalplus.cp_array_his = [];  
stalplus.fr_his = []; 
stalplus.non_fal_per_run_total = zeros(opt.fr_runs,1);                                         
stalplus.non_fal_per_run = zeros(opt.fr_runs,opt.fr_n_tests);
stalplus.fal_per_run_total = zeros(opt.fr_runs,1);
stalplus.fal_per_run = zeros(opt.fr_runs,opt.fr_n_tests);
stalplus.falsification_amount = 0;




% load model custom simulator
% ===========================
if opt.SimulinkSingleOutput == 2
    load_system(model);
    set_param(model, 'SolverType', 'Fixed-step');
    set_param(model, 'Solver', 'ode5');
    set_param(model, 'FixedStep', num2str(opt.SampTime));
    set_param(model, 'StopTime', num2str(opt.time));
    set_param(model,'FastRestart','on');
end
    
    

% If the seed for the random number generator is prespecified set it
% ==================================================================
if opt.seed ~= 0
    if verLessThan('matlab', '7.12')
        stream = RandStream('mt19937ar','Seed',opt.seed);
        RandStream.setDefaultStream(stream);
    else
        rng(opt.seed);
    end
end



% RUN S-TaLiRo tooling
% ====================
disp('====================================================');
for w = 1:opt.fr_runs   
    % Run S-TaliRo+
    disp(['S-TaLiRo+ run ' num2str(w) ' / ' num2str(opt.fr_runs) ' of ' model]);
    disp([datestr(now)]);
    disp('====================================================');
    tic;
    [results_tmp, his_tmp, cp_array_his, fr_his, fals, best_rob2, best_rob2_index]...
        = staliro_plus_opt_V02(model,phi,preds,opt);
    stalplus_run(w).runtime = toc;
    stalplus_run(w).falsified = fals;
    stalplus_run(w).optRob = best_rob2;
    stalplus_run(w).optRobIndex = best_rob2_index;
    stalplus_run(w).stalplus_iter = results_tmp;
    stalplus_run(w).n_tests = size(stalplus_run(w).stalplus_iter,2);
    stalplus_run_his(w).stalplus_iter = his_tmp;
    stalplus.cp_array_his = [stalplus.cp_array_his; cp_array_his];  
    stalplus.fr_his = [stalplus.fr_his; fr_his];  
    

         
    % Provide statictics
    for k = 1:stalplus_run(w).n_tests
        if stalplus_run(w).stalplus_iter(k).falsified == 1
            stalplus.fal_per_run_total(w) = stalplus.fal_per_run_total(w) + 1;
            stalplus.fal_per_run(w,k) = k;
        else
            stalplus.non_fal_per_run_total(w) = stalplus.non_fal_per_run_total(w) + 1;
            stalplus.non_fal_per_run(w,k) = k;         
        end
    end
    
    % Report results
    disp(' ');
    disp('> TEST REPORT');
    disp('> -----------');
    disp(['> ' num2str(stalplus.fal_per_run_total(w)) ' out of ' num2str(stalplus_run(w).n_tests) ' iterations (' num2str((stalplus.fal_per_run_total(w)/stalplus_run(w).n_tests)*100) '%) falsify specification']);
    disp(['> Best iteration: ' num2str(stalplus_run(w).optRobIndex)]);
    disp(['> Best phi-value: ' num2str(stalplus_run(w).optRob)]);
    disp(['> S-TaLiRo+ runtime: ' num2str(stalplus_run(w).runtime) 's / ' num2str(stalplus_run(w).runtime/60) 'min']);
    disp(' ');
    disp('====================================================');
    
end
runtime=toc;




% Close model custom simulator
% ============================
if opt.SimulinkSingleOutput == 2
   set_param(model,'FastRestart','off');
   close_system(model,0);
end



% Post-processing data
% ====================

% Generate S-TaLiRo+ run statictics
for k= 1:opt.fr_runs
    if stalplus_run(k).falsified == 1
        stalplus.falsification_amount = stalplus.falsification_amount + 1;
    end
end

% Round boolean data points (interpolation correction)
for k= 1:size(opt.interpolationtype,2)                      
    if strcmp(opt.interpolationtype(1,k),'bool')
        for y= 1:opt.fr_runs
            for w= 1:opt.fr_n_tests
                for z= 1:opt.runs
                    for p= 1:opt.optim_params.n_tests
                        if k > 1
                            tmp = 0;
                            for h = 1:k-1
                                tmp = tmp + stalplus_run(y).stalplus_iter(w).CP_select(1,h); 
                            end
                            tmp = tmp + 1;
                            stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(p,tmp:tmp-1+stalplus_run(y).stalplus_iter(w).CP_select(1,k)) = ...
                                round(stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(p,tmp:tmp-1+stalplus_run(y).stalplus_iter(w).CP_select(1,k)));
                        else
                            stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(p,1:stalplus_run(y).stalplus_iter(w).CP_select(1,k)) = ...
                                round(stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(p,1:stalplus_run(y).stalplus_iter(w).CP_select(1,k)));
                        end
                    end
                end
            end
        end
    end
end

if opt.DataExport == 2
    % Data re-location input and input samples (U), and output (Y)
    for y = 1:opt.fr_runs 
        for w= 1:stalplus_run(y).n_tests
            for z = 1:opt.runs
                for p = 1:size(stalplus_run(y).stalplus_iter(w).CP_select,2)
                    tmp1 = stalplus_run_his(y).stalplus_iter(w).stal_run(z).UT(:,p);
                    k = p;
                    while k <= size(stalplus_run_his(y).stalplus_iter(w).stal_run(z).UT,2)-size(stalplus_run(y).stalplus_iter(w).CP_select,2)
                        k = k + size(stalplus_run(y).stalplus_iter(w).CP_select,2);
                        tmp1 = [tmp1 stalplus_run_his(y).stalplus_iter(w).stal_run(z).UT(:,k)];
                    end
                    stalplus_run_his(y).stalplus_iter(w).stal_run(z).U{1,p} = round(tmp1,decimal_round);

                    if p == 1
                        stalplus_run_his(y).stalplus_iter(w).stal_run(z).U{2,p} =...
                            round(stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(:,1:stalplus_run(y).stalplus_iter(w).CP_select(1,p))',decimal_round);
                    else
                        tmp3 = 0;
                        for b = 1:p-1
                           tmp3 = tmp3 + stalplus_run(y).stalplus_iter(w).CP_select(1,b);  
                        end
                        stalplus_run_his(y).stalplus_iter(w).stal_run(z).U{2,p} = ...
                            round(stalplus_run_his(y).stalplus_iter(w).stal_run(z).samples(:,(tmp3+1):(tmp3+stalplus_run(y).stalplus_iter(w).CP_select(1,p)))',decimal_round);
                    end
                end
            end
        end
    end

    % Time points input data samples
    for y= 1:opt.fr_runs 
        for k= 1:stalplus_run(y).n_tests           
            for p= 1:opt.runs 
                for w = 1:size(stalplus_run(y).stalplus_iter(k).CP_select,2)
                    stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{1,w} = (0:opt.SampTime:opt.TotSimTime)';
                    stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w} = (0:opt.time/(stalplus_run(y).stalplus_iter(k).CP_select(1,w)-1):opt.time)';   
                    stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w} = round(stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w},decimal_round_timestep);

                    if size(stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w}) == 1 
                        stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w} = [stalplus_run_his(y).stalplus_iter(k).stal_run(p).T{2,w}; opt.TotSimTime];
                        stalplus_run_his(y).stalplus_iter(k).stal_run(p).U{2,w} = [stalplus_run_his(y).stalplus_iter(k).stal_run(p).U{2,w}; stalplus_run_his(y).stalplus_iter(k).stal_run(p).U{2,w}];
                    end
                end
            end
        end
    end

    % Data re-location output (Y)
    for y = 1:opt.fr_runs 
        for p= 1:stalplus_run(y).n_tests
            for z = 1:opt.runs
                for w = 1:size(preds(1).A,2)
                    tmp1 = stalplus_run_his(y).stalplus_iter(p).stal_run(z).YT(:,w);
                    k = w;
                    while k <= size(stalplus_run_his(y).stalplus_iter(p).stal_run(z).YT,2)-size(preds(1).A,2)
                        k = k + size(preds(1).A,2);
                        tmp1 = [tmp1 stalplus_run_his(y).stalplus_iter(p).stal_run(z).YT(:,k)];
                    end
                    stalplus_run_his(y).stalplus_iter(p).stal_run(z).Y{1,w} = round(tmp1,decimal_round);
                end
            end
        end
    end
end

end



