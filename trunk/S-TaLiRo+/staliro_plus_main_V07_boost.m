% =========================================================================
% Title         : staliro_plus_main_V07_boost.m
% Version       : V0.7
% Author        : Arend Aerts
% Project       : Model-based design and verification of mechatronic sys. 
% Date          : 28-04-2016
% Description   : Main run file boost converter
%
%                                ---
% 
% V0.1          : Initial run functonality + structure
%
% V0.2          : Frequency coverage continuous signals
%
% V0.3          : First ready-to-use version
%
% V0.4          : Frquency optimization loop
%
% =========================================================================

global staliro_opt;

% WORKSPACE CONFIG
% ================
clc, clear all, close all

% Define MATLAB workspace
cd('C:\Users\aaerts\Desktop\staliro_plus_V07_boost')

% Model file name (in workspace)
model = 'Boost_converter_V112';

% Specification BOOST
phi = '[](!r2 /\ r1)';       

% 1st predicate
preds(1).str='r1';
preds(1).A = [1 0];
preds(1).b = 245;
% 
% % 2nd predicate
preds(2).str='r2';
preds(2).A = [1 0];
preds(2).b = 235;


% INIT / CONFIGURATION
% ====================

% Include options file
opt = staliro_options();                                                    % Create an staliro_options object with the default options

% Simulation input options (modified autotrans)
% opt.time = 30;                                                              % Total Simulation time
% opt.SampTime = 0.04;   
% opt.input_range = [0 100; 0 100];                                                  % The constraints on the input signal defined as a range
% opt.cp_array = [7 210];                                                       % The number of control points for the input signal
% opt.interpolationtype = {'pchip','pchip'};                                          % Interpolation type between input points
% Simulation input options (modified autotrans)
opt.time = 0.005;                                                              % Total Simulation time
opt.SampTime = 0.0000001;   
opt.input_range = [0 4];                                                  % The constraints on the input signal defined as a range
opt.cp_array = [7];                                                       % The number of control points for the input signal
opt.interpolationtype = {'pchip'};                                          % Interpolation type between input points


% S-TaLiRo options
opt.runs = 1;                                                              % Number of S-TaLiro Runs
opt.optim_params.n_tests = 40;                                             % Number of S-TaLiRo iterations per run
opt.optim_params.UpdateInterval = 4;                                        % Update interval (amplitude) optimization
opt.optim_params.dispStart = 0.75;
opt.optim_params.dispAdap = 10;
opt.optim_params.minDisp = 0.01;
opt.optim_params.maxDisp = 0.99;
opt.optim_params.betaXStart = -15;
opt.optim_params.betaXAdap = 50;
opt.optim_params.acRatioMax = 0.55;
opt.optim_params.acRatioMin = 0.45;
opt.optimization_solver = 'SA_Taliro';
opt.dispinfo = 0;

% S-TaLiRo+ options
opt.fr_inputs = [1];                                                         % Frequency input selection
opt.fr_range = [200 750];                                                          % Frequency range selection (Hz)
opt.fr_runs = 25;                                                            % Number of S-TaLiro+ runs
opt.fr_n_tests = 4;                                                         % Number of S-TaLiRo+ iterations per fr_run
opt.fr_UpdateInterval = 1;                                                  % Update interval (frequency) optimization
opt.fr_dispStart = 0.75;
opt.fr_dispAdap = 10;
opt.fr_minDisp = 0.01;
opt.fr_maxDisp = 0.99;
opt.fr_betaXStart = -15;
opt.fr_betaXAdap = 50;
opt.fr_acRatioMax = 0.55;
opt.fr_acRatioMin = 0.45;
opt.fr_optimization_solver = 'SA_Taliro';                                   % SA_Taliro or UR_Taliro

% Benchmark options
opt.stalplus = 'on';                                                       % Select S-tal mode (1 = S-TaLiRo / 2 = S-TaLiRo+)
opt.SimulinkSingleOutput = 2;                                               % Select simulator  (1 = simulink / 2 = S-simulink fast restart)
opt.getNewSampleMode = 1;                                                   % Select PS scheme  (1 = Hit-and-Run / 2 = decoupled Hit-and-Run)
opt.DataExport = 2;                                                         % Select logging    (1 = general / 2 = general + IO)
opt.seed = 123;

% Update global variable
staliro_opt = opt;


% RUN STALIRO TOOLING
% ===================
tStart = tic;
[stalplus_run, stalplus_run_his, thesis] = staliro_plus_sub_V07(model,phi,preds,opt);
thesis.runtime = toc(tStart);
disp(' ');
disp('> TEST REPORT S-TaLiRo+');
disp('> ---------------------');
disp(['> ' num2str(thesis.falsification_amount) ' out of ' num2str(opt.fr_runs) ' runs (' num2str((thesis.falsification_amount/opt.fr_runs)*100) '%) falsify specification']);
disp(['> S-TaLiRo+ runtime: ' num2str(round(thesis.runtime,3)) ' s / ' num2str(round(thesis.runtime/60,3)) ' min / ' num2str(round(thesis.runtime/3600,3)) ' hours']);
disp(' ');
disp('====================================================');


%% Calculate statistics S-Tal+
% ============================
tmp1=[];
tmp2=[];
for k=1:size(stalplus_run,2)
    tmp1 = [tmp1 stalplus_run(k).optRob];
    tmp2 = [tmp2 stalplus_run(k).runtime];
end
tmp11 = tmp1(tmp1>0);

thesis.NF_robustness_minimum = min(tmp11);
thesis.NF_robustness_average = mean(tmp11);
thesis.NF_robustness_variance = var(tmp11);
thesis.NF_time_minimum = min(tmp2);
thesis.NF_time_average = mean(tmp2);
thesis.NF_time_variance = var(tmp2);

%% Calculate statistics S-Tal
% ===========================
% tmp1=[];
% tmp2=[];
% for k=1:size(stalplus_run,2)
%     tmp1 = [tmp1 stalplus_run.stalplus_iter.stal_run.bestRob];
%     tmp2 = [tmp2 stalplus_run.stalplus_iter.stal_run.time];
% end
% tmp11 = tmp1(tmp1>0);
% 
% thesis.NF_robustness_minimum = min(tmp11);
% thesis.NF_robustness_average = mean(tmp11);
% thesis.NF_robustness_variance = var(tmp11);
% thesis.ALL_time_minimum = min(tmp2);
% thesis.ALL_time_average = mean(tmp2);
% thesis.ALL_time_variance = var(tmp2);


