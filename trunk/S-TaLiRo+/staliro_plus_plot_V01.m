% =========================================================================
% Title         : staliro_plot_V01.m
% Version       : V0.1
% Author        : Arend Aerts
% Project       : Model-based design and verification of mechatronic sys. 
% Date          : 09-03-2016
% Description   : Plot file
%
%                                ---
% 
% V0.1          : Initial run functonality + structure
%
% =========================================================================

function staliro_plot_V01(results, his, stal, plot2, opt)

if plot2.initial == 1
    
    % Plot Input signal overview per run
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        for z = 1:size(plot2.in_var,2)
            z1 = plot2.in_var(1,z);
            figure;
            for w = 1:size(plot2.stal_run,2)
                w1 = plot2.stal_run(1,w);
                subplot(size(plot2.stal_run,2),1,w);
                hold on;              
                xx1 = his(y1,w1).T{1,z1};
                yy1 = his(y1,w1).U{1,z1};
                xx2 = his(y1,w1).T{2,z1};
                yy2 = his(y1,w1).U{2,z1};
                for k = 1 : results(y1).run(w1).nTests
                    if k == 1
                        plot(xx1,yy1(:,k),'LineWidth',2);
                        ax = gca;
                        ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                        scatter(xx2,yy2(:,k),50,'filled');
                    else
                        if k == results(y1).run(w1).nTests
                            plot(xx1,yy1(:,k),'--','LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            plot(xx1,yy1(:,k));
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),30,'filled');
                        end
                    end
                end
                grid on;
                title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Input signal ' num2str(z1)]);
                xlabel('Time [s]');
                ylabel('Value [/]');
            end
        end
    end
    
    
    % Plot output signal overview per run
%     for y = 1:size(plot2.fr_run,2)
%         y1 = plot2.fr_run(1,y);
%         for z = 1:size(plot2.in_var,2)
%             figure;
%             for w = 1:size(plot2.stal_run,2)
%                 w1 = plot2.stal_run(1,w);
%                 subplot(size(plot2.stal_run,2),1,w);
%                 hold on;              
%                 xx1 = his(y1,w1).T{1,z};
%                 yy1 = his(y1,w1).Y{1,z};
%                 for k = 1 : results(y1).run(w1).nTests
%                     if k == 1
%                         plot(xx1,yy1(:,k),'LineWidth',2);
%                     else
%                         if k == results(y1).run(w1).nTests
%                             plot(xx1,yy1(:,k),'--','LineWidth',2);
%                         else
%                             plot(xx1,yy1(:,k));
%                         end
%                     end
%                 end
%                 grid on;
%                 title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Output signal ' num2str(plot2.in_var(1,z))]);
%                 xlabel('Time [s]');
%                 ylabel('Value [/]');
%             end
%         end
%     end
    
    
    % Plot Input signal initial overview per run
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        for z = 1:size(plot2.in_var,2)
            z1 = plot2.in_var(1,z);
            figure;
            hold on;              
            for w = 1:size(plot2.stal_run,2)
                w1 = plot2.stal_run(1,w);
                xx1 = his(y1,w1).T{1,z1};
                yy1 = his(y1,w1).U{1,z1};
                xx2 = his(y1,w1).T{2,z1};
                yy2 = his(y1,w1).U{2,z1};
                
                if results(y1).run(w1).falsified == 1
                    plot(xx1,yy1(:,1),'--','LineWidth',2);
                    ax = gca;
                    ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                    scatter(xx2,yy2(:,1),50,'filled');
                else
                    plot(xx1,yy1(:,1),'LineWidth',2);
                    ax = gca;
                    ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                    scatter(xx2,yy2(:,1),50,'filled');
                end
            end
            grid on;
            title(['S-TaLiRo run ' num2str(y1) ', Input signal ' num2str(z1) ' (initial overview)']);
            xlabel('Time [s]');
            ylabel('Value [/]');
        end
    end  
    
    % Plot Input signal initial overview all run
    for z = 1:size(plot2.in_var,2)
        figure;
        hold on;
        z1 = plot2.in_var(1,z);
        for y = 1:size(plot2.fr_run,2)
            y1 = plot2.fr_run(1,y);   
            for w = 1:size(plot2.stal_run,2)
                w1 = plot2.stal_run(1,w);
                xx1 = his(y1,w1).T{1,z1};
                yy1 = his(y1,w1).U{1,z1};
                xx2 = his(y1,w1).T{2,z1};
                yy2 = his(y1,w1).U{2,z1};
                
                if results(y1).run(w1).falsified == 1
                    plot(xx1,yy1(:,1),'--','LineWidth',2);
                    ax = gca;
                    ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                    scatter(xx2,yy2(:,1),50,'filled');
                else
                    plot(xx1,yy1(:,1),'LineWidth',2);
                    ax = gca;
                    ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                    scatter(xx2,yy2(:,1),50,'filled');
                end
            end
        end
        grid on;
        title(['Input signal ' num2str(z1) ' (initial overview, all runs)']);
        xlabel('Time [s]');
        ylabel('Value [/]');
    end  
end






if size((stal.non_fal_run_total(stal.non_fal_run_total > 0)),1) >=1 && plot2.non_fal == 1
    % Plot phi-value decrease overview non-falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.non_fal_run_total(y1) > 0
            figure;
            for k= 1:stal.non_fal_run_total(y1)
                k2 = k;
                while stal.non_fal_run(y1,k2) == 0
                    k2 = k2 + 1;
                end
                subplot(stal.non_fal_run_total(y1),1,k);
                bar(his(y1,stal.non_fal_run(y1,k2)).rob);
                grid on;
                title(['Non-falsification run ' num2str(y1) '.' num2str(stal.non_fal_run(y1,k2)) ', Phi-value overview']);
                xlabel('Iteration');
                ylabel('\phi-Value');
            end
        end
    end
    
    
    % Plot Input signal overview non-falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.non_fal_run_total(y1) > 0
            for z = 1:size(plot2.in_var,2)
                figure;
                w2 = 0;
                for w = 1:stal.non_fal_run_total(y1)
                    subplot(stal.non_fal_run_total(y1),1,w);
                    hold on;      
                    if w2<w
                        w2 = w;
                    else
                        w2 = w2+1;
                    end
                    while stal.non_fal_run(y1,w2) == 0
                        w2 = w2 + 1;
                    end
                    xx1 = his(y1,w2).T{1,z};
                    yy1 = his(y1,w2).U{1,z};
                    xx2 = his(y1,w2).T{2,z};
                    yy2 = his(y1,w2).U{2,z};
                    for k = 1 : results(y1).run(stal.non_fal_run(y1,w2)).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            if k == results(y).run(stal.non_fal_run(y1,w2)).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),50,'filled');
                            else
                                plot(xx1,yy1(:,k));
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),30,'filled');
                            end
                        end
                    end
                    grid on;
                    title(['Non-falsification run ' num2str(y1) '.' num2str(w) ', Input signal ' num2str(plot2.in_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');
                end
            end
        end
    end 
    
    % Plot output signal overview non-falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.non_fal_run_total(y1) > 0
            for z = 1:size(plot2.in_var,2)
                figure;
                w2 = 0;
                for w = 1:stal.non_fal_run_total(y1)
                    subplot(stal.non_fal_run_total(y1),1,w);
                    hold on;      
                    if w2<w
                        w2 = w;
                    else
                        w2 = w2+1;
                    end
                    while stal.non_fal_run(y1,w2) == 0
                        w2 = w2 + 1;
                    end
                    xx1 = his(y1,w2).T{1,z};
                    yy1 = his(y1,w2).Y{1,z};
                    for k = 1 : results(y1).run(stal.non_fal_run(y1,w2)).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                        else
                            if k == results(y1).run(stal.non_fal_run(y1,w2)).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                            else
                                plot(xx1,yy1(:,k));
                            end
                        end
                    end
                    grid on;
                    title(['Non-falsification run ' num2str(y1) '.' num2str(w) ', Output signal ' num2str(plot2.in_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');
                end
            end
        end
    end
end




if size((stal.fal_run_total(stal.non_fal_run_total > 0)),1) >=1 && plot2.fal == 1
    % Plot phi-value decrease overview falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.fal_run_total(y1) > 0
            figure;
            for k= 1:stal.fal_run_total(y1)
                k2 = k;
                while stal.fal_run(y1,k2) == 0
                    k2 = k2 + 1;
                end
                subplot(stal.fal_run_total(y1),1,k);
                bar(his(y1,stal.fal_run(y1,k2)).rob);
                grid on;
                title(['Falsification run ' num2str(y1) '.' num2str(stal.fal_run(y1,k2)) ', Phi-value overview']);
                xlabel('Iteration');
                ylabel('\phi-Value');
            end
        end
    end
    
    
    % Plot Input signal overview falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.fal_run_total(y1) > 0
            for z = 1:size(plot2.in_var,2)
                figure;
                w2 = 0;
                for w = 1:stal.fal_run_total(y1)
                    subplot(stal.fal_run_total(y1),1,w);
                    hold on;      
                    if w2<w
                        w2 = w;
                    else
                        w2 = w2+1;
                    end
                    while stal.fal_run(y1,w2) == 0
                        w2 = w2 + 1;
                    end
                    xx1 = his(y1,w2).T{1,z};
                    yy1 = his(y1,w2).U{1,z};
                    xx2 = his(y1,w2).T{2,z};
                    yy2 = his(y1,w2).U{2,z};
                    for k = 1 : results(y).run(stal.fal_run(y1,w2)).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            if k == results(y1).run(stal.fal_run(y1,w2)).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),50,'filled');
                            else
                                plot(xx1,yy1(:,k));
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),30,'filled');
                            end
                        end
                    end
                    grid on;
                    title(['Falsification run ' num2str(y1) '.' num2str(w) ', Input signal ' num2str(plot2.in_var(1,z))]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');
                end
            end
        end
    end
    
    
    
    % Plot output signal overview falsification runs
    for y = 1:size(plot2.fr_run,2)
        y1 = plot2.fr_run(1,y);
        if stal.fal_run_total(y1) > 0
            for z = 1:size(plot2.in_var,2)
                figure;
                w2 = 0;
                for w = 1:stal.fal_run_total(y1)
                    subplot(stal.fal_run_total(y1),1,w);
                    hold on;      
                    if w2<w
                        w2 = w;
                    else
                        w2 = w2+1;
                    end
                    while stal.fal_run(y1,w2) == 0
                        w2 = w2 + 1;
                    end
                    xx1 = his(y1,w2).T{1,z};
                    yy1 = his(y1,w2).Y{1,z};
                    for k = 1 : results(y1).run(stal.fal_run(y1,w2)).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                        else
                            if k == results(y1).run(stal.fal_run(y1,w2)).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                            else
                                plot(xx1,yy1(:,k));
                            end
                        end
                    end
                    grid on;
                    title(['Falsification run ' num2str(y1) '.' num2str(w) ', Output signal ' num2str(plot2.in_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');
                end
            end
        end
    end
end

% Plot combined input output overview
if plot2.inout == 1
    
    if size(plot2.in_var,2) < size(plot2.out_var,2)
        % Plot Input signal overview per run
        for y = 1:size(plot2.fr_run,2)
            y1 = plot2.fr_run(1,y);
            for z = 1:size(plot2.in_var,2)
                figure;
                z1 = plot2.in_var(1,z);
                ii = 1;
                for w = 1:size(plot2.stal_run,2)
                    w1 = plot2.stal_run(1,w);
                    subplot(size(plot2.stal_run,2),2,ii);
                    ii = ii + 1;
                    hold on;              
                    xx1 = his(y1,w1).T{1,z1};
                    yy1 = his(y1,w1).U{1,z1};
                    xx2 = his(y1,w1).T{2,z1};
                    yy2 = his(y1,w1).U{2,z1};
                    yy3 = his(y1,w1).Y{1,out_var(1,z)};
                    for k = 1 : results(y1).run(w1).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            if k == results(y1).run(w1).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),50,'filled');
                            else
                                plot(xx1,yy1(:,k));
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),30,'filled');
                            end
                        end
                    end
                    grid on;
                    title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Input signal ' num2str(plot2.in_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');

                    subplot(size(plot2.stal_run,2),2,ii);

                    ii = ii + 1;
                    hold on;
                    for k = 1 : results(y1).run(w1).nTests
                        if k == 1
                            plot(xx1,yy3(:,k),'LineWidth',2);
                        else
                            if k == results(y1).run(w1).nTests
                                plot(xx1,yy3(:,k),'--','LineWidth',2);
                            else
                                plot(xx1,yy3(:,k));
                            end
                        end
                    end
                    grid on;
                    title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Output signal ' num2str(plot2.out_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');

                end
            end
        end
    else
        % Plot Input signal overview per run
        for y = 1:size(plot2.fr_run,2)
            y1 = plot2.fr_run(1,y);
            for z = 1:size(plot2.out_var,2)
                figure;
                z1 = plot2.out_var(1,z);
                ii = 1;
                for w = 1:size(plot2.stal_run,2)
                    w1 = plot2.stal_run(1,w);
                    subplot(size(plot2.stal_run,2),2,ii);
                    ii = ii + 1;
                    hold on;              
                    xx1 = his(y1,w1).T{1,z1};
                    yy1 = his(y1,w1).U{1,z1};
                    xx2 = his(y1,w1).T{2,z1};
                    yy2 = his(y1,w1).U{2,z1};
                    yy3 = his(y1,w1).Y{1,z1};
                    for k = 1 : results(y1).run(w1).nTests
                        if k == 1
                            plot(xx1,yy1(:,k),'LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            if k == results(y1).run(w1).nTests
                                plot(xx1,yy1(:,k),'--','LineWidth',2);
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),50,'filled');
                            else
                                plot(xx1,yy1(:,k));
                                ax = gca;
                                ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                                scatter(xx2,yy2(:,k),30,'filled');
                            end
                        end
                    end
                    grid on;
                    title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Input signal ' num2str(plot2.in_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');

                    subplot(size(plot2.stal_run,2),2,ii);

                    ii = ii + 1;
                    hold on;
                    for k = 1 : results(y1).run(w1).nTests
                        if k == 1
                            plot(xx1,yy3(:,k),'LineWidth',2);
                        else
                            if k == results(y1).run(w1).nTests
                                plot(xx1,yy3(:,k),'--','LineWidth',2);
                            else
                                plot(xx1,yy3(:,k));
                            end
                        end
                    end
                    grid on;
                    title(['S-TaLiRo run ' num2str(y1) '.' num2str(w1) ', Output signal ' num2str(plot2.out_var(1,z)) ]);
                    xlabel('Time [s]');
                    ylabel('Value [/]');

                end
            end
        end
    end
end

if plot2.invarall == 1;
    
    
    % Plot Input signal overview all runs
    for z = 1:size(plot2.in_var,2)
        z1 = plot2.in_var(1,z);
        figure;
        hold on;
        for y = 1:size(plot2.fr_run,2)
            y1 = plot2.fr_run(1,y);
            for w = 1:size(plot2.stal_run,2)
                w1 = plot2.stal_run(1,w);
                xx1 = his(y1,w1).T{1,z1};
                yy1 = his(y1,w1).U{1,z1};
                xx2 = his(y1,w1).T{2,z1};
                yy2 = his(y1,w1).U{2,z1};
                for k = 1 : results(y1).run(w1).nTests
                    if k == 1
                        plot(xx1,yy1(:,k),'LineWidth',2);
                        ax = gca;
                        ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                        scatter(xx2,yy2(:,k),50,'filled');
                    else
                        if k == results(y1).run(w1).nTests
                            plot(xx1,yy1(:,k),'--','LineWidth',2);
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),50,'filled');
                        else
                            plot(xx1,yy1(:,k));
                            ax = gca;
                            ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
                            scatter(xx2,yy2(:,k),30,'filled');
                        end
                    end
                end
            end
        end
        grid on;
        title(['Input signal ' num2str(z1) ' overview (all runs)']);
        xlabel('Time [s]');
        ylabel('Value [/]');
    end 
end

if plot2.outvarall == 1;
    % Plot Input signal overview all runs
    for z = 1:size(plot2.out_var,2)
        z1 = plot2.out_var(1,z);
        figure;
        hold on;
        for y = 1:size(plot2.fr_run,2)
            y1 = plot2.fr_run(1,y);
            for w = 1:size(plot2.stal_run,2)
                w1 = plot2.stal_run(1,w);
                xx1 = his(y1,w1).T{1,z1};
                yy1 = his(y1,w1).Y{1,z1};
                for k = 1 : results(y1).run(w1).nTests
                    if k == 1
                        plot(xx1,yy1(:,k),'LineWidth',2);
                    else
                        if k == results(y1).run(w1).nTests
                            plot(xx1,yy1(:,k),'--','LineWidth',2);
                        else
                            plot(xx1,yy1(:,k));
                        end
                    end
                end
            end
        end
        grid on;
        title(['Output signal ' num2str(z1) ' overview (all runs)']);
        xlabel('Time [s]');
        ylabel('Value [/]');
    end 
end
end