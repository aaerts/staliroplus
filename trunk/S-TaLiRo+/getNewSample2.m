function [newSample, cp_array] = getNewSample2(varargin)        

global staliro_opt;

if nargin == 1
    
    m = size(varargin{1},1);
    inpRangeOrig = varargin{1}; 
    newSample = (inpRangeOrig(:,1)-inpRangeOrig(:,2)).*rand(m,1)+inpRangeOrig(:,2);

elseif nargin == 3 || nargin == 4 

    m = size(varargin{1},1);
    curSampleOrig = varargin{1};
    inpRangeOrig = varargin{2};
    dispL = varargin{3};

    if staliro_opt.getNewSampleMode == 1
        % This is the case where the control points are distributed equally in time
         Ix_nonzero =  find(inpRangeOrig(:,1) - inpRangeOrig(:,2));
         inpRange = ones(m,2);
         inpRange(Ix_nonzero,1) = 0;
         curSample = ones(m,1);
         curSample(Ix_nonzero) = (curSampleOrig(Ix_nonzero)-inpRangeOrig(Ix_nonzero,1))./(inpRangeOrig(Ix_nonzero,2)-inpRangeOrig(Ix_nonzero,1));
        %First get a random unit vector
        %Second find the max/min offsets along this vector
        %choose a point
        rUnitVector = randn(m,1);

       % rUnitVector = rUnitVector/ norm(rUnitVector);
        lam1=(inpRange(:,1)-curSample)./rUnitVector;
        lam2=(inpRange(:,2)-curSample)./rUnitVector;

        for i=1:1:m
            if (lam1(i,1)> lam2(i,1))
                tmp=lam2(i,1);
                lam2(i,1)=lam1(i,1);
                lam1(i,1)=tmp;
            end
            assert(lam1(i,1) <= eps('single'));
            assert(lam2(i,1) >= -eps('single') );
        end

        l1=max(lam1(Ix_nonzero));
        l2=min(lam2(Ix_nonzero));
        % Handle case where inpRange restricts some dimension to one point
        % In this case lam1 and lam2 will each contain a 0 at that dimension, forcing r to be
        % 0, and the newSample to equal the curSample. So we ignore these
        % restricted dimensions from the range computation  

        r=mcGenerateRandomNumber(l1,l2,dispL);
        weightVector = zeros(m,1);
        weightVector(Ix_nonzero) = r;

        % Transform new point back to original search hypercube
        newSample = (curSample+weightVector.*rUnitVector).*(inpRangeOrig(:,2)-inpRangeOrig(:,1))+inpRangeOrig(:,1);  
    else
        newSample = [];
        for k = 1:size(staliro_opt.fr_inputs,2)
            %% This is the case where the control points are distributed equally in time
            Ix_nonzero =  find(inpRangeOrig(k,1) - inpRangeOrig(k,2));
            if ~isempty(Ix_nonzero)
                inpRange = ones(1,2);
                inpRange(Ix_nonzero,1) = 0;
                curSample = ones(1,1);
                curSample(Ix_nonzero) = (curSampleOrig(k*Ix_nonzero)-inpRangeOrig(k*Ix_nonzero,1))./(inpRangeOrig(k*Ix_nonzero,2)-inpRangeOrig(k*Ix_nonzero,1));


                %First get a random unit vector
                %Second find the max/min offsets along this vector
                %choose a point      
                rUnitVector = randn(1,1);
                lam1=(inpRange(1,1)-curSample)./rUnitVector;
                lam2=(inpRange(1,2)-curSample)./rUnitVector;


                if (lam1 > lam2)
                    tmp=lam2;
                    lam2=lam1;
                    lam1=tmp;
                end

                assert(lam1(1,1) <= eps('single'));
                assert(lam2(1,1) >= -eps('single'));

                % Handle case where inpRange restricts some dimension to one point
                % In this case lam1 and lam2 will each contain a 0 at that dimension, forcing r to be
                % 0, and the newSample to equal the curSample. So we ignore these
                % restricted dimensions from the range computation
                l1=max(lam1(Ix_nonzero));
                l2=min(lam2(Ix_nonzero));      

                r=mcGenerateRandomNumber(l1,l2,dispL);
                weightVector = zeros(1,1);
                weightVector(Ix_nonzero) = r;

                % Transform new point back to original search hypercube
                newSample = [newSample; (curSample+weightVector.*rUnitVector).*(inpRangeOrig(k,2)-inpRangeOrig(k,1))+inpRangeOrig(k,1)];
            else
                newSample = [newSample; curSampleOrig(k)];
            end
        end
    end
end


% Calculation cp_array
sin_points = 2;
cp_array_max_points = staliro_opt.time/staliro_opt.SampTime;
cp_array_min_points = 2;

fr_min = 1/staliro_opt.time; 
cp_array = round((newSample / fr_min) * sin_points);
cp_array = cp_array';

for y = 1:size(cp_array,2)
    if cp_array(1,y) > cp_array_max_points
        cp_array(1,y) = cp_array_max_points;
    else
        if cp_array(1,y) < cp_array_min_points
            cp_array(1,y) = cp_array_min_points;
        end
    end
end


end