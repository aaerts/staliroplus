% =========================================================================
% Title         : staliro_plus_opt_V02.m
% Version       : V0.2
% Author        : Arend Aerts
% Project       : Model-based design and verification of mechatronic sys. 
% Date          : 26-04-2016
% Description   : S-TaLiRo+ frequency optimization
%
%                                ---
% 
% V0.1          : Initial run functonality + structure
%
% V0.2          : Data output selection (save I/O data)
%
% =========================================================================
function [results, his, cp_array_his, fr_his, fals, best_rob2, best_rob2_index] = staliro_plus_opt_V02(model,phi,preds,opt)


% Declare global variables
global staliro_opt;
staliro_opt = opt;

% INIT
% ====
results = struct('stal_run',struct('bestRob',[],'bestSample',[],'nTests',[],'bestCost',[],'paramVal',[],'falsified',[],'time',[]),'fr_select',[],'CP_select',[],'optRob',[],'optRobIndex',[],'falsified',[]);
his = struct('stal_run',struct('rob',[],'samples',[],'cost',[],'YT',[],'UT',[],'T1',[]));
cp_array_his = [];
fr_his = [];

% Proposal scheme (Uniform random)
% ================================
if strcmp(opt.stalplus,'on') && size(opt.fr_inputs,2) > 0
    [curSample,opt.cp_array(opt.fr_inputs)] = getNewSample2(opt.fr_range);
    cp_array_his = [cp_array_his; opt.cp_array];
    fr_his = [fr_his; curSample'];
    results(1).fr_select = round(curSample',2);
    results(1).CP_select = opt.cp_array;
else
    curSample = -1;
    cp_array_his = [cp_array_his; opt.cp_array];
    results(1).CP_select = opt.cp_array;
end


% Progress report
% ===============
disp(' ');
disp(['> S-TaLiRo+ iteration ' num2str(1) ' / ' num2str(opt.fr_n_tests)]);
tmp = 1;
for w = 1:size(opt.interpolationtype,2)
    if strcmp(opt.interpolationtype(1,w),'pchip')
        if tmp <= size(opt.fr_inputs,2)
            disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(curSample(1,tmp)) ' Hz']); 
        else
            disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(-1) ' Hz']); 
        end
        tmp = tmp + 1;        
    else
        disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w))]); 
    end   
end



% Initial cost function (initial S-TaLiRo run)
% ============================================
[results_tmp, his_tmp] = staliro(model,opt.init_cond,opt.input_range,opt.cp_array,phi,preds,opt.time,opt);
best_rob = results_tmp.run(results_tmp.optRobIndex).bestRob;
results(1).stal_run = results_tmp.run;
results(1).optRob = best_rob;
results(1).optRobIndex = results_tmp.optRobIndex;
if opt.DataExport == 2
    his(1).stal_run = his_tmp;
else
    his_tmp = rmfield(his_tmp,'YT');
    his_tmp = rmfield(his_tmp,'UT');
    his_tmp = rmfield(his_tmp,'T1');
    his(1).stal_run = his_tmp;
end
best_rob2 = best_rob;
best_rob2_index = 1;
results(1).falsified_amount = 0;
if best_rob < 0
    results(1).falsified = 1;
    for k= 1:opt.runs
        if results(1).stal_run(k).falsified == 1
            results(1).falsified_amount = results(1).falsified_amount + 1;
        end
    end
    
    fals = 1;
else
    results(1).falsified = 0;
    results(1).falsified_amount = 0;
    fals = 0;
end
disp(['> Best phi-value: ' num2str(best_rob)]);


% Frequency optimization loop
% ===========================
if best_rob2 > 0
    if strcmp(opt.stalplus,'on') && strcmp(opt.fr_optimization_solver,'SA_Taliro') && opt.fr_n_tests >= 2 && size(opt.fr_inputs,2) > 0
        dispAdap = 1+opt.fr_dispAdap/100;
        betaXAdap = 1+opt.fr_betaXAdap/100;
        betaX = opt.fr_betaXStart;
        displace = opt.fr_dispStart;
        nTrials = 1;
        nAccepts = 0;
        disp(['> Displace: ' num2str(displace)]);
        disp(['> Beta: ' num2str(betaX)]);
        for k = 2:opt.fr_n_tests
            % Init results
            % ============
            results(k).falsified = 0;  
            
            % Update nTrials
            % ==============
            nTrials = nTrials + 1;     

            
            
            % Proposal scheme (simulated annealing)
            % =====================================
            [curSample1,opt.cp_array(opt.fr_inputs)] = getNewSample2(curSample,opt.fr_range,displace);
            cp_array_his = [cp_array_his; opt.cp_array];
            fr_his = [fr_his; curSample1'];
            results(k).fr_select = curSample1';
            results(k).CP_select = opt.cp_array;

            disp(' ');
            disp(['> S-TaLiRo+ iteration ' num2str(k) ' / ' num2str(opt.fr_n_tests)]);
            tmp = 1;
            for w = 1:size(opt.interpolationtype,2)
                if strcmp(opt.interpolationtype(1,w),'pchip')
                    if tmp <= size(opt.fr_inputs,2)
                        disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(curSample1(1,tmp)) ' Hz']); 
                    else
                        disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(-1) ' Hz']); 
                    end
                    tmp = tmp + 1;        
                else
                    disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w))]); 
                end   
            end



            % Cost function (S-TaLiRo run)
            % ============================
            [results_tmp, his_tmp] = staliro(model,opt.init_cond,opt.input_range,opt.cp_array,phi,preds,opt.time,opt);
            results(k).stal_run = results_tmp.run;
            if opt.DataExport == 2
                his(k).stal_run = his_tmp;
            else
                his_tmp = rmfield(his_tmp,'YT');
                his_tmp = rmfield(his_tmp,'UT');
                his_tmp = rmfield(his_tmp,'T1');
                his(k).stal_run = his_tmp;
            end
            best_rob1 = results_tmp.run(results_tmp.optRobIndex).bestRob;
            results(k).optRob = best_rob1;
            results(k).optRobIndex = results_tmp.optRobIndex;
            disp(['> Best phi-value: ' num2str(best_rob1)]);

            % Probobalistic acceptance (Usual Metropolis-Hastings criterion)
            % ==============================================================
            if ( mcAccept(best_rob1,best_rob,betaX) == 1 )
                if best_rob1 < best_rob
                    best_rob2 = best_rob1;
                    best_rob2_index = k;
                end
                nAccepts = nAccepts+1;
                curSample = curSample1;
                best_rob = best_rob1;
            end


            % Update acceptance criteria
            % ==========================
            if (mod(nTrials,opt.fr_UpdateInterval) == 0)                   
                acRatio=nAccepts/nTrials;
                disp(['> acratio: ' num2str(acRatio)]);
                if (acRatio > opt.fr_acRatioMax)
                    nTrials = 0;
                    nAccepts = 0;
                    % Increase beta - Reduce displacement
                    displace = displace/dispAdap; 
                    betaX = betaX*betaXAdap;
                elseif (acRatio < opt.fr_acRatioMin)
                    % reduce beta - Increase displacement
                    nTrials = 0;
                    nAccepts = 0;
                    displace = displace*dispAdap; 
                    betaX = betaX/betaXAdap;
                end
                if (displace >= opt.fr_maxDisp)
                    displace = opt.fr_maxDisp;
                end
                if (displace <= opt.fr_minDisp)
                    displace = opt.fr_minDisp;
                end
            end
            disp(['> Best_rob: ' num2str(best_rob)]);
            disp(['> Best_CP: ' num2str(curSample)]);
            disp(['> Displace: ' num2str(displace)]);
            disp(['> Beta: ' num2str(betaX)]);
            

            % Falsification check
            % ===================
            if best_rob < 0
                results(k).falsified = 1;
                results(k).falsified_amount = 0;
                for w= 1:opt.runs
                    if results(k).stal_run(w).falsified == 1
                        results(k).falsified_amount = results(k).falsified_amount + 1;
                    end
                end
                fals = 1;
                break;
            end
        end
    else
        if strcmp(opt.stalplus,'on')&& strcmp(opt.fr_optimization_solver,'UR_Taliro') && opt.fr_n_tests > 0 && size(opt.fr_inputs,2) > 0  
            for k = 2:opt.fr_n_tests
                % Generate input
                [curSample,opt.cp_array(opt.fr_inputs)] = getNewSample2(opt.fr_range);
                cp_array_his = [cp_array_his; opt.cp_array];
                fr_his = [fr_his; curSample'];
                results(k).fr_select = round(curSample',2);
                results(k).CP_select = opt.cp_array;

                % Print
                disp(' ');
                disp(['> S-TaLiRo+ iteration ' num2str(k) ' / ' num2str(opt.fr_n_tests)]);
                tmp = 1;
                for w = 1:size(opt.interpolationtype,2)
                    if strcmp(opt.interpolationtype(1,w),'pchip')
                        if tmp <= size(opt.fr_inputs,2)
                            disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(curSample(1,tmp)) ' Hz']); 
                        else
                            disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w)) ', f = ' num2str(-1) ' Hz']); 
                        end
                        tmp = tmp + 1;        
                    else
                        disp(['> Input ' num2str(w) ': ' opt.interpolationtype{1,w} ', CP = ' num2str(opt.cp_array(1,w))]); 
                    end   
                end

                % Run S-TaLiRo
                [results_tmp, his_tmp] = staliro(model,opt.init_cond,opt.input_range,opt.cp_array,phi,preds,opt.time,opt);
                best_rob = results_tmp.run(results_tmp.optRobIndex).bestRob;
                results(k).stal_run = results_tmp.run;
                results(k).optRob = best_rob;
                results(k).optRobIndex = results_tmp.optRobIndex;
                if opt.DataExport == 2
                    his(k).stal_run = his_tmp;
                else
                    his_tmp = rmfield(his_tmp,'YT');
                    his_tmp = rmfield(his_tmp,'UT');
                    his_tmp = rmfield(his_tmp,'T1');
                    his(k).stal_run = his_tmp;
                end
                results(k).falsified_amount = 0;
                disp(['> Best phi-value: ' num2str(best_rob)]);

                % Falsification check
                % ===================
                if best_rob < 0
                    results(k).falsified = 1;
                    results(k).falsified_amount = 0;
                    best_rob2 = best_rob;
                    best_rob2_index = k;
                    for w= 1:opt.runs
                        if results(k).stal_run(w).falsified == 1
                            results(k).falsified_amount = results(k).falsified_amount + 1;
                        end
                    end
                    fals = 1;
                    break;
                end
            end
        end
    end
end



  %% Auxiliary functions
    function rBool = mcAccept(newVal,curVal,betaX)
        if newVal < curVal
            rBool = 1;
        else
            rat = (newVal-curVal); %% rat >= 0 %% beta < 0
            rBool=0;
            if (exp(betaX*rat) >= rand(1))
                rBool=1;
            end
        end
    end


end



