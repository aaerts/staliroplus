/* Include files */

#include <stddef.h>
#include "blas.h"
#include "Boost_converter_V10_sfun.h"
#include "c1_Boost_converter_V10.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Boost_converter_V10_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c1_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c1_IN_Mode1                    ((uint8_T)1U)
#define c1_IN_Mode2                    ((uint8_T)2U)
#define c1_IN_Mode3                    ((uint8_T)3U)
#define c1_IN_Mode4                    ((uint8_T)4U)
#define c1_IN_Mode5                    ((uint8_T)5U)
#define c1_IN_Mode6                    ((uint8_T)6U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_h_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_i_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_j_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_k_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_l_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_m_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_n_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_o_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_p_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_q_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_r_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_s_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_t_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_u_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_v_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_w_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_x_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_y_debug_family_names[2] = { "nargin", "nargout" };

static const char * c1_ab_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_bb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_cb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_db_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_eb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_fb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_gb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_hb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_ib_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_jb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_kb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_lb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_mb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c1_nb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void initialize_params_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void enable_c1_Boost_converter_V10(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance);
static void disable_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static const mxArray *get_sim_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void set_sim_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray *c1_st);
static void c1_set_sim_state_side_effects_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void finalize_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void sf_gateway_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void mdl_start_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void zeroCrossings_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void derivatives_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void outputs_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void initSimStructsc1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void c1_eml_ini_fcn_to_be_inlined_321
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void c1_eml_term_fcn_to_be_inlined_321
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const real_T c1_b_u);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static real_T c1_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_nargout, const char_T *c1_identifier);
static real_T c1_b_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const boolean_T c1_b_u);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static boolean_T c1_c_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_sf_internal_predicateOutput, const char_T
  *c1_identifier);
static boolean_T c1_d_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const int32_T c1_b_u);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int32_T c1_e_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier);
static int32_T c1_f_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const uint8_T c1_b_u);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint8_T c1_g_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Mode1, const char_T *c1_identifier);
static uint8_T c1_h_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_e_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance);
static const mxArray *c1_f_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const boolean_T
   c1_b_u[7]);
static void c1_i_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u);
static void c1_j_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_dataWrittenToVector, const char_T
  *c1_identifier, boolean_T c1_y[7]);
static void c1_k_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  boolean_T c1_y[7]);
static const mxArray *c1_l_emlrt_marshallIn
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier);
static const mxArray *c1_m_emlrt_marshallIn
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray *c1_b_u,
   const emlrtMsgIdentifier *c1_parentId);
static void init_dsm_address_info(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_doSetSimStateSideEffects = 0U;
  chartInstance->c1_setSimStateSideEffectsInfo = NULL;
  chartInstance->c1_tp_Mode1 = 0U;
  chartInstance->c1_tp_Mode2 = 0U;
  chartInstance->c1_tp_Mode3 = 0U;
  chartInstance->c1_tp_Mode4 = 0U;
  chartInstance->c1_tp_Mode5 = 0U;
  chartInstance->c1_tp_Mode6 = 0U;
  chartInstance->c1_is_active_c1_Boost_converter_V10 = 0U;
  chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_Boost_converter_V10(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  uint32_T c1_prevAniVal;
  c1_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c1_is_active_c1_Boost_converter_V10 == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode4) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode5) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode6) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  }

  _SFD_SET_ANIMATION(c1_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static const mxArray *get_sim_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  const mxArray *c1_st = NULL;
  c1_st = NULL;
  sf_mex_assign(&c1_st, c1_e_emlrt_marshallOut(chartInstance), false);
  return c1_st;
}

static void set_sim_state_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray *c1_st)
{
  c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_st));
  chartInstance->c1_doSetSimStateSideEffects = 1U;
  c1_update_debugger_state_c1_Boost_converter_V10(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void c1_set_sim_state_side_effects_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  if (chartInstance->c1_doSetSimStateSideEffects != 0) {
    chartInstance->c1_tp_Mode1 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode1);
    chartInstance->c1_tp_Mode2 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode2);
    chartInstance->c1_tp_Mode3 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode3);
    chartInstance->c1_tp_Mode4 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode4);
    chartInstance->c1_tp_Mode5 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode5);
    chartInstance->c1_tp_Mode6 = (uint8_T)
      (chartInstance->c1_is_c1_Boost_converter_V10 == c1_IN_Mode6);
    chartInstance->c1_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c1_setSimStateSideEffectsInfo);
}

static void sf_gateway_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  uint32_T c1_b_debug_family_var_map[3];
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 1.0;
  boolean_T c1_out;
  real_T c1_c_nargin = 0.0;
  real_T c1_c_nargout = 1.0;
  boolean_T c1_b_out;
  real_T c1_d_nargin = 0.0;
  real_T c1_d_nargout = 1.0;
  boolean_T c1_c_out;
  real_T c1_e_nargin = 0.0;
  real_T c1_e_nargout = 0.0;
  real_T c1_f_nargin = 0.0;
  real_T c1_f_nargout = 1.0;
  boolean_T c1_d_out;
  real_T c1_g_nargin = 0.0;
  real_T c1_g_nargout = 1.0;
  boolean_T c1_e_out;
  real_T c1_h_nargin = 0.0;
  real_T c1_h_nargout = 0.0;
  real_T c1_i_nargin = 0.0;
  real_T c1_i_nargout = 1.0;
  boolean_T c1_f_out;
  real_T c1_j_nargin = 0.0;
  real_T c1_j_nargout = 1.0;
  boolean_T c1_g_out;
  real_T c1_k_nargin = 0.0;
  real_T c1_k_nargout = 1.0;
  boolean_T c1_h_out;
  real_T c1_l_nargin = 0.0;
  real_T c1_l_nargout = 1.0;
  boolean_T c1_i_out;
  real_T c1_m_nargin = 0.0;
  real_T c1_m_nargout = 1.0;
  boolean_T c1_j_out;
  real_T c1_n_nargin = 0.0;
  real_T c1_n_nargout = 0.0;
  real_T c1_o_nargin = 0.0;
  real_T c1_o_nargout = 1.0;
  boolean_T c1_k_out;
  real_T c1_p_nargin = 0.0;
  real_T c1_p_nargout = 1.0;
  boolean_T c1_l_out;
  real_T c1_q_nargin = 0.0;
  real_T c1_q_nargout = 0.0;
  real_T c1_r_nargin = 0.0;
  real_T c1_r_nargout = 1.0;
  boolean_T c1_m_out;
  real_T c1_s_nargin = 0.0;
  real_T c1_s_nargout = 1.0;
  boolean_T c1_n_out;
  real_T c1_t_nargin = 0.0;
  real_T c1_t_nargout = 0.0;
  real_T c1_u_nargin = 0.0;
  real_T c1_u_nargout = 0.0;
  real_T c1_v_nargin = 0.0;
  real_T c1_v_nargout = 0.0;
  real_T c1_w_nargin = 0.0;
  real_T c1_w_nargout = 0.0;
  real_T c1_x_nargin = 0.0;
  real_T c1_x_nargout = 0.0;
  real_T c1_y_nargin = 0.0;
  real_T c1_y_nargout = 0.0;
  c1_set_sim_state_side_effects_c1_Boost_converter_V10(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (ssIsMajorTimeStep(chartInstance->S) != 0) {
    chartInstance->c1_lastMajorTime = _sfTime_;
    chartInstance->c1_stateChanged = (boolean_T)0;
    _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_off, 2U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u, 3U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u_min, 5U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u_max, 4U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_Kp, 1U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_Eref, 0U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_Eout, 7U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_in, 6U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
    chartInstance->c1_sfEvent = CALL_EVENT;
    _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    if (chartInstance->c1_is_active_c1_Boost_converter_V10 == 0U) {
      _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
      chartInstance->c1_stateChanged = true;
      chartInstance->c1_is_active_c1_Boost_converter_V10 = 1U;
      _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_g_debug_family_names,
        c1_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
        c1_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
        c1_sf_marshallIn);
      *chartInstance->c1_S_out = 0.0;
      chartInstance->c1_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_Eref = 240.0;
      chartInstance->c1_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_Eref, 0U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_Kp = 10.0;
      chartInstance->c1_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_Kp, 1U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_u_max = 0.8;
      chartInstance->c1_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u_max, 4U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_u_min = 0.0;
      chartInstance->c1_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u_min, 5U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      *chartInstance->c1_off = -0.5;
      chartInstance->c1_dataWrittenToVector[6U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c1_off, 2U, 5U, 0U,
                            chartInstance->c1_sfEvent, false);
      _SFD_SYMBOL_SCOPE_POP();
      chartInstance->c1_stateChanged = true;
      chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode2;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_Mode2 = 1U;
    } else {
      switch (chartInstance->c1_is_c1_Boost_converter_V10) {
       case c1_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_h_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        c1_out = CV_EML_IF(5, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                           *chartInstance->c1_u_min);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode1 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode2 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_s_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 7U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 7U,
              chartInstance->c1_sfEvent, false);
          }

          c1_b_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
            (*chartInstance->c1_u_min + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 4U, (*chartInstance->c1_u_min +
            *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_b_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode1 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode4;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode4 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
          }
        }
        break;

       case c1_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_j_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        c1_c_out = CV_EML_IF(1, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                             *chartInstance->c1_u_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_k_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 1U,
              chartInstance->c1_sfEvent, false);
          }

          *chartInstance->c1_u = *chartInstance->c1_u_max;
          chartInstance->c1_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u, 3U, 5U, 1U,
                                chartInstance->c1_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c1_tp_Mode2 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode3 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 14U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_q_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          c1_d_out = CV_EML_IF(14, 0, 0, CV_RELATIONAL_EVAL(5U, 14U, 0,
            ((*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
             *chartInstance->c1_Kp + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 4U, ((*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp +
            *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_d_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode2 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode5;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode5 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                         chartInstance->c1_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_l_debug_family_names,
              c1_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargin, 0U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargout, 1U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_out, 2U,
              c1_b_sf_marshallOut, c1_b_sf_marshallIn);
            if (!chartInstance->c1_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[2U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[4U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            c1_e_out = CV_EML_IF(4, 0, 0, (*chartInstance->c1_Eref -
              *chartInstance->c1_Eout) * *chartInstance->c1_Kp <
                                 *chartInstance->c1_u_min);
            _SFD_SYMBOL_SCOPE_POP();
            if (c1_e_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_m_debug_family_names,
                c1_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_nargin, 0U,
                c1_sf_marshallOut, c1_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_nargout, 1U,
                c1_sf_marshallOut, c1_sf_marshallIn);
              if (!chartInstance->c1_dataWrittenToVector[4U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 4U,
                  chartInstance->c1_sfEvent, false);
              }

              *chartInstance->c1_u = *chartInstance->c1_u_min;
              chartInstance->c1_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u, 3U, 5U, 4U,
                                    chartInstance->c1_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c1_tp_Mode2 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
              chartInstance->c1_stateChanged = true;
              chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode1;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_Mode1 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U,
                           chartInstance->c1_sfEvent);
            }
          }
        }
        break;

       case c1_IN_Mode3:
        CV_CHART_EVAL(0, 0, 3);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_n_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        c1_f_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          (*chartInstance->c1_u_max + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 4U, (*chartInstance->c1_u_max +
          *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_f_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode3 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode6;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode6 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_i_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          c1_g_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp, *chartInstance->c1_u_max, -1, 2U,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp < *chartInstance->c1_u_max));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_g_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode3 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode2;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode2 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
          }
        }
        break;

       case c1_IN_Mode4:
        CV_CHART_EVAL(0, 0, 4);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 8U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_r_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 8U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 8U,
            chartInstance->c1_sfEvent, false);
        }

        c1_h_out = CV_EML_IF(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
          (*chartInstance->c1_u_min + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 3U, (*chartInstance->c1_u_min +
          *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_h_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode4 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode1 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_u_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          c1_i_out = CV_EML_IF(9, 0, 0, (*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                               *chartInstance->c1_u_min);
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_i_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode4 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode5;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode5 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
          }
        }
        break;

       case c1_IN_Mode5:
        CV_CHART_EVAL(0, 0, 5);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 10U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_x_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        c1_j_out = CV_EML_IF(10, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp <
                             *chartInstance->c1_u_min);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_j_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_y_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 10U,
              chartInstance->c1_sfEvent, false);
          }

          *chartInstance->c1_u = *chartInstance->c1_u_min;
          chartInstance->c1_dataWrittenToVector[5U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u, 3U, 5U, 10U,
                                chartInstance->c1_sfEvent, false);
          _SFD_SYMBOL_SCOPE_POP();
          chartInstance->c1_tp_Mode5 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode4;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode4 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 13U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_p_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_o_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_o_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          c1_k_out = CV_EML_IF(13, 0, 0, CV_RELATIONAL_EVAL(5U, 13U, 0,
            ((*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
             *chartInstance->c1_Kp + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 3U, ((*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp +
            *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_k_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode5 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode2;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode2 = 1U;
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 12U,
                         chartInstance->c1_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_v_debug_family_names,
              c1_b_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_p_nargin, 0U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_p_nargout, 1U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_out, 2U,
              c1_b_sf_marshallOut, c1_b_sf_marshallIn);
            if (!chartInstance->c1_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[2U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            c1_l_out = CV_EML_IF(12, 0, 0, (*chartInstance->c1_Eref -
              *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                                 *chartInstance->c1_u_max);
            _SFD_SYMBOL_SCOPE_POP();
            if (c1_l_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_w_debug_family_names,
                c1_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_q_nargin, 0U,
                c1_sf_marshallOut, c1_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_q_nargout, 1U,
                c1_sf_marshallOut, c1_sf_marshallIn);
              if (!chartInstance->c1_dataWrittenToVector[3U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 12U,
                  chartInstance->c1_sfEvent, false);
              }

              *chartInstance->c1_u = *chartInstance->c1_u_max;
              chartInstance->c1_dataWrittenToVector[5U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c1_u, 3U, 5U, 12U,
                                    chartInstance->c1_sfEvent, false);
              _SFD_SYMBOL_SCOPE_POP();
              chartInstance->c1_tp_Mode5 = 0U;
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
              chartInstance->c1_stateChanged = true;
              chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode6;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
              chartInstance->c1_tp_Mode6 = 1U;
            } else {
              _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U,
                           chartInstance->c1_sfEvent);
            }
          }
        }
        break;

       case c1_IN_Mode6:
        CV_CHART_EVAL(0, 0, 6);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                     chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U,
                     chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_o_debug_family_names,
          c1_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_r_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_r_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 3U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 3U,
            chartInstance->c1_sfEvent, false);
        }

        c1_m_out = CV_EML_IF(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
          (*chartInstance->c1_u_max + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 3U, (*chartInstance->c1_u_max +
          *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_m_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode6 = 0U;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
          chartInstance->c1_stateChanged = true;
          chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode3;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Mode3 = 1U;
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                       chartInstance->c1_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_t_debug_family_names,
            c1_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_s_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_s_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          c1_n_out = CV_EML_IF(11, 0, 0, CV_RELATIONAL_EVAL(5U, 11U, 0,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp, *chartInstance->c1_u_max, -1, 2U,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp < *chartInstance->c1_u_max));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_n_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode6 = 0U;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
            chartInstance->c1_stateChanged = true;
            chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_Mode5;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
            chartInstance->c1_tp_Mode5 = 1U;
          } else {
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
          }
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
        break;
      }
    }

    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    if (chartInstance->c1_stateChanged) {
      ssSetSolverNeedsReset(chartInstance->S);
    }
  }

  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Boost_converter_V10) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_t_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_t_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_u_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_u_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_c_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_v_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_v_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 2U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_d_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_w_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_w_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 3U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode5:
    CV_CHART_EVAL(0, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_e_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_x_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_x_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 4U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode6:
    CV_CHART_EVAL(0, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_f_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_y_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_y_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 5U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }

  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_Boost_converter_V10MachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void zeroCrossings_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[3];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 1.0;
  boolean_T c1_out;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 1.0;
  boolean_T c1_b_out;
  real_T c1_c_nargin = 0.0;
  real_T c1_c_nargout = 1.0;
  boolean_T c1_c_out;
  real_T c1_d_nargin = 0.0;
  real_T c1_d_nargout = 1.0;
  boolean_T c1_d_out;
  real_T c1_e_nargin = 0.0;
  real_T c1_e_nargout = 1.0;
  boolean_T c1_e_out;
  real_T c1_f_nargin = 0.0;
  real_T c1_f_nargout = 1.0;
  boolean_T c1_f_out;
  real_T c1_g_nargin = 0.0;
  real_T c1_g_nargout = 1.0;
  boolean_T c1_g_out;
  real_T c1_h_nargin = 0.0;
  real_T c1_h_nargout = 1.0;
  boolean_T c1_h_out;
  real_T c1_i_nargin = 0.0;
  real_T c1_i_nargout = 1.0;
  boolean_T c1_i_out;
  real_T c1_j_nargin = 0.0;
  real_T c1_j_nargout = 1.0;
  boolean_T c1_j_out;
  real_T c1_k_nargin = 0.0;
  real_T c1_k_nargout = 1.0;
  boolean_T c1_k_out;
  real_T c1_l_nargin = 0.0;
  real_T c1_l_nargout = 1.0;
  boolean_T c1_l_out;
  real_T c1_m_nargin = 0.0;
  real_T c1_m_nargout = 1.0;
  boolean_T c1_m_out;
  real_T c1_n_nargin = 0.0;
  real_T c1_n_nargout = 1.0;
  boolean_T c1_n_out;
  real_T *c1_zcVar;
  c1_zcVar = (real_T *)(ssGetNonsampledZCs_wrapper(chartInstance->S) + 0);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (chartInstance->c1_lastMajorTime == _sfTime_) {
    *c1_zcVar = -1.0;
  } else {
    chartInstance->c1_stateChanged = (boolean_T)0;
    if (chartInstance->c1_is_active_c1_Boost_converter_V10 == 0U) {
      chartInstance->c1_stateChanged = true;
    } else {
      switch (chartInstance->c1_is_c1_Boost_converter_V10) {
       case c1_IN_Mode1:
        CV_CHART_EVAL(0, 0, 1);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_h_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 5U,
            chartInstance->c1_sfEvent, false);
        }

        c1_out = CV_EML_IF(5, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                           *chartInstance->c1_u_min);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_s_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 7U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 7U,
              chartInstance->c1_sfEvent, false);
          }

          c1_b_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0,
            (*chartInstance->c1_u_min + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 4U, (*chartInstance->c1_u_min +
            *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_b_out) {
            chartInstance->c1_stateChanged = true;
          }
        }
        break;

       case c1_IN_Mode2:
        CV_CHART_EVAL(0, 0, 2);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_j_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 1U,
            chartInstance->c1_sfEvent, false);
        }

        c1_c_out = CV_EML_IF(1, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                             *chartInstance->c1_u_max);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_c_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_q_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 14U,
              chartInstance->c1_sfEvent, false);
          }

          c1_d_out = CV_EML_IF(14, 0, 0, CV_RELATIONAL_EVAL(5U, 14U, 0,
            ((*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
             *chartInstance->c1_Kp + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 4U, ((*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp +
            *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_d_out) {
            chartInstance->c1_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_l_debug_family_names,
              c1_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargin, 0U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargout, 1U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_out, 2U,
              c1_b_sf_marshallOut, c1_b_sf_marshallIn);
            if (!chartInstance->c1_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[2U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[4U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 4U,
                chartInstance->c1_sfEvent, false);
            }

            c1_e_out = CV_EML_IF(4, 0, 0, (*chartInstance->c1_Eref -
              *chartInstance->c1_Eout) * *chartInstance->c1_Kp <
                                 *chartInstance->c1_u_min);
            _SFD_SYMBOL_SCOPE_POP();
            if (c1_e_out) {
              chartInstance->c1_stateChanged = true;
            }
          }
        }
        break;

       case c1_IN_Mode3:
        CV_CHART_EVAL(0, 0, 3);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_n_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 2U,
            chartInstance->c1_sfEvent, false);
        }

        c1_f_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          (*chartInstance->c1_u_max + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 4U, (*chartInstance->c1_u_max +
          *chartInstance->c1_off) + *chartInstance->c1_S_in > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_f_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_i_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 6U,
              chartInstance->c1_sfEvent, false);
          }

          c1_g_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp, *chartInstance->c1_u_max, -1, 2U,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp < *chartInstance->c1_u_max));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_g_out) {
            chartInstance->c1_stateChanged = true;
          }
        }
        break;

       case c1_IN_Mode4:
        CV_CHART_EVAL(0, 0, 4);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_r_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_h_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 8U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 8U,
            chartInstance->c1_sfEvent, false);
        }

        c1_h_out = CV_EML_IF(8, 0, 0, CV_RELATIONAL_EVAL(5U, 8U, 0,
          (*chartInstance->c1_u_min + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 3U, (*chartInstance->c1_u_min +
          *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_h_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_u_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_i_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 9U,
              chartInstance->c1_sfEvent, false);
          }

          c1_i_out = CV_EML_IF(9, 0, 0, (*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                               *chartInstance->c1_u_min);
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_i_out) {
            chartInstance->c1_stateChanged = true;
          }
        }
        break;

       case c1_IN_Mode5:
        CV_CHART_EVAL(0, 0, 5);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_x_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_j_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(5U, 5U, 10U,
            chartInstance->c1_sfEvent, false);
        }

        c1_j_out = CV_EML_IF(10, 0, 0, (*chartInstance->c1_Eref -
          *chartInstance->c1_Eout) * *chartInstance->c1_Kp <
                             *chartInstance->c1_u_min);
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_j_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_p_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 13U,
              chartInstance->c1_sfEvent, false);
          }

          c1_k_out = CV_EML_IF(13, 0, 0, CV_RELATIONAL_EVAL(5U, 13U, 0,
            ((*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
             *chartInstance->c1_Kp + *chartInstance->c1_off) +
            *chartInstance->c1_S_in, 0.0, -1, 3U, ((*chartInstance->c1_Eref -
            *chartInstance->c1_Eout) * *chartInstance->c1_Kp +
            *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_k_out) {
            chartInstance->c1_stateChanged = true;
          } else {
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_v_debug_family_names,
              c1_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_nargin, 0U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_nargout, 1U,
              c1_sf_marshallOut, c1_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l_out, 2U,
              c1_b_sf_marshallOut, c1_b_sf_marshallIn);
            if (!chartInstance->c1_dataWrittenToVector[1U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[2U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            if (!chartInstance->c1_dataWrittenToVector[3U]) {
              _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 12U,
                chartInstance->c1_sfEvent, false);
            }

            c1_l_out = CV_EML_IF(12, 0, 0, (*chartInstance->c1_Eref -
              *chartInstance->c1_Eout) * *chartInstance->c1_Kp >
                                 *chartInstance->c1_u_max);
            _SFD_SYMBOL_SCOPE_POP();
            if (c1_l_out) {
              chartInstance->c1_stateChanged = true;
            }
          }
        }
        break;

       case c1_IN_Mode6:
        CV_CHART_EVAL(0, 0, 6);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_o_debug_family_names,
          c1_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_nargin, 0U, c1_sf_marshallOut,
          c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_nargout, 1U,
          c1_sf_marshallOut, c1_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_m_out, 2U, c1_b_sf_marshallOut,
          c1_b_sf_marshallIn);
        if (!chartInstance->c1_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 3U,
            chartInstance->c1_sfEvent, false);
        }

        if (!chartInstance->c1_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 3U,
            chartInstance->c1_sfEvent, false);
        }

        c1_m_out = CV_EML_IF(3, 0, 0, CV_RELATIONAL_EVAL(5U, 3U, 0,
          (*chartInstance->c1_u_max + *chartInstance->c1_off) +
          *chartInstance->c1_S_in, 0.0, -1, 3U, (*chartInstance->c1_u_max +
          *chartInstance->c1_off) + *chartInstance->c1_S_in <= 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c1_m_out) {
          chartInstance->c1_stateChanged = true;
        } else {
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c1_t_debug_family_names,
            c1_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_nargin, 0U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_nargout, 1U,
            c1_sf_marshallOut, c1_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_n_out, 2U,
            c1_b_sf_marshallOut, c1_b_sf_marshallIn);
          if (!chartInstance->c1_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(0U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          if (!chartInstance->c1_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(4U, 5U, 11U,
              chartInstance->c1_sfEvent, false);
          }

          c1_n_out = CV_EML_IF(11, 0, 0, CV_RELATIONAL_EVAL(5U, 11U, 0,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp, *chartInstance->c1_u_max, -1, 2U,
            (*chartInstance->c1_Eref - *chartInstance->c1_Eout) *
            *chartInstance->c1_Kp < *chartInstance->c1_u_max));
          _SFD_SYMBOL_SCOPE_POP();
          if (c1_n_out) {
            chartInstance->c1_stateChanged = true;
          }
        }
        break;

       default:
        CV_CHART_EVAL(0, 0, 0);

        /* Unreachable state, for coverage only */
        chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
        break;
      }
    }

    if (chartInstance->c1_stateChanged) {
      *c1_zcVar = 1.0;
    } else {
      *c1_zcVar = -1.0;
    }
  }
}

static void derivatives_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 0.0;
  real_T c1_c_nargin = 0.0;
  real_T c1_c_nargout = 0.0;
  real_T c1_d_nargin = 0.0;
  real_T c1_d_nargout = 0.0;
  real_T c1_e_nargin = 0.0;
  real_T c1_e_nargout = 0.0;
  real_T c1_f_nargin = 0.0;
  real_T c1_f_nargout = 0.0;
  real_T *c1_Eref_dot;
  real_T *c1_Kp_dot;
  real_T *c1_u_max_dot;
  real_T *c1_u_min_dot;
  real_T *c1_u_dot;
  real_T *c1_off_dot;
  c1_off_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 5);
  c1_u_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 4);
  c1_u_min_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 3);
  c1_u_max_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 2);
  c1_Kp_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 1);
  c1_Eref_dot = (real_T *)(ssGetdX_wrapper(chartInstance->S) + 0);
  *c1_Eref_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_Eref_dot, 0U, 1U, 0U, chartInstance->c1_sfEvent,
                        false);
  *c1_Kp_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_Kp_dot, 1U, 1U, 0U, chartInstance->c1_sfEvent, false);
  *c1_u_max_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_u_max_dot, 4U, 1U, 0U, chartInstance->c1_sfEvent,
                        false);
  *c1_u_min_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_u_min_dot, 5U, 1U, 0U, chartInstance->c1_sfEvent,
                        false);
  *c1_u_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_u_dot, 3U, 1U, 0U, chartInstance->c1_sfEvent, false);
  *c1_off_dot = 0.0;
  _SFD_DATA_RANGE_CHECK(*c1_off_dot, 2U, 1U, 0U, chartInstance->c1_sfEvent,
                        false);
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Boost_converter_V10) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_c_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 2U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_d_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 3U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode5:
    CV_CHART_EVAL(0, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_e_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 4U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode6:
    CV_CHART_EVAL(0, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_f_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 5U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }
}

static void outputs_c1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  uint32_T c1_debug_family_var_map[2];
  real_T c1_nargin = 0.0;
  real_T c1_nargout = 0.0;
  real_T c1_b_nargin = 0.0;
  real_T c1_b_nargout = 0.0;
  real_T c1_c_nargin = 0.0;
  real_T c1_c_nargout = 0.0;
  real_T c1_d_nargin = 0.0;
  real_T c1_d_nargout = 0.0;
  real_T c1_e_nargin = 0.0;
  real_T c1_e_nargout = 0.0;
  real_T c1_f_nargin = 0.0;
  real_T c1_f_nargout = 0.0;
  _sfTime_ = sf_get_time(chartInstance->S);
  switch (chartInstance->c1_is_c1_Boost_converter_V10) {
   case c1_IN_Mode1:
    CV_CHART_EVAL(0, 0, 1);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 0U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode2:
    CV_CHART_EVAL(0, 0, 2);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_b_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 1U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode3:
    CV_CHART_EVAL(0, 0, 3);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_c_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_c_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 0.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 2U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode4:
    CV_CHART_EVAL(0, 0, 4);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_d_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_d_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 3U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode5:
    CV_CHART_EVAL(0, 0, 5);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_e_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_e_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 4U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
    break;

   case c1_IN_Mode6:
    CV_CHART_EVAL(0, 0, 6);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c1_f_debug_family_names,
      c1_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargin, 0U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_f_nargout, 1U, c1_sf_marshallOut,
      c1_sf_marshallIn);
    *chartInstance->c1_S_out = 1.0;
    chartInstance->c1_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c1_S_out, 8U, 4U, 5U,
                          chartInstance->c1_sfEvent, false);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    break;

   default:
    CV_CHART_EVAL(0, 0, 0);

    /* Unreachable state, for coverage only */
    chartInstance->c1_is_c1_Boost_converter_V10 = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    break;
  }
}

static void initSimStructsc1_Boost_converter_V10
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_eml_ini_fcn_to_be_inlined_321
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_eml_term_fcn_to_be_inlined_321
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)c1_machineNumber;
  (void)c1_chartNumber;
  (void)c1_instanceNumber;
}

static const mxArray *c1_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const real_T c1_b_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_emlrt_marshallOut(chartInstance, *(real_T
    *)c1_inData), false);
  return c1_mxArrayOutData;
}

static real_T c1_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_nargout, const char_T *c1_identifier)
{
  real_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_nargout), &c1_thisId);
  sf_mex_destroy(&c1_nargout);
  return c1_y;
}

static real_T c1_b_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_d0, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  *(real_T *)c1_outData = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const boolean_T c1_b_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 11, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_b_emlrt_marshallOut(chartInstance,
    *(boolean_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static boolean_T c1_c_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_sf_internal_predicateOutput, const char_T
  *c1_identifier)
{
  boolean_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_sf_internal_predicateOutput), &c1_thisId);
  sf_mex_destroy(&c1_sf_internal_predicateOutput);
  return c1_y;
}

static boolean_T c1_d_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  boolean_T c1_y;
  boolean_T c1_b0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_b0, 1, 11, 0U, 0, 0U, 0);
  c1_y = c1_b0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  *(boolean_T *)c1_outData = c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_Boost_converter_V10_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static const mxArray *c1_c_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const int32_T c1_b_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_c_emlrt_marshallOut(chartInstance,
    *(int32_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static int32_T c1_e_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier)
{
  int32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  return c1_y;
}

static int32_T c1_f_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_i0, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  *(int32_T *)c1_outData = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const uint8_T c1_b_u)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_b_u, 3, 0U, 0U, 0U, 0), false);
  return c1_y;
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  sf_mex_assign(&c1_mxArrayOutData, c1_d_emlrt_marshallOut(chartInstance,
    *(uint8_T *)c1_inData), false);
  return c1_mxArrayOutData;
}

static uint8_T c1_g_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Mode1, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Mode1),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Mode1);
  return c1_y;
}

static uint8_T c1_h_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)chartInstanceVoid;
  *(uint8_T *)c1_outData = c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_mxArrayInData), c1_varName);
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_e_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance)
{
  const mxArray *c1_y;
  int32_T c1_i1;
  boolean_T c1_bv0[7];
  c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(10, 1), false);
  sf_mex_setcell(c1_y, 0, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_S_out));
  sf_mex_setcell(c1_y, 1, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_Eref));
  sf_mex_setcell(c1_y, 2, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_Kp));
  sf_mex_setcell(c1_y, 3, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_off));
  sf_mex_setcell(c1_y, 4, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_u));
  sf_mex_setcell(c1_y, 5, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_u_max));
  sf_mex_setcell(c1_y, 6, c1_emlrt_marshallOut(chartInstance,
    *chartInstance->c1_u_min));
  sf_mex_setcell(c1_y, 7, c1_d_emlrt_marshallOut(chartInstance,
    chartInstance->c1_is_active_c1_Boost_converter_V10));
  sf_mex_setcell(c1_y, 8, c1_d_emlrt_marshallOut(chartInstance,
    chartInstance->c1_is_c1_Boost_converter_V10));
  for (c1_i1 = 0; c1_i1 < 7; c1_i1++) {
    c1_bv0[c1_i1] = chartInstance->c1_dataWrittenToVector[c1_i1];
  }

  sf_mex_setcell(c1_y, 9, c1_f_emlrt_marshallOut(chartInstance, c1_bv0));
  return c1_y;
}

static const mxArray *c1_f_emlrt_marshallOut
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const boolean_T
   c1_b_u[7])
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_b_u, 11, 0U, 1U, 0U, 1, 7), false);
  return c1_y;
}

static void c1_i_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u)
{
  boolean_T c1_bv1[7];
  int32_T c1_i2;
  *chartInstance->c1_S_out = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("S_out", c1_b_u, 0)), "S_out");
  *chartInstance->c1_Eref = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Eref", c1_b_u, 1)), "Eref");
  *chartInstance->c1_Kp = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Kp", c1_b_u, 2)), "Kp");
  *chartInstance->c1_off = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("off", c1_b_u, 3)), "off");
  *chartInstance->c1_u = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("u", c1_b_u, 4)), "u");
  *chartInstance->c1_u_max = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("u_max", c1_b_u, 5)), "u_max");
  *chartInstance->c1_u_min = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("u_min", c1_b_u, 6)), "u_min");
  chartInstance->c1_is_active_c1_Boost_converter_V10 = c1_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_active_c1_Boost_converter_V10",
       c1_b_u, 7)), "is_active_c1_Boost_converter_V10");
  chartInstance->c1_is_c1_Boost_converter_V10 = c1_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell("is_c1_Boost_converter_V10",
       c1_b_u, 8)), "is_c1_Boost_converter_V10");
  c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c1_b_u, 9)), "dataWrittenToVector", c1_bv1);
  for (c1_i2 = 0; c1_i2 < 7; c1_i2++) {
    chartInstance->c1_dataWrittenToVector[c1_i2] = c1_bv1[c1_i2];
  }

  sf_mex_assign(&chartInstance->c1_setSimStateSideEffectsInfo,
                c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c1_b_u, 10)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c1_b_u);
}

static void c1_j_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_dataWrittenToVector, const char_T
  *c1_identifier, boolean_T c1_y[7])
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_k_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_dataWrittenToVector),
                        &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_dataWrittenToVector);
}

static void c1_k_emlrt_marshallIn(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance, const mxArray *c1_b_u, const emlrtMsgIdentifier *c1_parentId,
  boolean_T c1_y[7])
{
  boolean_T c1_bv2[7];
  int32_T c1_i3;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_b_u), c1_bv2, 1, 11, 0U, 1, 0U, 1, 7);
  for (c1_i3 = 0; c1_i3 < 7; c1_i3++) {
    c1_y[c1_i3] = c1_bv2[c1_i3];
  }

  sf_mex_destroy(&c1_b_u);
}

static const mxArray *c1_l_emlrt_marshallIn
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier)
{
  const mxArray *c1_y = NULL;
  emlrtMsgIdentifier c1_thisId;
  c1_y = NULL;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  sf_mex_assign(&c1_y, c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_setSimStateSideEffectsInfo), &c1_thisId), false);
  sf_mex_destroy(&c1_b_setSimStateSideEffectsInfo);
  return c1_y;
}

static const mxArray *c1_m_emlrt_marshallIn
  (SFc1_Boost_converter_V10InstanceStruct *chartInstance, const mxArray *c1_b_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  (void)c1_parentId;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_duplicatearraysafe(&c1_b_u), false);
  sf_mex_destroy(&c1_b_u);
  return c1_y;
}

static void init_dsm_address_info(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_Boost_converter_V10InstanceStruct
  *chartInstance)
{
  chartInstance->c1_S_in = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_S_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_Eout = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_Eref = (real_T *)(ssGetContStates_wrapper(chartInstance->S)
    + 0);
  chartInstance->c1_Kp = (real_T *)(ssGetContStates_wrapper(chartInstance->S) +
    1);
  chartInstance->c1_u_max = (real_T *)(ssGetContStates_wrapper(chartInstance->S)
    + 2);
  chartInstance->c1_u_min = (real_T *)(ssGetContStates_wrapper(chartInstance->S)
    + 3);
  chartInstance->c1_u = (real_T *)(ssGetContStates_wrapper(chartInstance->S) + 4);
  chartInstance->c1_off = (real_T *)(ssGetContStates_wrapper(chartInstance->S) +
    5);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c1_Boost_converter_V10_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2258267182U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1570456997U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4146760209U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2486877342U);
}

mxArray* sf_c1_Boost_converter_V10_get_post_codegen_info(void);
mxArray *sf_c1_Boost_converter_V10_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("lBGXor5s4O8CQFZUyV7K4F");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,6,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo = sf_c1_Boost_converter_V10_get_post_codegen_info
      ();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_Boost_converter_V10_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_Boost_converter_V10_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("plant_model_chart");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_Boost_converter_V10_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_Boost_converter_V10_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_Boost_converter_V10(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[21],T\"S_out\",},{M[5],M[53],T\"Eref\",},{M[5],M[73],T\"Kp\",},{M[5],M[79],T\"off\",},{M[5],M[77],T\"u\",},{M[5],M[74],T\"u_max\",},{M[5],M[75],T\"u_min\",},{M[8],M[0],T\"is_active_c1_Boost_converter_V10\",},{M[9],M[0],T\"is_c1_Boost_converter_V10\",},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 10, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_Boost_converter_V10_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Boost_converter_V10InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Boost_converter_V10MachineNumber_,
           1,
           6,
           15,
           0,
           9,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Boost_converter_V10MachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Boost_converter_V10MachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Boost_converter_V10MachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"Eref");
          _SFD_SET_DATA_PROPS(1,0,0,0,"Kp");
          _SFD_SET_DATA_PROPS(2,0,0,0,"off");
          _SFD_SET_DATA_PROPS(3,0,0,0,"u");
          _SFD_SET_DATA_PROPS(4,0,0,0,"u_max");
          _SFD_SET_DATA_PROPS(5,0,0,0,"u_min");
          _SFD_SET_DATA_PROPS(6,1,1,0,"S_in");
          _SFD_SET_DATA_PROPS(7,1,1,0,"Eout");
          _SFD_SET_DATA_PROPS(8,2,0,1,"S_out");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_CH_SUBSTATE_COUNT(6);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,2);
          _SFD_CH_SUBSTATE_INDEX(3,3);
          _SFD_CH_SUBSTATE_INDEX(4,4);
          _SFD_CH_SUBSTATE_INDEX(5,5);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,0);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,0);
        }

        _SFD_CV_INIT_CHART(6,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(13,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(14,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(12,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(10,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,0,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(5,0,0,1,24,1,24);
        _SFD_CV_INIT_EML(6,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,2,24,-1,2);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,24,1,24);
        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,1,24,1,24);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,2,24,-1,4);
        _SFD_CV_INIT_EML(3,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(3,0,0,1,25,1,25);
        _SFD_CV_INIT_EML_RELATIONAL(3,0,0,2,25,-1,3);
        _SFD_CV_INIT_EML(13,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(13,0,0,1,34,1,34);
        _SFD_CV_INIT_EML_RELATIONAL(13,0,0,2,34,-1,3);
        _SFD_CV_INIT_EML(14,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(14,0,0,1,33,1,33);
        _SFD_CV_INIT_EML_RELATIONAL(14,0,0,2,33,-1,4);
        _SFD_CV_INIT_EML(8,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(8,0,0,1,25,1,25);
        _SFD_CV_INIT_EML_RELATIONAL(8,0,0,2,25,-1,3);
        _SFD_CV_INIT_EML(7,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(7,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(7,0,0,2,24,-1,4);
        _SFD_CV_INIT_EML(11,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(11,0,0,1,24,1,24);
        _SFD_CV_INIT_EML_RELATIONAL(11,0,0,2,24,-1,2);
        _SFD_CV_INIT_EML(9,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(9,0,0,1,24,1,24);
        _SFD_CV_INIT_EML(12,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(12,0,0,1,24,1,24);
        _SFD_CV_INIT_EML(10,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(10,0,0,1,24,1,24);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Boost_converter_V10MachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Boost_converter_V10InstanceStruct *chartInstance;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)
      chartInfo->chartInstance;
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(6U, chartInstance->c1_S_in);
        _SFD_SET_DATA_VALUE_PTR(8U, chartInstance->c1_S_out);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c1_Eout);
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c1_Eref);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c1_Kp);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c1_u_max);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c1_u_min);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c1_u);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c1_off);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "su6oLcvQFwYpK4GsFbhNtGC";
}

static void sf_opaque_initialize_c1_Boost_converter_V10(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c1_Boost_converter_V10
    ((SFc1_Boost_converter_V10InstanceStruct*) chartInstanceVar);
  initialize_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c1_Boost_converter_V10(void *chartInstanceVar)
{
  enable_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c1_Boost_converter_V10(void *chartInstanceVar)
{
  disable_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_zeroCrossings_c1_Boost_converter_V10(void
  *chartInstanceVar)
{
  zeroCrossings_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_derivatives_c1_Boost_converter_V10(void *chartInstanceVar)
{
  derivatives_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_outputs_c1_Boost_converter_V10(void *chartInstanceVar)
{
  outputs_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c1_Boost_converter_V10(void *chartInstanceVar)
{
  sf_gateway_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_Boost_converter_V10(SimStruct*
  S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c1_Boost_converter_V10
    ((SFc1_Boost_converter_V10InstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_Boost_converter_V10(SimStruct* S, const
  mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c1_Boost_converter_V10(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_Boost_converter_V10InstanceStruct*) chartInstanceVar
      )->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Boost_converter_V10_optimization_info();
    }

    finalize_c1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_Boost_converter_V10((SFc1_Boost_converter_V10InstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_Boost_converter_V10(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c1_Boost_converter_V10
      ((SFc1_Boost_converter_V10InstanceStruct*)(chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c1_Boost_converter_V10(SimStruct *S)
{
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_Boost_converter_V10_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2997583473U));
  ssSetChecksum1(S,(714055020U));
  ssSetChecksum2(S,(2384607785U));
  ssSetChecksum3(S,(2788879306U));
  ssSetNumContStates(S,6);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_Boost_converter_V10(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c1_Boost_converter_V10(SimStruct *S)
{
  SFc1_Boost_converter_V10InstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc1_Boost_converter_V10InstanceStruct *)utMalloc(sizeof
    (SFc1_Boost_converter_V10InstanceStruct));
  memset(chartInstance, 0, sizeof(SFc1_Boost_converter_V10InstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_Boost_converter_V10;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_Boost_converter_V10;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c1_Boost_converter_V10;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_Boost_converter_V10;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c1_Boost_converter_V10;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_Boost_converter_V10;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_Boost_converter_V10;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_Boost_converter_V10;
  chartInstance->chartInfo.zeroCrossings =
    sf_opaque_zeroCrossings_c1_Boost_converter_V10;
  chartInstance->chartInfo.outputs = sf_opaque_outputs_c1_Boost_converter_V10;
  chartInstance->chartInfo.derivatives =
    sf_opaque_derivatives_c1_Boost_converter_V10;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_Boost_converter_V10;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_Boost_converter_V10;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c1_Boost_converter_V10;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  crtInfo->isEnhancedMooreMachine = 0;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->fCheckOverflow = sf_runtime_overflow_check_is_on(S);
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
}

void c1_Boost_converter_V10_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_Boost_converter_V10(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_Boost_converter_V10(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_Boost_converter_V10(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_Boost_converter_V10_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
