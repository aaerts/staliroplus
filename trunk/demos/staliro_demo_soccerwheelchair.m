clear
close all

cd('C:\Users\aaerts\Dropbox\TUE\ALTEN\Workspace (Matlab)\S-TaLiRo\trunk\demos\SystemModelsAndData')

disp(' ')
disp('This is an S-TaLiRo demo on a Simulink model of the DC-DC boost converter.')
disp('The goal is to falsify the following requirement:')
disp('>> With a output current ranging between 0A and 3A the DC-DC boost converter should have an output voltage with a maximum error margin of 2%<<')
model = 'Boost_converter_V10';

disp(' ')
disp('The constraints on the initial conditions defined as a hypercube:')
init_cond = []

disp(' ')
disp('The constraints on the input signal defined as a range:')
input_range = [0 4]
disp(' ')
disp('The number of control points for the input signal:')
cp_array = 5

disp(' ')
disp('The specification:')
phi = '((!(<> r2)) /\ (<> r1))'%'(<> r1 /\ <> r2)' 

ii = 1;
preds(ii).str='r1';
preds(ii).A = [1];
preds(ii).b = 245;

ii = ii+1;
preds(ii).str='r2';
preds(ii).A = [1];
preds(ii).b = 235;

disp(' ')
disp('Total Simulation time:')
time = 0.005

disp(' ')
disp('Create an staliro_options object with the default options:')
opt = staliro_options();

disp(' ')
disp('Change options:')
opt.runs = 1;
opt.SampTime = 0.0000001;
opt.optim_params.dispAdap = 10;
opt.optim_params.minDisp = -4.99;
opt.optim_params.maxDisp = 4.99;
opt.optim_params.betaXAdap = 100;
opt.optim_params.acRatioMax = 0.55;
opt.optim_params.acRatioMin = 0.45;
opt.optim_params.UpdateInterval = 5;
n_tests = 25;
opt.interpolationtype={'pconst'};
opt.n_workers=2;

disp(' ')
disp (' Select a solver to use ')
disp (' 1. Simulated Annealing Method. ')
disp (' 2. Cross Entropy Method. ' )
disp (' 3. Uniform Random Sampling. ')
disp (' 4. Genetic Algorithm.')
disp (' ')
form_id = input ('Select an option (1-4): ');
if (form_id == 1)
    opt.optimization_solver = 'SA_Taliro';
else
    if (form_id == 2)
    opt.optimization_solver = 'CE_Taliro';
    else if (form_id == 3)
            opt.optimization_solver = 'UR_Taliro';
        else
            opt.optimization_solver = 'GA_Taliro';
        end
    end
end
opt.optim_params.n_tests = n_tests;

disp(' ')
disp('Running S-TaLiRo with chosen solver ...')
tic
[results, his] = staliro(model,init_cond,input_range,cp_array,phi,preds,time,opt);
runtime=toc;

runtime

results.run(results.optRobIndex).bestRob

[T1,XT1,YT1,IT1] = SimSimulinkMdl(model,init_cond,input_range,cp_array,results.run(results.optRobIndex).bestSample(:,1),time,opt);

YT1 = round(YT1*100)/100;
XT1 = round(XT1*100)/100;

figure;
subplot(2,2,1);
bar(his.rob)
title('Robustness values');
xlabel('Iteration');
ylabel('\phi-Value');

subplot(2,2,2);
hold on;
p=0;
for k = 2 : 1 : results.run.nTests
   p=[p sqrt(sum((his.samples(k,:)-his.samples(k-1,:)).^2)/length(his.samples(1,:)))];
end
bar(p)
% for k = 1 : 1 : results.run.nTests
%     if k == 1
%         plot([0:(time/(cp_array-1)):time],(his.samples(1,:)-his.samples(k,:)),'LineWidth',3);
%     else
%         if k == results.run.nTests
%             plot([0:(time/(cp_array-1)):time],(his.samples(1,:)-his.samples(k,:)),'LineWidth',3);
%         else
%             plot([0:(time/(cp_array-1)):time],(his.samples(1,:)-his.samples(k,:)));
%         end
%     end
% end
title('Input signal deviation');
xlabel('Iout [A]');
ylabel('Value');

subplot(2,2,[3 4]);
hold on;
for k = 1 : 1 : results.run.nTests
    if k == 1
        stairs([0:(time/(cp_array-1)):time],his.samples(k,:),'LineWidth',3);
    else
        if k == results.run.nTests
            stairs([0:(time/(cp_array-1)):time],his.samples(k,:),'LineWidth',3);
        else
            stairs([0:(time/(cp_array-1)):time],his.samples(k,:));
        end
    end
end
title('Input signal "u"');
xlabel('Time [s]');
ylabel('Iout [A]');

figure;
subplot(2,1,1);
plot(IT1(:,1),IT1(:,2))
title('Iout');
xlabel('Time [s]');
ylabel('Current [A]');
subplot(2,1,2);
plot(T1,YT1(:,1))
title('Vout');
xlabel('Time [s]');
ylabel('Voltage [V]');
hold on;
plot([0 time],[235 235],'r');
plot([0 time],[245 245],'r');
axis([0 time 230 250]);

cd('..')


